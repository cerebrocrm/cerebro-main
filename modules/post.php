<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   Copyright 2014 Left Join Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Performs database update operations at POST request
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class Post extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('permission' => 'permission level', 'preprocessing' => 'functions to execute before data is stored', 'postprocessing' => 'functions to execute after data is stored and item ID has been generated');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('CheckPost' => 'post form processing');
	//Object functions and variables go here

	/**
	 * Process POST data
	 *
	 * @return ID Returns id on insert/update, 1 on sucessful delete
	 */
	public function CheckPost() {
		$permission = $this->link('permission');
		$preprocessing = $this->link('preprocessing');
		$postprocessing = $this->link('postprocessing');
		if ($permission[0] <= Users::GetCurrentUser()['user_level']) {
			if (isset($_POST['table']) || isset($_POST['remove']) || isset($_POST['id'])) {
				global $connectionmanager;
				if ($_POST['table'] == 'item_links') {
					if (!(isset($_POST['note_id'])) || $_POST['note_id'] == 0) {
						if (mysqli_num_rows(mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE primary_meta=0 ((item1 = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['item1']) . " AND item2 = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['item2']) . ") OR (item2 = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['item1']) . " AND item1 = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['item2']) . "))")) > 0) return 0;
					}
					if (!(isset($_POST['item1']))) {
						mysqli_query($connectionmanager->connection, "UPDATE item_links SET note_id=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['note_id']) . " WHERE link_id=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['link_id']));
						return 0;
					}
					$item1 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['item1'])));
					$item2 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['item2'])));
					$note_id = 0;
					if (isset($_POST['note_id'])) $note_id = mysqli_real_escape_string($connectionmanager->connection, $_POST['note_id']);
					mysqli_query($connectionmanager->connection, "INSERT INTO item_links (item1,item2,note_id,cat1,cat2,link_date) VALUES (" . $item1['id'] . "," . $item2['id'] . "," . $note_id . "," . $item1['category'] . "," . $item2['category'] . "," . time() . ")");
					return 0;
				}
				if ($_POST['table'] == 'global_notes') {
					if (isset($_POST['note_id']) && !(isset($_POST['note_type']))) {
						mysqli_query($connectionmanager->connection, "UPDATE global_notes SET note_text = '" . mysqli_real_escape_string($connectionmanager->connection, $_POST['note_text']) . "', note_date=" . time() . " WHERE note_id=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['note_id']));
						$insert = $_POST['note_id'];
					} elseif (isset($_POST['note_text'])) {
						$note_item = mysqli_real_escape_string($connectionmanager->connection, $_POST['note_item']);
						$note_text = mysqli_real_escape_string($connectionmanager->connection, $_POST['note_text']);
						$note_user = 0;
						if (isset($_POST['note_user'])) $note_user = mysqli_real_escape_string($connectionmanager->connection, $_POST['note_user']);
						$note_type = 0;
						if (isset($_POST['note_type'])) $note_type = mysqli_real_escape_string($connectionmanager->connection, $_POST['note_type']);
						$note_sticky = 0;
						if (isset($_POST['note_sticky'])) $note_sticky = mysqli_real_escape_string($connectionmanager->connection, $_POST['note_sticky']);
						$note_reply = 0;
						if (isset($_POST['note_reply'])) $note_reply = mysqli_real_escape_string($connectionmanager->connection, $_POST['note_reply']);
						mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item,note_text,note_date,note_user,note_type,note_sticky,note_reply) VALUES (" . $note_item . ",'" . $note_text . "'," . time() . "," . $note_user . "," . $note_type . "," . $note_sticky . "," . $note_reply . ")");
						$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $note_item . " AND note_text LIKE '" . $note_text . "'"));
						$insert = $insert['note_id'];
					} else {
						mysqli_query($connectionmanager->connection, "UPDATE global_notes SET note_type = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['note_type']) . " WHERE note_id=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['note_id']));
						$insert = $_POST['note_id'];
					}
					if (isset($_POST['autoemail']) && $_POST['autoemail'] != '') {
						//file_put_contents('test.log', ' 1 '.$_POST['autoemail'], FILE_APPEND | LOCK_EX);
						$this->auto_email($note_item, $_POST['autoemail']);
					}
					return $insert;
				}
				global $config;
				$config_solar = array('adapteroptions' => array('host' => $config['solr_host'], 'port' => $config['solr_port'], 'path' => $config['solr_path'], 'core' => $config['solr_core']));
				$client = new Solarium_Client($config_solar);
				$update = $client->createUpdate();
				if (isset($_POST['remove'])) {
					if ($_POST['remove'] == 'item') {
						mysqli_query($connectionmanager->connection, "DELETE FROM items WHERE id='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . "'");
						mysqli_query($connectionmanager->connection, "DELETE FROM item_links WHERE item1='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . "' OR item2='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . "'");
						mysqli_query($connectionmanager->connection, "DELETE FROM extra_char WHERE item_id='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . "'");
						mysqli_query($connectionmanager->connection, "DELETE FROM extra_int WHERE item_id='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . "'");
						mysqli_query($connectionmanager->connection, "DELETE FROM extra_tiny WHERE item_id='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . "'");
						$update->addDeleteById($_POST['id']);
						$update->addCommit();
						$result = $client->update($update);
						return 0;
					}
					if ($_POST['remove'] == 'link') {
						mysqli_query($connectionmanager->connection, "DELETE FROM item_links WHERE (item1='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['item1']) . "' AND item2='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['item2']) . "') OR (item2='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['item1']) . "' AND item1='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['item2']) . "')");
						return 0;
					}
				}
				//Get table details
				$cat = mysqli_real_escape_string($connectionmanager->connection, $_POST['category']);
				//$category = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection,"SELECT * FROM categories WHERE cat_name LIKE '".$cat."'"));
				//Get table fields
				$fields = mysqli_query($connectionmanager->connection, "SELECT * FROM meta WHERE cat_id = '" . $cat . "'");
				$count = mysqli_num_rows($fields);
				//Loop through fields, checking for post data
				foreach ($preprocessing as $function) {
					if ($function != null) {
						include 'functions/' . $function . '.php';
						$_POST = $function($_POST);
					}
				}
				if (isset($_POST['id']) && $_POST['id'] != 0 && $_POST['id'] != '') {
					// so we're updating an existing item
					$update->addDeleteById($_POST['id']);
					$update->addCommit();
					$result = $client->update($update);
					$update = $client->createUpdate();
					$doc1 = $update->createDocument();
					$change_summary = 'The following updates were made by ' . Users::GetCurrentUser()['name'] . ': ';
					$insert = mysqli_real_escape_string($connectionmanager->connection, $_POST['id']);
					$doc1->id = $_POST['id'];
					$old = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . $insert));
					$doc1->category = $old['category'];
					$oldcat = $old['category'];
					if ($old['name'] != $_POST['name'] && $_POST['name'] != '' && $_POST['name'] != ' ') {
						$doc1->name = $_POST['name'];
						$change_summary.= 'name - ' . $old['name'] . ' => ' . $_POST['name'] . ', ';
						mysqli_query($connectionmanager->connection, "UPDATE items SET name = '" . mysqli_real_escape_string($connectionmanager->connection, $_POST['name']) . "' WHERE id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']));
					} else $doc1->name = $old['name'];
					while ($row = mysqli_fetch_array($fields)) {
						if ($row['type'] == 1) {
							$type = 'extra_char';
							$solr = $row['name'] . '_char';
						} elseif ($row['type'] == 2) {
							$type = 'extra_int';
							$solr = $row['name'] . '_int';
						} else {
							$type = 'extra_tiny';
							$solr = $row['name'] . '_tiny';
							if (isset($_POST[$row['name']]) && $_POST[$row['name']] != 1) {
								$_POST[$row['name']] = 0;
							}
						}
						if ($row['linkid'] == 0) $old = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM " . $type . " WHERE meta_id = " . $row['id'] . " AND item_id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['id'])));
						else $old = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE primary_meta = " . $row['linkid'] . " AND link_level = " . $row['link_level'] . " AND item1 = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['id'])));
						if (!(array_key_exists('val', $old))) {
							if ($row['type'] == 1) $old['val'] = '';
							else $old['val'] = 0;
						}
						if (array_key_exists('item2', $old)) $old['val'] = $old['item2'];
						if (isset($_POST[$row['name']]) && $_POST[$row['name']] !== '') {
							if ($row['data_format'] == 3) $_POST[$row['name']] = genpwd($_POST[$row['name']]);
							if ($_POST[$row['name']] != $old['val']) {
								if ($row['data_format'] != 3) $change_summary.= $row['name'] . ' - ' . $old['val'] . ' => ' . $_POST[$row['name']] . ', ';
								if ($row['linkid'] == 0 && $row['type'] != 1) {
									mysqli_query($connectionmanager->connection, "DELETE FROM " . $type . " WHERE meta_id = " . $row['id'] . " AND item_id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']));
									mysqli_query($connectionmanager->connection, "INSERT INTO " . $type . " (val,meta_id,item_id) VALUES (" . mysqli_real_escape_string($connectionmanager->connection, $_POST[$row['name']]) . "," . $row['id'] . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . ")");
								} elseif ($row['linkid'] == 0) {
									mysqli_query($connectionmanager->connection, "DELETE FROM " . $type . " WHERE meta_id = " . $row['id'] . " AND item_id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']));
									if ($row['type'] == 3) mysqli_query($connectionmanager->connection, "INSERT INTO " . $type . " (val,meta_id,item_id) VALUES (" . mysqli_real_escape_string($connectionmanager->connection, $_POST[$row['name']]) . "," . $row['id'] . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . ")");
									else {
										mysqli_query($connectionmanager->connection, "INSERT INTO " . $type . " (val,meta_id,item_id) VALUES ('" . mysqli_real_escape_string($connectionmanager->connection, $_POST[$row['name']]) . "'," . $row['id'] . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . ")");
									}
								} else {
									mysqli_query($connectionmanager->connection, "DELETE FROM item_links WHERE primary_meta = " . $row['linkid'] . " AND link_level=" . $row['link_level'] . " AND item1 = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']));
									$item = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST[$row['name']])));
									mysqli_query($connectionmanager->connection, "INSERT INTO item_links (item1,item2,primary_meta,link_date,cat1,cat2,link_level) VALUES (" . $insert . "," . $item['id'] . "," . $item['category'] . "," . time() . "," . $oldcat . "," . $item['category'] . "," . $row['link_level'] . ")");
								}
								$doc1->$solr = $_POST[$row['name']];
							} elseif ($old['val'] !== '' && $old['val'] !== 0) $doc1->$solr = $old['val'];
						} elseif ($old['val'] !== '' && $old['val'] !== 0) $doc1->$solr = $old['val'];
					}
					if (!isset($_POST['autoupdate'])) {
						mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item,note_text,note_date,note_user,note_type) VALUES (" . mysqli_real_escape_string($connectionmanager->connection, $_POST['id']) . ",'" . $change_summary . "'," . time() . "," . Users::GetCurrentuser()['id'] . ",8)");
					}
				} else {
					// so we're creating a new item
					//if ($_POST[$row['name']] == 'undefined') $_POST[$row['name']] = 0;
					mysqli_query($connectionmanager->connection, "INSERT INTO items (name,category) VALUES ('" . mysqli_real_escape_string($connectionmanager->connection, $_POST['name']) . "'," . mysqli_real_escape_string($connectionmanager->connection, $_POST['category']) . ") ");
					$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE category=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['category']) . " AND name LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $_POST['name']) . "' ORDER BY id DESC"));
					$insert = $insert['id'];
					$doc1 = $update->createDocument();
					$doc1->id = $insert;
					$doc1->category = $_POST['category'];
					// If there's no name... but there is a contact_first, we use that data
					if (!isset($_POST['name']) && isset($_POST['contact_first'])) {
						$_POST['name'] = $_POST['contact_first'] . ' ' . $_POST['contact_last'];
					}
					$doc1->name = $_POST['name'];
					$change_summary = '';
					while ($row = mysqli_fetch_array($fields)) {
						if ($row['type'] == 1) {
							$type = 'extra_char';
							$solr = $row['name'] . '_char';
						} elseif ($row['type'] == 2) {
							$type = 'extra_int';
							$solr = $row['name'] . '_int';
						} else {
							$type = 'extra_tiny';
							$solr = $row['name'] . '_tiny';
						}
						if (isset($_POST[$row['name']]) && $_POST[$row['name']] !== '') {
							if ($row['data_format'] == 3) $_POST[$row['name']] = genpwd($_POST[$row['name']]);
							if ($row['linkid'] == 0) {
								if ($row['type'] == 3) mysqli_query($connectionmanager->connection, "INSERT INTO " . $type . " (item_id,meta_id,val) VALUES (" . $insert . "," . $row['id'] . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST[$row['name']]) . ")");
								else mysqli_query($connectionmanager->connection, "INSERT INTO " . $type . " (item_id,meta_id,val) VALUES (" . $insert . "," . $row['id'] . ",'" . mysqli_real_escape_string($connectionmanager->connection, $_POST[$row['name']]) . "')");
							} else {
								$item = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST[$row['name']])));
								mysqli_query($connectionmanager->connection, "INSERT INTO item_links (item1,item2,primary_meta,link_date,cat1,cat2,link_level) VALUES (" . $insert . "," . $item['id'] . "," . $item['category'] . "," . time() . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST['category']) . "," . $item['category'] . "," . $row['link_level'] . ")");
							}
							$doc1->$solr = $_POST[$row['name']];
							//file_put_contents('email_log.log', $row['name'].':'.$_POST[$row['name']].' ', FILE_APPEND | LOCK_EX);

						}
					}
				}
				$update->addDocuments(array($doc1));
				$update->addCommit();
				try {
					$result = $client->update($update);
				}
				catch(Exception $e) {
					die(fail_error('Caught exception: ', $e->getMessage(), "\n"));
				}
				foreach ($postprocessing as $function) {
					if ($function != null) {
						include 'functions/' . $function . '.php';
						$function($insert, $_POST);
					}
				}
				$note = 0;
				$i = 0;
				if ((isset($_POST['note']) && $_POST['note'] != '') || isset($_POST['note_type'])) {
					if (isset($_POST['note_type'])) $note_type = mysqli_real_escape_string($connectionmanager->connection, $_POST['note_type']);
					else $note_type = 0;
					$hash = hash('md5', $insert . $note_type . $_POST['note']);
					mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item,note_text,note_date,note_user,note_type,note_hash) VALUES (" . $insert . ",'" . mysqli_real_escape_string($connectionmanager->connection, $_POST['note']) . "'," . time() . "," . Users::GetCurrentuser()['id'] . "," . $note_type . ",'" . $hash . "')");
					$note = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $insert . " AND note_hash LIKE '" . $hash . "'"));
					$note = $note['note_id'];
				}
				if (isset($_POST['filter_cat']) && isset($_POST['filter_trigger'])) {
					mysqli_query($connectionmanager->connection, "DELETE FROM item_links WHERE (item1=" . $insert . " AND cat2=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['filter_cat']) . ") OR (item2=" . $insert . " AND cat1=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['filter_cat']) . ")");
				}
				while (isset($_POST['link_' . $i])) {
					$item = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . mysqli_real_escape_string($connectionmanager->connection, $_POST['link_' . $i])));
					mysqli_query($connectionmanager->connection, "INSERT INTO item_links (item1,item2,link_date,cat1,cat2,note_id) VALUES (" . $insert . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST['link_' . $i]) . "," . time() . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST['category']) . "," . $item['category'] . "," . $note . ")");
					$i++;
				}
				if (isset($_POST['autoemail'])) {
					$this->auto_email($insert, $_POST['autoemail']);
				}
				if (isset($_POST['multinote']) && isset($_POST['id']) && $_POST['id'] != 0) {
					$prevnote = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_type=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['note_type']) . " AND note_item=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['id'])));
					mysqli_query($connectionmanager->connection, "UPDATE global_notes SET note_text='" . mysqli_real_escape_string($connectionmanager->connection, urldecode($_POST['multinote'])) . "' WHERE note_id=" . $prevnote['note_id']);
					$multi_id = $prevnote['note_id'];
				} elseif (isset($_POST['multinote'])) {
					mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item,note_text,note_date,note_user,note_type) VALUES (" . $insert . ",'" . mysqli_real_escape_string($connectionmanager->connection, urldecode($_POST['multinote'])) . "'," . time() . "," . Users::GetCurrentuser()['id'] . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST['note_type']) . ")");
					$multi_id = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $insert . " AND note_text LIKE '" . mysqli_real_escape_string($connectionmanager->connection, urldecode($_POST['multinote'])) . "'"));
					$multi_id = $multi_id['note_id'];
				}
				//Add mutual links between multiple items
				if (isset($_POST['multilink']) && $_POST['multilink'] != '') {
					$links = mysqli_real_escape_string($connectionmanager->connection, urldecode($_POST['multilink']));
					$links = explode(',', $links);
					$links[] = $insert;
					$check = array();
					foreach ($links as $l1ind => $link1) {
						$check[$link1] = 1;
						foreach ($links as $link2) {
							if (!(array_key_exists($link2, $check)) && $link1 != '' && $link1 != ' ' && $link2 != '' && $link2 != ' ') {
								$prev_links1 = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE note_id=" . $multi_id . " AND item1=" . $link1 . " AND item2=" . $link2);
								$prev_links2 = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE note_id=" . $multi_id . " AND item2=" . $link1 . " AND item1=" . $link2);
								if (mysqli_num_rows($prev_links1) == 0 && mysqli_num_rows($prev_links2) == 0) {
									$item1 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . $link1));
									$item2 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . $link2));
									mysqli_query($connectionmanager->connection, "INSERT INTO item_links (item1,item2,note_id,cat1,cat2,link_date) VALUES (" . $item1['id'] . "," . $item2['id'] . "," . $multi_id . "," . $item1['category'] . "," . $item2['category'] . "," . time() . ")");
								}
							}
						}
					}
					$linkclean = 'DELETE FROM item_links WHERE note_id=' . $multi_id;
					foreach ($check as $li => $lv) if ($li != $insert && $li != '' && $li != ' ') $linkclean.= ' AND item1 != ' . $li . ' AND item2 !=' . $li;
					mysqli_query($connectionmanager->connection, $linkclean);
				}
				if (isset($_POST['function'])) {
					include_once 'functions/' . $_POST['function'] . '.php';
					$funct = $_POST['function'];
					$funct($insert, $_POST);
				}
				$_SESSION['msg'] = Array('type' => 'correct', 'message' => 'Your ' . rtrim($_POST['table'], 's') . ' has been successfully saved. ' . $change_summary, 'id' => $insert, 'table' => $_POST['table']);
				$_SESSION['just_updated'] = Array('type' => $_POST['table'], 'id' => $insert);
				//file_put_contents('email_log.log',print_r($_POST,1).'
				//', FILE_APPEND | LOCK_EX);
				if (isset($_POST['redirect']) && $_POST['redirect'] != '') return header('Location: ' . $_POST['redirect'] . $insert);
				else return $insert;
			}
		} else {
			return null;
		}
	}

	/**
	* Sends an email
	*
	* Wrapper function to auto_email.
	*
	* @param string/integer $item The item to be emailed, either an item ID or email address
	* @param array $text $text = Array('text' => string, 'subject' => string );
	* @return string 1 or 0 for success or failure or 'Failed' if there is no email provider set
	*/
	public function Email($item, $text) {
		return $this->auto_email($item, null, $text);
	}

	/**
	* Runs an auto email using SES, Mandrill or SendGrid
	*
	* @global Object $dbmanager
	* @global Object $connectionmanager
	* @param string/integer $item The item to be emailed, either an item ID or email address
	* @param string $autoemail Reference to a specific auto email on the database
	* @param array $text $text = Array('text' => string, 'subject' => string );
	* @return string 1 or 0 for success or failure or 'Failed' if there is no email provider set
	*/
	function auto_email($item, $autoemail, $text = null) {
		global $dbmanager;
		global $connectionmanager;
		$service = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_service'"));
		$return = 'Failed';


		if ($service['pref_value'] == 'ses') {
			$return = $this->ses_auto_email($item, $autoemail, $text);
		} elseif ($service['pref_value'] == 'mandrill') {
			$return = $this->mandrill_auto_email($item, $autoemail, $text);
		} elseif ($service['pref_value'] == 'sendgrid') {
			$return = $this->sendgrid_auto_email($item, $autoemail, $text);
		} else {
			$return = 'Failed - no email service listed.';
		}

		return $return;
	}
	function sendgrid_auto_email($item, $autoemail, $text = null) {
		//file_put_contents('test.log', ' auto ', FILE_APPEND | LOCK_EX);
		global $dbmanager;
		global $connectionmanager;
		// include the SendGrid library
		require ("includes/vendor/autoload.php");
		//Set up
		$api1 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'sendgrid_api_key'"));
		$newsletter_reminder = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_newsletter_reminder'"));
		$from_email = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_default_from'"));
		$api = array(0 => $api1['pref_value'],);
		$sendgrid = new SendGrid($api[0]);
		// check for predefined message
		$message = mysqli_query($connectionmanager->connection, "SELECT * FROM auto_emails WHERE source LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $autoemail) . "'");
		// build message from text either from SQL or passed in text
		if (mysqli_num_rows($message) > 0 || $text != null) {
			// put the correct message array in the $message field
			$message = mysqli_fetch_assoc($message);
			if ($text != null) $message = $text;
			if (strpos($item, '@') !== false) {
				// we already have an email address
				$to_email = $item;
				$msg = $message['text'];
			} else {
				// get email address
				$contact = $dbmanager->Query(array('name', 'contact_email', 'contact_recieve_newsletters'), array(array('id' => $item)), null, 0, 0);
				$to_email = $contact['result'][$item]['contact_email'];
				if ($contact['result'][$item]['contact_recieve_newsletters'] == 1) $msg = $message['text'] . $newsletter_reminder['pref_value'];
				else $msg = $message['text'];
				//Add campaign entry
				global $connectionmanager;
				mysqli_query($connectionmanager->connection, "INSERT INTO email_campaigns (campaign_subject, campaign_no_sent, campaign_full_text) VALUES
					(
					'" . mysqli_real_escape_string($connectionmanager->connection, $message['subject']) . "',
					1,
					'" . mysqli_real_escape_string($connectionmanager->connection, $message['text']) . "'
					)
					");
				$cid = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM email_campaigns WHERE campaign_subject LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $message['subject']) . "' AND campaign_full_text LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $message['text']) . "'"));
				$cid = $cid['campaign_id'];
				// Add note
				mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item, note_text, note_date,note_type,note_campaign) VALUES
					(
					" . $item . ",
					'Emailed **" . mysqli_real_escape_string($connectionmanager->connection, $message['subject']) . "**',
					" . time() . ",
					6,
					" . $cid . "
					)
				");
			}
			// HTML format for message
			$msg = '<html>
					<body>
					<img border="0" src="http://' . $_SERVER['SERVER_NAME'] . '/go/header.php?i=' . $cid . '&c=' . $item . '"/>
					<br />
					' . $msg . '
					</body>
					</html>';
			// SendGrid specific settings
			if (strpos($message['from_email'], '@') !== false) {
				$from_email = $message['from_email'];
			} else {
				$from_email = $from_email['pref_value'];
			}
			$email = new SendGrid\Email();
			$email->addTo($to_email)->setFrom($from_email)->setSubject($message['subject'])->setHtml($message['text']);
			// catch the error
			$return = 1;
			try {
				$sendgrid->send($email);
			}
			catch(\SendGrid\Exception $e) {
				$return = 0;
				echo 'Fail! - ' . $e->getCode();
				foreach ($e->getErrors() as $er) {
					echo $er;
				}
			}
		}
		return 0;
	}
	function ses_auto_email($item, $autoemail, $text = null) {
		echo 'logged SES email start';
		//file_put_contents('test.log', ' auto ', FILE_APPEND | LOCK_EX);
		global $dbmanager;
		global $connectionmanager;
		// include the Amazon library
		require_once ("includes/ses-utils.php");
		$api1 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'ses_key1'"));
		$api2 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'ses_key2'"));
		$newsletter_reminder = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_newsletter_reminder'"));
		$from_email = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_default_from'"));
		$api = array(0 => $api1['pref_value'], 1 => $api2['pref_value']);
		// check for predefined message
		$message = mysqli_query($connectionmanager->connection, "SELECT * FROM auto_emails WHERE source LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $autoemail) . "'");
		// build message from text either from SQL or passed in text
		if (mysqli_num_rows($message) > 0 || $text != null) {
			// put the correct message array in the $message field
			$message = mysqli_fetch_assoc($message);
			if ($text != null) $message = $text;
			if (strpos($item, '@') !== false) {
				// we already have an email address
				$to_email = $item;
				$msg = $message['text'];
			} else {
				// get email address
				$contact = $dbmanager->Query(array('name', 'contact_email', 'contact_recieve_newsletters'), array(array('id' => $item)), null, 0, 0);
				$to_email = $contact['result'][$item]['contact_email'];
				if ($contact['result'][$item]['contact_recieve_newsletters'] == 1) $msg = $message['text'] . $newsletter_reminder['pref_value'];
				else $msg = $message['text'];
				//Add campaign entry
				global $connectionmanager;
				mysqli_query($connectionmanager->connection, "INSERT INTO email_campaigns (campaign_subject, campaign_no_sent, campaign_full_text) VALUES
					(
					'" . mysqli_real_escape_string($connectionmanager->connection, $message['subject']) . "',
					1,
					'" . mysqli_real_escape_string($connectionmanager->connection, $message['text']) . "'
					)
					");
				$cid = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM email_campaigns WHERE campaign_subject LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $message['subject']) . "' AND campaign_full_text LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $message['text']) . "'"));
				$cid = $cid['campaign_id'];
				// Add note
				mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item, note_text, note_date,note_type,note_campaign) VALUES
					(
					" . $item . ",
					'Emailed **" . mysqli_real_escape_string($connectionmanager->connection, $message['subject']) . "**',
					" . time() . ",
					6,
					" . $cid . "
					)
				");
			}
			// HTML format for message
			$msg = '<html>
					<body>
					<img border="0" src="http://' . $_SERVER['SERVER_NAME'] . '/go/header.php?i=' . $cid . '&c=' . $item . '"/>
					<br />
					' . $msg . '
					</body>
					</html>';
			// SES specific settings
			$from_email = $from_email['pref_value'];
			$from_email2 = str_replace('@', '_', $from_email) . '@cerebro.org.uk';
			$params = array("to" => $to_email, "subject" => $message['subject'], "message" => $message['text'], "from" => $from_email2, "replyTo" => $from_email,);
			$aws = new SESUtils($api[0], $api[1]);
			$res = $aws->sendMail($params);
			if ($res->success == 1) {
				echo 'Success! - ' . $res->message_id;
			} else {
				echo 'Fail! - ' . $res->result_text;
			}
		}
		return 0;
	}
	function mandrill_auto_email($item, $autoemail, $text = null) {
		//file_put_contents('test.log', ' auto ', FILE_APPEND | LOCK_EX);
		global $dbmanager;
		global $connectionmanager;
		$api = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'mandrill_api_key'"));
		$message = mysqli_query($connectionmanager->connection, "SELECT * FROM auto_emails WHERE source LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $autoemail) . "'");
		$newsletter_reminder = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_newsletter_reminder'"));
		$from_email = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_default_from'"));
		//file_put_contents('email_log.log', " SELECT * FROM module_preferences WHERE pref_name LIKE 'mandrill_api_key' ", FILE_APPEND | LOCK_EX);
		//file_put_contents('email_log.log', " SELECT * FROM auto_emails WHERE source LIKE '".mysqli_real_escape_string($connectionmanager ->connection,$autoemail)."' ", FILE_APPEND | LOCK_EX);
		if (mysqli_num_rows($message) > 0 || $text != null) {
			//file_put_contents('test.log', ' num_rows ', FILE_APPEND | LOCK_EX);
			$message = mysqli_fetch_assoc($message);
			if ($text != null) $message = $text;
			define('MANDRILL_API_KEY', $api['pref_value']);
			$contact = $dbmanager->Query(array('name', 'contact_email', 'contact_recieve_newsletters'), array(array('id' => $item)), null, 0, 0);
			if ($contact['result'][$item]['contact_recieve_newsletters'] == 1) $msg = $message['text'] . $newsletter_reminder['pref_value'];
			else $msg = $message['text'];
			$mandrill = new Mandrill();
			$message = array('subject' => $message['subject'], 'from_email' => $from_email['pref_value'], 'html' => $msg, 'merge_vars' => null, 'tags' => null, 'attachments' => null, 'to' => array(array('email' => $contact['result'][$item]['contact_email'], 'name' => $contact['result'][$item]['name'])));
			$result = $mandrill->messages->send($message);
			//file_put_contents('webhooks.log', ' '.serialize($result).' ', FILE_APPEND | LOCK_EX);

		}
		return 0;
	}
}
?>
