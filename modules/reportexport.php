<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Generates csv report from database content
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class ReportExportRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('category' => 'item category', 'function' => 'extra functions to run', 'functionstore' => 'parameters for passing to functions', 'fields' => 'database fields selected', 'list' => 'linked list', 'ids' => 'linked ids', 'title' => 'table name', 'counts' => 'countable db items', 'order' => 'db ordering');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetReport' => 'Returns report');
	//Object functions and variables go here

	/**
	 * Generate link to call report function
	 *
	 * @return Content Returns html link to report function
	 */
	var $data;
	//temp storage for functions
	var $functionstore;
	public function GetReport() {
		global $page;
		$output = '


					<a href="?method=GenerateReport&instance=' . $this->id . gets() . '" class="button"><span class="fa fa-download" title="Download a report"></span></a>

				';
		return $output;
	}
	/**
	 * Generate csv report
	 *
	 * @return File Returns csv file
	 */
	public function GenerateReportExcel() {
		require_once 'classes/PHPExcel.php';
		global $connectionmanager;
		$template = mysqli_real_escape_string($connectionmanager->connection, $_GET['template']);
		$name = mysqli_real_escape_string($connectionmanager->connection, $_GET['name']);
		if (isset($_GET['start'])) $start = mysqli_real_escape_string($connectionmanager->connection, $_GET['start']);
		else $start = '';
		if (isset($_GET['start'])) $end = mysqli_real_escape_string($connectionmanager->connection, $_GET['end']);
		else $end = '';
		$data = mysqli_query($connectionmanager->connection, "SELECT * FROM report_fill_data WHERE template_id = " . $template . " ORDER BY data_type");
		$objReader = PHPExcel_IOFactory::createReader("Excel2007");
		$objPHPExcel = $objReader->load("templates/template" . $template . ".xlsx");
		$objPHPExcel->setActiveSheetIndex(0);
		//data_type 0 = single result, 1 = fill down
		//note: column indexing 0 based, row indexing 1 based >:(
		while ($row = mysqli_fetch_assoc($data)) {
			//varstart = start, varend = end...
			$query = str_replace("varstart", $start, $row['query']);
			$query = str_replace("varend", $end, $query);
			$query = mysqli_query($connectionmanager->connection, $query);
			$i = 0;
			if ($row['data_type'] == 0) {
				$results = mysqli_fetch_row($query);
				foreach ($results as $result) {
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($row['start_column'] + $i), $row['start_row'], $result);
					$i++;
				}
			} else {
				while ($results = mysqli_fetch_row($query)) {
					$j = 0;
					$objPHPExcel->getActiveSheet()->insertNewRowBefore(($row['start_row'] + $i + 1), 1);
					foreach ($results as $result) {
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(($row['start_column'] + $j), ($row['start_row'] + $i), $result);
						$j++;
					}
					$i++;
				}
			}
		}
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=" . $name . '.xlsx');
		header("Content-Transfer-Encoding: binary ");
		$objWriter->save('php://output');
	}
	public function GenerateReport() {
		$column = $this->link('fields');
		$functions = $this->link('function');
		$functionstore = $this->link('functionstore');
		$category = $this->link('category');
		$list = $this->link('list');
		$ids = $this->link('ids');
		if ($list[0] == '') $list[0] = $this->id;
		$conds = array();
		if ($ids[0] != '') $conds = $ids[0];
		$conds[] = array('category' => $category[0]);
		$counts = $this->link('counts');
		$order = $this->link('order');
		$title = $this->link('title');
		global $dbmanager;
		$data = $dbmanager->Query($column, $conds, $order, 0, $list[0]);
		$numfields = count($column);
		$output = '';
		//Add header
		$output.= '"' . $title[0] . ' - ","' . $data['result_count'] . '"';
		$output.= "\n";
		$output.= "\n";
		//Add item counts
		if ($counts[0] != '') {
			$counts = $dbmanager->Fields($counts);
		}
		foreach ($counts as $index => $val) {
			$output.= '"' . $val['friendly_name'] . ' ","#"';
			$output.= "\n";
			foreach ($val['options'] as $countindex => $option) {
				$countconds = $conds;
				$countconds[] = array($index => $countindex);
				$countdata = $dbmanager->Query(array('name'), $countconds, null, 0, $list[0]);
				$output.= '"' . $option . '","' . $countdata['result_count'] . '",';
				$output.= "\n";
			}
		}
		$output.= "\n";
		$output.= "\n";
		//Add headings
		for ($i = 0;$i < $numfields;$i++) {
			$data['result'][0][$column[$i]] = $data[$column[$i]]['friendly_name'];
		}
		//Run functions on dataset
		$this->data = $data['result'];
		$this->functionstore = array();
		foreach ($functionstore as $index => $val) {
			$this->functionstore[$index] = $val;
		}
		foreach ($functions as $function) {
			if ($function != null) {
				include 'functions/' . $function . '.php';
				$function($this->id);
			}
		}
		ksort($this->data);
		//Add main data set
		foreach ($this->data as $index => $row) {
			for ($i = 0;$i < count($this->data[0]);$i++) {
				$output.= "\"" . $row[$column[$i]] . "\",";
			}
			$output.= "\n";
		}
		//Add csv specific data
		header("Content-type: application/vnd.ms-excel");
		header("Content-disposition: csv; filename=" . $title[0] . "_report.csv");
		print $output;
		exit;
	}
}
?>
