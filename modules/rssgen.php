<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Generates list of items of data in JSON format
 * This module was originally intended to support RSS,
 * but JSON is much more flexible.
 *
 * The name is left unchanged for compatibility reasons...
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class RSSGen extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('category' => 'DB category', 'column' => 'column data', 'conditions' => 'extra conditions', 'paginate' => 'pagination size (default 10)', 'order' => 'default list ordering', 'filter' => 'filter parameters', 'content' => 'header content', 'remove' => 'item or link', 'file_name' => 'location to store files', 'quick_lookup' => 'Generate an index based upon 2 variables 0 and 1 - 1 will be the key and 0 the variable');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetRSS' => 'Returns RSS', 'SaveRSSBatch' => 'Saves to given file location');
	//Object functions and variables go here
	var $count;
	var $ids;
	//Object functions and variables go here

	/**
	 * Generates list of items
	 *
	 * @return Content Returns html/scripting representation of list
	 */
	public function GetRSS() {
		//error_reporting(1);
		//Get setup data, query DBmanager
		$column = $this->link('column');
		$conditions = $this->link('conditions');
		$numfields = count($column);
		$order = $this->link('order');
		$content = $this->link('content');
		$category = $this->link('category');
		$remove = $this->link('remove');
		$paginate = $this->link('paginate');
		$quick_lookup = $this->link('quick_lookup');
		// Fix unlimited results
		$paginate[0] = 0;
		$listplace = 0; //if ($remove[0] != '')  $column[] = $remparams[1];
		$listvars = $column;
		if ($category[0] == 1) $listvars[] = 'contact_last';
		global $dbmanager;
		global $item_id;
		$ids = array();
		$convars = array(array('category' => $category[0]));
		foreach ($conditions as $conindex => $conval) {
			if ($conval == 'item_id') $conval = $item_id;
			if ($conval != '') $convars[] = array($conindex => $conval);
		}
		$data = $dbmanager->Query($listvars, $convars, $order, $paginate[0], $this->id);
		$fields = $dbmanager->Fields($column);
		global $page;
		$currentpage = $page;
		//Set count variable
		$this->count = $data['result_count'];
		//Generate quick lookup table by shorturl
		//$this->quick_lookup = array_column($records, 'last_name', 'id');
		if (isset($quick_lookup[0]) && isset($quick_lookup[1])) {
			foreach ($data['result'] as $r) {
				$data['quick_lookup'][$r[$quick_lookup[0]]] = $r[$quick_lookup[1]];
			}
		}
		//echo $data['query'];
		$output = '';
		if ($data['result_count'] > 0) {
			global $item_id;
			$this->ids = $ids;
			if ($this->permissions['GetList'] <= Users::GetCurrentUser()['user_level']) {
				return json_encode($data);
			}
		} else {
			return null;
		}
	}
	/**
	 * Generates list of items and saves it to a particular file location
	 *
	 * @return Content Returns html/scripting representation of list
	 */
	public function SaveRSSBatch() {
		$filename = $this->link('file_name');
		$filename = 'api/exports/' . $filename[0] . '.json';
		$content = $this->GetRSS();
		file_put_contents($filename, $content);
		return 'Saved to ' . $filename . '
';
	}
}
?>
