<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates sidebar content
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class Sidebar extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('custom_kiosk' => 'Enter the filename of a custom kiosk (if used)','custom_feedback' => 'Enter the filename of a custom kiosk (if used)');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('ESA_summary' => 'ESA summary', 'EventFeedback' => 'Returns event feedback ratings', 'EventHowHeard' => 'Returns event how heard stats', 'sidebar_directory' => 'Returns directory info', 'ContactSummary' => 'Sidebar info summary', 'ContactHowWeMet' => 'Contact how we met info', 'ESA_sidebar' => 'ESA previous team levels', 'EventAttendance' => 'Drop off rate and other event stats', 'KioskButton' => 'Events kiosk button', 'FeedbackButton' => 'Events feedback kiosk button', 'testFeedback' => 'test feedback', 'Map_sidebar' => 'Display a map when a location or contact has a postcode assigned');
	//Object functions and variables go here
	public function sidebar_directory() {
		global $item_id;
		global $connectionmanager;
		global $dbmanager;
		$data = $dbmanager->Query(array('contact_role', 'contact_title', 'contact_year_study', 'contact_orgcode'), array(array('id' => $item_id)), null, 0, $this->id);
		//$query = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection,"SELECT * FROM contacts WHERE contact_id =".mysqli_real_escape_string($connectionmanager ->connection,$item_id)));
		$query = $data['result'][$item_id];
		$output = '';
		if ($query['contact_role'] != '' && $query['contact_title'] != '') {
			if (levenshtein($query['contact_role'], $query['contact_title']) < 4) $output.= 'Role: ' . $query['contact_role'] . '<br />';
			else $output.= 'Role: ' . $query['contact_role'] . '<br />(' . $query['contact_title'] . ')<br />';
		} elseif ($query['contact_role'] != '') $output.= 'Role: ' . $query['contact_role'] . '<br />';
		elseif ($query['contact_title'] != '') $output.= 'Role: ' . $query['contact_title'] . '<br />';
		if ($query['contact_year_study'] != 0) $output.= 'Year of study: ' . $query['contact_year_study'] . '<br />';
		if ($query['contact_orgcode'] != '') {
			$depts = simplexml_load_file('hierarchy.xml');
			$result = $depts->xpath('ROW[LEVEL5="' . $query['contact_orgcode'] . '"]');
			$output.= 'Organisation: ' . $result[0]->LEVEL5DESC;
		}
		if ($output != '') $output = '<h2>Work info</h2>' . $output;
		return $output;
	}
	public function ContactSummary() {
		$output = '<h2>Contact details</h2>';
		global $connectionmanager;
		global $dbmanager;
		global $item_id;
		$data = $dbmanager->Query(array('contact_retired', 'contact_is_key', 'contact_priority', 'contact_email', 'contact_recieve_newsletters', 'contact_phone', 'contact_cell', 'contact_org', 'contact_when_we_spoke', 'contact_is_active'), array(array('id' => $item_id)), null, 0, $this->id, 1);
		$row = $data['result'][$item_id];
		$created_check = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE (id = " . mysqli_real_escape_string($connectionmanager->connection, $item_id) . " )"));
		//time check
		$created = max(strtotime($created_check['created']), strtotime($row['contact_when_we_spoke']));
		$now = time() - 15552000;
		if (isset($row['contact_retired'])) {
			$special_check = false; // do we need to add a separator?
			if ($row['contact_retired'] == 1) {
				$output.= '<p class="warning">Left the organisation</p>';
			} else {
				if (isset($row['contact_is_key'])) {
					if ($row['contact_is_key'] == 1) {
						$output.= '<p class="key">Key contact</p>';
						$special_check = true;
					}
				}
				if (isset($row['contact_priority'])) {
					if ($row['contact_priority'] == 1) {
						$output.= '<p class="mip">Important contact</p>';
						$special_check = true;
					} elseif ($row['contact_priority'] == 2) {
						$output.= '<p class="vip">Very important contact</p>';
						$special_check = true;
					}
				}
				if ($special_check == true) {
					$output.= '<hr />';
				}
			}
		}
		$output.= '<p>';
		if (isset($row['contact_email']) && $row['contact_email'] != null) {
			$output.= '<p><span class="fa fa-envelope fa-fw fa-2x" title="Email address" ></span> ' . $row['contact_email'] . '</p>';
		}
		if (isset($row['contact_recieve_newsletters']) && $row['contact_recieve_newsletters'] == 1) {
			$output.= '<strong style="margin-left:37px;">No newsletters</strong><br />';
		}
		if (isset($row['contact_phone']) && $row['contact_phone'] != null) {
			$output.= '<p><span class="fa fa-phone fa-fw fa-2x" title="Phone" ></span> ' . $row['contact_phone'] . '</p>';
		}
		if (isset($row['contact_cell']) && $row['contact_cell'] != null) {
			$output.= '<p><span class="fa fa-mobile fa-fw fa-2x" title="Mobile phone" ></span> &nbsp; ' . $row['contact_cell'] . '</p>';
		}
		$output.= '</p>';
		//points based system to determine if a contact is active or not
		$contact_active = 1;
		$justification_log = array('positive' => array(), 'negative' => array());
		if ($row['contact_retired'] == 1) {
			$contact_active--;
			$justification_log['negative'][] = 'They have left the University.';
		}
		if (!isset($row['contact_org']) || $row['contact_org'] == null || $row['contact_org'] == '') {
			// the contact does not exist on the system
			$contact_active--;
			$justification_log['negative'][] = 'They are not verified as a staff member or student';
		} else {
			$contact_active++;
			$justification_log['positive'][] = 'They are verified as a staff member or student';
		}
		$recent_event = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT max(link_date) as recent_event FROM item_links LEFT JOIN global_notes on item_links.note_id = global_notes.note_id  WHERE ((item1 = '" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . "' AND cat2 = 5) OR (item2 = '" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . "') AND cat1 = 5) AND primary_meta = 0 ORDER BY note_date DESC")) [0];
		// were they at an event in the last 6 months
		if ($recent_event > $now) {
			//yes so do nothing, for now!
			$contact_active++;
			$justification_log['positive'][] = 'They booked for an event in the last 6 months';
		} else {
			//so they might be inactive
			$contact_active--;
			$justification_log['negative'][] = 'They haven\'t booked for an event in the last 6 months';
		}
		// Checking for recent notes and events
		$recent_note = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE (note_item = " . mysqli_real_escape_string($connectionmanager->connection, $item_id) . " AND (note_type = 5 OR note_type = 4 OR note_type = 9 OR note_type = 10) ) ORDER BY note_date DESC");
		$number_notes = mysqli_num_rows($recent_note);
		$most_recent_note = mysqli_fetch_assoc($recent_note);
		// We want the most recent event or note date only
		if ($most_recent_note['note_date'] > $recent_event) {
			$recent_event = $most_recent_note['note_date'];
		}
		$when_we_spoke_unix = strtotime($row['contact_when_we_spoke']);
		if ($when_we_spoke_unix < 1) {
			$when_we_spoke_unix = (time() - 6.307e+7);
		}
		// Get a percentage for activity
		$activity_percent = round(1 / ($number_notes / $when_we_spoke_unix * 1000), 0);
		if ($activity_percent > 100) {
			$activity_percent = 100;
		}
		if ($activity_percent < 1) {
			$activity_percent = 0;
		}
		// checking for last six months of activity
		$check = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE (note_item = " . mysqli_real_escape_string($connectionmanager->connection, $item_id) . " AND (note_type = 5 OR note_type = 4 OR note_type = 9 OR note_type = 10) AND note_date > " . (time() - 15552000) . ")");
		// were they added in the last 6 months?
		if ($created > $now) {
			//yes so do nothing, for now!
			$contact_active++;
			$justification_log['positive'][] = 'They were added in the last 6 months';
		} else {
			//so they might be inactive
			if (mysqli_num_rows($check) == 0) {
				$contact_active--;
				$justification_log['negative'][] = 'They haven\'t met us, emailed or read an email in the last 6 months';
			} else {
				$contact_active = $contact_active + 2;
				$justification_log['positive'][] = 'They met us, emailed us or read an email in the last 6 months';
			}
		}
		$activity_verbose_string = 'Last active ' . str_replace(array('a little while ago', 'more than ten years ago'), 'a very long time ago', fuzzy_time($recent_event)) . ', ' . $activity_percent . '% active';
		if ($contact_active < 1) {
			$contact_active = 0;
			$output.= '<br/><span class="fa fa-times fa-fw fa-2x"></span> Flagged as potentially inactive';
			$output.= '<br/>' . $activity_verbose_string;
			$output.= '<div class="sidebar_unfolding">
				<h4>Why?</h4>
				<ul class="square">';
			foreach ($justification_log['negative'] as $explanation) {
				$output.= '<li>' . $explanation . '</li>';
			}
			$output.= '</ul>
			</div>';
		} else {
			$contact_active = 1;
			$output.= '<br/><span class="fa fa-check fa-fw fa-2x"></span> Flagged as active';
			$output.= '<br/>' . $activity_verbose_string;
			$output.= '<div class="sidebar_unfolding">
				<h4>Why?</h4>
				<ul class="square">';
			foreach ($justification_log['positive'] as $explanation) {
				$output.= '<li>' . $explanation . '</li>';
			}
			$output.= '</ul>
			</div>';
		}
		/*
			table:items
			category:1
		*/
		// send post
		$post = new Post;
		$_POST['id'] = $item_id;
		$_POST['contact_is_active'] = $contact_active;
		$_POST['autoupdate'] = 1;
		$_POST['table'] = 'items';
		$_POST['category'] = 1;
		$_POST['function'] = 'post_contact';
		$update = $post->CheckPost();
		return $output;
	}
	public function ContactHowWeMet() {
		global $dbmanager;
		global $item_id;
		$data = $dbmanager->Query(array('contact_specific_meeting'), array(array('id' => $item_id)), null, 0, $this->id, 0);
		if (isset($data['result'][$item_id]['contact_specific_meeting']) && $data['result'][$item_id]['contact_specific_meeting'] != null && $data['result'][$item_id]['contact_specific_meeting'] != '') return '<h2>How we met</h2><p>' . $data['result'][$item_id]['contact_specific_meeting'] . '<br /></p>';
	}
	
	/**
	* Display a button to open the sign in kiosk
	*
	* @return string HTML output of the button
	*/
	public function KioskButton() {
		$custom_kiosk = $this->link('custom_kiosk');
		if (isset($custom_kiosk[0]) && $custom_kiosk != null) {
			return '<a href="'.$custom_kiosk[0].'.php?event_id=' . $_GET['id'] . '" class="button"><span class="fa fa-external-link fa-2x" title="Open Kiosk" target="_new" ></span> Sign-in kiosk</a> ';
		} else {
			return '<a href="kiosk.php?event_id=' . $_GET['id'] . '" class="button"><span class="fa fa-external-link fa-2x" title="Open Kiosk" target="_new" ></span> Sign-in kiosk</a> ';
		}
		
	}
	
	/**
	* Display a button to open the feedback kiosk
	*
	* @return string HTML output of the button
	*/
	public function FeedbackButton() {
		$custom_feedback = $this->link('custom_feedback');
		if (isset($custom_feedback[0]) && $custom_feedback != null) {
			return '<a href="'.$custom_feedback[0].'.php?event_id=' . $_GET['id'] . '" class="button"><span class="fa fa-commenting fa-2x" title="Open feedback kisok" target="_new"></span> Feedback</a> ';
		} else {
			return '<a href="kiosk_feedback.php?event_id=' . $_GET['id'] . '" class="button"><span class="fa fa-commenting fa-2x" title="Open feedback kisok" target="_new"></span> Feedback</a> ';
		}
		
	}
	
	/**
	* Display event attendance statistics
	*
	* @global Object $connectionmanager Used to search the event links
	* @global integer $item_id Searches for data based upon this item
	* @return string HTML output
	*/
	public function EventAttendance() {
		global $connectionmanager;
		global $item_id;
		$stats = mysqli_query($connectionmanager->connection, "SELECT count(link_id) as count, note_type FROM item_links LEFT JOIN global_notes on item_links.note_id = global_notes.note_id WHERE (item1 = $item_id OR item2 = $item_id) AND note_type IS NOT NULL GROUP BY note_type");
		$data = array();
		while ($row = mysqli_fetch_assoc($stats)) {
			$data[$row['note_type']] = $row['count'];
		}
		$output.= '<h3>Attendance</h3>';
		$output.= '<p><strong>Booked:</strong> ' . ($data['52'] + $data['53']) . ' (minus duplicates and cancellations)</p>';
		$output.= '<p><strong>Attended:</strong> ' . ($data['51'] + $data['53']) . ' (including ' . $data['51'] . ' walk-ins)</p>';
		if ($data['51'] > 0 && ($data['52'] + $data['53']) > 0) {
			$output.= '<p>(' . round(100 - ($data['51'] + $data['53']) / ($data['52'] + $data['53']) * 100) . '% drop off rate)</p>';
		}
		return $output;
	}
	public function EventFeedback() {
		global $connectionmanager;
		global $item_id;
		$total = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT SUM(val) FROM extra_int WHERE item_id=" . $item_id . " AND (meta_id=619 OR meta_id=620 OR meta_id=621) ORDER BY meta_id ASC"));
		$total = $total[0];
		$data = mysqli_query($connectionmanager->connection, "SELECT * FROM extra_int WHERE item_id=" . $item_id . " AND (meta_id=619 OR meta_id=620 OR meta_id=621) ORDER BY meta_id ASC");
		$titles = array(array('Good', ' #197b16'), array('Average', '#fd7d22'), array('Bad', '#c0061e'));
		$output.= "<script>google.load('visualization', '1', {packages: ['corechart', 'bar']});
google.setOnLoadCallback(drawMaterial);

function drawMaterial() {
      var data = google.visualization.arrayToDataTable([
        ['', 'Responses', { role: 'annotation' } ],
      ";
		$i = 0;
		$percent_green = 0;
		while ($row = mysqli_fetch_assoc($data)) {
			if ($i == 0) {
				$percent_green = round(($row['val'] / $total) * 100);
			}
			$output.= "['" . $titles[$i][0] . "', " . $row['val'] . ", '" . round(($row['val'] / $total) * 100) . "'],
";
			$i++;
		}
		$output.= "]);

      var options = {
		theme: 'maximized',
       
        legend: { position: 'none' },
        bars: 'horizontal'
      };
      var material = new google.charts.Bar(document.getElementById('feedback_chart'));
      material.draw(data, options);
    }
		</script>
		
		<style type='text/css'>
			#feedback_chart div {
				margin: 0 !important;
				width: 100%;
				padding: 0;
			}
		</style>
		<h3>Feedback</h3>
		<div id='feedback_chart'></div>
		<p><strong>$percent_green% positive</strong> out of $total people who left feedback</p>";
		if ($i > 0) return $output;
	}
	public function ESA_summary() {
		return '<div class="five columns omega action_bar"><a href="?page=esa-team-add-edit&id=' . $_GET['id'] . '" class="button"><img src="images/edit_icon.png" alt="Edit" title="Edit" class="solo"></a></div>';
	}
	public function ESA_sidebar() {
		global $connectionmanager;
		global $dbmanager;
		$level = 0;
		$levels = array('', 'Bronze', 'Silver', 'Gold');
		$offices = array(0, 0, 0);
		$labs = array(0, 0, 0);
		$data = $dbmanager->Query(array('level', 'prev_id'), array(array('id' => $_GET['id'])), null, 0, $this->id);
		foreach ($data['result'] as $result) {
			$level = $result['level'];
			$prev_id = $result['prev_id'];
		}
		if ($level != 0) {
			$level = str_split($level);
			$offices[2] = $level[1];
			$labs[2] = $level[2];
		}
		$prev = mysqli_query($connectionmanager->connection, "SELECT * FROM esa_teamlevel WHERE teamlevel_team=" . $prev_id);
		while ($row = mysqli_fetch_assoc($prev)) {
			if ($row['teamlevel_workbook'] == 2) $offices[0] = $row['teamlevel_level'];
			elseif ($row['teamlevel_workbook'] == 5) $offices[1] = $row['teamlevel_level'];
			elseif ($row['teamlevel_workbook'] == 3) $labs[0] = $row['teamlevel_level'];
			elseif ($row['teamlevel_workbook'] == 6) $labs[1] = $row['teamlevel_level'];
		}
		if (array_sum($offices) + array_sum($labs) > 0) $output = '<strong>Awards participation</strong><br/><br/>';
		if ($offices[0] || $labs[0] != 0) $output.= '2014:<br/>';
		if ($offices[0] != 0) $output.= 'Offices:' . $levels[$offices[0]] . '<br/>';
		if ($labs[0] != 0) $output.= 'Labs:' . $levels[$labs[0]] . '<br/>';
		if ($offices[1] || $labs[1] != 0) $output.= '<br/>2015:<br/>';
		if ($offices[1] != 0) $output.= 'Offices:' . $levels[$offices[1]] . '<br/>';
		if ($labs[1] != 0) $output.= 'Labs:' . $levels[$labs[1]] . '<br/>';
		if ($offices[2] || $labs[2] != 0) $output.= '<br/>2016:<br/>';
		if ($offices[2] != 0) $output.= 'Offices:' . $levels[$offices[2]] . '<br/>';
		if ($labs[2] != 0) $output.= 'Labs:' . $levels[$labs[2]] . '<br/>';
		return $output;
	}
	public function Map_sidebar() {
		global $connectionmanager;
		global $dbmanager;
		global $item_id;
		global $connectionmanager;
		// get the google maps API key
		$google_maps_api_key = mysqli_fetch_array(mysqli_query($connectionmanager->connection, "SELECT *  FROM `module_preferences` WHERE `pref_name` = 'google_maps_api_key' LIMIT 1")) ['pref_value'];
		// try to get the postcode for the current item
		$data = $dbmanager->Query(array('location_postcode', 'contact_postcode', 'category'), array(array('id' => $item_id)), null, 0, $this->id);
		$output = '';
		$location = '';
		echo $result['contact_location'];
		foreach ($data['result'] as $key => $result) {
			if (isset($result['location_postcode'])) {
				$extra_text = 'This place is located at ';
				$location.= $result['location_postcode'];
			} elseif (isset($result['contact_postcode'])) {
				$extra_text = 'This contact is located at ';
				$location = $result['contact_postcode'];
			} elseif ($result['category'] == 1) {
				$linked_locations = mysqli_query($connectionmanager->connection, "SELECT * FROM `item_links` WHERE (item1 = $item_id OR item2 = $item_id) AND is_primary = 1 AND primary_meta = 2 LIMIT 1");
				while ($row = mysqli_fetch_assoc($linked_locations)) {
					$linked_location = $dbmanager->Query(array('location_postcode'), array(array('id' => $row['item2'])), null, 0, $this->id);
					foreach ($linked_location['result'] as $k => $r) {
						$location = $r['location_postcode'];
						$extra_text = 'This contact works at ' . $r['name'] . ', ';
					}
				}
			}
			if ($location != null) {
				$output.= '<h3>Location</h3><p>' . $extra_text . '<strong>' . $location . '</strong></p>';
				$location_latlng = mysqli_fetch_array(mysqli_query($connectionmanager->connection, "SELECT *  FROM `postcode_lookup` WHERE `postcode` LIKE '$location'"));
				if ($location_latlng['latitude'] != null && $location_latlng['longitude'] !== null) {
					$output.= '<img src="https://maps.googleapis.com/maps/api/staticmap?center=' . $location_latlng['latitude'] . ',' . $location_latlng['longitude'] . '&zoom=16&size=300x300&maptype=roadmap
&markers=color:green%7C' . $location_latlng['latitude'] . ',' . $location_latlng['longitude'] . '&key=' . $google_maps_api_key . '" alt="Map of ' . $location . '" />';
				}
				return $output;
			}
		}
	}
	public function EventHowHeard() {
		global $connectionmanager;
		global $item_id;
		$total = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT SUM(val) FROM extra_int WHERE item_id=" . $item_id . " AND (meta_id=612 OR meta_id=613 OR meta_id=614 OR meta_id=615 OR meta_id=616 OR meta_id=617 OR meta_id=618) ORDER BY meta_id ASC"));
		$total = $total[0];
		$data = mysqli_query($connectionmanager->connection, "SELECT * FROM extra_int WHERE item_id=" . $item_id . " AND (meta_id=612 OR meta_id=613 OR meta_id=614 OR meta_id=615 OR meta_id=616 OR meta_id=617 OR meta_id=618) ORDER BY meta_id ASC");
		$output = '<h3>How people heard</h3>';
		$title = array(612 => 'University website/App', 613 => 'Another website', 614 => 'Email or invitation', 615 => 'Social media', 616 => 'Flyer or poster', 617 => 'Word of Mouth', 618 => 'Other');
		$results = array();
		while ($row = mysqli_fetch_assoc($data)) {
			$results[$row['val']].= '<p><strong>' . $title[$row['meta_id']] . ':</strong> ' . $row['val'] . ' (' . round($row['val'] / $total * 100) . '%)</p>';
		}
		krsort($results, SORT_NUMERIC);
		foreach ($results as $result) {
			$output.= $result;
		}
		if ($total > 0) return $output;
	}
	public function testFeedback() {
		global $dbmanager;
		$data = $dbmanager->Query(array('votes'), array(array('id' => $_GET['id'])), null, 0, 0);
		foreach ($data['result'] as $key => $result) {
			$votes = unserialize($result['votes']);
			print_r($votes);
		}
	}
}
?>