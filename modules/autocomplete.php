<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates autocomplete dropdown
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class Autocomplete extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('types' => 'set of types');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('AutoAll' => 'autocomplete');
	//Object functions and variables go here
	
	/**
	 * Executes autocomplete function
	 *
	 * @return array Returns JSON array autocomplete data
	 */
	public function AutoAll() {
		global $dbmanager;
		$return = array();
		global $connectionmanager;
		$_GET['parama_all_0'] = $_GET['term'];
		$_GET['op_all_0'] = 'contain';
		if (!(isset($_GET['field_1_0']))) $_GET['field_all_0'] = 'name';
		else $_GET['field_all_0'] = $_GET['field_1_0'];
		$_GET['inf_all_0'] = 'reg_0';
		$catlist = mysqli_query($connectionmanager->connection, "SELECT * FROM categories");
		$categories = array();
		while ($row = mysqli_fetch_assoc($catlist)) {
			$categories[$row['cat_id']] = array($row['cat_name'], $row['cat_page']);
		}
		$duplicate = array();
		if (isset($_GET['type'])) {
			//Return autocomplete suggests for single type
			$data = $dbmanager->Query(array($_GET['field_all_0'], 'name', 'id', 'contact_title'), array(array('category' => $_GET['type'])), null, 4, $this->id);
			foreach ($data['result'] as $row) {
				$key = $row[$_GET['field_all_0']];
				if ($row['contact_title'] != '') $row['contact_title'] = ' - ' . $row['contact_title'];
				if (!(array_key_exists("$key", $duplicate))) $return[] = array('value' => $row[$_GET['field_all_0']], 'name' => $row['name'] . $row['contact_title'], 'id' => $row['id'], 'category' => $_GET['type'], 'catname' => $categories[$_GET['type']][0], 'page' => $categories[$_GET['type']][1]);
				$duplicate["$key"] = 1;
			}
		} else {
			//Return autocomplete suggests for set of types
			$types = $this->link('types');
			$set = explode(',', $types[$_GET['set']]);
			foreach ($set as $category) {
				$data = $dbmanager->Query(array('name', 'id', 'contact_title'), array(array('category' => $category)), null, 4, $this->id);
				foreach ($data['result'] as $row) {
					$key = $row['name'];
					if ($row['contact_title'] != '') $row['contact_title'] = ' - ' . $row['contact_title'];
					if (!(array_key_exists("$key", $duplicate))) $return[] = array('value' => $row['name'] . $row['contact_title'], 'name' => $row['name'], 'id' => $row['id'], 'category' => $category, 'catname' => $categories[$category][0], 'page' => $categories[$category][1]);
					$duplicate["$key"] = 1;
				}
			}
		}
		echo json_encode($return);
	}
}
?>