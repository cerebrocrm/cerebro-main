<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Generates data input form
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class Wizard extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('images' => 'some content', 'text' => 'descriptive text', 'vars' => 'form variables', 'title' => 'form title', 'table' => 'DB table', 'post' => 'DBmanager', 'detail' => 'detail page', 'category' => 'other db categories', 'form' => 'integrated forms', 'hidden' => 'hidden elements');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetForm' => 'Returns form');
	//Object functions and variables go here

	/**
	 * Generates form
	 *
	 * @return Content Returns html/scripting representation of form
	 */
	public function EMailCheck() {
		global $connectionmanager;
		$check = mysqli_query($connectionmanager->connection, "SELECT * FROM contacts WHERE contact_email LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $_POST['email']) . "'");
		if (mysqli_num_rows($check) > 0) {
			$row = mysqli_fetch_assoc($check);
			return $row['contact_id'];
		} else return 0;
	}
	/**
	 * Generates form
	 *
	 * @return Content Returns html/scripting representation of form
	 */
	public function GetForm() {
		global $page;
		$imglist = array();
		$elements = array();
		$table = $this->link('table');
		$detail = $this->link('detail');
		$form = $this->link('form');
		$inputs = '';
		$datastring = 'var dataString = "table=' . $table[0] . '"';
		$img = $this->link('images');
		$header = $this->link('header');
		$hidden = $this->link('hidden');
		$text = $this->link('text');
		$vars = $this->link('vars');
		$title = $this->link('title');
		$post = $this->link('post');
		$parameters = array();
		$category = $this->link('category');
		$summary = '';
		$summaryscript = '';
		if (isset($_GET['id'])) $id = "&id=" . $_GET['id'];
		else $id = '';
		global $ {
			'module_' . $post[0]
		};
		$fields = $ {
			'module_' . $post[0]
		}->Fields();
		$output = '<div class="row form_display"><div class="row top_part">

                <div class="six columns alpha title_bar" >
                    <h2>' . $title[0] . '</h2>
                </div>

            </div>';
		$output.= '
                    <form id="contract_form" action="?page=post" method="POST"> ';
		//store images for latter append
		foreach ($img as $index => $value) {
			if (strpos($index, ',')) {
				$pos = explode(',', $index);
				$imglist[$pos[0]][$pos[1]][$pos[2]][$pos[3]] = $value;
			}
		}
		$forms = 0;
		foreach ($form as $index => $value) {
			if (strpos($index, ',')) {
				$pos = explode(',', $index);
				$elements[$pos[0]][$pos[1]][$pos[2]][0] = $value;
				$forms++;
			}
		}
		foreach ($hidden as $index => $value) {
			if (strpos($index, ',')) {
				$pos = explode(',', $index);
				$elements[$pos[0]][$pos[1]][$pos[2]][$pos[3]] = '<input type="hidden" name="' . $value . '" id="' . $value . '">';
			}
		}
		//section text
		foreach ($text as $index => $value) {
			if ($value != '') {
				$pos = explode(',', $index);
				if (array_key_exists($pos[0], $imglist) && array_key_exists($pos[1], $imglist[$pos[0]]) && array_key_exists($pos[2], $imglist[$pos[0]][$pos[1]]) && array_key_exists(0, $imglist[$pos[0]][$pos[1]][$pos[2]])) {
					$img1 = '<img src="' . $imglist[$pos[0]][$pos[1]][$pos[2]][0] . '"/>';
				}
				$element = '<div class="ten columns alpha omega">
                                ' . $value . '
                            </div>';
				$elements[$pos[0]][$pos[1]][$pos[2]][0] = $element;
			}
		}
		//Get fill values if applicable
		/* if (isset($_GET['id'])) {

		                  foreach($fields as $index => $value){
		                              if ($value['type'] == 'primary') $primary = $index;
		                  }

		                  foreach ($vars as $index => $value) {

		                      if (strpos($index,',')) {
		                          $data = explode(',',$value);
		                          $parameters[] = $data[0];
		                      }

		                  }

		                  $filldata = ${'module_' . $post[0]}->Query($parameters,array($primary => $_GET['id']),'list',array(),0,'');
		                  $fillvars = mysqli_fetch_assoc($filldata['result']);
		      }*/
		//Add each variable to form
		foreach ($vars as $index => $value) {
			if (strpos($index, ',')) {
				$pos = explode(',', $index);
				$data = explode(',', $value);
				if (count($pos) == 5) $datacat = $pos[5];
				else $datacat = '';
				switch ($fields[$data[0]]['type']) {
					case 'tiny':
						if (is_array($fillvars) && $fillvars[$data[0]] == 1) $fillval = ' checked ';
						else $fillval = '';
						$element = '<div class="' . $data[1] . ' checkbox">
                                <label for="' . $data[0] . '_0" ><span class="form_label_text">' . $fields[$data[0]]['friendly_name'] . '</label>
                                <input class="' . $datacat . '" name="' . $data[0] . '_0" type="checkbox" id="' . $data[0] . '_0" value="1" ' . $fillval . '/>
                            </div>';
						$inputs.= 'var ' . $data[0] . '_0 = $("input#' . $data[0] . '_0:checked").val();
                        ';
						$summary.= $fields[$data[0]]['friendly_name'] . ': <div id = "summary_' . $data[0] . '"></div></br>';
						$summaryscript.= 'var summary_' . $data[0] . ' = $("input#' . $data[0] . '_0:checked").val();
											if ( summary_' . $data[0] . ' == 0)
											{
												$("#summary_' . $data[0] . '").text("No");
											}
											else
											{
												$("#summary_' . $data[0] . '").text("Yes");
											}';
						break;
					case 'option':
						$element = '<div class="' . $datacat . ' ' . $data[1] . '">
                            <select id="' . $data[0] . '_0" name="' . $data[0] . '_0" class="summary">';
						$element.= '<option value="NOT_SELECTED">' . $fields[$data[0]]['friendly_name'] . '</option>';
						foreach ($fields[$data[0]]['options'] as $optindex => $optvalue) {
							$isselected = '';
							if (is_array($fillvars) && $fillvars[$data[0]] == $optindex) $isselected = ' selected ';
							$element.= '<option value="' . $optindex . '" ' . $isselected . '>' . $optvalue . '</option>';
						}
						$element.= '</select></div>';
						$inputs.= 'var ' . $data[0] . '_0 = $("#' . $data[0] . '_0").val();
                        ';
						$summary.= $fields[$data[0]]['friendly_name'] . ': <div id = "summary_' . $data[0] . '"></div></br>';
						$summaryscript.= 'var summary_' . $data[0] . ' = $("#' . $data[0] . '_0 option:selected").text();
											$("#summary_' . $data[0] . '").text(summary_' . $data[0] . ');
											';
						break;
					case 'password':
						$img1 = '';
						$img2 = '';
						if (is_array($fillvars)) $fillval = $fillvars[$data[0]];
						else $fillval = '';
						if (array_key_exists(2, $data)) {
							$class = 'required';
							$class2 = 'req';
						} else {
							$class = '';
							$class2 = '';
						}
						if (array_key_exists($pos[0], $imglist) && array_key_exists($pos[1], $imglist[$pos[0]]) && array_key_exists($pos[2], $imglist[$pos[0]][$pos[1]]) && array_key_exists($pos[3], $imglist[$pos[0]][$pos[1]][$pos[2]])) {
							$img1 = '<img src="' . $imglist[$pos[0]][$pos[1]][$pos[2]][$pos[3]] . '" alt="' . $fields[$data[0]]['friendly_name'] . '" />';
							$img2 = ' has_image ';
						}
						$element = '<div class="' . $data[1] . '" id="' . $data[0] . '">
                                <label for="' . $data[0] . '_0" class="' . $class . '">' . $img1 . '<span class="form_label_text">' . $fields[$data[0]]['friendly_name'] . '</span> </label>
                                <input type="password" name="' . $data[0] . '_0" id="' . $data[0] . '_0" ' . $img2 . ' class="' . $class2 . $img2 . '" value="' . $fillval . '"/>
                            </div>
                            ';
						$inputs.= 'var ' . $data[0] . '_0 = $("input#' . $data[0] . '_0").val();
                        ';
						break;
					default:
						$img1 = '';
						$img2 = '';
						if (is_array($fillvars)) $fillval = $fillvars[$data[0]];
						else $fillval = '';
						if (array_key_exists(2, $data)) {
							$class = 'required';
							$class2 = 'req';
						} else {
							$class = '';
							$class2 = '';
						}
						if (array_key_exists($pos[0], $imglist) && array_key_exists($pos[1], $imglist[$pos[0]]) && array_key_exists($pos[2], $imglist[$pos[0]][$pos[1]]) && array_key_exists($pos[3], $imglist[$pos[0]][$pos[1]][$pos[2]])) {
							$img1 = '<img src="' . $imglist[$pos[0]][$pos[1]][$pos[2]][$pos[3]] . '" alt="' . $fields[$data[0]]['friendly_name'] . '" />';
							$img2 = ' has_image ';
						}
						$element = '<div class="' . $data[3] . '" id="' . $data[0] . '">
                                <label for="' . $data[0] . '_0" class="' . $class . '">' . $img1 . '<span class="form_label_text ' . $data[0] . '_0">' . $data[1] . '</span> </label>
                                <input type="text" name="' . $data[0] . '_0" id="' . $data[0] . '_0" ' . $img2 . ' class="' . $datacat . ' summary ' . $class2 . $img2 . '" value="' . $fillval . '"/>
                            </div>
                            ';
						$inputs.= 'var ' . $data[0] . '_0 = $("input#' . $data[0] . '_0").val();
                        ';
						$summary.= $fields[$data[0]]['friendly_name'] . ': <div id = "summary_' . $data[0] . '"></div></br>';
						$summaryscript.= 'var summary_' . $data[0] . ' = $("#' . $data[0] . '_0").val();
											$("#summary_' . $data[0] . '").text(summary_' . $data[0] . ');
											';
						break;
					}
					$datastring.= ' + "&' . $data[0] . '_0=" + ' . $data[0] . '_0';
					$elements[$pos[0]][$pos[1]][$pos[2]][$pos[3]] = $element;
				}
			}
			$script = '<script type="text/javascript">';
			//Assemble elements into complete form
			for ($l = 0;$l < count($elements);$l++) {
				$script.= '$( "#button_' . $l . '" ).click(function () {
			$(".form_segment").hide();
			$("#segment_button_' . ($l + 1) . '").show();

		});
		$( "#back_button_' . $l . '" ).click(function () {
			$(".form_segment").hide();
						$("#summary_page").hide();

			$("#segment_button_' . ($l - 1) . '").show();

		});';
				$output.= '<div class="form_segment" id="segment_button_' . $l . '">';
				for ($i = 0;$i < count($elements[$l]);$i++) {
					if (!($i % 2)) {
						$output.= '<div class="form_area">';
					} else {
						$output.= '<div class="form_area even">';
					}
					for ($j = 0;$j < count($elements[$l][$i]);$j++) {
						$output.= '<div class="row primary_item">';
						for ($k = 0;$k < count($elements[$l][$i][$j]);$k++) {
							$output.= $elements[$l][$i][$j][$k];
						}
						$output.= '</div>';
					}
					$output.= '</div>';
				}
				if ($l == (count($elements) - $forms - 1)) $output.= '<input type="hidden" name="table" value="' . $table[0] . '"><input type="hidden" name="redirect" value="' . $detail[0] . '"></form>';
				$output.= '</div>';
			}
			$output.= '</div><div id="summary_page" class="form_segment"><div class = "row">' . $summary . '</br> </div><div class="six columns omega action_bar"> <a href="#" class="button" id="back">Back</a><a href="#" class="button" id="submit">Submit</a></div></div>';
			if (isset($_GET['id']) && $_GET['id'] != '') {
				$reload = '?page=' . $detail[0] . '&id=' . $id . '&nv=';
				$datastring.= ' + "&' . $primary . '_0=' . $_GET['id'] . '"';
			} else {
				$reload = '?page=' . $detail[0] . '&id=';
			}
			$datastring.= ';
        ';
			foreach ($form as $integrated) {
				if ($integrated != '') $l--;
			}
			//add scripting
			$script.= '
$(document).ready(function () {


	$( "#confirm" ).click(function () {
		' . $summaryscript . '
		$(".form_segment").hide();
		$("#summary_page").show();

			$( "#back" ).click(function () {
			$("#summary_page").hide();
			$("#segment_button_' . ($l - 1) . '").show();
		});


	});

    $( "#submit" ).click(function () {

		$("#contract_form").submit();


	});


		$(".form_segment").hide();
		$("#segment_button_0").show();

});

</script>';
			//$output .='<div class="four columns omega action_bar">
			//         <a href="#" class="button" id="submit">Submit</a>
			//   </div></form></div>';
			$final = $output . $script;
			if ($this->permissions['GetForm'] <= Users::GetCurrentUser()['user_level']) {
				return $final;
			} else {
				return null;
			}
		}
	}
?>
