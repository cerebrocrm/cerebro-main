<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Handles security report functionality
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class myConverter {
	public static function convertToUTF8IconvIgnore($text, $originalCharset) {
		if ($originalCharset === 'unknown-8bit' || $originalCharset === 'x-user-defined') {
			$originalCharset = "latin1";
		}
		return iconv($originalCharset, 'utf-8//TRANSLIT', $text);
	}
}
class ParseSecurity extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('server' => 'pop3 mail server address', 'username' => 'username', 'password' => 'password');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('UpdateSecurity' => 'Parse incoming security mail and update notes', 'DisplaySecuritySidebar' => 'Display security reports on sidebar', 'DisplaySecuritySimpleReport' => 'Basic security report per month for every building', 'DisplaySecurityFull' => 'Full size chart of security reports over time', 'SecurityBuildingReport' => 'Weekly report generator', 'EmailWeeklyReports' => 'Send weekly report emails');
	//Object functions and variables go here

	/**
	 * Check if a mail hash already exists and return true if it does
	 *
	 * @return boolean
	 */
	function CheckHashExists($string) {
		global $connectionmanager;
		$string = preg_replace('/\[.*?\] /', '', $string);
		$hash = md5($string);
		$query = mysqli_query($connectionmanager->connection, "SELECT COUNT(*) FROM global_notes WHERE note_hash = '" . $hash . "'");
		$hashcount = mysqli_fetch_row($query);
		if ($hashcount[0] > 0) {
			return true;
		} else {
			return false;
		}
	}
	function upload_stats($network, $account, $users, $date = null) {
		global $connectionmanager;
		$network = mysqli_real_escape_string($connectionmanager->connection, $network);
		$account = mysqli_real_escape_string($connectionmanager->connection, $account);
		$users = mysqli_real_escape_string($connectionmanager->connection, $users);
		if ($date == null) {
			$date = date("Y-m-d");
		}
		$do = mysqli_query($connectionmanager->connection, "INSERT INTO socialstats (network, account, users, date) VALUES
						(
						'" . $network . "',
						'" . $account . "',
						'" . $users . "',
						'" . $date . "'
						)
					");
	}
	function get_location_id($building_number, $return_array = 0) {
		global $dbmanager;
		$post = new Post();
		// build post to add note
		$location = $dbmanager->Query(array('id'), array(array('building_number' => $building_number)), null, 0, 0);
		$location_id = 0;
		$name = null;
		if (count($location['result']) < 1) {
			$thisr = $this->id;
			$_GET = Array('j_' . $thisr => 1, 'field_' . $thisr . '_0' => 'building_number', 'op_' . $thisr . '_0' => 'contain', 'inf_' . $thisr . '_0' => 'zend_' . $thisr, 'parama_' . $thisr . '_0' => $building_number);
			$results = $dbmanager->Query(Array('building_number'), Array(Array('category' => 2)), null, 0, $thisr);
			foreach ($results['result'] as $result) {
				if ($result['name'] != null) {
					if ($result['building_number'] == $building_number) {
						$location_id = $result['id'];
						$name = $result['name'];
					} elseif (strpos($result['building_number'], ',') !== false) {
						$numbers = explode(',', $result['building_number']);
						foreach ($numbers as $number) {
							if (trim($number) == $building_number) {
								$location_id = $result['id'];
								$name = $result['name'];
							}
						}
					}
				}
			}
		} else {
			foreach ($location['result'] as $result) {
				$location_id = $result['id'];
				$name = $result['name'];
			}
		}
		if ($return_array == 1) {
			return Array('name' => $name, 'id' => $location_id);
		} else {
			return $location_id;
		}
	}
	/**
	 * Parse incoming mail and update notes
	 *
	 * @return void
	 */
	public function UpdateSecurity() {
		require_once "ezc/Base/base.php";
		$n = - 1;
		$loads = spl_autoload_functions();
		if (!(is_array($loads))) $loads = array();
		if (!(array_key_exists('my_autoloader', $loads))) {
			if (!(function_exists(my_autoloader))) {
				function my_autoloader($className) {
					ezcBase::autoload($className);
				}
			}
			spl_autoload_register('my_autoloader');
		}
		if (!function_exists('__autoload')) {
			function __autoload($className) {
				ezcBase::autoload($className);
			}
		}
		global $connectionmanager;
		global $dbmanager;
		$post = new Post();
		$server = $this->link('server');
		$user = $this->link('username');
		$pass = $this->link('password');
		$imap = new ezcMailImapTransport($server[0]);
		$imap->authenticate($user[0], $pass[0]);
		$mailboxes = $imap->listMailboxes();
		$imap->selectMailbox('Inbox');
		$messages = $imap->listMessages();
		//print_r($messages);
		ezcMailCharsetConverter::setConvertMethod(array('myConverter', 'convertToUTF8IconvIgnore'));
		$parser = new ezcMailParser();
		$stats = Array();
		$incidents = Array();
		foreach ($messages as $messageNr => $size) {
			$n++;
			$set = new ezcMailVariableSet($imap->top($messageNr, 5000));
			$mail = $parser->parseMail($set);
			$mail = $mail[0];
			$text = $mail->fetchParts() [0]->text;
			$doc = new DOMDocument();
			$doc->loadHTML($text);
			$tables = $doc->getElementsByTagName('table');
			$rows = $tables->item(0)->getElementsByTagName('tr');
			$i = 1;
			$incidents[$n] = Array();
			$incidents[$n]['Reported'] = date(DATE_RFC822, $mail->timestamp);
			$incidents[$n]['Incident number'] = substr($mail->subject, (strpos($mail->subject, '#:') + 3));
			foreach ($rows as $row) {
				$lines = explode(PHP_EOL, trim($row->textContent));
				$bldg = strpos($lines[0], 'Bldg ');
				$bldg1 = strpos($lines[1], 'Bldg ');
				if ($i == 5) {
					$incidents[$n]['Note'] = $lines[0];
				}
				// CAMPUS
				if ($lines[0] == 'Energy Saving') {
					$incidents[$n]['Campus'] = $lines[1];
				}
				// BUILDING
				if ($bldg !== false) {
					$incidents[$n]['Building number'] = trim(substr($lines[0], ($bldg + 5)));
					$incidents[$n]['Building'] = trim(substr($lines[0], 0, ($bldg - 3)));
				} elseif ($bldg1 !== false) {
					$incidents[$n]['Building number'] = trim(substr($lines[1], ($bldg1 + 5)));
					$incidents[$n]['Building'] = trim(substr($lines[1], 0, ($bldg1 - 3)));
				}
				if (strtolower($incidents[$n]['Building']) == 'roslin institute') {
					$incidents[$n]['Building number'] = 781;
				}
				if (strpos($lines[0], 'Floor') !== false) {
					$incidents[$n]['Room'] = trim($lines[0]);
				} elseif (strpos($lines[0], 'Room ') !== false) {
					$incidents[$n]['Room'] = trim($lines[0]);
				}
				// TYPE
				if (strpos($lines[0], 'Electrical Equipment') !== false || strpos($lines[1], 'Electrical Equipment')) {
					$incidents[$n]['Type'] = 'Electrical Equipment';
				} elseif (strpos($lines[0], 'Lights Turned Off') !== false) {
					$incidents[$n]['Type'] = trim($lines[0]);
				} elseif (strpos($lines[1], 'Lights Turned Off') !== false) {
					$incidents[$n]['Type'] = trim($lines[0]);
				}
				// SEVERITY
				if (strpos($lines[0], 'Over 20') !== false || strpos($lines[1], 'Over 20')) {
					$incidents[$n]['Severity'] = 'Over 20 Lights';
				} elseif (strpos($lines[0], '1 - 10 Lights') !== false || strpos($lines[1], '1 - 10 Lights')) {
					$incidents[$n]['Severity'] = '1 - 10 Lights';
				} elseif (strpos($lines[0], '10 - 20 Lights') !== false || strpos($lines[1], '10 - 20 Lights')) {
					$incidents[$n]['Severity'] = '10 - 20 Lights';
				}
				$i++;
			}
			$rows = explode("\n", $text);
			array_shift($text);
			$imap->delete($messageNr);
		}
		foreach ($incidents as $incident) {
			$incident_building = ltrim($incident['Building number'], '0');
			if ($incident['Building number'] != 'NOT_SELECTED' && $incident['Building number'] != null) {
				// update stats
				$date = date('Y-m-d', strtotime($incident['Reported']));
				$month = date('M', strtotime($incident['Reported']));
				$stats[$incident['Building number']][$date] = $stats[$incident['Building number']][$date] + 1;
				//$stats[$incident['Building number']][$month] = $stats[$incident['Building number']][$month] + 1;
				$location_id = $this->get_location_id($incident['Building number']);
				if ($location_id !== 0) {
					$text = 'Security have reported a sustainability issue here.

- **Security\'s incident number**: ' . $incident['Incident number'] . '
- **Reported**: ' . $incident['Reported'] . '
- **Type**: ' . $incident['Type'] . '
- **Severity**: ' . $incident['Severity'] . '
- **Summary**: ' . $incident['Note'] . '

	';
					$_POST = array();
					$_POST['table'] = 'global_notes';
					$_POST['note_type'] = 0;
					$_POST['note_text'] = $text;
					$_POST['note_item'] = $location_id;
					$_POST['note_date'] = strtotime($incident['Reported']);
					$dopost = $post->CheckPost();
					switch ($incident['Severity']) {
						case 'Over 20 Lights':
							$qty = 3;
						break;
						case '10 - 20 Lights':
							$qty = 2;
						break;
						case '1 - 10 Lights':
							$qty = 1;
						break;
						default:
							$qty = 0;
						break;
					}
					$this->upload_stats('secreport-detailed', $incident['Building number'] . '-' . $incident['Type'], $qty, $date);
				}
			}
		}
		$imap->expunge();
		foreach ($stats as $n => $stat) {
			$building_number = $n;
			foreach ($stat as $date => $qty) {
				$this->upload_stats('secreport', $building_number, $qty, $date);
			}
		}
		return 'mail parsing complete';
	}
	/**
	 * Parse incoming mail and update notes
	 *
	 * @return void
	 */
	public function DisplaySecuritySidebar() {
		global $connectionmanager;
		global $item_id;
		global $dbmanager;
		$output = '

		<h3>Security reports</h3>';
		$chartdata = array();
		$location = $dbmanager->Query(array('building_number'), array(array('id' => $item_id)), null, 0, 0);
		$building_number = 0;
		foreach ($location['result'] as $n => $result) $building_number = $result['building_number'];
		if (strpos($building_number, ',') !== false) {
			$building_number_temp = '';
			$numbers = explode(',', $building_number);
			$count = count($numbers);
			$n = 1;
			foreach ($numbers as $number) {
				if ($n == 1) {
					$building_number_temp.= trim($number) . ' ';
				} else {
					$building_number_temp.= 'OR account = ' . trim($number) . ' ';
				}
				$n++;
			}
			$building_number = $building_number_temp;
		}
		$data = mysqli_query($connectionmanager->connection, "SELECT SUM(users) as count, date FROM socialstats WHERE network = 'secreport' AND account = $building_number GROUP BY date ORDER BY date ASC

      	");
		$current_row = '';
		$total_incidents = 0;
		while ($row = mysqli_fetch_assoc($data)) {
			$total_incidents = ($total_incidents + $row['count']);
			$chartdata[$row['date']] = ($row['count']);
		}
		if ($total_incidents > 0) {
			$datatable = '["Date","Incidents"],
	';
			$d = 0;
			$count_d = count($chartdata);
			foreach ($chartdata as $id => $value) {
				$usedate = 'new Date("' . $id . '")';
				$datatable.= '[' . $usedate . ',' . $chartdata[$id] . '],';
				$d++;
			}
			$datatable = rtrim($datatable, ',');
			$output.= '
			<style type="text/css">
				#chart_div div {
					margin: 0 !important;
					padding: 0 !important;
				}
			</style>
			  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
			  <script type="text/javascript">
		google.load("visualization", "1", {packages: [\'corechart\']});
		google.setOnLoadCallback(drawSignUps);

		function drawSignUps()
		{';
			$output.= "var data = google.visualization.arrayToDataTable([
	" . $datatable . "
			]);


			var options = {
			theme: 'maximized',
			chartArea: {width: '95%', height: '95%'},
			isStacked: true,
			hAxis: {
			  title: 'Date',
			  format: 'd/M/yy'
			},
			vAxis: {
			  title: 'Number of reports'
			}
		  };

		  var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
		  chart.draw(data, options);
	}
		  </script>
		  <div id=\"chart_div\" style=\"height: 300px;\"> </div>";
		}
		$output.= '<p>Security have supplied <strong>' . $total_incidents . '</strong> total reports at this location since August 2015. <a href="?page=security-reports">View all reports</a></p>

';
		return $output;
	}
	/**
	 * Parse incoming mail and update notes
	 *
	 * @return void
	 */
	public function DisplaySecuritySimpleReport() {
		global $connectionmanager;
		global $item_id;
		global $dbmanager;
		$output = '
		<div class="seven columns alpha">
		<h3>Reports by month</h3>
		</div>
		<style type="text/css">
			#chart_div div {
				margin: 0 !important;
				padding: 0 !important;

			}

			#yeararea .chosen-drop {
				width: 200px !important;
			}

			#yeararea .chosen-single {
				width: 176px !important;
			}
		</style>

		<script type="text/javascript">

		$(function() {
			$("#year").change(function(e) {
				$("#yearform").submit();
			});
		  });

		</script>
		<form id="yearform" method="get" action="./">
		<input type="hidden" name="page" value="security-reports" />
		<div id="yeararea" class="three columns omega  ">
		<select name="year" id="year" data-placeholder="Choose a year" class="two columns">
		<option value=""></option>';
		foreach (range(date('Y'), 2015) as $x) {
			$output.= '<option value="' . $x . '"' . ($x === $year ? ' selected="selected"' : '') . '>' . $x . '</option>';
		}
		$output.= '
		</select>
		</div>
		</form>
		  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
		  <script type="text/javascript">
    google.load("visualization", "1", {packages: [\'table\']});
    google.setOnLoadCallback(drawSignUps);

    function drawSignUps()
    {';
		$chartdata = array();
		if (isset($_GET['year'])) {
			$year = mysqli_real_escape_string($connectionmanager->connection, $_GET['year']);
		} else {
			$year = date('Y');
		}
		$data = mysqli_query($connectionmanager->connection, "SELECT account, Year(date) as year, Month(date) as month, SUM(users) as sum FROM socialstats WHERE network = 'secreport' AND Year(date) = '" . $year . "' GROUP BY year, month , account ORDER BY date ASC

      	");
		$current_row = '';
		$total_incidents = 0;
		while ($row = mysqli_fetch_assoc($data)) {
			$total_incidents = ($total_incidents + $row['sum']);
			$chartdata[$row['account']][$row['year']][$row['month']] = ($row['sum']);
			$chartdata[$row['account']][$row['year']]['total'] = ($chartdata[$row['account']][$row['year']]['total'] + $row['sum']);
			if ($chartdata[$row['account']][$row['year']]['name'] == null) {
				// check item id
				$location = $this->get_location_id($row['account'], 1);
				$coordinators = $dbmanager->Links(30285, 1, $location['id'], 2);
				if (count($coordinators) > 0) {
					$chartdata[$row['account']][$row['year']]['number_coords'] = count($coordinators);
				} else {
					$chartdata[$row['account']][$row['year']]['number_coords'] = 0;
				}
				$name = $location['name'];
				if ($name == '') {
					$name = 'Unknown (ID: ' . $row['account'] . ')';
				}
				$chartdata[$row['account']][$row['year']]['name'] = '<a href=\'/?page=location-details&id=' . $location['id'] . '\'>' . $name . '</a>';
			}
		}
		$datatable = '["Location","Number coords","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","Total"],
';
		$d = 0;
		$count_d = count($chartdata);
		foreach ($chartdata as $id => $data) {
			foreach ($data as $year => $data2) {
				$datatable.= '["' . $data2['name'] . '",' . ($data2['number_coords']) . ',' . ($data2['1']) . ',' . ($data2['2']) . ',' . ($data2['3']) . ',' . ($data2['4']) . ',' . ($data2['5']) . ',' . ($data2['6']) . ',' . ($data2['7']) . ',' . ($data2['8']) . ',' . ($data2['9']) . ',' . ($data2['10']) . ',' . ($data2['11']) . ',' . ($data2['12']) . ',' . ($data2['total']) . '],
';
			}
			$d++;
		}
		$datatable = rtrim($datatable, ',');
		$output.= "var data = google.visualization.arrayToDataTable([
" . $datatable . "
        ]);
      var options = {allowHtml: true};
      var table = new google.visualization.Table(document.getElementById('table_div'));
      table.draw(data, options);
}
      </script>
      <div id=\"table_div\" style=\"width: 100%;\"> </div>";
		return $output;
	}
	/**
	 * Parse incoming mail and update notes
	 *
	 * @return void
	 */
	public function DisplaySecurityFull() {
		global $connectionmanager;
		global $item_id;
		global $dbmanager;
		$output = '

		<h2>Security reports</h2>
		<style type="text/css">
			#chart_div div {
				margin: 0 !important;
				padding: 0 !important;
			}
		</style>
		  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
		  <script type="text/javascript">
    google.load("visualization", "1", {packages: [\'corechart\']});
    google.setOnLoadCallback(drawSignUps);

    function drawSignUps()
    {';
		$chartdata = array();
		$location = $dbmanager->Query(array('building_number'), array(array('id' => $item_id)), null, 0, 0);
		$building_number = 0;
		foreach ($location['result'] as $n => $result) $building_number = $result['building_number'];
		$data = mysqli_query($connectionmanager->connection, "SELECT SUM(users) as count, date FROM socialstats WHERE network = 'secreport' GROUP BY date ORDER BY date ASC

      	");
		$current_row = '';
		$total_incidents = 0;
		while ($row = mysqli_fetch_assoc($data)) {
			$total_incidents = ($total_incidents + $row['count']);
			$chartdata[$row['date']] = ($row['count']);
		}
		$datatable = '["Date","Incidents"],
';
		$d = 0;
		$count_d = count($chartdata);
		foreach ($chartdata as $id => $value) {
			$usedate = 'new Date("' . $id . '")';
			$datatable.= '[' . $usedate . ',' . $chartdata[$id] . '],';
			$d++;
		}
		$datatable = rtrim($datatable, ',');
		$output.= "var data = google.visualization.arrayToDataTable([
" . $datatable . "
        ]);


        var options = {
        theme: 'maximized',
        chartArea: {width: '95%', height: '95%'},
        isStacked: true,
        hAxis: {
          title: 'Date',
          format: 'd/M/yy'
        },
        vAxis: {
          title: 'Number of reports'
        }
      };

      var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
      chart.draw(data, options);
}
      </script>
      <div id=\"chart_div\" style=\"height: 300px;\"> </div>";
		$output.= '<p>Security have supplied <strong>' . $total_incidents . '</strong> verified reports at all locations since August 2015.</p>

';
		return $output;
	}
	public function SecurityBuildingReport($building_number = 2702) {
		global $connectionmanager;
		global $dbmanager;
		// get building data
		$building = $this->get_location_id($building_number, 1);
		// get simple stats data
		$data = mysqli_query($connectionmanager->connection, "SELECT account, Year(date) as year, Month(date) as month, SUM(users) as sum FROM socialstats WHERE network = 'secreport' AND account = $building_number AND Year(date) = " . date('Y') . " AND (Month(date) = " . date('n', strtotime('0 month')) . " OR Month(date) = " . date('n', strtotime('-1 month')) . " OR Month(date) = " . date('n', strtotime('-2 month')) . ") GROUP BY year, month , account ORDER BY date ASC

				");
		// build little graph and trend description
		$sparkdata = '';
		$months = Array();
		$days = Array();
		$n = 0;
		while ($row = mysqli_fetch_assoc($data)) {
			if ($n == 0) {
				$sparkdata.= $row['sum'];
			} else {
				$sparkdata.= ',' . $row['sum'];
			}
			$months[] = $row['sum'];
			$days[] = cal_days_in_month(CAL_GREGORIAN, $row['month'], $row['year']);
			$n++;
		}
		$trend = ($months[1] + $months[0]) / ($days[1] + $days[0]); //average per day issues
		$todays_day = date('n');
		$trend_text = '';
		if (count($months) < 3) {
			$trend_text = 'Your building hasn\'t had very many issues over the last few months. Please keep an eye on these issues to make sure that doesn\'t change';
		} else {
			//echo $trend.' - '. ($months[2] / $todays_day);
			if ($trend < ($months[2] / $todays_day)) {
				$trend_text = 'That\'s a <strong>' . round($trend / ($months[2] / $todays_day), 2) * 100 . '% increase in issues</strong> compared to the last 2 months.';
			} else {
				$trend_text = 'That\'s a <strong>' . round(($months[2] / $todays_day) / $trend, 2) * 100 . '% decrease in issues</strong> compared to the last 2 months.';
			}
		}
		// build more detailed report list
		$data = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item = " . mysqli_real_escape_string($connectionmanager->connection, $building['id']) . " AND note_text LIKE '%Security\'s incident number%' AND note_date > " . strtotime('-1 week'));
		$notes = Array();
		while ($row = mysqli_fetch_assoc($data)) {
			$notes[] = $row;
		}
		$weekly_trend = '';
		if (count($notes) > ($trend * 7)) {
			$weekly_trend = 'That\'s higher than average.';
		} elseif (count($notes) < ($trend * 7)) {
			$weekly_trend = 'That\'s lower than average.';
		} else {
			$weekly_trend = 'That\'s about the same as usual.';
		}
		// get details of energy coordinators in this building
		$project_id = Array(30285); //energy coords project id
		$contacts = Array();
		foreach ($project_id as $id) {
			$coordinators = $dbmanager->Links($id, 1, $building['id'], 2);
			foreach ($coordinators as $coordinator) {
				$location = $dbmanager->Query(array('id', 'contact_first', 'contact_last', 'contact_email'), array(array('id' => $coordinator)), null, 0, 0);
				foreach ($location['result'] as $the_id => $result) {
					$contacts[$the_id] = $result;
				}
			}
		}
		$report = '
		<div style="font-family: sans-serif;">
		<h3>' . $building['name'] . '</h3>
		<p>Dear [contact_first],</p>
		<p><i>Weekly energy saving report for the 7 days ending ' . date('d M Y') . '</i></p>

		<p>We\'re sending you this summary because you are listed as a stakeholder or energy coordinator for this building. If that\'s not correct, please get in touch and we will update our records.</p>

		<p>This summary is based upon routine security checks of almost every University building. When lights or equipment are left switched on, or when windows and doors are left open, an action is taken (e.g. lights switched off) and a record is kept.</p>

		<p>These checks have happened for years, but these weekly summary reports allow us to let you know about these issues for the first time in the hope that we can help you to address them.</p>

		<h3>Monthly summary</h3>
		<ul>
		<li>So far this month there have been <strong>' . $months[(count($months) - 1) ] . ' issues</strong>.</li>
		<li style="margin-top: 10px;">' . $trend_text . '</li>
		</ul>';
		// removed graph - <img src="./sparkline.php?size=25x40&data='.$sparkdata.'" style="vertical-align: middle; margin-right: 10px;"/>
		$report.= '<h3>This week</h3>

		<ul>

		<li>There were <strong>' . count($notes) . ' issues this week</strong>. ' . $weekly_trend . '</li>

		</ul>';
		if (count($notes) > 0) {
			$report.= '

		<h4>They were...</h4>

		';
			foreach ($notes as $n => $note) {
				$report.= '<div style="border: 1px solid black; padding: 5px; margin: 5px;  width: 50%;">' . str_replace(Array('Security\'s i', 'Security have reported a sustainability issue here.'), Array('I', 'A sustainability issue has been reported here.'), parse_markdown($note['note_text'])) . '</div>';
			}
			$report.= '

		</div>
		';
		}
		echo "

				";
		return Array('report' => $report, 'contacts' => $contacts, 'subject' => 'Switch energy report for ' . $building['name']);
	}
	public function EmailWeeklyReports() {
		global $connectionmanager;
		global $dbmanager;
		$user_name = Users::GetCurrentUser()['name'];
		$user_email = Users::GetCurrentUser()['email'];
		$output = '';
		// get list of location ids that have reports this week
		$data = mysqli_query($connectionmanager->connection, "SELECT account FROM socialstats WHERE network = 'secreport' AND date between date_sub(now(),INTERVAL 1 WEEK) and now() GROUP BY account

		");
		// loop through them and send the reports
		while ($row = mysqli_fetch_assoc($data)) {
			$report = $this->SecurityBuildingReport($row['account']);
			$vars = $report['contacts'];
			$message = $report['report'];
			$subject = $report['subject'];
			// send message
			$newpost = new Post;
			foreach ($vars as $the_id => $var) {
				$result = $newpost->Email(trim(stripslashes($var['contact_email'])), array('text' => str_replace('[contact_first]', $var['contact_first'], $message), 'subject' => $subject));
			}
		}
		$output = '1';
		return $output;
	}
}
?>
