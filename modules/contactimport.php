<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   Copyright 2014 Left Join Ltd.
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Performs database update operations at POST request
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class ContactImport extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('permission' => 'permission level');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetImport' => 'get fileupload interface', 'Import' => 'process csv file');
	//Object functions and variables go here
	
	/**
	 * Process POST data
	 *
	 * @return ID Returns id on insert/update, 1 on sucessful delete
	 */
	public function Import() {
		if (!empty($_FILES['csv'])) {
			//include("modules/activedirectory.php");
			$ad = new ActiveDirectory();
			$post = new Post();
			$handle = fopen($_FILES['csv']['tmp_name'], "r");
			$row = 0;
			$fields = array();
			global $dbmanager;
			while ($data = fgetcsv($handle)) {
				// first process array keys
				if ($row == 0) {
					foreach ($data as $key => $value) {
						$fields[$key] = $value;
						if ($value == 'contact_email') $emailfield = $key;
					}
				} else {
					$_POST = array();
					$mail = $ad->SetPOST(array('mail' => $data[$emailfield]));
					$check = $dbmanager->Query(array('id'), array(array('contact_email' => $mail['contact_email'])), null, 0, 0);
					if ($check['result_count'] > 0) {
						foreach ($check['result'] as $result) $id = $result['id'];
						$_POST['table'] = 'items';
						$_POST['category'] = 1;
						$_POST['id'] = $id;
					} else {
						$_POST = $mail;
					}
					foreach ($data as $key => $value) {
						$field = $fields[$key];
						if ($field != 'contact_email') $_POST[$field] = $value;
					}
					//print_r($_POST);
					$post->CheckPost();
				}
				$row++;
			}
			$_SESSION['msg'] = Array('type' => 'info', 'message' => 'processed ' . $row . ' contacts');
		}
	}
	public function GetImport() {
		global $page;
		$output = '
		 <h3>Upload your spreadsheet</h3>
		 <p>Please use the CSV format - <a href="http://www.cerebro.org.uk/sample-export.csv">download a sample</a>.</p>
		 
		 <form name="0" enctype="multipart/form-data" action="?page=' . $page . '" method="POST">			 
		 
			<div class="row form_display">
				<input type="hidden" id="import_mode" name="import_mode" value="submitted_file" />
				<div class="row">
					<input type="file" name="csv" id="csv">
				</div>
				<div class="row">
					<input type="submit" name="submit" value="Import" />
				</div>
			</div>
		 
		 </form>
		
		';
		return $output;
	}
}
?>