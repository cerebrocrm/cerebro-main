<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-15 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Generates data input form
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class Hire extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('db' => 'DBManager of linked item', 'listgen' => 'Listgen to call for UserList function');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('HireSummary' => 'Returns hire form', 'HireStatus' => 'Current status of hire item', 'SignInForm' => 'Sign in an item', 'SignOutForm' => 'Sign out an item', 'IssueForm' => 'Register an issue for a particular bike', 'UserList' => 'Re-purpose a list');
	//Object functions and variables go here
	public $preEmail_js = 'var preEmail = function (e) {

			var pre_email = $("#booking_uun_0").val();
			if (pre_email == \'\') {
				var pre_email = $("#booking_email_0").val();

			}


			var current_id = "";

			if (current_id > 0) {
				/* don\'t load db */
				return false;
			} else {
				/* get ajax */
				$.getJSON( "http://www.sustainability.ed.ac.uk/cerebro/?mail="+pre_email, function( data ) {


					var items = [];

					var status = data[\'status\'];

					if (status != \'left\') {

						if ( $("#booking_email_0").val(data[\'email\']) != \'\') {
							$("#booking_email_0").val(data[\'email\']);
							$("label[for=\'contact_email_0\']").children(".form_label_text").hide();
						}
						if ( $("#booking_contact_title_0").val(data[\'title\']) != \'\') {
							$("#booking_contact_title_0").val(data[\'title\']);

						}

						if ( $("#booking_contact_org_0").val(data[\'org\']) != \'\') {
							$("#booking_contact_org_0").val(data[\'org\']);

						}
						/* choose type */

						if (data[\'title\'] != \'\') {
							var title = data[\'title\'];
						} else {
							var title = \'\';
						}
						var type = data[\'detail_type\'];
						if (type == "Regular staff") {

							if (title.indexOf("Lectur") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Prof") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Principal") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Chair") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Teach") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Research") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Fellow") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Lab Manager") > -1) {
								$("#booking_contact_type_0").val(\'2\');
							} else if (title.indexOf("Technic") > -1) {
								$("#booking_contact_type_0").val(\'2\');
							} else {
								$("#booking_contact_type_0").val(\'3\');
							}

						} else if (type == "Staff visitor") {
							if (title.indexOf("Lectur") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Prof") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Principal") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Chair") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Teach") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Research") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Fellow") > -1) {
								$("#booking_contact_type_0").val(\'1\');
							} else if (title.indexOf("Lab Manager") > -1) {
								$("#booking_contact_type_0").val(\'2\');
							} else if (title.indexOf("Technic") > -1) {
								$("#booking_contact_type_0").val(\'2\');
							} else {
								$("#booking_contact_type_0").val(\'3\');
							}
						} else if (type == "Postgraduate student") {
							$("#booking_contact_type_0").val(\'4\');
						} else {
							alert("Students are not permitted to use this service!");
						}
						$("#booking_contact_type_0").trigger("chosen:updated");

						/* name */
						var fullName = data[\'name\'];
						var contact_first = fullName.split(\' \').slice(0, -1).join(\' \');
						var contact_last = fullName.split(\' \').slice(-1).join(\' \');

						$("#booking_first_name_0").val(contact_first);
						$("label[for=\'booking_first_name_0\']").children(".form_label_text").hide();
						$("#booking_last_name_0").val(contact_last);
						$("label[for=\'booking_last_name_0\']").children(".form_label_text").hide();

						if ($("#position_div").children().length < 4) {
							$("#position_div").append(\' <div class="five columns validation meh"><p>Did we get these right?</p></div>	\');
						}

						$("label[for=\'booking_uun_0\']").children(".form_label_text").hide();
						$("label[for=\'booking_email_0\']").children(".form_label_text").hide();


					} else {
						$("#booking_email_0").val(pre_email);
						$("label[for=\'booking_email_0\']").children(".form_label_text").hide();
					}



				});
			}
		}

		$("#booking_uun_0").blur(preEmail);
		$("#booking_email_0").blur(preEmail);
	';
	function Settings() {
		// global settings for the workbook module
		return array('items_table' => 'bikes', 'bookings_table' => 'bookings', 'name_singular' => 'bike', 'name_plural' => 'bikes', 'manager_column' => 'bike_manager', 'location_column' => 'bike_location');
	}
	function humanTiming($time) {
		$time = time() - $time - 3600; // to get the time since that moment
		$tokens = array(31536000 => 'year', 2592000 => 'month', 604800 => 'week', 86400 => 'day', 3600 => 'hour', 60 => 'minute', 1 => 'second');
		foreach ($tokens as $unit => $text) {
			if ($time < $unit) continue;
			$numberOfUnits = floor($time / $unit);
			return $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '') . ' ago';
		}
	}
	/**
	 * Generates summary of hire item
	 *
	 * @return Content Returns html/scripting representation of form
	 */
	public function HireSummary($vehicle_id = null) {
		global $page;
		global $connectionmanager;
		$output = '';
		// get settings
		$settings = $this->Settings();
		if ($vehicle_id == null) {
			$vehicle_id = mysqli_real_escape_string($connectionmanager->connection, $_GET['id']);
		} else {
			$vehicle_id = mysqli_real_escape_string($connectionmanager->connection, $vehicle_id);
		}
		$hire_status = $this->GetHireStatus($vehicle_id);
		$data = mysqli_query($connectionmanager->connection, "
		SELECT
		" . $settings['name_singular'] . '_id' . ',' . $settings['name_singular'] . '_name' . ',' . $settings['manager_column'] . ',' . $settings['location_column'] . " FROM " . $settings['items_table'] . " WHERE " . $settings['name_singular'] . '_id = \'' . mysqli_real_escape_string($connectionmanager->connection, $vehicle_id) . "' LIMIT 1");
		$stats = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT
		SUM(booking_distance), COUNT(*) FROM `bookings` WHERE booking_status = 2 AND booking_" . $settings['name_singular'] . " = $vehicle_id"));
		while ($row = mysqli_fetch_array($data)) {
			$output.= '<div class="row top_part"><div class="four columns alpha"><h2>' . stripslashes($row[$settings['name_singular'] . '_name']) . '</h2>
			</div>';
			$output.= '<div class="six columns omega action_bar">';
			// switch option by current status
			if ($hire_status['booking_status'] == '1') {
				$output.= '<a href="?page=sign-out&amp;booking_bike_0=' . $vehicle_id . '" class="button hire_signout"><img src="images/status_icon.png" alt="Sign out" title="Sign out" class="solo"/>Sign out</a>';
			} elseif ($hire_status['booking_status'] == '2') {
				$output.= '<a href="?page=sign-in&amp;booking_bike_0=' . $vehicle_id . '" class="button hire_signin"><img src="images/status_icon.png" alt="Sign in" title="Sign in"/>Sign in</a>';
			} else {
				if (Users::GetCurrentUser()['user_level'] == 1 || Users::GetCurrentUser()['user_level'] == 2) {
					$output.= '<a href="?page=sign-in&amp;booking_bike_0=' . $vehicle_id . '" class="button hire_signin"><img src="images/issue_icon.png" alt="Issue reported" title="Issue reported"/>Make bike available</a>';
				} else {
					$output.= '<a href="#" class="button disabled"><img src="images/issue_icon.png" alt="Issue reported" title="Issue reported"/>Issue reported</a>';
				}
			}
			if (Users::GetCurrentUser()['user_level'] === '1') {
				$output.= '
				<a href="?page=' . $settings['name_singular'] . '-add-edit&id=' . $vehicle_id . '" class="button"><img src="images/edit_icon.png" alt="Edit" title="Edit" class="solo"/></a><a href="#" class="button"><img src="images/delete_icon.png" class="solo delete_item" alt="Delete" title="Delete"/></a>';
			}
			$output.= '</div></div>';
			$output.= '<div class="ten columns alpha summary" >';
			$output.= '<p class="contact_location"><img src="images/location_icon.png" alt="Location:" title="Location" />' . $hire_status['human_summary'] . '</p>';
			$output.= '<h4>Accessories</h4>';
			$output.= '<p><img src="images/helmet.png" alt="Helmet:" title="Helmet" /> Helmet is <strong>' . $hire_status['human_helmet'] . '</strong></p>';
			$output.= '<p><img src="images/hv.png" alt="High visibility jacket:" title="High visibility jacket" /> HV jacket is <strong>' . $hire_status['human_hv'] . '</strong></p>';
			$output.= '<p><img src="images/pannier1.png" alt="Pannier1:" title="Pannier 1" /> Pannier 1 is <strong>' . $hire_status['human_pannier1'] . '</strong></p>';
			$output.= '<p><img src="images/pannier2.png" alt="Pannier 2:" title="Pannier 2" /> Pannier 2 is <strong>' . $hire_status['human_pannier2'] . '</strong><br /></p>';
			$output.= '<p>Travelled about ' . round($stats[0], 2) . ' km over ' . $stats[1] . ' journeys (' . round($stats[0] / $stats[1], 2) . ' km/journey)</p>';
			$output.= '</div>';
			$output.= '
			<script type="text/javascript">
				$( document ).ready(function() {
					$( ".delete_item" ).click(function (event) {
						var remove_prompt = confirm("Are you sure you wish to delete this item?");
						if (remove_prompt) {
							var dataString = "table=' . $settings['items_table'] . 'remove=1&firstval=' . $vehicle_id . '";
							$.ajax({
								type: "POST",
								url: "?page==post",
								data: dataString,
								 success: function () {
									window.location.assign("?page=' . $settings['name_plural'] . '");
								}
							});
						}
						event.preventDefault();
					});




					$(\'.hire_signout\').magnificPopup({
					  type: \'iframe\',
					  iframe: {
						 markup: \'<div class="white-popup sixteen columns">\'+
									\'<div class="mfp-close"></div>\'+
									\'<iframe class="mfp-iframe" frameborder="0" allowfullscreen style="width:100%; height: 100%; min-height: 40em;"></iframe>\'+\'</div>\'
					  }

					});

					$(\'.hire_signin\').magnificPopup({
					  type: \'iframe\',
					  iframe: {
						 markup: \'<div class="white-popup sixteen columns">\'+
									\'<div class="mfp-close"></div>\'+
									\'<iframe class="mfp-iframe" frameborder="0" allowfullscreen style="width:100%; height: 100%; min-height: 40em;"></iframe>\'+\'</div>\'
					  }

					});
				});
			</script>';
		}
		return $output;
	}
	/**
	 * Returns status of hire item
	 *
	 * @return Content Returns html/scripting representation of form
	 */
	public function HireStatus() {
		// SELECT * FROM `bookings` WHERE booking_bike = 1 ORDER BY booking_id DESC LIMIT 1
		global $page;
		global $connectionmanager;
		$output = '';
		// get settings
		$settings = $this->Settings();
		$data = $this->GetHireStatus($_GET['id']);
		print_r($data);
	}
	function GetHireStatus($id) {
		global $page;
		global $connectionmanager;
		$output = array();
		// get settings
		$settings = $this->Settings();
		$data = mysqli_query($connectionmanager->connection, "
		SELECT *,UNIX_TIMESTAMP(booking_time) as booking_unix FROM " . $settings['bookings_table'] . " WHERE booking_" . $settings['name_singular'] . ' = \'' . mysqli_real_escape_string($connectionmanager->connection, $id) . "' ORDER BY booking_id DESC LIMIT 1");
		$output = mysqli_fetch_array($data);
		// generate human readable summary
		$output['human_summary'] = '';
		if ($output['booking_status'] == 1) {
			$output['human_summary'] = 'Available';
			if ($output['booking_helmet_status'] == 1) {
				$output['human_helmet'] = 'in';
			} else {
				$output['human_helmet'] = 'unknown';
			}
			if ($output['booking_hv_status'] == 1) {
				$output['human_hv'] = 'in';
			} else {
				$output['human_hv'] = 'unknown';
			}
			if ($output['booking_pannier_status_1'] == 1) {
				$output['human_pannier1'] = 'in';
			} else {
				$output['human_pannier2'] = 'unknown';
			}
			if ($output['booking_pannier_status_2'] == 1) {
				$output['human_pannier2'] = 'in';
			} else {
				$output['human_pannier2'] = 'unknown';
			}
		} elseif ($output['booking_status'] == 2) {
			$output['human_summary'] = '<span title="' . $output['booking_time'] . '">Borrowed by <i>' . $output['booking_first_name'] . ' ' . $output['booking_last_name'] . '</i> ' . $this->humanTiming($output['booking_unix'] . '</span>');
			if ($output['booking_helmet_status'] == 1) {
				$output['human_helmet'] = 'out';
			} else {
				$output['human_helmet'] = 'in';
			}
			if ($output['booking_hv_status'] == 1) {
				$output['human_hv'] = 'out';
			} else {
				$output['human_hv'] = 'in';
			}
			if ($output['booking_pannier_status_1'] == 1) {
				$output['human_pannier1'] = 'out';
			} else {
				$output['human_pannier2'] = 'in';
			}
			if ($output['booking_pannier_status_2'] == 1) {
				$output['human_pannier2'] = 'out';
			} else {
				$output['human_pannier2'] = 'in';
			}
		} else {
			$output['human_summary'] = '<span title="' . $output['booking_time'] . '">Issue reported ' . $this->humanTiming($output['booking_unix'] . '</span>');
			if ($output['booking_helmet_status'] == 1) {
				$output['human_helmet'] = 'in';
			} else {
				$output['human_helmet'] = 'unknown';
			}
			if ($output['booking_hv_status'] == 1) {
				$output['human_hv'] = 'in';
			} else {
				$output['human_hv'] = 'unknown';
			}
			if ($output['booking_pannier_status_1'] == 1) {
				$output['human_pannier1'] = 'in';
			} else {
				$output['human_pannier2'] = 'unknown';
			}
			if ($output['booking_pannier_status_2'] == 1) {
				$output['human_pannier2'] = 'in';
			} else {
				$output['human_pannier2'] = 'unknown';
			}
		}
		// use over the last 30 days
		$stats = mysqli_query($connectionmanager->connection, "
		SELECT COUNT(*) FROM `bookings` WHERE booking_status = 1 AND (booking_time > DATE_SUB(now(), INTERVAL 30 DAY)) AND booking_" . $settings['name_singular'] . ' = \'' . mysqli_real_escape_string($connectionmanager->connection, $id) . "' ORDER BY booking_id DESC LIMIT 1");
		$stats = mysqli_fetch_row($stats);
		$output['stats_30d'] = $stats[0];
		return $output;
	}
	public function SignInForm() {
		global $page;
		global $connectionmanager;
		$name = explode(' ', Users::GetCurrentUser()['name']);
		$name_first = array_shift($name);
		$name_last = implode(' ', $name);
		$output = '';
		// get settings
		$settings = $this->Settings();
		// get last hire
		$data = $this->GetHireStatus($_GET['booking_bike_0']);
		if ($data['booking_status'] == '2') {
			$output.= '


			<script type="text/javascript">

			$(function () {

						' . $this->preEmail_js . '
						$("#check_toggle").click(function (event) {
							event.preventDefault();
							$(".icheck").prop("checked", true);
						});

						$("#submit").click(function (e) {



						if ($("#booking_battery_0").is(":checked")) {

							if ($("#booking_keys_0").is(":checked")) {

								var empty_flds = 0;
								  $(".767_req").each(function () {
									if (!$.trim($(this).val())) {
										empty_flds++;
									}
								  });
								  if (empty_flds != 0) {
									alert("Please complete all required fields");
								  } else {
								var booking_uun_0 = $("input#booking_uun_0").val();

								var booking_bike_0 = $("#booking_bike_0").val();


								var booking_helmet_status_0 = $("input#booking_helmet_status_0:checked").val();

								if (booking_helmet_status_0  == undefined) {
									var booking_helmet_status_0 =  $("#booking_helmet_status_0:hidden").val();
								}



								var booking_pannier_status_1_0 = $("input#booking_pannier_status_1_0:checked").val();

								if (booking_pannier_status_1_0 == undefined) {
									var booking_pannier_status_1_0 = $("#booking_pannier_status_1_0:hidden").val();
								}

								var booking_hv_status_0 = $("input#booking_hv_status_0:checked").val();

								if (booking_hv_status_0  == undefined) {
									var booking_hv_status_0 = $("#booking_hv_status_0:hidden").val();
								}

								var booking_pannier_status_2_0 = $("input#booking_pannier_status_2_0:checked").val();

								if (booking_pannier_status_2_0  == undefined) {
									var booking_pannier_status_2_0 =  $("#booking_pannier_status_2_0:hidden").val();
								}


								var booking_status_0 = $("#booking_status_0").val();
								var booking_time_0 = $("#booking_time_0").val();
								var booking_kept_overnight_0 = $("input#booking_kept_overnight_0:checked").val();
								var booking_first_name_0 = $("#booking_first_name_0").val();
								var booking_last_name_0 = $("#booking_last_name_0").val();


								var dataString = "table=bookings" + "&booking_pannier_status_1_0=" + booking_pannier_status_1_0 + "&booking_hv_status_0=" + booking_hv_status_0 + "&booking_helmet_status_0=" + booking_helmet_status_0 + "&booking_bike_0=" + booking_bike_0 + "&booking_pannier_status_2_0=" + booking_pannier_status_2_0+ "&booking_status_0=" + booking_status_0+"&booking_time_0=" + booking_time_0+ "&booking_kept_overnight_0=" + booking_kept_overnight_0+ "&booking_first_name_0=" + booking_first_name_0+ "&booking_last_name_0=" + booking_last_name_0;

								$.ajax({
									type: "POST",
									url: "?page=post",
									data: dataString,
									 success: function (result) {
										if (booking_status_0 == 3) {
											window.location.replace("?page=issue-add-edit-miniform&issue_bike_0="+booking_bike_0);

										} else {
											window.location.replace("done.html");
										}

									}
								});

								}

							} else {
								alert("They need to return the keys!");
							}

						} else {
						alert("They need to return the battery!");
						}
						e.preventDefault();
					 });

					});</script><div class="row form_display"><div class="row top_part">

					<div class="six columns alpha title_bar" >
						<h2>Sign in this bike</h2>
					</div>

				</div><p>Required fields are in <strong>bold.</strong></p>
						<form action="">

								<input type="hidden" id="booking_bike_0" name="booking_bike_0" value="' . $_GET['booking_bike_0'] . '" />

								<input type="hidden" id="booking_time_0" name="booking_time_0" value="' . date("Y-m-d H:i:s") . '">

								<input type="hidden" id="booking_first_name_0" name="booking_first_name_0" value="' . $name_first . '">

								<input type="hidden" id="booking_last_name_0" name="booking_last_name_0" value="' . $name_last . '">

								<div class="form_area even"><div class="row primary_item"><div class="">
									<h4>How\'s the bike?</h4>
								</div><div class="">
										<select id="booking_status_0" name="booking_status_0"><option value="NOT_SELECTED">Please select</option><option value="1" >OK for the next person to ride</option><option value="3" >Something needs to be fixed before it can be used again</option></select></div>

								</div>

								<div class="row primary_item">
								<div class=" checkbox" style="clear: both;">
									<label for="booking_kept_overnight_0" ><span class="form_label_text">Was the bike <strong>kept overnight</strong>?</label>
									<input name="booking_kept_overnight_0" type="checkbox" id="booking_kept_overnight_0" value="1" />
								</div>
								</div>

								</div>

								<div class="form_area"><div class="row primary_item"><div class="">
									<h4>Were all the accessories returned?</h4>
									<p>All of the items below were taken by ' . $data['booking_first_name'] . ' when they borrowed the bike.</p>
								</div>

								<div class="action_bar" style="float: right;">

								<a href="#" class="button" id="check_toggle">Check all</a>

								</div>



							<div class=" checkbox" style="clear: both;">
								<label for="booking_battery_0" ><span class="form_label_text">Battery returned</label>
								<input class="icheck" name="booking_battery_0" type="checkbox" id="booking_battery_0" value="1" />
							</div>

							<div class=" checkbox" style="clear: both;">
								<label for="booking_keys_0" ><span class="form_label_text">Keys returned</label>
								<input class="icheck" name="booking_keys_0" type="checkbox" id="booking_keys_0" value="1" />
							</div>


				';
			if ($data['booking_helmet_status'] == 1) {
				$output.= '<div class=" checkbox" style="clear: both;">
									<label for="booking_helmet_status_0" ><span class="form_label_text">Helmet returned</label>
									<input class="icheck" name="booking_helmet_status_0" type="checkbox" id="booking_helmet_status_0" value="1" />
								</div>';
			} else {
				$output.= '<input name="booking_helmet_status_0" type="hidden" id="booking_helmet_status_0" value="1" />';
			}
			if ($data['booking_hv_status'] == 1) {
				$output.= '<div class=" checkbox" style="clear: both;">
									<label for="booking_hv_status_0" ><span class="form_label_text">HV jacket returned</label>
									<input class="icheck" name="booking_hv_status_0" type="checkbox" id="booking_hv_status_0" value="1" />
								</div>';
			} else {
				$output.= '<input name="booking_hv_status_0" type="hidden" id="booking_hv_status_0" value="1" />';
			}
			if ($data['booking_pannier_status_1'] == 1) {
				$output.= '<div class=" checkbox" style="clear: both;">
									<label for="booking_pannier_status_1_0" ><span class="form_label_text">Pannier 1 returned</label>
									<input class="icheck" name="booking_pannier_status_1_0" type="checkbox" id="booking_pannier_status_1_0" value="1" />
								</div>';
			} else {
				$output.= '<input name="booking_pannier_status_1_0" type="hidden" id="booking_pannier_status_1_0" value="1" />';
			}
			if ($data['booking_pannier_status_2'] == 1) {
				$output.= '<div class=" checkbox" style="clear: both;">
									<label for="booking_pannier_status_2_0" ><span class="form_label_text">Pannier 2 returned</label>
									<input class="icheck" name="booking_pannier_status_2_0" type="checkbox" id="booking_pannier_status_2_0" value="1" />
								</div>';
			} else {
				$output.= '<input name="booking_pannier_status_2_0" type="hidden" id="booking_pannier_status_2_0" value="1" />';
			}
			$output.= '
								</div></div></div><div class="four columns omega action_bar">
						<a href="#" class="button" id="submit">Submit</a>
					</div></form></div>



			';
		} elseif ($data['booking_status'] == '3' || $data['booking_status'] == null) {
			$output.= '


			<script type="text/javascript">

			$(function () {

						' . $this->preEmail_js . '
						$("#check_toggle").click(function (event) {
							event.preventDefault();
							$("input:checkbox").prop("checked", true);
						});
						$("#submit").click(function (e) {
							var empty_flds = 0;
							  $(".767_req").each(function () {
								if (!$.trim($(this).val())) {
									empty_flds++;
								}
							  });
							  if (empty_flds != 0) {
								alert("Please complete all required fields");
							  } else {
							var booking_pannier_status_1_0 = $("input#booking_pannier_status_1_0:checked").val();
							var booking_hv_status_0 = $("input#booking_hv_status_0:checked").val();
							var booking_uun_0 = $("input#booking_uun_0").val();
							var booking_helmet_status_0 = $("input#booking_helmet_status_0:checked").val();
							var booking_bike_0 = $("#booking_bike_0").val();
							var booking_pannier_status_2_0 = $("input#booking_pannier_status_2_0:checked").val();
							var booking_status_0 = $("#booking_status_0").val();
							var booking_time_0 = $("#booking_time_0").val();
							var booking_first_name_0 = $("#booking_first_name_0").val();
							var booking_last_name_0 = $("#booking_last_name_0").val();



							var dataString = "table=bookings" + "&booking_pannier_status_1_0=" + booking_pannier_status_1_0 + "&booking_hv_status_0=" + booking_hv_status_0 + "&booking_helmet_status_0=" + booking_helmet_status_0 + "&booking_bike_0=" + booking_bike_0 + "&booking_pannier_status_2_0=" + booking_pannier_status_2_0+ "&booking_status_0=" + booking_status_0+"&booking_time_0=" + booking_time_0 + "&booking_first_name_0=" + booking_first_name_0+ "&booking_last_name_0=" + booking_last_name_0;

							$.ajax({
								type: "POST",
								url: "?page=post",
								data: dataString,
								 success: function (result) {


										window.location.replace("done.html");

								}
							});
							}
							e.preventDefault();
						});
					});</script><div class="row form_display"><div class="row top_part">

					<div class="six columns alpha title_bar" >
						<h2>Return this bike to service</h2>
						<p>By completing this form, you are putting a bike that had an issue back into service.</p>
					</div>

				</div><p>Required fields are in <strong>bold.</strong></p>
						<form action="">

								<input type="hidden" id="booking_bike_0" name="booking_bike_0" value="' . $_GET['booking_bike_0'] . '" />

								<input type="hidden" id="booking_time_0" name="booking_time_0" value="' . date("Y-m-d H:i:s") . '">

								<input type="hidden" id="booking_first_name_0" name="booking_first_name_0" value="' . $name_first . '">

								<input type="hidden" id="booking_last_name_0" name="booking_last_name_0" value="' . $name_last . '">


								<div class="form_area even"><div class="row primary_item"><div class="">
									<h4>How\'s the bike?</h4>
								</div><div class="">
										<select id="booking_status_0" name="booking_status_0"><option value="1" >OK for the next person to ride</option></select></div>

								</div></div><div class="form_area"><div class="row primary_item"><div class="">
									<h4>Are all the accessories available?</h4>

								</div>

								<div class="action_bar" style="float: right;">

								<a href="#" class="button" id="check_toggle">Check all</a>

								</div>


								<div class=" checkbox" style="clear: both;">
								<label for="booking_battery_0" ><span class="form_label_text">Battery returned</label>
								<input class="icheck" name="booking_battery_0" type="checkbox" id="booking_battery_0" value="1" />
								</div>

								<div class=" checkbox" style="clear: both;">
								<label for="booking_keys_0" ><span class="form_label_text">Keys returned</label>
								<input class="icheck" name="booking_keys_0" type="checkbox" id="booking_keys_0" value="1" />
							</div>

								';
			$output.= '<div class=" checkbox" style="clear: both;">
									<label for="booking_helmet_status_0" ><span class="form_label_text">Helmet returned</label>
									<input name="booking_helmet_status_0" type="checkbox" id="booking_helmet_status_0" value="1" />
								</div>';
			$output.= '<div class=" checkbox" style="clear: both;">
									<label for="booking_hv_status_0" ><span class="form_label_text">HV jacket returned</label>
									<input name="booking_hv_status_0" type="checkbox" id="booking_hv_status_0" value="1" />
								</div>';
			$output.= '<div class=" checkbox" style="clear: both;">
									<label for="booking_pannier_status_1_0" ><span class="form_label_text">Pannier 1 returned</label>
									<input name="booking_pannier_status_1_0" type="checkbox" id="booking_pannier_status_1_0" value="1" />
								</div>';
			$output.= '<div class=" checkbox" style="clear: both;">
									<label for="booking_pannier_status_2_0" ><span class="form_label_text">Pannier 2 returned</label>
									<input name="booking_pannier_status_2_0" type="checkbox" id="booking_pannier_status_2_0" value="1" />
								</div>';
			$output.= '
								</div></div></div><div class="four columns omega action_bar">
						<a href="#" class="button" id="submit">Submit</a>
					</div></form></div>



			';
		} else {
			$output.= '<h3>This bike has already been signed in</h3>
			<p>Please <a href="?page=sign-out&booking_bike_0=' . $_GET['booking_bike_0'] . '">sign it out</a> instead.</p>';
		}
		return $output;
	}
	public function SignOutForm() {
		global $page;
		global $connectionmanager;
		$output = '';
		// get settings
		$settings = $this->Settings();
		// get bike location
		$data = mysqli_query($connectionmanager->connection, "
		SELECT
		" . $settings['name_singular'] . '_id' . ',' . $settings['name_singular'] . '_name' . ',' . $settings['manager_column'] . ',' . $settings['location_column'] . " FROM " . $settings['items_table'] . " WHERE " . $settings['name_singular'] . '_id = \'' . mysqli_real_escape_string($connectionmanager->connection, $_GET['booking_bike_0']) . "' LIMIT 1");
		while ($row = mysqli_fetch_array($data)) {
			$location = $row[$settings['location_column']];
		}
		$output.= '

		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAt3Jt7Lh5JAtA4KNk6WHKDmKvjE-4NFhs&sensor=false&libraries=geometry">
		</script>

		<script type="text/javascript">

		//convert postcode to latlong
		function codeLatLng(address) {
			var geocoder;

			geocoder = new google.maps.Geocoder();


		}

        $(function () {

					' . $this->preEmail_js . '
					$("#check_toggle").click(function (event) {
						event.preventDefault();
						$("input:checkbox").prop("checked", true);
					});


					$("#submit").click(function (e) {

						if ($("#booking_battery_0").is(":checked")) {

							if ($("#booking_keys_0").is(":checked")) {
								var empty_flds = 0;
								  $(".767_req").each(function () {
									if (!$.trim($(this).val())) {
										empty_flds++;
									}
								  });
								  if (empty_flds != 0) {
								      alert("Please complete all required fields");
								  } else {
									var booking_pannier_status_1_0 = $("input#booking_pannier_status_1_0:checked").val();
									var booking_hv_status_0 = $("input#booking_hv_status_0:checked").val();
									var booking_uun_0 = $("input#booking_uun_0").val();
									var booking_email_0 = $("input#booking_email_0").val();
									var booking_first_name_0 = $("input#booking_first_name_0").val();
									var booking_last_name_0 = $("input#booking_last_name_0").val();
									var booking_destination_0 = $("#booking_destination_0").val();
									var booking_helmet_status_0 = $("input#booking_helmet_status_0:checked").val();
									var booking_bike_0 = $("#booking_bike_0").val();
									var booking_how_0 = $("#booking_how_0").val();
									var booking_pannier_status_2_0 = $("input#booking_pannier_status_2_0:checked").val();
									var booking_contact_org_0 = $("#booking_contact_org_0").val();
									var booking_contact_title_0 = $("#booking_contact_title_0").val();
									var booking_contact_type_0 = $("#booking_contact_type_0").val();
									var booking_status_0 = $("#booking_status_0").val();
									var booking_time_0 = $("#booking_time_0").val();
									var booking_distance_0 = $("#booking_distance_0").val();

									if (booking_how_0 != "NOT_SELECTED") {



										var dataString = "table=bookings" + "&booking_pannier_status_1_0=" + booking_pannier_status_1_0 + "&booking_hv_status_0=" + booking_hv_status_0 + "&booking_uun_0=" + booking_uun_0 + "&booking_email_0=" + booking_email_0 + "&booking_first_name_0=" + booking_first_name_0 + "&booking_last_name_0=" + booking_last_name_0 + "&booking_destination_0=" + booking_destination_0 + "&booking_helmet_status_0=" + booking_helmet_status_0 + "&booking_bike_0=" + booking_bike_0 + "&booking_how_0=" + booking_how_0 + "&booking_pannier_status_2_0=" + booking_pannier_status_2_0+ "&booking_status_0=" + booking_status_0+ "&booking_contact_org_0=" + encodeURIComponent(booking_contact_org_0)+ "&booking_contact_title_0=" + encodeURIComponent(booking_contact_title_0)+ "&booking_contact_type_0=" + booking_contact_type_0+"&booking_time_0=" + booking_time_0+"&booking_distance_0=" + booking_distance_0;

										$.ajax({
											type: "POST",
											url: "?page=post",
											data: dataString,
											 success: function (result) {
												window.location.replace("done.html");

											}
										});
									} else {
										alert("Please choose how they would have travelled otherwise");
									}
								  }
							} else {
								alert("They need to take the keys!");
							}
						} else {
							alert("They need to take the battery!");
						}
						e.preventDefault();
                    });


					$("#booking_destination_0").change(function() {

						var p1 = $("#start").val() + \', United Kingdom\';
						var p2 = $("#booking_destination_0 option:selected").val() + \', United Kingdom\';

						if (p2 != \'OTHER, United Kingdom\') {


							var geocoder;
							geocoder = new google.maps.Geocoder();



							geocoder.geocode( { \'address\': p1}, function(results, status) {
								if (status == google.maps.GeocoderStatus.OK) {
									var p1c = results[0].geometry.location;

									var geocoder2;
									geocoder2 = new google.maps.Geocoder();

									geocoder2.geocode( { \'address\': p2}, function(results, status) {
										if (status == google.maps.GeocoderStatus.OK) {
											var p2c = results[0].geometry.location;




											var directionsService;
											directionsService = new google.maps.DirectionsService();

											var request = {
												  origin: p1c,
												  destination: p2c,
												  travelMode: google.maps.TravelMode.WALKING
											  };
											directionsService.route(request, function(response, status) {
												if (status == google.maps.DirectionsStatus.OK) {
													  var distance_metres = response.routes[0].legs[0].distance.value;
													  var distance_km = ((distance_metres/1000)*2).toFixed(1);

													  $(".results").html(distance_km + \' km \');

													  $("#booking_distance_0").val(distance_km);

													  $(".map").html(\'<img src="http://maps.googleapis.com/maps/api/staticmap?center=\'+encodeURIComponent(p1)+\'&zoom=10&size=400x80&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:A%7C\'+encodeURIComponent(p1)+\'&markers=color:red%7Clabel:B%7C\'+encodeURIComponent(p2)+\'&sensor=false" alt="Map of your start and end points" />\');
													  $(".copyright").html(response.routes[0].copyrights);

												}
											});

										} else {
											alert("Geocode was not successful for the following reason: " + status);
										}
									});
								} else {
									alert("Geocode was not successful for the following reason: " + status);
								}
							});
						} else {
							$( "#bookdest" ).replaceWith( \'<input type="text" placeholder="Enter other destination" name="booking_destination_0" id="booking_destination_0"> <p><br /></p>\' );
						}

					});

                });</script><div class="row form_display"><div class="row top_part">

                <div class="six columns alpha title_bar" >
                    <h2>Sign out this bike</h2>
                </div>

            </div><p>Required fields are in <strong>bold.</strong></p>
                    <form action="">

                            <input type="hidden" id="booking_bike_0" name="booking_bike_0" value="' . $_GET['booking_bike_0'] . '" />

							<input type="hidden" id="start" name="start" value="' . $location . '" />

							<input type="hidden" id="booking_distance_0" name="booking_distance_0" value="0" />

							<input type="hidden" id="booking_status_0" name="booking_status_0" value="2" />

							<input type="hidden" id="booking_contact_type_0" name="booking_contact_type_0" value="" />

							<input type="hidden" id="booking_contact_org_0" name="booking_contact_org_0" value="" />

							<input type="hidden" id="booking_contact_title_0" name="booking_contact_title_0" value="" />

							<input type="hidden" id="booking_time_0" name="booking_time_0" value="' . date("Y-m-d H:i:s") . '">


							<div class="form_area even"><div class="row primary_item"><div class="">
                                <h4>About them</h4>
                            </div><div class="" id="booking_uun" style="width: 48%; float: left;">
                                <label for="booking_uun_0" class=" required"><span class="form_label_text">University user name</span> </label>
                                <input type="text" name="booking_uun_0" id="booking_uun_0"  class="767_req" value=""/>
                            </div>
                            <div class="" id="booking_email" style="width: 48%; float: right;">
                                <label for="booking_email_0" class=""><span class="form_label_text">Email</span> </label>
                                <input type="text" name="booking_email_0" id="booking_email_0"  class="" value=""/>
                            </div>
                            </div><div class="row primary_item"><div class="" id="booking_first_name" style="width: 48%; float: left;">
                                <label for="booking_first_name_0" class=""><span class="form_label_text">First name</span> </label>
                                <input type="text" name="booking_first_name_0" id="booking_first_name_0"  class="" value=""/>
                            </div>

							<div class="" id="booking_last_name" style="width: 48%; float: right;">
                                <label for="booking_last_name_0" class=""><span class="form_label_text">Last name</span> </label>
                                <input type="text" name="booking_last_name_0" id="booking_last_name_0"  class="" value=""/>
                            </div>
                            </div></div><div class="form_area"><div class="row primary_item"><div class="">
                                <h4 style="float:left;">About their journey</h4> <p style="float: right; padding-top: 6px;" class="results"> </p>
                            </div><div class="bookdest" id="bookdest">
                            <select id="booking_destination_0" name="booking_destination_0"><option value="NOT_SELECTED">Their destination</option>
							<option value="Alison House, EH8 9DF" >Alison House</option>
							<option value="Bristo Square, EH8 9AL" >Bristo Square</option>
							<option value="Buccleuch Place, EH8 9LJ" >Buccleuch Place</option>
							<option value="Chambers Street, EH1 1HT" >Chambers Street</option>
							<option value="Easter Bush, EH25 9RG" >Easter Bush (Roslin)</option>
							<option value="Edinburgh College of Art, EH3 9DF" >Edinburgh College of Art</option>
							<option value="George Square, EH8 9LH" >George Square</option>
							<option value="Holyrood, EH8 8AQ" >Holyrood (Moray House)</option>
							<option value="Infirmary Street, EH1 1LZ" >Infirmary Street</option>
							<option value="Kings Buildings, EH9 3JF" >Kings Buildings</option>
							<option value="Library Annexe, EH12 9EB" >Library Annexe</option>
							<option value="Pleasance (CSE and EUSA), EH8 9TJ" >Pleasance (CSE and EUSA)</option>
							<option value="Royal Edinburgh Hospital, EH10 5HF" >Royal Edinburgh Hospital</option>
							<option value="Western General, EH4 2XU" >Western General (IGMM)</option>
							<option value="OTHER" >Other</option></select>

							</div>


							<div class="" id="booking_how">

								 <select id="booking_how_0" name="booking_how_0"><option value="NOT_SELECTED">How would they have travelled otherwise?</option>
								 <option value="Walking" >Walking</option>
								 <option value="Bus" >Bus</option>
								 <option value="Taxi" >Taxi</option>
								 <option value="Train" >Train</option>
								 <option value="Private car" >Private car</option>
								 <option value="City Car Club" >City Car Club</option>
								 <option value="Cycling" >Cycling</option>
								 <option value="Tram" >Tram</option>
								 <option value="OTHER" >Other</option></select>
                            </div>




                            </div></div><div class="form_area even"><div class="row primary_item"><div class="">
                                <h4>What else are they borrowing?</h4>
                            </div>

							<div class="action_bar" style="float: right;">

							<a href="#" class="button" id="check_toggle">Check all</a>

							</div>

							<div class=" checkbox" style="clear: both;">
								<label for="booking_battery_0" ><span class="form_label_text">Battery </label>
								<input class="icheck" name="booking_battery_0" type="checkbox" id="booking_battery_0" value="1" />
							</div>

							<div class=" checkbox" style="clear: both;">
								<label for="booking_keys_0" ><span class="form_label_text">Keys</label>
								<input class="icheck" name="booking_keys_0" type="checkbox" id="booking_keys_0" value="1" />
							</div>

							<div class=" checkbox" style="clear: both;">
                                <label for="booking_helmet_status_0" ><span class="form_label_text">Helmet</label>
                                <input name="booking_helmet_status_0" type="checkbox" id="booking_helmet_status_0" value="1" />
                            </div><div class=" checkbox">
                                <label for="booking_hv_status_0" ><span class="form_label_text">HV jacket</label>
                                <input name="booking_hv_status_0" type="checkbox" id="booking_hv_status_0" value="1" />
                            </div><div class=" checkbox">
                                <label for="booking_pannier_status_1_0" ><span class="form_label_text">Pannier 1</label>
                                <input name="booking_pannier_status_1_0" type="checkbox" id="booking_pannier_status_1_0" value="1" />
                            </div><div class=" checkbox">
                                <label for="booking_pannier_status_2_0" ><span class="form_label_text">Pannier 2</label>
                                <input name="booking_pannier_status_2_0" type="checkbox" id="booking_pannier_status_2_0" value="1" />
                            </div></div></div><div class="four columns omega action_bar">

                    <a href="#" class="button" id="submit">Submit</a>

					<p class="map"> </p>
							<p class="copyright"> </p>
                </div></form></div>



		';
		return $output;
	}
	public function IssueForm() {
		global $connectionmanager;
		$settings = $this->Settings();
		$data = mysqli_query($connectionmanager->connection, "
		SELECT bike_manager,user_name FROM " . $settings['items_table'] . " LEFT JOIN users ON bike_manager = user_id WHERE " . $settings['name_singular'] . '_id = \'' . mysqli_real_escape_string($connectionmanager->connection, $_GET['issue_bike_0']) . "'  LIMIT 1");
		$row = mysqli_fetch_array($data);
		$output = '
		<script type="text/javascript">

        $(function () {
                    $("#submit").click(function (e) {
                        var empty_flds = 0;
                          $(".767_req").each(function () {
                            if (!$.trim($(this).val())) {
                                empty_flds++;
                            }
                          });
                          if (empty_flds != 0) {
                            alert("Please complete all required fields");
                          } else {
                        var issue_assigned_to_0 = $("#issue_assigned_to_0").val();
                        var issue_description_0 = $("input#issue_description_0").val();
                        var issue_name_0 = $("input#issue_name_0").val();
                        var issue_bike_0 = $("#issue_bike_0").val();
						var issue_status_0 = $("#issue_status_0").val();
                        var dataString = "table=issues" + "&issue_assigned_to_0=" + issue_assigned_to_0 + "&issue_description_0=" + issue_description_0 + "&issue_name_0=" + issue_name_0 + "&issue_bike_0=" + issue_bike_0+ "&issue_status_0=" + issue_status_0;

                        $.ajax({
                            type: "POST",
                            url: "?page=post",
                            data: dataString,
                             success: function (result) {
                                window.location.replace("done.html");

                            }
                        });
                        }
						e.preventDefault();
                    });
                });</script><div class="row form_display"><div class="row top_part">

                <div class="six columns alpha title_bar" >
                    <h2>Report an issue for this bike</h2>
                </div>

            </div><p>Required fields are in <strong>bold.</strong></p>
                    <form action="">

					 <input type="hidden" id="issue_bike_0" name="issue_bike_0" value="' . $_GET['issue_bike_0'] . '" />

					  <input type="hidden" id="issue_status_0" name="issue_status_0" value="1" />

					<div class="form_area even"><div class="row primary_item"><div class="">
                                <h4>What went wrong?</h4>
                            </div><div class="" id="issue_name">
                                <label for="issue_name_0" class=""><span class="form_label_text">Short description</span> </label>
                                <input type="text" name="issue_name_0" id="issue_name_0"  class="" value=""/>
                            </div>
                            <div class="" id="issue_description">
                                <label for="issue_description_0" class=""><span class="form_label_text">Full description</span> </label>
                                <input type="text" name="issue_description_0" id="issue_description_0"  class="" value=""/>
                            </div>
                            </div></div><div class="form_area"><div class="row primary_item"><div class="">
                                <h4>Who can fix it?</h4>
                            </div><div class="">
                            <select id="issue_assigned_to_0" name="issue_assigned_to_0">
							<option value="' . $row['bike_manager'] . '" >' . $row['user_name'] . '</option>
							<option value="60" >Transport Office</option></select></div></div></div><div class="four columns omega action_bar">
                    <a href="#" class="button" id="submit">Submit</a>
                </div></form></div>';
		return $output;
	}
	public function UserList() {
		global $connectionmanager;
		$conditional = ' WHERE 1 ';
		if (Users::GetCurrentUser()['user_level'] >= 2) {
			$conditional = ' WHERE bike_id = ' . $user[10] . ' OR bike_id = ' . $user[11]; // not sure where $user[10|11] are defined
			$stats = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT SUM(booking_distance), COUNT(*) FROM bookings WHERE booking_status = 2 AND (booking_bike = " . $user[10] . " OR booking_bike = " . $user[11] . ')'));
		} else {
			$stats = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT SUM(booking_distance), COUNT(*) FROM bookings WHERE booking_status = 2 "));
		}
		$query = 'SELECT * FROM bikes ' . $conditional;
		$DBmanager = $this->link('listgen');
		$db = new DBManagerRev2($DBmanager[0]);
		$data = $db->Query(null, null, null, null, null, $query);
		$output.= '
		 <h2>Bikes</h2>
		 <p>These bikes have travelled about ' . round($stats[0], 2) . ' km over ' . $stats[1] . ' journeys (' . round($stats[0] / $stats[1], 2) . ' km/journey)</p>

		 <ul id="sortable" style="margin-top: 0.5em !important;">';
		foreach ($data['result']['results'] as $row) {
			$hire_status = $this->GetHireStatus($row['bike_id']);
			if ($hire_status['booking_status'] == '1') {
				$output.= '<li class="five columns mainitem  help" style="min-height: 14em !important;">
            <div ><h3><a href="?page=bike-details&amp;id=' . $row['bike_id'] . '">' . $row['bike_name'] . '</a></h3></div>';
			} else {
				$output.= '<li class="five columns mainitem " style="min-height: 14em !important;">
				<div ><h3><a href="?page=bike-details&amp;id=' . $row['bike_id'] . '">' . $row['bike_name'] . '</a></h3></div>';
			}
			$output.= '<div class="summary" >';
			if ($hire_status['booking_status'] == '1') {
				$output.= '<p class="contact_location"><img src="images/location_icon.png" alt="Location:" title="Location" />Available at ' . $row['bike_location'] . '</p>';
			} else {
				$output.= '<p ><img src="images/location_icon.png" alt="Location:" title="Location" />' . $hire_status['human_summary'] . '</p>';
			}
			$output.= '</div>

			<div class="action_bar">
			<a href="?page=bike-details&amp;id=' . $row['bike_id'] . '" class="button">View details</a>
			</div>

		</li>';
		}
		if (Users::GetCurrentUser()['user_level'] == 1) {
			$output.= '

			<li class="five columns mainitem welcome">
			<div>

			<h3>Service statistics</h3>

			<p>Journeys on the eCycle scheme have offset the following journeys:</p>

			<ul class="square">

			';
			$stats2 = $db->Query(null, null, null, null, null, "SELECT booking_how, ROUND(SUM(booking_distance),1) as distance, COUNT(*) as total FROM bookings WHERE booking_status = 2 AND booking_how != 'undefined' AND booking_how != '' GROUP BY booking_how");
			foreach ($stats2['result']['results'] as $row) {
				$output.= '<li>' . $row['total'] . ' trips by ' . $row['booking_how'] . ' (' . $row['distance'] . ' km)</li>';
			}
			$output.= '</ul>';
			$stats3 = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT COUNT(*) as total FROM bookings WHERE booking_kept_overnight = 1"));
			$output.= '<p>Bikes have been kept overnight ' . $stats3[0] . ' times.</p>';
			$output.= '</div></li>';
		}
		$output . '</ul>';
		return $output;
	}
}
?>
