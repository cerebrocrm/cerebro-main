<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Groups module outputs in a tabbed view
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class TabGenRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('Tabs' => 'name of each tab', 'content' => 'tab content', 'count' => 'count value for each tab', 'default' => 'tab selected by default', 'category' => 'item category', 'vars' => 'extra link vars', 'functions' => 'extra dataset processing functions', 'header' => 'category header content', 'skeleton2_mode' => 'Set to 1 to use Skeleton 2 view', 'primary' => 'include primary links');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetTabs' => 'returns tabs', 'LinkTabs' => 'returns linked item tabs', 'LinkIDs' => 'returns linked IDs');
	//Object functions and variables go here
	var $ids;
	var $notes;
	var $items;
	var $order;
	var $tabs;
	//temp storage for functions
	var $functionstore;
	public function SetLinkIDs($item2 = null) {
		global $connectionmanager;
		global $item_id;
		$primary = $this->link('primary');
		$query2a = '';
		$query2b = '';
		if ($item2 != null) {
			$query2a = ' AND item2=' . mysqli_real_escape_string($connectionmanager->connection, $item2);
			$query2b = ' AND item1=' . mysqli_real_escape_string($connectionmanager->connection, $item2);
		}
		//Get all links
		if ($primary[0] == '') $query = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links LEFT JOIN global_notes on item_links.note_id = global_notes.note_id  WHERE ((item1 = '" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . "' " . $query2a . ") OR (item2 = '" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . "' " . $query2b . ")) AND primary_meta = 0 ORDER BY item_links.note_id DESC, link_date DESC");
		else $query = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links LEFT JOIN global_notes on item_links.note_id = global_notes.note_id  WHERE ((item1 = '" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . "' " . $query2a . ") OR (item2 = '" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . "' " . $query2b . ")) ORDER BY item_links.note_id DESC, link_date DESC");
		$notes = array();
		$itemcheck = array();
		//Sort links by item and pull out ids
		while ($row = mysqli_fetch_assoc($query)) {
			if ($row['item1'] == $item_id) {
				$notes[$row['item2']][$row['note_id']] = array($row);
				$itemcheck[] = array('id' => $row['item2']);
			} elseif ($row['item1']) {
				$notes[$row['item1']][$row['note_id']] = array($row);
				$itemcheck[] = array('id' => $row['item1']);
			}
		}
		$this->ids = $itemcheck;
		$this->notes = $notes;
	}
	public function LinkIDs() {
		if ($this->ids == null) $this->SetLinkIDs();
		return $this->ids;
	}
	public function LinkNotes() {
		global $connectionmanager;
		global $item_id;
		$this->SetLinkIDs($_GET['item2']);
		$notes = '';
		foreach ($this->notes[$_GET['item2']] as $subresult) {
			$reply = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_reply = " . mysqli_real_escape_string($connectionmanager->connection, $subresult[0]['note_id']));
			while ($row = mysqli_fetch_assoc($reply)) {
				$this->notes[$_GET['item2']][$row['note_reply']][] = $row;
			}
		}
		foreach ($this->notes[$_GET['item2']] as $subresult) {
			$notes.= NoteThread(array($subresult));
		}
		return $notes;
	}
	public function LinkData() {
		if ($this->ids == null) $this->SetLinkIDs();
		global $item_id;
		global $connectionmanager;
		global $dbmanager;
		global $categories;
		$order = array();
		$vars = $this->link('vars');
		$functions = $this->link('functions');
		//Get note threads
		$reply = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE (note_item = '" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . "' AND note_reply != 0)");
		while ($row = mysqli_fetch_assoc($reply)) {
			$this->notes[$row['note_item']][$row['note_reply']][] = $row;
		}
		$results = array();
		//Get item data for each id
		$data = $dbmanager->Query($vars, $this->ids, null, 0, $this->id);
		foreach ($data['result'] as $index => $val) {
			if (array_key_exists($val['category'], $categories)) {
				$order[$categories[$val['category']]]++;
				$results[$categories[$val['category']]][$index] = $val;
			}
		}
		$this->items = $results;
		$this->order = $order;
		//Run any functions on the dataset
		foreach ($functions as $function) {
			if ($function != null) {
				include 'functions/' . $function . '.php';
				$function($this->id);
			}
		}
	}
	public function FormatLinkData() {
		$this->LinkData();

		$vars = $this->link('vars');
		$this->tabs = array();
		global $item_id;
		$i = 0;
		$l = 0;
		foreach ($this->items as $categoryindex => $resultcategory) {

			$content = '<div class = "row notes_subtab notes_display linktabs">';
			$content.= '

		<div class="row">
            <div class="form-search">
              <input type="text" placeholder="Filter this list" class="form-control overview-filter" data-items="4" data-provide="typeahead" name="q" id="overview-filter">
			  <span id="searchclear" class="glyphicon glyphicon-remove-circle"></span>
			  <p> <br /></p>
            </div>
		</div>';
			foreach ($resultcategory as $resultindex => $result) {
				if ($i == 0) {
					$even = '';
					$i = 1;
				} else {
					$even = 'even';
					$i = 0;
				}
				$notes = '<div id="hidden_note_' . $l . '" class="note_hidden">';
				$k = $i;
				$attend = 0;
				$attendtype = 0;
				$text = '';
				$resname = $result['name'];
				foreach ($this->notes[$resultindex] as $subresult) {
					if ($subresult[0]['item1'] == $item_id) $resid = $subresult[0]['item2'];
					else $resid = $subresult[0]['item1'];
					$resname = $result['name'];
					$linkid = $subresult[0]['link_id'];
					if ($subresult[0]['note_text'] != '') {
						$text = substr(strip_tags(parse_markdown($subresult[0]['note_text'])), 0, 40) . '...';
						//$notes .= NoteThread(array($subresult));

					}
				}
				$notes.= '</div>';
				$content.= '<div class="row primary_item ' . $even . '" style="border-left: 7px solid #' . stringToColourCode($resname) . '">';
				$notes = str_replace('replacevar', stringToColourCode($resname), $notes);
				$content.= '<div class="three columns alpha">';
				$content.= '<p class="issue"><a href="?id=' . $resid . '">' . $resname . '</a></p>';
				$content.= '</div>';
				for ($j = 0;$j < count($vars);$j++) $content.= '<div class="one columns">' . $result[$vars[$j]] . '</div>';
				$content.= '<div class="four columns">';
				$content.= parse_markdown(stripslashes($text));
				if ($text != '') $content.= '</div><div class="four columns omega action_bar"> <a href="#" id="note_' . $l . '" class="button borderless open_thread_' . $this->id . '"><span class="fa fa-chevron-down" id="note_' . $l . '_b1" title="Open"></span><span class="fa fa-chevron-up" id="note_' . $l . '_b2" title="Open" style="display: none;"></span></a> ';
				else $content.= '</div><div class="four columns omega action_bar"> ';
				$content.= '<a href="#" class="button remove_link" id="' . $resid . '"><span class="fa fa-minus" title="Remove this item from the list"></span></a></div>';
				$l++;
				$content.= '</div>';
				$content.= $notes;
			}
			$content.= '</div>';
			$this->tabs[$categoryindex] = $content;
		}
	}
	public function GetTabs() {
		global $item_id;
		$params = $this->link('Tabs');
		$content = $this->link('content');
		$count = $this->link('count');
		$default = $this->link('default');
		$tab = 'tabs_' . $this->id;
		$output = '<div class="row info_section"><div class="ten columns omega sub_nav"><div class="tabs">';
		$script = '<script>';
		$script2 = '<script>';
		$contentoutput = '';
		$null = true;
		if ($this->tabs != null) $null = false;
		for ($i = 0;$i < count($params);$i++) {
			if ($params[$i] != '') {
				$this->tabs[$params[$i]] = $content[$i];
				$this->order[$params[$i]] = $count[$i];
				if ($count[$i] != '') $null = false;
			}
		}
		arsort($this->order);
		if ($default[0] == null) {
			foreach ($this->order as $index => $value) {
				$default[0] = $index;
				break;
			}
		}
		//Generate tab code
		foreach ($this->order as $category => $count) {
			$number = '';
			if ($null == false) $number = '<span class="big_number">' . $count . '</span>';
			if ($category == $default[0]) {
				$select = 'class="selected ' . $tab . '_button"';
				$script2.= '$(function () {
                    $( ".' . $tab . '" ).hide();
                    $( ".' . $category . '" ).show();
                });
                ';
			} else $select = 'class="' . $tab . '_button"';
			$script.= '$( "#' . $category . '_button" ).click(function (e) {
                            $( ".' . $tab . '_button" ).removeClass("selected");
                            $( ".' . $tab . '" ).hide();
                            $( "#' . $category . '_button" ).addClass("selected");
                            $( ".' . $category . '" ).show();
                            e.preventDefault();
                        });
                        ';
			$output.= '<a href="#" id= "' . $category . '_button" ' . $select . '>' . $number . $category . '</a>';
			$contentoutput.= '<div class = "' . $category . ' ' . $tab . '">' . $this->tabs[$category] . '</div>';
		}
		$output.= '</div></div></div>';
		$script.= '</script>';
		$script2.= '</script>';
		$script.= '
		<script>
						$( ".note_hidden" ).hide();
						var clickflag = 0;
						$( ".remove_link" ).click(function (e) {

								if (clickflag != 1){
									clickflag = 1;
									var remove_prompt = confirm("Are you sure you wish to remove this link?");
									if (remove_prompt) {
										var id = $(this).attr("id");
										var dataString = "remove=link&item1=' . $item_id . '&item2=" + id;
										$.ajax({
											type: "POST",
											url: "?page=post",
											data: dataString,
											 success: function (result) {
												location.reload();
											}
										});
									}

								}
								e.preventDefault();
                            });
							';
		$script.= '

						$(".overview-filter").keyup(function(e) {
							e.preventDefault();
							//$("#searchclear").show();
							var overview_search = $(this).val().toLowerCase();
							$( ".linktabs .row .issue" ).each(function( index ) {
								var check_string = $(this).text().toLowerCase();
								if (check_string.indexOf(overview_search) !== -1) {
									$(this).parent().parent().show();
								} else {
									$(this).parent().parent().hide();
								}

							});
						   });
						   ';
		$script.= '
		$( ".open_thread_' . $this->id . '" ).click(function (e) {
							$("#hidden_"+$(this).attr("id")).slideToggle();
							$("#hidden_"+$(this).attr("id")).html("<img src=\'images/loading.gif\' alt=\'Loading...\' /> Please wait");
							var selector = "#hidden_"+$(this).attr("id");
							var item2 = $( this ).next().attr("id");
							$.get( "?page=' . $_GET['page'] . '&id=' . $_GET['id'] . '&instance=' . $this->id . '&method=LinkNotes&item2="+item2, function( data ) {
							  $(selector).html(data);
							  $( ".note_reply_textbox" ).hide();
							});
							$("#"+$(this).attr("id")+"_b1").toggle();
							$("#"+$(this).attr("id")+"_b2").toggle();
                            e.preventDefault();
                    });
					</script>';
		return $output . $contentoutput . $script . $script2 . $this->functionstore['content'];
	}
	public function LinkTabs() {
		$skeleton2_mode = $this->link('skeleton2_mode');
		if ($skeleton2_mode[0] == '1') {
			$this->LinkData();
			$items = $this->items;
			if (count($items) > 0) {
				$data = array('results' => $items, 'vars' => $this->link('vars'), 'order' => $this->order);
				$view = file_get_contents('views/listtabs.html');
				$view = str_replace("<<data>>", json_encode($data), $view);
				$output.= $view;
				return $output;
			} else {
				return '<div class="row sixteen columns"><p><span class="fa fa-info-circle"></span> ' . T('There are no linked items here') . '</p></div>';
			}
		} else {
			$this->FormatLinkData();
			return $this->GetTabs();
		}
	}
}
?>
