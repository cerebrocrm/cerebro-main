<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Generates list of items of data
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class ListGenRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('category' => 'DB category', 'column' => 'column data', 'conditions' => 'extra conditions', 'order' => 'default list ordering', 'filter' => 'filter parameters', 'content' => 'header content', 'query' => 'freelist DB query (columns=name,friendly_name,css)', 'skeleton2_mode' => 'Set to 1 to use Skeleton 2 view');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetList' => 'Returns list', 'FreeList' => 'Non DBmanager based list', 'GetIDList' => 'Returns list of item IDs', 'GetCount' => 'Returns number of items in list', 'GetPrintButton' => 'Add a print button to the actionbar', 'adjacent_sidebar' => 'Sidebar adjacent items', 'GetHorizontalList' => 'Mini list for home pages');
	//Object functions and variables go here
	var $count;
	var $paginated;
	var $result;
	var $column;
	//friendly_name,css
	var $formatting;
	//friendly_name,options
	var $filter;
	//temp storage for functions
	var $functionstore;
	/**
	 * @return integer Count of items in the list
	 */
	public function GetCount() {
		//Run data fetch and return count
		$paginate = 10;
		$query = $this->link('query');
		if (isset($_GET['print'])) $paginate = 0;
		if ($query[0] == '') $this->GetData($paginate);
		else $this->FreeList();
		return $this->count;
	}
	/**
	 * @return string Formatted list assembled from custom query
	 */
	public function FreeList() {
		global $connectionmanager;
		global $item_id;
		$query = $this->link('query');
		$column = $this->link('column');
		//perform query
		$query = mysqli_real_escape_string($connectionmanager->connection, str_replace('item_id', $item_id, $query[0]));
		$data = mysqli_query($connectionmanager->connection, $query);
		$resultcount = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT FOUND_ROWS()"));
		//Set list variables
		$this->count = $resultcount['FOUND_ROWS()'];
		$formatting = array();
		$columns = array();
		$special = array();
		for ($i = 0;$i < count($column);$i++) {
			//name,friendly_name,css
			$item = explode(',', $column[$i]);
			$formatting[] = array('css' => $item[2], 'friendly_name' => $item[1]);
			$columns[] = $i;
			if (isset($item[3])) {
				// we have a page to link to [3] => page, [4] => id variable
				$special[$i] = array('page' => $item[3], 'id' => $item[4]);
			}
		}
		$this->column = $columns;
		$this->formatting = $formatting;
		//Format mysql result into correct array structure
		$result = array();
		while ($row = mysqli_fetch_assoc($data)) {
			$item = array();
			for ($i = 0;$i < count($column);$i++) {
				$param = explode(',', $column[$i]);
				if (isset($special[$i])) {
					$item[$i] = '<a href="?page=' . $special[$i]['page'] . '&amp;id=' . $row[$special[$i]['id']] . '">' . $row[$param[0]] . '</a>';
				} else {
					$item[$i] = $row[$param[0]];
				}
			}
			$result[] = $item;
		}
		$this->result = $result;
		return $this->FormatList(true);
	}
	/**
	 * @return string Formatted list assembled from standard query
	 */
	public function GetList() {
		//Run data fetch and return list
		$paginate = 10;
		if (isset($_GET['print'])) $paginate = 0;
		$this->GetData($paginate);
		return $this->FormatList();
	}
	/**
	 *Sets data variables in format required by FormatList
	 *
	 * @return void
	 */
	private function GetData($paginate) {
		if ($this->count != false && $this->paginated == $paginate) {
			//function previously executed - return result directly
			return $result;
		} else {
			global $dbmanager;
			global $item_id;
			$formatting = array();
			$filterdata = null;
			//Get variables to query DB for
			$column = $this->link('column');
			$conditions = $this->link('conditions');
			$order = $this->link('order');
			$category = $this->link('category');
			$filter = $this->link('filter');
			if ($filter[0] != null) {
				$listvars = array_values(array_unique(array_merge($column, $filter)));
				$filterdata = array();
			} else $listvars = $column;
			//Get field data and check for function data dependancies
			$functions = array();
			$fields = $dbmanager->Fields($listvars, $category[0]);
			for ($i = 0;$i < count($column);$i++) {
				$formatting[$i]['friendly_name'] = $fields[$column[$i]]['friendly_name'];
				if ($fields[$column[$i]]['type'] == 3) $formatting[$i]['css'] = 'one columns';
				else $formatting[$i]['css'] = 'two columns';
				if ($fields[$column[$i]]['data_format'] == 4) {
					//Add function data dependencies to variables list
					$functions[] = $column[$i];
					$dependencies = explode(',', $field['options']);
					foreach ($dependencies as $var) {
						if ($var != null) $listvars[] = $var;
					}
				}
			}
			if ($filter[0] != null) {
				foreach ($filter as $item) {
					$filterdata[$item]['friendly_name'] = $fields[$item]['friendly_name'];
					$filterdata[$item]['options'] = $fields[$item]['options'];
				}
			}
			//Get list of conditions and perform query
			$convars = array(array('category' => $category[0]));
			foreach ($conditions as $conindex => $conval) {
				if ($conval == 'item_id') $conval = $item_id;
				if ($conval != null) $convars[] = array($conindex => $conval);
			}
			$data = $dbmanager->Query($listvars, $convars, $order, $paginate, $this->id);
			$this->result = $data['result'];
			$this->count = $data['result_count'];
			$this->paginated = $paginate;
			$this->formatting = $formatting;
			$this->filter = $filterdata;
			$this->column = $column;
			//Run any functions on the dataset
			foreach ($functions as $function) {
				include_once 'functions/' . $function . '.php';
				$function($this->id);
			}
		}
	}
	/**
	 * @return array Array of ids in DBmanager condition format assembled from standard query
	 */
	public function GetIDList() {
		//Run data fetch and format result into correct array format
		$this->GetData(0);
		$ids = array();
		foreach ($this->result as $ind => $row) {
			$ids[] = array('id' => $ind);
		}
		return $ids;
	}
	/**
	 *
	 * @return string HTML link to print format list
	 */
	public function GetPrintButton() {
		global $page;
		$me = strlen($_SERVER['QUERY_STRING']) ? basename($_SERVER['PHP_SELF']) . "?" . $_SERVER['QUERY_STRING'] : basename($_SERVER['PHP_SELF']);
		$output.= '


					<a href="' . $me . '&print" class="button" target="_new"><img src="images/print.png" class="solo" title="Print this page" alt="Print this page"/></a>

				';
		return $output;
	}
	/**
	 *
	 * @return string Sidebar item containing previous/next items in list
	 */
	public function adjacent_sidebar() {
		if (isset($_COOKIE['listref'])) {
			$c = curl_init($_SERVER['HTTP_HOST'] . $_COOKIE['listref'] . '&listplace=' . $_COOKIE['listplace'] . '&method=adjacent&instance=' . $_COOKIE['listinst']);
			curl_setopt($c, CURLOPT_VERBOSE, 1);
			curl_setopt($c, CURLOPT_COOKIE, 'session=' . session_id() . ';');
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$page = curl_exec($c);
			curl_close($c);
			return '<div class="prev_next">' . $page . '</div>
			<script>
			$( ".adjacent_prev" ).click(function () {
						document.cookie="listplace=' . ($_COOKIE['listplace'] - 1) . '";
						document.cookie="listinst=' . $_COOKIE['listinst'] . '";
						document.cookie="listref=' . $_COOKIE['listref'] . '";
                    });
			$( ".adjacent_next" ).click(function () {
						document.cookie="listplace=' . ($_COOKIE['listplace'] + 1) . '";
						document.cookie="listinst=' . $_COOKIE['listinst'] . '";
						document.cookie="listref=' . $_COOKIE['listref'] . '";
                    });

			</script>
			';
		} else return '';
	}
	/**
	 *
	 * @return string HTML links to previous/next item in list
	 */
	public function adjacent() {
		if (isset($_GET['listplace'])) {
			$listplace = 0;
			$j = $_GET['listplace'];
			$output = '';
			if ($j != 0) {
				$img = '<img src="images/up.png" alt="previous"> ';
				$class = "adjacent_prev";
				$_GET[$this->id . '_place'] = $j - 1;
				$this->GetData(1);
				foreach ($this->result as $result) {
					$id = $result['id'];
					$name = $result['name'];
				}
				$output.= '<a class="' . $class . ' button" href="?id=' . $id . '">' . $img . $name . '</a></br>';
			}
			$class = "disabled";
			$img = '<img src="images/current.png"  alt="current"> ';
			$_GET[$this->id . '_place'] = $j;
			$this->GetData(1);
			foreach ($this->result as $result) {
				$id = $result['id'];
				$name = $result['name'];
			}
			$output.= '<a class="' . $class . ' button" href="?id=' . $id . '">' . $img . $name . '</a></br>';
			$img = '<img src="images/down.png"  alt="next"> ';
			$class = "adjacent_next";
			$_GET[$this->id . '_place'] = $j + 1;
			$this->GetData(1);
			foreach ($this->result as $result) {
				$id = $result['id'];
				$name = $result['name'];
			}
			$output.= '<a class="' . $class . ' button" href="?id=' . $id . '">' . $img . $name . '</a></br>';
			return $output;
		}
	}
	/**
	 * Switch that returns either Skeleton 1 or Skeleton 2 format lists (Skeleton 2 uses a better table format to display lists)
	 * @return string HTML/scripts constituting formatted list
	 */
	private function FormatList($force_no_pagination = false) {
		$content = $this->link('content');
		$skeleton2_mode = $this->link('skeleton2_mode');
		global $page;
		if ($this->count > 0) {
			if ($skeleton2_mode[0] == '1') {
				$output.= $this->FormatList_skeleton2();
			} else {
				$output.= $this->FormatList_skeleton1($force_no_pagination);
			}
			//Add scripts
			global $item_id;
			$output.= '<script type="text/javascript">';
			if (isset($_GET['id'])) $remdata = '"remove=link&item1=" + id + "&item2=' . $item_id . '";';
			else $remdata = '"remove=item&id=" + id;';
			$output.= '$( ".remove_' . $this->id . '" ).click(function (e) {
									var remove_prompt = confirm("Are you sure you wish to remove this item?");
									if (remove_prompt) {
										var id = $(this).attr("id");
										var dataString = ' . $remdata . '
										$.ajax({
											type: "POST",
											url: "?page=post",
											data: dataString,
											 success: function (result) {
												location.reload();
											}
										});
									}
									e.preventDefault();
								});';
			$output.= '
					$( ".listlink" ).click(function () {
							var linkplace = $(this).attr("id");
							document.cookie="listplace=" + linkplace;
							document.cookie="listinst=' . $this->id . '";
							document.cookie="listref=' . $_SERVER['REQUEST_URI'] . '";


						});
						$("#setplace").bind("enterKey",function (e) {
                            var newplace = $("#setplace").val() - 1;
                            if ($(this).parent().parent().attr("class") == "row magnific_content")
                            {
                            $( ".magnific_content" ).load( "?' . $this->id . '_place=" + newplace + "' . $gets . '&tab=' . $this->id . '", function() {
                                 $(".listcontrol").on("click",function(e) {
                                    e.preventDefault();
                                    $( ".magnific_content" ).load($(this).attr("href"),flag);
                                });
                            });
                            }
                            else
                            {

                                window.location.replace("?' . $this->id . '_place=" + newplace + "' . gets() . '&tab=' . $this->id . '");
                            }

                        });
                        $("#setplace").keyup(function (e) {
                            if (e.keyCode == 13) {
                                $(this).trigger("enterKey");
                            }
                        });



			';
			$output.= '</script>';
		} else {
			$output = 'No items in this list';
		}
		return $output;
	}
	/**
	 *
	 * @return string HTML/scripts constituting formatted list using Skeleton 2 formatting
	 */
	private function FormatList_skeleton2() {
		$content = $this->link('content');
		global $page;
		//Find current sorting, store other get parameters
		foreach ($_GET as $index => $value) {
			if ($index != $this->id . '_asc' && $index != $this->id . '_desc') {
				$gets.= '&' . $index . '=' . $value;
			} else {
				if ($index == $this->id . '_asc') {
					$get = $_GET[$this->id . '_asc'];
					$order = 'asc';
				} else {
					$get = $_GET[$this->id . '_desc'];
					$order = 'desc';
				}
			}
		}
		//Generate headings
		$numfields = count($this->column);
		$gets = null;
		$get = null;
		$order = null;
		$next = '_asc';
		$sort = 'no';
		$alt = 'Not sorted';
		foreach ($_GET as $index => $value) {
			if ($index != $this->id . '_asc' && $index != $this->id . '_desc') {
				$gets.= '&' . $index . '=' . $value;
			} else {
				if ($index == $this->id . '_asc') {
					$get = $_GET[$this->id . '_asc'];
					$order = 'asc';
				} else {
					$get = $_GET[$this->id . '_desc'];
					$order = 'desc';
				}
			}
		}
		$sorting = array();
		$i = 0;
		foreach ($this->formatting as $format) {
			$sorting[$i] = array('friendly_name' => $format['friendly_name'], 'column' => $this->column[$i],);
			if ($get == $sorting[$i]['column']) {
				$sorting[$i]['order'] = $order;
			} else {
				$sorting[$i]['order'] = 'no';
			}
			switch ($sorting[$i]['order']) {
				case 'asc':
					$sorting[$i]['alt'] = T('Sorted ascending');
					$sorting[$i]['next'] = '_desc';
				break;
				case 'desc':
					$sorting[$i]['alt'] = T('Sorted descending');
					$sorting[$i]['next'] = '_asc';
				break;
				case 'no':
					$sorting[$i]['alt'] = T('Not sorted');
					$sorting[$i]['next'] = '_asc';
				break;
			}
			$i++;
		}
		$fixed = array();
		foreach ($this->result as $index => $r) {
			$fixed[$index] = $r;
			$fixed[$index]['action_bar'] = '<a href="#" class="button lj_action remove_' . $this->id . '" id="' . $index . '"><span class="fa fa-minus fa-2x" aria-hidden="true"title="Remove"></a>';
		}
		$data = array('id' => $this->id, 'content' => $this->link('content'), 'column' => $this->column, 'formatting' => $sorting, 'result' => $fixed, 'gets' => $gets);
		$view = file_get_contents('views/list.html');
		$view = str_replace("<<data>>", json_encode($data), $view);
		if ($this->filter != null) {
			$output.= $this->GetFilter();
		}
		$output.= $view;
		return $output;
	}
	/**
	 *
	 * @return string HTML/scripts constituting formatted list using Skeleton 1 formatting
	 */
	private function FormatList_skeleton1($force_no_pagination = false) {
		$content = $this->link('content');
		global $page;
		$output = '';
		//add header content
		foreach ($content as $item) {
			$output.= $item;
		}
		if ($this->filter != null) {
			$output.= $this->GetFilter();
		}
		//Find current sorting, store other get parameters
		foreach ($_GET as $index => $value) {
			if ($index != $this->id . '_asc' && $index != $this->id . '_desc') {
				$gets.= '&' . $index . '=' . $value;
			} else {
				if ($index == $this->id . '_asc') {
					$get = $_GET[$this->id . '_asc'];
					$order = 'asc';
				} else {
					$get = $_GET[$this->id . '_desc'];
					$order = 'desc';
				}
			}
		}
		//Generate headings
		$numfields = count($this->column);
		$gets = null;
		$get = null;
		$order = null;
		$next = '_asc';
		$sort = 'no';
		$alt = 'Not sorted';
		foreach ($_GET as $index => $value) {
			if ($index != $this->id . '_asc' && $index != $this->id . '_desc') {
				$gets.= '&' . $index . '=' . $value;
			} else {
				if ($index == $this->id . '_asc') {
					$get = $_GET[$this->id . '_asc'];
					$order = 'asc';
				} else {
					$get = $_GET[$this->id . '_desc'];
					$order = 'desc';
				}
			}
		}
		$output.= '<div class="row list_headings">';
		for ($i = 0;$i < $numfields;$i++) {
			$append = '';
			if ($i == 0) $append = ' alpha ';
			elseif ($i == $numfields - 1) $append = ' omega ';
			if ($get == $this->column[$i]) {
				$sort = $order;
			} else {
				$sort = 'no';
			}
			switch ($sort) {
				case 'asc':
					$alt = 'Sorted ascending';
					$next = '_desc';
				break;
				case 'desc':
					$alt = 'Sorted descending';
					$next = '_asc';
				break;
				case 'no':
					$alt = 'Not sorted';
					$next = '_asc';
				break;
			}
			if ($this->column[$i] !== $i) $output.= '<div class="' . $this->formatting[$i]['css'] . $append . '">
                            <a class="listcontrol button borderless" href="?' . $this->id . $next . '=' . $this->column[$i] . $gets . '" class="button borderless"><img src="images/sort_' . $sort . '.png" alt="' . $alt . '" /> ' . $this->formatting[$i]['friendly_name'] . '</a>
                        </div>';
			//No sorting on columns with numerical indexes
			else $output.= '<div class="' . $this->formatting[$i]['css'] . $append . '"><strong>' . $this->formatting[$i]['friendly_name'] . '</strong></div>';
		}
		$output.= '</div>';
		//Generate list
		$listplace = 0;
		$j = 0;
		foreach ($this->result as $ind => $row) {
			if ($j == 0) {
				$even = '';
				$j = 1;
			} else {
				$even = 'even';
				$j = 0;
			}
			$output.= '<div class="row primary_item ' . $even . '"><form method="post" id = "form_' . $listplace . '"><input type= "hidden" name="listplace" value ="' . $listplace . '"><input type= "hidden" name="listref" value ="' . $_SERVER['REQUEST_URI'] . '">';
			for ($i = 0;$i < $numfields;$i++) {
				$append = '';
				if ($i == 0) $append = ' alpha ';
				elseif ($i == $numfields - 1) $append = ' omega ';
				$output.= '<div class="' . $this->formatting[$i]['css'] . $append . '">';
				$output.= $row[$this->column[$i]];
				$output.= '</div>';
			}
			$output.= '<div class="one columns omega action_bar"><a href="#" class="button remove_' . $this->id . '" id="' . $ind . '"><span class="fa fa-minus fa-2x" aria-hidden="true"title="Remove"></a></div>';
			$output.= "</form></div>";
			$listplace++;
		}
		//Add pagination
		if ($this->count > 10 && !isset($_GET['print']) && $force_no_pagination != true) {
			$gets = null;
			$number_pages = ceil($this->count / 10); // adding total page count -JF
			$pagenum = 0;
			foreach ($_GET as $index => $value) {
				if ($index != $this->id . '_place') {
					$gets.= '&' . $index . '=' . $value;
				} else {
					$pagenum = $value;
				}
			}
			$prev = $pagenum - 1;
			$next = $pagenum + 1;
			$output.= '<div class="pagination">';
			if ($pagenum != 0) { // page button is clickable or disabled depending on status, makes the UI neater
				$output.= '<a class="listcontrol" href="?' . $this->id . '_place=' . $prev . $gets . '&tab=' . $this->id . '">Previous </a>';
			} else {
				$output.= '<a class="disabled" href="#">Previous </a>';
			}
			$output.= '<input type="text" id="setplace" size="2" value ="' . $next . '"> of ' . $number_pages . ' ';
			if ($pagenum < ($this->count / 10) - 1) {
				$output.= '<a class="listcontrol" href="?' . $this->id . '_place=' . $next . $gets . '&tab=' . $this->id . '"> Next</a>';
			} else {
				$output.= '<a class="disabled" href="#"> Next</a>';
			}
			$output.= '</div>';
		}
		return $output;
	}
	/**
	 *
	 * @return string HTML/scripts constituting formatted list using Skeleton 2 formatting
	 */
	private function GetFilter() {
		global $page;
		//Add filter select
		$output.= '<div class="row"><select multiple style="width:150px;" id="filter_' . $this->id . '" data-placeholder="Filter this list">';
		$output.= '<option value = "none"></option>';
		if ($this->filter) {
			foreach ($this->filter as $filteritem => $filterdata) {
				$output.= '<optgroup label="' . $filterdata['friendly_name'] . '">';
				foreach ($filterdata['options'] as $optindex => $optionval) {
					$optselect = '';
					$k = 0;
					while (isset($_GET['field_' . $this->id . '_' . $k])) {
						if ($_GET['field_' . $this->id . '_' . $k] == $filteritem && $_GET['parama_' . $this->id . '_' . $k] == $optindex) $optselect = ' selected ';
						$k++;
					}
					$output.= '<option value = "' . $optindex . '"' . $optselect . '>' . $optionval . '</option>';
				}
				$output.= '</optgroup>';
			}
		}
		$output.= '</select>';
		$output.= '<a href="" id = "multiselect_' . $this->id . '" class="button action_bar">Filter</a></div>';
		//Add filter scripting
		$output.= '
	<style type="text/css">
		#filter_' . $this->id . ' {
			width: 66% !important;
			height: 40px;
		}

		#filter_' . $this->id . ' .chosen-drop {
			margin-top: 15px;
		}
	</style>
	';
		$output.= '<script type="text/javascript">';
		$output.= '
$( "#multiselect_' . $this->id . '" ).click(function(e) {
	var data = "?page=' . $page . '";
	var type = null;
	var i = 0;
	var andor = "OR";
	var cat = null;
	$("#filter_' . $this->id . '").find("option:selected").each(function(i, val){
		if ($(val).parent().attr("label") == cat)
		{
			andor = "OR";
		}
		else
		{
			andor = "AND";
		}
		cat = $(val).parent().attr("label");

		switch ($(val).parent().attr("label"))
		{
		';
		foreach ($this->filter as $filteritem => $filterdata) {
			$output.= '
			case "' . $filterdata['friendly_name'] . '":
			  type="' . $filteritem . '";
			  break;
			  ';
		}
		$output.= '
			}
			data = data + "&field_' . $this->id . '_"+i+"="+type+"&op_' . $this->id . '_"+i+"=equal&inf_' . $this->id . '_"+i+"=reg_' . $this->id . '&parama_' . $this->id . '_"+i+"=" + $(val).val() + "&and_' . $this->id . '_"+i+"=" + andor;
			i = i+1;
		});
		window.location.replace(data);
		e.preventDefault();
	});
	</script>';
		return $output;
	}
	/**
	 * @return string Formatted list assembled from standard query
	 */
	public function GetHorizontalList() {
		//Run data fetch and return list
		$paginate = 3;
		$this->GetData($paginate);
		$fixed = array();
		foreach ($this->result as $index => $row) {
			$fixed[$index] = $row;
			$fixed[$index]['fuzzy_time'] = fuzzy_time(trim($row['start_date']) . ' ' . trim($row['start_time']));
			$fixed[$index]['booked'] = 0; // replace with query
			$fixed[$index]['cancelled'] = 0; // replace ditto
			if (strtotime($row['start_date']) < time() && strtotime($row['end_date']) > time()) {
				$fixed[$index]['status'] = 'current';
			} elseif (strtotime($row['end_date']) < time()) {
				$fixed[$index]['status'] = 'past';
			} else {
				$fixed[$index]['status'] = 'future';
			}
		}
		$data = array('id' => $this->id, 'content' => $this->link('content'), 'column' => $this->column, 'formatting' => $this->formatting, 'result' => $fixed, 'booked' => T('booked'), 'cancelled' => T('cancelled'), 'new_event' => T('new event'));
		$view = file_get_contents('views/horizontal.html');
		$view = str_replace("<<data>>", json_encode($data), $view);
		$output.= $view;
		return $output;
	}
}
?>
