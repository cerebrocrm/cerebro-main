<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Provides various methods for generating simple content
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class ContentGenRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('content' => 'content to be stored', 'actionbar' => 'items to be wrapped in action bar', 'db' => 'The DBManager where information is stored', 'the_field' => 'The field which contains data to be displayed', 'the_id' => 'The id variable in the database', 'raw_content' => 'content to be stored as plain html without formatting');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetContent' => 'Returns content', 'GetActionBar' => 'Returns action bar', 'GetTitleByID' => 'Get the title according to the ID of the current page', 'GetEditButtonByID' => 'Get the edit button for the ID of the current page', 'GetRawContent' => 'Get plain text or html content for use in page elements etc');
	//Define any links that need special editors ('linkvar1' => 'editor_required' (e.g. wysiwyg, formeditor))
	public $special_editor = array('content' => 'wysiwyg');
	/**
	 * Wraps module outputs in an action bar
	 *
	 * @return Content Returns html action bar item
	 */
	public function GetActionBar() {
		$output.= '<div class="row">
        <div class="ten columns alpha omega action_bar">';
		$items = $this->link('actionbar');
		foreach ($items as $item) {
			$output.= $item;
		}
		$output.= '</div></div>';
		return $output;
	}
	/**
	 * Stores content for display
	 *
	 * @return Content Returns content
	 */
	public function GetContent() {
		$content = $this->link('content');
		return $content[0];
	}
	/**
	 * Stores raw content for display
	 *
	 * @return Content Returns content
	 */
	public function GetRawContent() {
		$content = $this->link('raw_content');
		return $content[0];
	}
	/**
	 * Generate a title for given database item
	 *
	 * @return Content Returns html title section
	 */
	public function GetTitleByID() {
		error_reporting(1);
		$output = '';
		$DBmanager = $this->link('db');
		//get db information
		global $connectionmanager;
		global $ {
			'module_' . $DBmanager[0]
		};
		$table = $ {
			'module_' . $DBmanager[0]
		}->GetTable();
		$the_field = $this->link('the_field');
		$the_id = $this->link('the_id');
		global $item_id;
		// Query($vars,$conditions,$format,$order,$paginate,$freetext) - we're using freetext
		$data = $ {
			'module_' . $DBmanager[0]
		}->Query(null, null, null, '', 1, 'SELECT ' . mysqli_real_escape_string($connectionmanager->connection, $the_id[0]) . ', ' . mysqli_real_escape_string($connectionmanager->connection, $the_field[0]) . ' FROM ' . $table . ' WHERE ' . mysqli_real_escape_string($connectionmanager->connection, $the_id[0]) . ' = ' . mysqli_real_escape_string($connectionmanager->connection, $item_id)); //no pagination for printing
		return '<div class="five columns alpha list_heading_float"><h2> ' . $data['result']['results'][0][$the_field[0]] . '</h2></div>';
	}
	/**
	 * Generate an edit button from the given database entry
	 *
	 * @return Content Returns html button
	 */
	public function GetEditButtonByID() {
		error_reporting(1);
		$output = '';
		global $item_id;
		$DBmanager = $this->link('db');
		global $ {
			'module_' . $DBmanager[0]
		};
		$table = $ {
			'module_' . $DBmanager[0]
		}->GetTable();
		return '<a href="/?page=' . rtrim($table, 's') . '-add-edit&id=' . $item_id . '" class="button"><img src="images/edit_icon.png" alt="Edit" title="Edit" class="solo" /></a>';
	}
}
?>
