<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Wraps content in a popup
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class PopupRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('type' => 'type', 'add' => 'add new item page', 'list' => 'list module this popup is connected to');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetPopup' => 'Returns popup content', 'GetButton' => 'Returns button');
	//Object functions and variables go here
	
	/**
	 * Generate popup
	 *
	 * @return Content Returns html/scripting representing popup content
	 */
	public function GetPopup() {
		global $item_id;
		global $page;
		$type = $this->link('type');
		$add = $this->link('add');
		$list = $this->link('list');
		$output = '<div id = "magnific_' . $this->id . '" class="white-popup mfp-hide">';
		$output.= '<div class="row">
                    <div class="ten columns omega action_bar">
                        <div class="search">
                            <input type="text" id="magnific_input_' . $this->id . '" placeholder="Search for ' . $type[0] . '"/>
                        </div>
                        <a href="?page=' . $add[0] . '" class="button"><img src="images/add_icon.png" alt="Add"/> New ' . $type[0] . '</a>
                        <a href="" class="button"><img src="images/close_icon.png" alt="Close"/> Close</a>
                    </div>
                    <input type="hidden" class="flag" value = "0">
                    <div class = "row magnific_content" style="clear:both;">

                    </div>
                </div>
                ';
		$output.= '</div>';
		$output.= '<script>
                    function flag() {
                      $(".listcontrol").on("click",function(e) { 
                                e.preventDefault();
                                $( ".magnific_content" ).load($(this).attr("href"),flag);
                            });
                    }
                    $("#magnific_input_' . $this->id . '").bind("enterKey",function (e) {
                        var magnificId = $(".magnific_content").attr("id");
                        var magnificSearch = $("#magnific_input_' . $this->id . '").val();
                        $( ".magnific_content" ).load( "?page=' . $page . '&instance=' . $list[0] . '&method=GetList&field_all_0=all&op_all_0=similar&inf_all_0=zend_0&parama_all_0=" + magnificSearch + "&id=' . $item_id . '", function() {
                             $(".listcontrol").on("click",function(e) { 
                                e.preventDefault();
                                $( ".magnific_content" ).load($(this).attr("href"),flag);
                            });
                        });
                        
                    });
                    $("#magnific_input_' . $this->id . '").keyup(function (e) {
                        if (e.keyCode == 13) {
                            $(this).trigger("enterKey");
                        }
                    });
                     $(document).ready(function () {
                    $( ".magnific_' . $this->id . '" ).click(function () {
                        var currentId = $(this).attr("id");
                         $(".magnific_content").attr("id", currentId)
                    });
                    $(".magnific_' . $this->id . '").magnificPopup({
                      type:"inline",
                      showCloseBtn: false,
                      midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don\'t provide alternative source in href.
                    });
                    
                });
                    </script>';
		return $output;
	}
	/**
	 * Generate link to popup
	 *
	 * @return Content Returns html link to popup content
	 */
	public function GetButton() {
		$type = $this->link('type');
		return '<a href="#magnific_' . $this->id . '" class="open-popup-link button magnific_' . $this->id . '"><img src="images/add_icon.png" class="solo" alt="Add ' . $type[0] . '"/>Add ' . $type[0] . '</a>';
	}
}
?>