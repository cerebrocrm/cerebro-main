<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Note add with mentions
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class Stats extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('ids' => 'ID list', 'nonchron' => 'Correlation stats without chronological ordering');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('NoteStats' => 'stats based upon note data', 'corellation_summary' => 'event participation correlation', 'ItemStats' => 'stats based upon item links', 'AttributeStats' => 'stats based upon item attributes', 'EventNetwork' => 'Network map of event attendees', 'ESAStats' => 'ESA specific stats', 'trend_summary' => 'Summary of trends in linked contacts');
	//Object functions and variables go here

	/**
	 * Generates note add box
	 *
	 * @return Data Returns JSON autocomplete data
	 */
	public function ESAStats() {
		global $dbmanager;
		global $connectionmanager;
		$group = array();
		$campus = array();
		$level = array(0, 0, 0, 0);
		$level14 = array(0, 0, 0, 0);
		$level15 = array(0, 0, 0, 0);
		$level_labs = array(0, 0, 0, 0);
		$level14_labs = array(0, 0, 0, 0);
		$level15_labs = array(0, 0, 0, 0);
		$staff = 0;
		$students = 0;
		$depts = simplexml_load_file('hierarchy.xml');
		$grouplist = array('Other');
		$campuslist = array(1 => 'Central area', 2 => 'King\'s buildings', 3 => 'Little France', 4 => 'Western General', 5 => 'Easter Bush', 6 => 'Student Accomodation');
		$checkarray = array();
		foreach ($depts->ROW as $dept) {
			$deptname = str_replace(' ', '', $dept->LEVEL2DESC);
			if (!(array_key_exists($deptname, $checkarray)) && $dept->LEVEL2DESC != 'Non-specific units') {
				$grouplist[] = "$dept->LEVEL2DESC";
				$checkarray["$deptname"] = 1;
			}
		}
		$clean_data = array();
		$data = $dbmanager->Query(array('level', 'group_school', 'campus', 'num_staff', 'num_students'), array(array('category' => 6)), null, 0, $this->id);
		foreach ($data['result'] as $result) {
			if (array_key_exists($grouplist[$result['group_school']], $group)) $group[$grouplist[$result['group_school']]]++;
			else $group[$grouplist[$result['group_school']]] = 1;
			if (array_key_exists($campuslist[$result['campus']], $campus)) $campus[$campuslist[$result['campus']]]++;
			else $campus[$campuslist[$result['campus']]] = 1;
			$teamlevel = str_split($result['level']);
			$level[$teamlevel[1]]++;
			$level_labs[$teamlevel[2]]++;
			if ($result['num_staff'] != '') $staff+= $result['num_staff'];
			if ($result['num_students'] != '') $students+= $result['num_students'];
			// team champion stats
			$members = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE item2=" . $result['id'] . " AND cat1= 1 AND cat2=6");
			$ids = array();
			while ($row = mysqli_fetch_assoc($members)) {
				$ids[] = array('id' => $row['item1']);
			}
			$members = $dbmanager->Query(array('name'), $ids, null, 0, $this->id);
			$idlist1 = '(';
			$idlist2 = '(';
			$i = 0;
			foreach ($ids as $id) {
				if ($i == 0) {
					$idlist1.= 'item1=' . $id['id'];
					$idlist2.= 'item2=' . $id['id'];
				} else {
					$idlist1.= ' OR item1=' . $id['id'];
					$idlist2.= ' OR item2=' . $id['id'];
				}
				$i++;
			}
			$idlist1.= ')';
			$idlist2.= ')';
			$i = 0;
			$current1 = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE (item2=30310 OR item2=30285 OR item2=30312) AND " . $idlist1 . " GROUP BY item1");
			$current2 = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE (item1=30310 OR item1=30285 OR item1=30312) AND " . $idlist2 . " GROUP BY item2");
			$result['num_champions'] = mysqli_num_rows($current1) + mysqli_num_rows($current2);
			// team member stats
			$members = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE item2=" . $result['id'] . " AND cat1= 1 AND cat2=6 AND link_level=1");
			$ids = array();
			while ($row = mysqli_fetch_assoc($members)) {
				$ids[] = array('id' => $row['item1']);
			}
			$members = $dbmanager->Query(array('name'), $ids, null, 0, $this->id);
			$result['num_members'] = $members['result_count'];
			$result['member_names'] = '';
			foreach ($members['result'] as $member) {
				$result['member_names'].= $member['name'] . ', ';
			}
			if ($result['num_staff'] != '' || $result['num_students'] != '' || $result['num_members'] > 1 || $result['num_champions'] > 0) {
				$clean_data[] = $result;
			}
		}
		$prev = mysqli_query($connectionmanager->connection, "SELECT * FROM esa_teamlevel");
		while ($row = mysqli_fetch_assoc($prev)) {
			if ($row['teamlevel_workbook'] == 2) $level14[$row['teamlevel_level']]++;
			elseif ($row['teamlevel_workbook'] == 5) $level15[$row['teamlevel_level']]++;
			elseif ($row['teamlevel_workbook'] == 3) $level14_labs[$row['teamlevel_level']]++;
			elseif ($row['teamlevel_workbook'] == 6) $level15_labs[$row['teamlevel_level']]++;
		}
		//echo $staff.' ';
		//echo $students;
		arsort($campus);
		arsort($group);
		$output = '<h2>Awards stats</h2>';
		$output.= '#Staff reached by awards this year: ' . $staff . '<br/>';
		$output.= '#Students reached by awards this year: ' . $students . '<br/><br/>';
		$output.= ' <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {"packages":["corechart","bar","table"]});
      google.charts.setOnLoadCallback(drawOffices);
      function drawOffices() {
        var data = google.visualization.arrayToDataTable([
          ["Year", "Bronze", "Silver", "Gold"],
          ["2014", ' . $level14[1] . ', ' . $level14[2] . ', ' . $level14[3] . '],
          ["2015", ' . $level15[1] . ', ' . $level15[2] . ', ' . $level15[3] . '],
          ["2016", ' . $level[1] . ', ' . $level[2] . ', ' . $level[3] . ']
        ]);

        var options = {
		colors: ["#af6a40", "#888f98", "#c69849"],
          chart: {
            title: "Teams by level (offices)",
          },
        };

        var chart = new google.charts.Bar(document.getElementById("teamlevels_office"));

        chart.draw(data, options);
      }
    </script>
	<div id="teamlevels_office" class="eight columns"></div>';
		$output.= '
    <script type="text/javascript">
      google.charts.setOnLoadCallback(drawLabs);
      function drawLabs() {
        var data = google.visualization.arrayToDataTable([
          ["Year", "Bronze", "Silver", "Gold"],
          ["2014", ' . $level14_labs[1] . ', ' . $level14_labs[2] . ', ' . $level14_labs[3] . '],
          ["2015", ' . $level15_labs[1] . ', ' . $level15_labs[2] . ', ' . $level15_labs[3] . '],
          ["2016", ' . $level_labs[1] . ', ' . $level_labs[2] . ', ' . $level_labs[3] . ']
        ]);

        var options = {
		colors: ["#af6a40", "#888f98", "#c69849"],
          chart: {
            title: "Teams by level (labs)",
          },
        };

        var chart = new google.charts.Bar(document.getElementById("teamlevels_labs"));

        chart.draw(data, options);
      }
    </script>
	<div id="teamlevels_labs" class="eight columns"></div>';
		$output.= '<script type="text/javascript">
    google.charts.setOnLoadCallback(drawGroup);
    function drawGroup() {
      var data = google.visualization.arrayToDataTable([
	  ["Group/School", "#" ],';
		foreach ($group as $index => $value) {
			if ($index != '') $output.= '["' . $index . '", ' . $value . '],';
		}
		$output.= '  ]);


      var options = {

		  title: "Teams by Group/School",
        legend: { position: "none" },
		vAxis: {textPosition: "in"}
      };
      var chart = new google.visualization.BarChart(document.getElementById("teams_group"));
      chart.draw(data, options);
  }
  </script>
<div id="teams_group" class="eight columns"></div>';
		$output.= '<script type="text/javascript">
    google.charts.setOnLoadCallback(drawCampus);
    function drawCampus() {
      var data = google.visualization.arrayToDataTable([
	  ["Campus", "#" ],';
		foreach ($campus as $index => $value) {
			if ($index != '') $output.= '["' . $index . '", ' . $value . '],';
		}
		$output.= '  ]);


      var options = {
        title: "Teams by Campus",
        legend: { position: "none" },
		vAxis: {textPosition: "in"}
      };
      var chart = new google.visualization.BarChart(document.getElementById("teams_campus"));
      chart.draw(data, options);
  }
  </script>
<div id="teams_campus" class="eight columns"></div>';
		$output.= '<script type="text/javascript">
    google.charts.setOnLoadCallback(teamsDetailed);
    function teamsDetailed() {
      var data = google.visualization.arrayToDataTable([
	  ["Team", "Students", "Staff", "Champions", "Members", "Member names"],';
		foreach ($clean_data as $index => $d) {
			if ($index != '') $output.= '["' . $d['name'] . '", ' . $d['num_students'] . ',' . $d['num_staff'] . ',' . $d['num_champions'] . ',' . $d['num_members'] . ',"' . $d['member_names'] . '"],';
		}
		$output.= '  ]);


      var options = {
        title: "Engagement by team",
      };
      var table = new google.visualization.Table(document.getElementById("teams_detailed"));
      table.draw(data, options);
  }
  </script>
<div id="teams_detailed" class="eight columns"></div>';
		return $output;
	}
	public function corellation_summary() {
		$events = $this->correlation_data();
		arsort($events);
		if (count($events) > 0) {
			$output = '<strong>Top 3 items linked with these contacts:</strong><br/><br/>';
			$k = 0;
			foreach ($events as $i => $v) {
				if ($k == 3) break;
				$output.= $i . ": " . $v . "<br/>";
				$k++;
			}
			$output.= '<a href="?page=' . $_GET['page'] . '&id=' . $_GET['id'] . '&instance=' . $this->id . '&method=correlation" class="open-popup-link button stats_popup"><span class="fa fa-chevron-down" title="Show extra information"></span> More</a>
			<script>$( document ).ready(function() {

				$(\'.stats_popup\').magnificPopup({
				  type: \'iframe\',
				  iframe: {
					 markup: \'<div class="white-popup sixteen columns" style="min-width: 80%;">\'+
								\'<div class="mfp-close"></div>\'+
								\'<iframe class="mfp-iframe" frameborder="0" allowfullscreen style="width:100%; height: 100%; min-height: 40em;"></iframe>\'+\'</div>\'
				  }

				});
			});
			</script>';
		}
		return $output;
	}
	public function correlation_data() {
		global $dbmanager;
		global $connectionmanager;
		$nonchron = $this->link('nonchron');
		$id = mysqli_real_escape_string($connectionmanager->connection, $_GET['id']);
		$category = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id=" . $id));
		$category = $category['category'];
		$contactlist = [];
		$events = [];
		$check = [];
		$contacts = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE (item1=" . $id . " AND cat2 =1) OR (item2=" . $id . " AND cat1 =1)");
		while ($row = mysqli_fetch_assoc($contacts)) {
			if ($row['cat2'] == 1) $switch = 'item2';
			else $switch = 'item1';
			$cid = $row[$switch];
			$contactlist[$cid] = $row['link_date'];
		}
		$string1 = '(';
		$string2 = '(';
		$k = 0;
		foreach ($contactlist as $i => $v) {
			if ($k == 0) {
				$string1.= ' item1=' . $i . ' ';
				$string2.= ' item2=' . $i . ' ';
			} else {
				$string1.= 'OR item1=' . $i . ' ';
				$string2.= 'OR item2=' . $i . ' ';
			}
			$k++;
		}
		$string1.= ')';
		$string2.= ')';
		if ($nonchron[0] != 1) $query = 'SELECT * FROM item_links WHERE (cat1=5 AND ' . $string2 . ') OR (cat2=5 AND ' . $string1 . ')';
		else $query = 'SELECT * FROM item_links WHERE (cat1!=1 AND cat1!=99 AND ' . $string2 . ') OR (cat2!=1 AND cat2!=99 AND ' . $string1 . ')';
		$events = mysqli_query($connectionmanager->connection, $query);
		$eventlist = [];
		while ($row = mysqli_fetch_assoc($events)) {
			if ($row['cat2'] != 1) {
				$switch = 'item2';
				$switch2 = 'item1';
			} else {
				$switch = 'item1';
				$switch2 = 'item2';
			}
			$eid = $row[$switch];
			if ($nonchron[0] != 1) {
				if (!(isset($_GET['after'])) || $_GET['after'] == 0) {
					if ($row['link_date'] <= $contactlist[$row[$switch2]]) {
						if (!(array_key_exists($row[$switch2], $check))) {
							$check[$row[$switch2]] = 1;
							if (!(array_key_exists($eid, $eventlist))) $eventlist[$eid] = 1;
							else $eventlist[$eid]++;
						}
					}
				} else {
					if ($row['link_date'] > $contactlist[$row[$switch2]]) {
						if (!(array_key_exists($row[$switch2], $check))) {
							$check[$row[$switch2]] = 1;
							if (!(array_key_exists($eid, $eventlist))) $eventlist[$eid] = 1;
							else $eventlist[$eid]++;
						}
					}
				}
			} else {
				if (!(array_key_exists($row[$switch2], $check))) {
					$check[$row[$switch2]] = 1;
					if (!(array_key_exists($eid, $eventlist))) $eventlist[$eid] = 1;
					else $eventlist[$eid]++;
				}
			}
		}
		$events = [];
		foreach ($eventlist as $i => $v) {
			$name = $dbmanager->Query(array('name'), array(array('id' => $i)), null, 0, 1);
			if ($i != $_GET['id']) $events[$name['result'][$i]['name']] = $v;
		}
		if (!(isset($_GET['chron']))) arsort($events);
		return $events;
	}
	public function correlation() {
		$events = $this->correlation_data();
		$nonchron = $this->link('nonchron');
		$output = '
				<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>

				<style type="text/css">

					.chart_container, .chart_container div, #stats_menu, #stats_menu div {
						margin: 0 !important;
						padding: 0 !important;
					}

				</style> ' . "

				<script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}\"></script>";
		$pre = '';
		$post = '';
		$chron = '';
		if (isset($_GET['after']) && $_GET['after'] == 1) $post = 'checked';
		else $pre = 'checked';
		if (isset($_GET['chron']) && $_GET['chron'] == 1) $chron = 'checked';
		$menu = '';
		if ($nonchron[0] != 1) $menu = '<label for="chron" >Chronological order</label><input name="chron" type="checkbox" id="chron" value="1" ' . $chron . ' /><br/>
		Pre <input type="radio" id="after1" name="after" value="0" ' . $pre . ' /><br />
		Post <input type="radio" name="after" id="after2" value="1" ' . $post . ' /><br />
		<input type="hidden" name="page" value="' . $_GET['page'] . '"><input type="hidden" name="id" value="' . $_GET['id'] . '"><input type="hidden" name="method" value="correlation"><input type="hidden" name="instance" value="' . $this->id . '">
		';
		$output.= '<div id ="magnific_stats_content" class="white-popup mfp-hide"><form id="stats_form" action="index.php" method="GET"><div class = "">
		<br/><h2>Items linked with these contacts</h2><br/>' . $menu . '</div></form>';
		$output.= '<script>

		$("#chron").click(function() {
		   $( "#stats_form" ).submit();
		});
		$("#after1").click(function() {
		   $( "#stats_form" ).submit();
		});
		$("#after2").click(function() {
		   $( "#stats_form" ).submit();
		});
		</script>';
		$output.= '<div class="row"><div class="chart_container" id="' . $this->id . '_attrib" style="width:100%; height: 15em;"></div></div></div>';
		$output.= "<script>
									google.load('visualization', '1', {packages: ['corechart']});
					google.setOnLoadCallback(drawChart);

					function drawChart() {

					 var data = google.visualization.arrayToDataTable([

					";
		$output.= " ['', 'Contacts in common' ],";
		foreach ($events as $i => $v) {
			$output.= "['" . $i . "', " . $v . "],";
		}
		$output.= "]);

					  var view = new google.visualization.DataView(data);

					   var options = {
						title: 'Breakdown by item',
						bar: {groupWidth: '75%'},
						theme: 'maximized',
						legend: { position: 'none' },
						colors: ['#85AFDD','#D6D6D6','#C61E54','#000000'],
						 hAxis: { textPosition: 'none' }";
		$output.= "
					  };

					  var chart = new google.visualization.ColumnChart(document.getElementById('" . $this->id . "_attrib'));

					  chart.draw(view, options);

					}
					  </script>";
		return mini_template($output, 1);
	}
	public function NoteStats() {
		global $dbmanager;
		global $connectionmanager;
		$depts = simplexml_load_file('hierarchy.xml');
		$deptlist = array();
		foreach ($depts as $dept) {
			$deptlist["$dept->LEVEL5"] = "$dept->LEVEL2";
		}
		//print_r($deptlist);
		//$result = $depts->xpath('ROW[LEVEL5="'.$query['contact_orgcode'].'"]');
		//$output .= 'Organisation: '. $result[0]->LEVEL5DESC;
		$contacts = $dbmanager->Query(array('contact_orgcode'), array(array('category' => 1)), null, 0, $this->id, 1);
		if ($_GET['normalise'] == 1) {
			$normalise = array();
			foreach ($contacts['result'] as $result) {
				if ($result['contact_orgcode'] != '' && $result['contact_orgcode'] != 'D878') $normalise[$deptlist[$result['contact_orgcode']]] = $normalise[$deptlist[$result['contact_orgcode']]] + 1;
			}
		}
		$notes = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_type=5 ORDER BY note_date ASC");
		$check = 0;
		$data = array();
		if ($_GET['linear'] != 1) {
			$total = array('other' => 0, 'SAS' => 0, 'ISG' => 0, 'SCE' => 0, 'HSS' => 0, 'MVM' => 0, 'CSG' => 0);
			while ($row = mysqli_fetch_assoc($notes)) {
				$switch = $deptlist[$contacts['result'][$row['note_item']]['contact_orgcode']];
				if ($contacts['result'][$row['note_item']]['contact_orgcode'] != '' && $contacts['result'][$row['note_item']]['contact_orgcode'] != 'D878') {
					$total["$switch"] = $total["$switch"] + 1;
				} else $total["other"] = $total["other"] + 1;
				$data[date('Y,m,d', $row['note_date']) ]['other'] = $total['other'];
				$data[date('Y,m,d', $row['note_date']) ]['SAS'] = $total['SAS'];
				$data[date('Y,m,d', $row['note_date']) ]['ISG'] = $total['ISG'];
				$data[date('Y,m,d', $row['note_date']) ]['SCE'] = $total['SCE'];
				$data[date('Y,m,d', $row['note_date']) ]['HSS'] = $total['HSS'];
				$data[date('Y,m,d', $row['note_date']) ]['MVM'] = $total['MVM'];
				$data[date('Y,m,d', $row['note_date']) ]['CSG'] = $total['CSG'];
			}
		} else {
			while ($row = mysqli_fetch_assoc($notes)) {
				$switch = $deptlist[$contacts['result'][$row['note_item']]['contact_orgcode']];
				if ($contacts['result'][$row['note_item']]['contact_orgcode'] != '' && $contacts['result'][$row['note_item']]['contact_orgcode'] != 'D878') {
					$data[date('Y,m,d', $row['note_date']) ]["$switch"] = $data[date('Y,m,d', $row['note_date']) ]["$switch"] + 1;
				} else $data[date('Y,m,d', $row['note_date']) ]['other'] = $data[date('Y,m,d', $row['note_date']) ]['other'] + 1;
				if (!(array_key_exists('other', $data[date('Y,m,d', $row['note_date']) ]))) $data[date('Y,m,d', $row['note_date']) ]['other'] = 0;
				if (!(array_key_exists('SAS', $data[date('Y,m,d', $row['note_date']) ]))) $data[date('Y,m,d', $row['note_date']) ]['SAS'] = 0;
				if (!(array_key_exists('ISG', $data[date('Y,m,d', $row['note_date']) ]))) $data[date('Y,m,d', $row['note_date']) ]['ISG'] = 0;
				if (!(array_key_exists('SCE', $data[date('Y,m,d', $row['note_date']) ]))) $data[date('Y,m,d', $row['note_date']) ]['SCE'] = 0;
				if (!(array_key_exists('HSS', $data[date('Y,m,d', $row['note_date']) ]))) $data[date('Y,m,d', $row['note_date']) ]['HSS'] = 0;
				if (!(array_key_exists('MVM', $data[date('Y,m,d', $row['note_date']) ]))) $data[date('Y,m,d', $row['note_date']) ]['MVM'] = 0;
				if (!(array_key_exists('CSG', $data[date('Y,m,d', $row['note_date']) ]))) $data[date('Y,m,d', $row['note_date']) ]['CSG'] = 0;
			}
		}
		//echo $check;
		$output = " <script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}\"></script>";
		$output.= '<div id="' . $this->id . '_note"></div>';
		$output.= "<script>
		            google.load('visualization', '1', {packages: ['corechart']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('date', 'X');
	  ";
		if ($_GET['normalise'] != 1) $output.= "data.addColumn('number', 'other');";
		$output.= "
	  data.addColumn('number', 'SAS');
	  data.addColumn('number', 'ISG');
	  data.addColumn('number', 'SCE');
	  data.addColumn('number', 'HSS');
	  data.addColumn('number', 'MVM');
	  data.addColumn('number', 'CSG');

      data.addRows([
        ";
		$i = 1;
		foreach ($data as $index => $val) {
			if ($_GET['normalise'] != 1) $output.= '[new Date(' . $index . '), ' . $val['other'] . ', ' . $val['SAS'] . ', ' . $val['ISG'] . ', ' . $val['SCE'] . ', ' . $val['HSS'] . ', ' . $val['MVM'] . ', ' . $val['CSG'] . '],';
			else {
				$val['SAS'] = $val['SAS'] / $normalise['SAS'];
				$val['ISG'] = $val['ISG'] / $normalise['ISG'];
				$val['SCE'] = $val['SCE'] / $normalise['SCE'];
				$val['HSS'] = $val['HSS'] / $normalise['HSS'];
				$val['MVM'] = $val['MVM'] / $normalise['MVM'];
				$val['CSG'] = $val['CSG'] / $normalise['CSG'];
				$output.= '[new Date(' . $index . '), ' . $val['SAS'] . ', ' . $val['ISG'] . ', ' . $val['SCE'] . ', ' . $val['HSS'] . ', ' . $val['MVM'] . ', ' . $val['CSG'] . '],';
			}
			$i++;
		}
		$output.= "
      ]);

      var options = {
        width: 600,
        height: 800,
        hAxis: {
          title: 'Time'
        },
        vAxis: {
          title: 'Total'
        }
      };

      var chart = new google.visualization.LineChart(
        document.getElementById('" . $this->id . "_note'));

      chart.draw(data, options);

    }
	  </script>";
		return $output;
	}
	public function ItemStatsTest() {
		global $dbmanager;
		global $connectionmanager;
		$depts = simplexml_load_file('hierarchy.xml');
		$deptlist = array();
		foreach ($depts as $dept) {
			$deptlist["$dept->LEVEL5"] = "$dept->LEVEL2";
		}
		$contacts = $dbmanager->Query(array('contact_orgcode'), array(array('category' => 1)), null, 0, $this->id, 1);
		$links = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE item1=30285 OR item2=30285 ORDER BY link_date ASC");
		$check = array();
		$data = array();
		$total = array('other' => 0, 'SAS' => 0, 'ISG' => 0, 'SCE' => 0, 'HSS' => 0, 'MVM' => 0, 'CSG' => 0);
		while ($row = mysqli_fetch_assoc($links)) {
			if ($row['item1'] == 30285 && !(array_key_exists($row['item2'], $check))) {
				$check[$row['item2']] = 1;
				//$total['total'] = $total['total'] + 1;
				$switch = $deptlist[$contacts['result'][$row['item2']]['contact_orgcode']];
				if ($contacts['result'][$row['item2']]['contact_orgcode'] != '') {
					$total["$switch"] = $total["$switch"] + 1;
				} else $total["other"] = $total["other"] + 1;
				$data[date('Y,m,d', $row['link_date']) ]['other'] = $total['other'];
				$data[date('Y,m,d', $row['link_date']) ]['SAS'] = $total['SAS'];
				$data[date('Y,m,d', $row['link_date']) ]['ISG'] = $total['ISG'];
				$data[date('Y,m,d', $row['link_date']) ]['SCE'] = $total['SCE'];
				$data[date('Y,m,d', $row['link_date']) ]['HSS'] = $total['HSS'];
				$data[date('Y,m,d', $row['link_date']) ]['MVM'] = $total['MVM'];
				$data[date('Y,m,d', $row['link_date']) ]['CSG'] = $total['CSG'];
			} elseif (!(array_key_exists($row['item1'], $check))) {
				$check[$row['item1']] = 1;
				//$total['total'] = $total['total'] + 1;
				$switch = $deptlist[$contacts['result'][$row['item1']]['contact_orgcode']];
				if ($contacts['result'][$row['item1']]['contact_orgcode'] != '') {
					$total["$switch"] = $total["$switch"] + 1;
				} else $total["other"] = $total["other"] + 1;
				$data[date('Y,m,d', $row['link_date']) ]['other'] = $total['other'];
				$data[date('Y,m,d', $row['link_date']) ]['SAS'] = $total['SAS'];
				$data[date('Y,m,d', $row['link_date']) ]['ISG'] = $total['ISG'];
				$data[date('Y,m,d', $row['link_date']) ]['SCE'] = $total['SCE'];
				$data[date('Y,m,d', $row['link_date']) ]['HSS'] = $total['HSS'];
				$data[date('Y,m,d', $row['link_date']) ]['MVM'] = $total['MVM'];
				$data[date('Y,m,d', $row['link_date']) ]['CSG'] = $total['CSG'];
			}
		}
		$output = " <script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}\"></script>";
		$output.= '<div id="' . $this->id . '_item"></div>';
		$output.= "<script>
		            google.load('visualization', '1', {packages: ['corechart']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('date', 'X');
      data.addColumn('number', 'other');
	  data.addColumn('number', 'SAS');
	  data.addColumn('number', 'ISG');
	  data.addColumn('number', 'SCE');
	  data.addColumn('number', 'HSS');
	  data.addColumn('number', 'MVM');
	  data.addColumn('number', 'CSG');

      data.addRows([
        ";
		$i = 1;
		foreach ($data as $index => $val) {
			$output.= '[new Date(' . $index . '), ' . $val['other'] . ', ' . $val['SAS'] . ', ' . $val['ISG'] . ', ' . $val['SCE'] . ', ' . $val['HSS'] . ', ' . $val['MVM'] . ', ' . $val['CSG'] . '],';
			$i++;
		}
		$output.= "
      ]);

      var options = {
        width: 600,
        height: 800,
        hAxis: {
          title: 'Time'
        },
        vAxis: {
          title: 'Total'
        }
      };

      var chart = new google.visualization.LineChart(
        document.getElementById('" . $this->id . "_item'));

      chart.draw(data, options);

    }
	  </script>";
		return $output;
	}
	public function AttributeStats() {
		global $dbmanager;
		global $connectionmanager;
		$depts = simplexml_load_file('hierarchy.xml');
		$deptlist = array();
		$desc = array();
		foreach ($depts as $dept) {
			$deptlist["$dept->LEVEL5"] = "$dept->LEVEL2";
			$deptlist["$dept->LEVEL6"] = "$dept->LEVEL2";
			$deptlist["$dept->LEVEL4"] = "$dept->LEVEL2";
			$deptlist["$dept->LEVEL2"][] = $dept->LEVEL5;
			$desc["$dept->LEVEL5"] = "$dept->LEVEL5DESC";
			$desc["$dept->LEVEL4"] = "$dept->LEVEL4DESC";
			$desc["$dept->LEVEL6"] = "$dept->LEVEL6DESC";
		}
		$fieldlist = array();
		$fields = mysqli_query($connectionmanager->connection, "SELECT * FROM meta WHERE (type = 3 OR type = 2) AND cat_id = 1 AND friendly_name NOT LIKE '' AND linkid = 0");
		$selectmenu = '';
		$i = 0;
		while ($row = mysqli_fetch_assoc($fields)) {
			if (!(isset($_GET['switch'])) && $i == 0) $_GET['switch'] = $row['name'];
			$arraykey = $row['name'];
			$fieldlist["$arraykey"] = array($row['type'], $row['friendly_name'], $row['options']);
			$selected = '';
			if ($_GET['switch'] == $row['name']) $selected = 'selected';
			$selectmenu.= '<option value="' . $row['name'] . '" ' . $selected . '>' . $row['friendly_name'] . '</option>';
			$i++;
		}
		$switch = $_GET['switch'];
		if ($fieldlist[$switch][0] == 3) {
			$total = array();
			$contacts = $dbmanager->Query(array('contact_orgcode', $switch), array(array('category' => 1)), null, 0, $this->id, 1);
			//if ($_GET['normalise'] != 1){
			$normalise = array();
			$second = array();
			foreach ($contacts['result'] as $result) {
				if ($result['contact_orgcode'] != '' && $result['contact_orgcode'] != 'D878' && (isset($_GET['dept']) && $_GET['dept'] != 'all')) {
					$second[$desc[$result['contact_orgcode']]] = $second[$desc[$result['contact_orgcode']]] + 1;
					$key = $desc[$result['contact_orgcode']];
					if ($deptlist[$result['contact_orgcode']] == $_GET['dept']) $total["$key"] = 0;
					$normalise[$desc[$result['contact_orgcode']]] = $normalise[$desc[$result['contact_orgcode']]] + 1;
				} elseif ($result['contact_orgcode'] != '' && $result['contact_orgcode'] != 'D878') {
					$normalise[$deptlist[$result['contact_orgcode']]] = $normalise[$deptlist[$result['contact_orgcode']]] + 1;
					$second[$result['contact_orgcode']] = $second[$result['contact_orgcode']] + 1;
				}
			}
			//}
			$check = array();
			foreach ($second as $index => $value) {
				$key = $deptlist["$index"];
				$second["$key"] = $second["$key"] + $value;
				$check[$key][$index] = $check[$key][$index] + $value;
			}
			/*foreach ($check as $index => $value){
			if ($index == 'SAS'){
				echo '---- '.$index.'<br>';
				foreach ($value as $subindex => $subval){

					echo $subindex.' - '.$desc[$subindex].' - '.$subval.'<br>';

				}
			}
			}*/
			$attrib = mysqli_query($connectionmanager->connection, "SELECT contact_orgcode FROM contacts WHERE " . $switch . "=1 AND contact_orgcode != '' AND contact_orgcode != 'D878'");
			/*	foreach ($contacts['result'] as $result){

				if ($result['sustainable_research'] == 1 && $result['contact_orgcode'] != 'D878' && $result['contact_orgcode'] != ''){

					$total[$deptlist[$result['contact_orgcode']]]++;

				}

			} */
			while ($row = mysqli_fetch_assoc($attrib)) {
				if (isset($_GET['dept']) && $_GET['dept'] != 'all' && $deptlist[$row['contact_orgcode']] == $_GET['dept']) {
					$arkey1 = $desc[$row['contact_orgcode']];
					$total["$arkey1"]++;
				} elseif (!(isset($_GET['dept'])) || $_GET['dept'] == 'all') $total[$deptlist[$row['contact_orgcode']]]++;
			}
			if ($_GET['normalise'] != 1) {
				foreach ($total as $index => $subtotal) {
					$second[$index] = $second[$index] - $total[$index];
					$total[$index] = $total[$index] / $normalise[$index];
					$second[$index] = $second[$index] / $normalise[$index];
				}
			} else {
				foreach ($total as $index => $subtotal) {
					$second[$index] = $second[$index] - $total[$index];
				}
			}
			//print_r($second);
			$output = " <script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}\"></script>";
			global $page;
			$output.= '<form id="stats_menu" action="" method="GET"><div class = "row form_display"><div class = "row primary_item"><div class = "form_area"><div class = "four columns alpha"><select name="switch" id="switch" data-placeholder="Select attribute">';
			$output.= $selectmenu;
			$checked = 'checked';
			$selected = array();
			$selected[$_GET['dept']] = 'selected';
			if (isset($_GET['normalise']) && $_GET['normalise'] == 1) $checked = '';
			$output.= '</select></div><div class = "four columns">
									<select name="dept" id="dept" data-placeholder="Select subset"><option value="all" >All</option><option value="SAS" ' . $selected['SAS'] . '>SAS</option><option value="ISG" ' . $selected['ISG'] . '>ISG</option><option value="SCE" ' . $selected['SCE'] . '>SCE</option><option value="HSS" ' . $selected['HSS'] . '>HSS</option><option value="MVM" ' . $selected['MVM'] . '>MVM</option><option value="CSG" ' . $selected['CSG'] . '>CSG</option></select><input type="hidden" name="page" value="' . $page . '"><input type="hidden" id="normalise" name="normalise" value="0"></div></div></div>';
			$output.= '<div class = "row primary_item"><div class = "form_area"><div class = "four columns checkbox alpha"><label for="normalise" ><span class="form_label_text">Normalise</label>
			<input name="denormalise" type="checkbox" id="denormalise" value="1" ' . $checked . '/></div></div></div>';
			$output.= '</div></form>';
			//<div class = "row primary_item"><div class = "form_area"><div class = "four columns checkbox alpha"><label for="normalise" ><span class="form_label_text">Normalise</label>
			//<input name="denormalise" type="checkbox" id="denormalise" value="1" '.$checked.' '.$disabled.'/></div></div></div>
			$output.= '<div id="' . $this->id . '_attrib"></div>';
			$output.= '<script>
	$("#denormalise").change(function() {
		var denorm = $("input#denormalise:checked").val();
		if (denorm != 1){
			$("#normalise").val("1");
		}

	   $( "#stats_menu" ).submit();
	});
	$("#switch").change(function() {
		var denorm = $("input#denormalise:checked").val();
		if (denorm != 1){
			$("#normalise").val("1");
		}
		$( "#stats_menu" ).submit();
	});
	$("#dept").change(function() {
	   $( "#stats_menu" ).submit();
	});
	</script>';
			$output.= "<script>
						google.load('visualization', '1', {packages: ['corechart']});
		google.setOnLoadCallback(drawChart);

		function drawChart() {

		 var data = google.visualization.arrayToDataTable([

      ";
			//if (isset($_GET['dept']) && $_GET['dept'] != 'all')
			$output.= " ['', 'Yes', 'No',
           { role: 'annotation' } ],";
			//else $output .= " ['Yes', 'No'],";
			foreach ($total as $index => $subtotal) {
				//if ($index != '' && (isset($_GET['dept']) && $_GET['dept'] != 'all'))
				if ($index != '') $output.= "['" . $index . "', " . $subtotal . ", " . $second[$index] . ",''],";
				//elseif ($index != '') $output .= "['".$index."', ".$subtotal."],";

			}
			$output.= "]);

		  var view = new google.visualization.DataView(data);

		   var options = {
			title: 'Data for " . $fieldlist[$switch][1] . "',
			width: 600,
			height: 400,
			bar: {groupWidth: '75%'},
			legend: { position: 'none' },
			 isStacked: true,";
			$output.= "
		  };

		  var chart = new google.visualization.ColumnChart(document.getElementById('" . $this->id . "_attrib'));

		  chart.draw(view, options);

		}
		  </script>";
		} else {
			$results = array();
			$attrib = $dbmanager->Query(array('contact_orgcode', $switch), array(array('category' => 1)), null, 0, $this->id, 1);
			foreach ($attrib['result'] as $result) {
				if ($result['contact_orgcode'] != 'D878' && $result['contact_orgcode'] != '') {
					if (isset($_GET['dept']) && $_GET['dept'] != 'all' && $deptlist[$result['contact_orgcode']] == $_GET['dept']) {
						$arkey1 = $desc[$result['contact_orgcode']];
						$arkey2 = $result[$switch];
						$results["$arkey1"]["$arkey2"]++;
					} elseif (!(isset($_GET['dept'])) || $_GET['dept'] == 'all') $results[$deptlist[$result['contact_orgcode']]][$result[$switch]]++;
				}
			}
			$tempoptions = explode(',', $fieldlist[$switch][2]);
			$options = array();
			foreach ($tempoptions as $option) {
				$option = explode(':', $option);
				$options[$option[0]] = $option[1];
			}
			$output = " <script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}\"></script>";
			global $page;
			$output.= '<form id="stats_menu" action="" method="GET"><div class = "row form_display"><div class = "row primary_item"><div class = "form_area"><div class = "four columns alpha"><select name="switch" id="switch" data-placeholder="Select attribute">';
			$output.= $selectmenu;
			$selected = array();
			$selected[$_GET['dept']] = 'selected';
			$output.= '</select></div><div class = "four columns">
									<select name="dept" id="dept" data-placeholder="Select subset"><option value="all" >All</option><option value="SAS" ' . $selected['SAS'] . '>SAS</option><option value="ISG" ' . $selected['ISG'] . '>ISG</option><option value="SCE" ' . $selected['SCE'] . '>SCE</option><option value="HSS" ' . $selected['HSS'] . '>HSS</option><option value="MVM" ' . $selected['MVM'] . '>MVM</option><option value="CSG" ' . $selected['CSG'] . '>CSG</option></select><input type="hidden" name="page" value="' . $page . '"></div></div></div></div></form>';
		}
		$i = 0;
		foreach ($results as $index => $result) {
			if ($index != '') {
				$output.= '<div id="' . $this->id . '_attrib_' . $i . '"></div>';
				$output.= '<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
		["Value", "#"],';
				$colours = '';
				$j = 0;
				foreach ($result as $subindex => $subresult) {
					$output.= '["' . $options[$subindex] . ' (' . $subresult . ')",     ' . $subresult . '],';
					$colours.= $j . ':{color:"' . stringToColourCode($subindex) . '"},';
					$j++;
				}
				$output.= '
        ]);

        var options = {
          title: "' . $index . '",
		  slices:{' . $colours . '}
        };

        var chart = new google.visualization.PieChart(document.getElementById("' . $this->id . '_attrib_' . $i . '"));

        chart.draw(data, options);
      }
    </script>
	<script>
	$("#dept").change(function() {
	   $( "#stats_menu" ).submit();
	});
	$("#switch").change(function() {
		$( "#stats_menu" ).submit();
	});
	</script>';
			}
			$i++;
		}
		return $output;
	}
	public function EventNetwork() {
		global $connectionmanager;
		global $dbmanager;
		$contacts = array();
		$eventlist = array();
		if (isset($_GET['series']) && $_GET['series'] != 'all') {
			$event_query = $dbmanager->Query(array('id'), array(array('event_series' => $_GET['series']), array('category' => 5)), null, 0, $this->id);
		} elseif (isset($_GET['published']) && $_GET['published'] == 1) {
			$event_query = $dbmanager->Query(array('id', 'event_series'), array(array('category' => 5), array('event_publish' => 1)), null, 0, $this->id);
		} else {
			$event_query = $dbmanager->Query(array('id', 'event_series'), array(array('category' => 5)), null, 0, $this->id);
		}
		$selected = array();
		for ($i = 0;$i < 9;$i++) $selected[$i] = '';
		if (isset($_GET['switch']) && $_GET['switch'] != 'all') $selected[$_GET['switch']] = 'selected';
		$output = '

		<link href="stylesheets/cy.css" rel="stylesheet" />
			<script src="js/arbor.js"></script>
			<script src="js/cola.v3.min.js"></script>
			<script src="js/dagre.js"></script>
			<script src="js/springy.js"></script>
			<script src="js/cytoscape.min.js"></script>
			<div class="row"><div class="cy" id="event_' . $this->id . '"></div></div>

					<div class="row">
		<form id="event_network">
		<select name="switch" id="switch" data-placeholder="Select contact_type">
		<option value ="all">All</option>
		<option value ="1" ' . $selected[1] . '>Academic Staff</option>
		<option value ="2" ' . $selected[2] . '>Technical Staff</option>
		<option value ="3" ' . $selected[3] . '>Non-Technical</option>
		<option value ="4" ' . $selected[4] . '>Undergraduate</option>
		<option value ="5" ' . $selected[5] . '>Postgraduate</option>
		<option value ="6" ' . $selected[6] . '>Alumnus</option>
		<option value ="7" ' . $selected[7] . '>Media Contact</option>
		<option value ="0" ' . $selected[0] . '>Other</option>
		</select>

		<select name="series" id="series" data-placeholder="Select event_series">
		<option value ="all">All</option>';
		foreach ($event_query['event_series']['options'] as $id => $series) {
			$selected = '';
			if ($id == $_GET['series']) {
				$selected = ' selected="selected" ';
			}
			$output.= '<option value ="' . $id . '" ' . $selected . '>' . $series . '</option>';
		}
		$output.= '
		</select>

		<input type="hidden" name="page" value="' . $_GET['page'] . '">
		</form>
		</div>
		<script>
		$("#switch").change(function() {
			$("#event_network").submit();
		});

		$("#series").change(function() {
			$("#event_network").submit();
		});


		</script>
		';
		foreach ($event_query['result'] as $row) {
			$eventlist[$row['id']][0] = $row['name'];
		}
		print_r($eventslist);
		$events = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE cat2=5 and cat1=1");
		while ($row = mysqli_fetch_assoc($events)) {
			if (isset($eventlist[$row['item2']][0])) {
				$contacts[$row['item1']][] = $row['item2'];
				$eventlist[$row['item2']][1]++;
			}
		}
		$events = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE cat1=5 and cat2=1");
		while ($row = mysqli_fetch_assoc($events)) {
			if (isset($eventlist[$row['item1']][0])) {
				$contacts[$row['item2']][] = $row['item1'];
				$eventlist[$row['item1']][1]++;
			}
		}
		foreach ($contacts as $index => $value) {
			$contacts[$index] = array_values(array_unique($value));
		}
		$temp = [];
		foreach ($contacts as $index => $value) {
			if (count($value) > 1) {
				if (isset($_GET['switch']) && $_GET['switch'] != 'all') {
					$data = $dbmanager->Query(array('id'), array(array('contact_type' => $_GET['switch']), array('id' => $index)), null, 0, $this->id);
					if ($data['result_count'] > 0) $temp[$index] = $value;
				} else $temp[$index] = $value;
			}
		}
		$contacts = $temp;
		$edgecheck = array();
		$edges = array();
		foreach ($contacts as $contact) {
			for ($i = 0;$i < (count($contact) - 1);$i++) {
				for ($j = $i;$j < (count($contact) - 1);$j++) {
					if ($edges[$contact[$i]][$contact[$j + 1]] > 0) $edges[$contact[$i]][$contact[$j + 1]]++;
					else $edges[$contact[$j + 1]][$contact[$i]]++;
				}
			}
		}
		$output.= "
<script>
$(function(){ // on dom ready

var elesJson = {
  nodes: [";
		$i = 0;
		$edgescript = '';
		//add edges
		foreach ($edges as $index => $val) {
			foreach ($val as $edgeindex => $edge) {
				if ($index != $edgeindex && $eventlist[$index][0] != '' && $eventlist[$edgeindex][0] != '') {
					$edgescript.= "
			{ data: { id: 'e" . $i . "', weight: " . $edge . ", source: '" . addslashes($eventlist[$index][0]) . "', target: '" . addslashes($eventlist[$edgeindex][0]) . "' } },
			";

					$edgecheck[$eventlist[$index][0]] = 1;
					$edgecheck[$eventlist[$edgeindex][0]] = 1;
					$i++;
				}
			}
		}
		//add nodes
		foreach ($eventlist as $event) {
			if (array_key_exists($event[0], $edgecheck)) $output.= "  { data: { id: '" . addslashes($event[0]) . "', foo: 3} },
	   ";
		}
		$output.= "
  ],

  edges: [";
		$output.= $edgescript;
		$output.= "
  ]
};


$('#event_" . $this->id . "').cytoscape({
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'background-color': '#6272A3',
        'shape': 'circle',
        'width': 'mapData(foo, 0, 10, 10, 30)',
        'height': 'mapData(foo, 0, 10, 10, 30)'

      })
    .selector('edge')
      .css({
        'width': 'mapData(weight, 1, 10, 1, 10)',
        'line-color': '#B1C1F2',
        'target-arrow-color': '#B1C1F2',
        'target-arrow-shape': 'none',
        'opacity': 0.8,
      })
	 .selector('edge:selected')
      .css({
        'width': 'mapData(weight, 1, 10, 1, 10)',
        'line-color': '#B1C1F2',
        'target-arrow-color': '#B1C1F2',
        'target-arrow-shape': 'none',
        'opacity': 0.8,
		'content': 'data(weight)'
      })
    .selector('node:selected')
      .css({
        'background-color': 'black',
        'line-color': 'black',
        'target-arrow-color': 'black',
        'source-arrow-color': 'black',
        'opacity': 1,
        'content': 'data(id)'
      }),

  elements: elesJson,

  layout: {
    name: 'arbor',
    directed: true,
    padding: 10
  },

  ready: function(){
    // ready 2
  }
});

}); // on dom ready
</script>";
		return $output;
	}
	public function ItemStats() {
		if (!($_GET['page'] == 'advanced-search' && !(isset($_GET['field_779_0'])))) {
			global $dbmanager;
			global $connectionmanager;
			global $item_id;
			$ids = $this->link('ids');
			$depts = simplexml_load_file('hierarchy.xml');
			$deptlist = array();
			$desc = array();
			foreach ($depts as $dept) {
				$deptlist["$dept->LEVEL5"] = "$dept->LEVEL2";
				$deptlist["$dept->LEVEL6"] = "$dept->LEVEL2";
				$deptlist["$dept->LEVEL4"] = "$dept->LEVEL2";
				$deptlist["$dept->LEVEL2"][] = $dept->LEVEL5;
				$desc["$dept->LEVEL5"] = "$dept->LEVEL5DESC";
				$desc["$dept->LEVEL4"] = "$dept->LEVEL4DESC";
				$desc["$dept->LEVEL6"] = "$dept->LEVEL6DESC";
			}
			$total = array();
			$contacts = $dbmanager->Query(array('contact_orgcode', $switch, 'contact_is_active', 'contact_type'), $ids[0], null, 0, $this->id, 1);
			if ($contacts['result_count'] < 2) return '';
			//if ($_GET['normalise'] != 1){
			$normalise = array();
			$second = array();
			$active_total = 0;
			$active = array();
			$contact_types = array();
			foreach ($contacts['result'] as $result) {
				if ($result['contact_is_active'] == 1) {
					$active_total++;
				}
				if ($result['contact_type'] != '') {
					$contact_types[$result['contact_type']] = ($contact_types[$result['contact_type']] + 1);
				}
				if ($result['contact_orgcode'] != '' && $result['contact_orgcode'] != 'D878' && (isset($_GET['dept']) && $_GET['dept'] != 'all')) {
					if ($deptlist[$result['contact_orgcode']] == $_GET['dept']) $normalise[$desc[$result['contact_orgcode']]] = $normalise[$desc[$result['contact_orgcode']]] + 1;
					if ($result['contact_is_active'] == 1) {
						$active[$desc[$result['contact_orgcode']]]++;
					}
				} elseif ($result['contact_orgcode'] != '' && $result['contact_orgcode'] != 'D878') {
					$normalise[$deptlist[$result['contact_orgcode']]] = $normalise[$deptlist[$result['contact_orgcode']]] + 1;
					if ($result['contact_is_active'] == 1) {
						$active[$deptlist[$result['contact_orgcode']]]++;
					}
				}
			}
			//}
			$no_entries_text = '';
			$no_entries_i = 0;
			if ((isset($_GET['dept']) && $_GET['dept'] != 'all')) {
				foreach ($deptlist[$_GET['dept']] as $key => $name) {
					if ((!array_key_exists($desc[trim($name) ], $normalise) || $normalise[$desc[trim($name) ]] == null) && strpos($no_entries_text, $desc[trim($name) ]) === false) {
						if ($no_entries_i == 0) {
							$no_entries_text.= '<strong>No contacts in:</strong> ' . $desc[trim($name) ];
							$no_entries_i = 1;
						} else {
							$no_entries_text.= ', ' . $desc[trim($name) ];
						}
					}
				}
			} else {
				$depts = array('SAS', 'ISG', 'SCE', 'CAHSS', 'MVM', 'CSG');
				foreach ($depts as $key => $name) {
					if ((!array_key_exists($name, $normalise) || $normalise[$name] == null) && strpos($no_entries_text, $normalise['name']) === false) {
						if ($no_entries_i == 0) {
							$no_entries_text.= '<strong>No contacts in:</strong> ' . $name;
							$no_entries_i = 1;
						} else {
							$no_entries_text.= ', ' . $name;
						}
					}
				}
			}
			$output = '

				<style type="text/css">

					.chart_container, .chart_container div, #stats_menu, #stats_menu div {
						margin: 0 !important;
						padding: 0 !important;
					}

				</style> ' . "

				<script type=\"text/javascript\" src=\"https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1','packages':['corechart']}]}\"></script>";
			global $page;
			// activity stats
			//print_r($contacts);
			$output.= '<h3>About these ' . count($contacts['result']) . ' contacts</h3><p><em>' . $active_total . ' active (' . round($active_total / $contacts['result_count'] * 100) . '%)</em></p>';
			arsort($contact_types);
			foreach ($contact_types as $type => $qty) {
				if ($qty > 0) {
					$output.= '<p><strong>' . $contacts['contact_type']['options'][$type] . '</strong>: ' . $qty . ' (' . round($qty / $contacts['result_count'] * 100) . '%)</p> ';
				}
			}
			$output.= '<form id="stats_menu" action="" method="GET"><div class = "row form_display"><div class = "row primary_item"><div class = "form_area">';
			$selected = array();
			$selected[$_GET['dept']] = 'selected';
			$output.= '<div class = "three columns">
										Show statistics for: <select name="dept" id="dept" data-placeholder="Select subset"><option value="all" >All</option><option value="SAS" ' . $selected['SAS'] . '>SAS</option><option value="ISG" ' . $selected['ISG'] . '>ISG</option><option value="SCE" ' . $selected['SCE'] . '>SCE</option><option value="HSS" ' . $selected['HSS'] . '>HSS</option><option value="MVM" ' . $selected['MVM'] . '>MVM</option><option value="CSG" ' . $selected['CSG'] . '>CSG</option><option value="NSU" ' . $selected['NSU'] . '>NSU</option></select><input type="hidden" name="page" value="' . $page . '"><input type="hidden" name="id" value="' . $_GET['id'] . '"><input type="hidden" id="normalise" name="normalise" value="0"></div></div></div>';
			$output.= '</div></form>';
			$output.= '<script>
					$("#denormalise").change(function() {
					var denorm = $("input#denormalise:checked").val();
					if (denorm != 1){
						$("#normalise").val("1");
					}

					$( "#stats_menu" ).submit();
					});
					$("#switch").change(function() {
					var denorm = $("input#denormalise:checked").val();
					if (denorm != 1){
						$("#normalise").val("1");
					}
					$( "#stats_menu" ).submit();
					});
					$("#dept").change(function() {
					$( "#stats_menu" ).submit();
					});
					</script>';
			if (count($normalise) > 0) {
				$output.= '<div class="chart_container" id="' . $this->id . '_attrib" style="width:100%; height: 15em;"></div>
					<p>' . $no_entries_text . '</p>';
				$output.= "<script>
									google.load('visualization', '1', {packages: ['corechart']});
					google.setOnLoadCallback(drawChart);

					function drawChart() {

					 var data = google.visualization.arrayToDataTable([

					";
				//if (isset($_GET['dept']) && $_GET['dept'] != 'all')
				$output.= " ['', 'Active','Inactive' ],";
				//else $output .= " ['Yes', 'No'],";
				arsort($normalise);
				foreach ($normalise as $index => $subtotal) {
					$subtotal = $subtotal - $active[$index];
					//if ($index != '' && (isset($_GET['dept']) && $_GET['dept'] != 'all'))
					if ($index != '' && strlen($index) > 0) {
						if (!array_key_exists($index, $active)) {
							$active[$index] = 0;
						}
						$output.= "['" . $index . "', " . $active[$index] . ", " . $subtotal . "],";
					}
					//elseif ($index != '') $output .= "['".$index."', ".$subtotal."],";

				}
				$output.= "]);

					  var view = new google.visualization.DataView(data);

					   var options = {
						title: 'Breakdown by department',
						bar: {groupWidth: '75%'},
						theme: 'maximized',
						legend: { position: 'none' },
						colors: ['#85AFDD','#D6D6D6','#C61E54','#000000'],
						 isStacked: true,";
				$output.= "
					  };

					  var chart = new google.visualization.ColumnChart(document.getElementById('" . $this->id . "_attrib'));

					  chart.draw(view, options);

					}
					  </script>";
			} else {
				$output.= '<p>' . $no_entries_text . '</p>';
			}
			return $output;
		}
	}
	public function trend_data() {
		global $dbmanager;
		global $connectionmanager;
		$id = mysqli_real_escape_string($connectionmanager->connection, $_GET['id']);
		$category = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id=" . $id));
		$category = $category['category'];
		$contactlist = [];
		$events = [];
		$check = [];
		$cids = array();
		$contacts = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE (item1=" . $id . " AND cat2 =1) OR (item2=" . $id . " AND cat1 =1)");
		while ($row = mysqli_fetch_assoc($contacts)) {
			if ($row['cat2'] == 1) $switch = 'item2';
			else $switch = 'item1';
			$cid = $row[$switch];
			if (!in_array($cid, $cids)) {
				$cids[] = $cid;
				$row['link_date'] = round($row['link_date'], -5);
				$contactlist[$row['link_date']] = $contactlist[$row['link_date']] + 1;
			}
		}
		ksort($contactlist, SORT_NUMERIC);
		return $contactlist;
	}
	public function trend_summary() {
		$data = $this->trend_data();
		$chartdata = '';
		$total = 0;
		$earliest_date = 9999999999999999999;
		$earliest_value = 0;
		foreach ($data as $unixtime => $count) {
			$total = $total + $count;
			if ($unixtime < $earliest_date && $unixtime > 0) {
				$earliest_date = $unixtime;
				$earliest_value = $total;
			}
			if ($unixtime !== 0) {
				$chartdata.= '[new Date(' . ($unixtime * 1000) . '), ' . $total . '],
';
			}
		}
		// calculate trends
		$time_between_dates = (time() - $earliest_date);
		$difference_between_values = ($total - $earliest_value);
		$trend = ($difference_between_values / $time_between_dates);
		$trend_text = '';
		if (time() > 1535673600) {
			$trend_text = '<p>The total number of contacts will be around <strong>' . ($total + round($trend * (strtotime('+2 Years') - time), 0)) . '</strong> by ' . date("j F Y", strtotime('+2 Years')) . '.</p>';
		} else {
			$trend_text = '<p>The total number of contacts will be around <strong>' . ($total + round($trend * (1535673600 - time()), 0)) . '</strong> by 31 August 2018.</p>';
		}
		$output.= ' <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script type="text/javascript">
		  google.charts.load("current", {"packages":["corechart","line"]});
		  google.charts.setOnLoadCallback(drawOffices);
		  function drawOffices() {
			var data = google.visualization.arrayToDataTable([
			  ["Date", "Contacts"],
			  ' . $chartdata . '
			]);

			var options = {
				legend: { position: \'none\' },
				trendlines: {
					0: {},
					1: {},
				} ,
			    chart: {
					theme: \'maximized\',
					colors: [\'#85AFDD\',\'#D6D6D6\',\'#C61E54\',\'#000000\'],
					hAxis: { textPosition: \'none\' },

				},
			};

			var chart = new google.charts.Line(document.getElementById("trend_graph"));

			chart.draw(data, options);
		  }
		</script>



			<h3>Trends</h3>
			<div id="trend_graph" class="chart_container"></div>
			' . $trend_text . '

            ';
		return $output;
	}
}
?>
