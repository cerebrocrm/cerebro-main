<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Handles email functionality
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class ParseEvents extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('server' => 'pop3 mail server address', 'username' => 'username', 'password' => 'password');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('UpdateEvents' => 'Parse incoming messages and update notes', 'ResetLog' => 'Reset non-flagged error logs');
	//Object functions and variables go here
	
	/**
	 * Check if a message hash already exists and return true if it does
	 *
	 * @return boolean
	 */
	function CheckHashExists($string) {
		global $connectionmanager;
		$string = preg_replace('/\[.*?\] /', '', $string);
		$hash = md5($string);
		$query = mysqli_query($connectionmanager->connection, "SELECT COUNT(*) FROM global_notes WHERE note_hash = '" . $hash . "'");
		$hashcount = mysqli_fetch_row($query);
		if ($hashcount[0] > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function ResetLog() {
		global $connectionmanager;
		$yesterday = 'events_' . date('m_d_Y', time() - 24 * 60 * 60) . '.php';
		$start = strtotime(date('d-m-Y', time() - 24 * 60 * 60));
		$end = strtotime(date('d-m-Y', time()));
		$log = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'events_log_count'"));
		$notes = mysqli_query($connectionmanager->connection, "SELECT note_id FROM global_notes WHERE (note_type=50 OR note_type=51 OR note_type=52 OR note_type=53) AND note_date > " . $start . " AND note_date < " . $end);
		if (mysqli_num_rows($notes) == $log['pref_value']) {
			unlink($yesterday);
			mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=0 WHERE pref_name LIKE 'events_log_count'");
		}
		return 1;
	}
	/**
	 * Parse incoming mail and update notes
	 *
	 * @return void
	 */
	public function UpdateEvents() {
		require_once "ezc/Base/base.php";
		$loads = spl_autoload_functions();
		if (!(is_array($loads))) $loads = array();
		if (!(array_key_exists('my_autoloader', $loads))) {
			if (!(function_exists(my_autoloader))) {
				function my_autoloader($className) {
					ezcBase::autoload($className);
				}
			}
			spl_autoload_register('my_autoloader');
		}
		global $connectionmanager;
		global $dbmanager;
		$server = $this->link('server');
		$user = $this->link('username');
		$pass = $this->link('password');
		$imap = new ezcMailImapTransport($server[0]);
		$imap->authenticate($user[0], $pass[0]);
		$mailboxes = $imap->listMailboxes();
		$imap->selectMailbox('Inbox');
		$messages = $imap->listMessages();
		//print_r($messages);
		$parser = new ezcMailParser();
		$transport = new ezcMailMtaTransport();
		$today = 'events_' . date('m_d_Y', time()) . '.php';
		foreach ($messages as $messageNr => $size) {
			try {
				$set = $imap->fetchByMessageNr($messageNr);
				$mail = $parser->parseMail($set);
				$mail = $mail[0];
				$parts = $mail->fetchParts();
				$subject = $mail->subject;
				$message = $parts[0]->text;
				$type = 0;
				$note = '';
				$txt = '
							
				';
				$txt.= print_r($mail, true);
				//file_put_contents($today, $txt.PHP_EOL , FILE_APPEND);
				mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'events_log_count'");
				if (stripos($subject, 'Order Notification for') !== false) {
					$type = 1;
					$name = trim(substr($subject, ($pos + 22)));
					$pos1 = stripos($message, 'mailto:');
					$email = trim(substr($message, ($pos1 + 7)));
					$pos2 = stripos($email, '<br />');
					$email = trim(substr($email, 0, $pos2));
					$email = explode(' ', $email);
					$email = $email[0];
					$pos1 = stripos($message, 'confirmation email for:');
					$name = trim(substr($message, ($pos1 + 23)));
					$pos2 = stripos($name, '<br />');
					$name = trim(substr($name, 0, $pos2));
					$name = explode(' ', $name);
					$event = trim(substr($subject, 23));
					$first = '';
					$last = '';
					for ($i = 0;$i < count($name);$i++) {
						if ($i == (count($name) - 1)) $last = $name[$i];
						else $first.= $name[$i];
					}
				} elseif (stripos($subject, '[Events Booking] booking:') !== false) {
					$type = 1;
					$pos1 = stripos($message, 'Name:');
					$name = trim(substr($message, ($pos1 + 5)));
					$pos2 = stripos($name, 'Email:');
					$name = trim(substr($name, 0, $pos2));
					$name = explode(' ', $name);
					$pos1 = stripos($message, 'Email:');
					$email = trim(substr($message, ($pos1 + 6)));
					$pos2 = stripos($email, 'UUN:');
					$email = trim(substr($email, 0, $pos2));
					$event = trim(substr($subject, 26));
					$first = '';
					$last = '';
					for ($i = 0;$i < count($name);$i++) {
						if ($i == (count($name) - 1)) $last = $name[$i];
						else $first.= $name[$i];
					}
				} elseif (stripos($subject, '[Events Booking] cancellation:') !== false) {
					$type = 2;
					$pos = stripos($subject, 'cancellation:');
					$event = trim(substr($subject, ($pos + 13)));
					$pos1 = stripos($message, 'Email:');
					$email = trim(substr($message, ($pos1 + 6)));
					$pos2 = stripos($email, 'UUN:');
					$email = trim(substr($email, 0, $pos2));
					$pos1 = stripos($message, 'Reason given:');
					if ($pos1 != 0) {
						$note = trim(substr($message, ($pos1 + 13)));
						$pos2 = stripos($note, 'Booking Details:');
						$note = trim(substr($note, 0, $pos2));
					}
				} elseif (stripos($subject, 'Cancelled for') !== false) {
					$type = 2;
					$pos = stripos($subject, 'Cancelled for');
					$event = trim(substr($subject, ($pos + 13)));
					$pos1 = stripos($message, 'Attendee Email:');
					$email = trim(substr($message, ($pos1 + 15)));
					$pos2 = stripos($email, 'Order Number:');
					$email = trim(substr($email, 0, $pos2));
				} else {
					$to = new ezcMailAddress('srs-comms@ed.ac.uk', 'SRS comms');
					$mail->to = array($to);
					$transport->send($mail);
				}
				$contact = $dbmanager->Query(array('id'), array(array('category' => 1), array('contact_email' => $email)), null, 0, 0);
				$event = $dbmanager->Query(array('id'), array(array('category' => 5), array('name' => $event)), null, 0, 0);
				foreach ($event['result'] as $result) $event_id = $result['id'];
				if ($contact['result_count'] > 0) {
					foreach ($contact['result'] as $result) $contact_id = $result['id'];
					if ($type == 1) {
						$hash = md5('52' . $mail->subject . $email);
						if ($this->CheckHashExists($hash) !== true) {
							mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item,note_date,note_type,note_hash) VALUES (" . $contact_id . "," . time() . ",52,'" . $hash . "')");
							$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $contact_id . " AND note_hash LIKE '" . $hash . "'"));
							$insert = $insert['note_id'];
							mysqli_query($connectionmanager->connection, "INSERT INTO item_links (item1,item2,note_id,cat1,cat2,link_date) VALUES (" . $contact_id . "," . $event_id . "," . $insert . ",1,5," . time() . ")");
						}
					} elseif ($type == 2) {
						$booked = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes LEFT JOIN item_links ON global_notes.note_id=item_links.note_id WHERE note_item=" . $contact_id . " AND (note_type = 52) AND (item1=" . $event_id . " OR item2 = " . $event_id . ")");
						if (mysqli_num_rows($booked) > 0) {
							if ($note != '') $note = ' giving reason: ' . $note;
							$booked = mysqli_fetch_assoc($booked);
							mysqli_query($connectionmanager->connection, "UPDATE global_notes SET note_type=50,note_text='" . $booked['note_text'] . " ****Cancelled booking**** " . mysqli_real_escape_string($connectionmanager->connection, $note) . "' WHERE note_id=" . $booked['note_id']);
						}
					}
				} elseif ($type == 1) {
					$ad = new ActiveDirectory();
					$check = json_decode($ad->GetSecureJSON($email));
					$postdata = array();
					$postdata['category'] = 1;
					$postdata['table'] = 'items';
					$name = explode(' ', $check->name);
					$postdata['contact_first'] = array_shift($name);
					$postdata['contact_last'] = trim(implode(' ', $name));
					if ($check->detail_type == 'Regular staff' || $check->detail_type == 'Staff visitor') {
						if (strpos($check->title, 'Lectur') !== false) {
							$postdata['contact_type'] = 1;
						} elseif (strpos($check->title, 'Prof') !== false) {
							$postdata['contact_type'] = 1;
						} elseif (strpos($check->title, 'Principal') !== false) {
							$postdata['contact_type'] = 1;
						} elseif (strpos($check->title, 'Chair') !== false) {
							$postdata['contact_type'] = 1;
						} elseif (strpos($check->title, 'Teach') !== false) {
							$postdata['contact_type'] = 1;
						} elseif (strpos($check->title, 'Research') !== false) {
							$postdata['contact_type'] = 1;
						} elseif (strpos($check->title, 'Fellow') !== false) {
							$postdata['contact_type'] = 1;
						} elseif (strpos($check->title, 'Lab Manager') !== false) {
							$postdata['contact_type'] = 2;
						} elseif (strpos($check->title, 'Technic') !== false) {
							$postdata['contact_type'] = 2;
						} else {
							$postdata['contact_type'] = 3;
						}
					} elseif ($check->detail_type == "Postgraduate student") {
						$postdata['contact_type'] = 4;
					} elseif ($check->detail_type == "Undergraduate student") {
						$postdata['contact_type'] = 5;
					} else {
						$postdata['contact_type'] = 0;
					}
					$postdata['contact_company'] = $check->org;
					$postdata['contact_email'] = $email;
					$postdata['contact_title'] = $check->title;
					$postdata['contact_when_we_spoke'] = date('Y-m-d');
					$postdata['contact_specific_meeting'] = $event_id;
					$postdata['contact_recieve_newsletters'] = 0;
					$_POST = $postdata;
					if (($_POST['contact_first'] == null || $_POST['contact_first'] == '')) {
						$_POST['contact_first'] = $first;
						$_POST['contact_last'] = $last;
					}
					$post = new Post;
					$contact_id = $post->CheckPost();
					mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item,note_date,note_type,note_hash) VALUES (" . $contact_id . "," . time() . ",52,'" . $hash . "')");
					$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $contact_id . " AND note_hash LIKE '" . $hash . "'"));
					$insert = $insert['note_id'];
					mysqli_query($connectionmanager->connection, "INSERT INTO item_links (item1,item2,note_id,cat1,cat2,link_date) VALUES (" . $contact_id . "," . $event_id . "," . $insert . ",1,5," . time() . ")");
				}
				$imap->delete($messageNr);
			}
			catch(Exception $e) {
				$imap->delete($messageNr);
				$txt = '
						
						';
				$txt.= print_r($e, true);
				file_put_contents('event_errors.log', $txt . PHP_EOL, FILE_APPEND);
				$imap->delete($messageNr);
			}
		}
		$imap->expunge();
		return 'message parsing complete';
	}
}
?>
