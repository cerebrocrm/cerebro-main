<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
error_reporting(1);
/**
 * Creates user editable workbooks.
 *
 * Tasks are assigned to workbooks, and workboks to teams.
 * Teams compete tasks to achieve levels within the scheme.
 *
 * @package cerebro
 * @subpackage workbooks
 *
 */
class Events extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array();
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('KisokButton' => 'Show button for Kiosk mode');
	//Object functions and variables go here
	function Settings() {
		// global settings for the workbook module
		return array('FY_start' => '01 April', 'FY_end' => '31 March', 'currency_symbol' => '£', 'decimal_symbol' => '.', 'thousands_separator' => ',');
	}
	public function KisokButton() {
		global $item_id;
		return '
		
		<a href="kiosk.php?event_id=' . $item_id . '" title="Open kiosk form for this event" class="button">Kiosk</a>';
	}
}
