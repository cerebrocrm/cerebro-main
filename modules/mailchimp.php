<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Handles Mailchimp integration
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class MailchimpRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('API' => 'Mailchimp API key', 'DB' => 'contact info db', 'DBnote' => 'contact note db', 'listID' => 'Mailchimp list ID', 'email' => 'email field', 'merge' => 'merge variables: VARNAME=>field', 'query' => 'query targetting relevant contacts');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('MailchimpUpdate' => 'Sends new contacts to Mailchimp', 'ContactUpdate' => 'Perform individual Mailchimp updates');
	//Object functions and variables go here
	
	/**
	 * Sends new contacts to Mailchimp
	 *
	 * @return Status Returns update status
	 */
	public function MailchimpUpdate() {
		require_once 'includes/mailchimp/MCAPI.class.php';
		// TEMP SQL
		global $connectionmanager;
		$mailchimp = mysqli_query($connectionmanager->connection, "SELECT * FROM mailchimp_api LIMIT 0,1");
		// Linked data
		$apikey = $this->link('API') [0];
		while ($row_mailchimp = mysqli_fetch_assoc($mailchimp)) {
			if ($apikey == null) {
				$apikey = $row_mailchimp['apikey'];
			}
			$listId = $row_mailchimp['listId'];
			$last_sync = $row_mailchimp['mailchimp_last_sync'];
		};
		//contacts to target
		$contact = mysqli_query($connectionmanager->connection, "SELECT * FROM history LEFT JOIN contacts on contact_id = history_linked_id LEFT JOIN users ON contact_user = user_id LEFT JOIN locations ON location_id = contact_location WHERE history_date >= $last_sync and (history_type = 'contacts' OR history_type = 'contact') AND (history_status = 1 OR history_status = 2) AND NOT contact_recieve_newsletters = 1 GROUP BY history_linked_id ");
		$contact_batch = Array();
		$row_count = 1;
		while ($row_contact = mysqli_fetch_assoc($contact)) {
			if (is_array($row_contact) && $row_contact['contact_email'] != null) {
				// update the contact!
				/*
				            
				                        [EMAIL] => xxx.yy@ed.ac.uk
				                                [FNAME] => contact_first
				                                [LNAME] => contact_list
				                                [CEREBRO_ID] => contact_id
				                                [TITLE] => contact_title
				                                [DEPT] => contact_company
				                                [PHONENUM] => contact_phone
				                                [WEB] => contact_web
				                                [MMERGE8] => reserved for extra fields - Awards
				                                [MMERGE9] => reserved for extra fields - Climate change
				                                [MMERGE10] => reserved for extra fields - Education for Sust
				                                [MMERGE11] => reserved for extra fields - Energy
				                                [MMERGE12] => reserved for extra fields - Sustainable research
				                                [MMERGE13] => reserved for extra fields - Travel
				                                [MMERGE14] => reserved for extra fields - Waste and recycling
				                                [HOWSPOKE] => contact_how_we_spoke
				                                [LOCATION] => LEFT JOIN location_name on contact_location
				                                [SPOKETO] => LEFT JOIN user_name on contact_user
				                                [MEETING] => contact_specific_meeting
				                                [COUNTRY] => not used
				                                [CELL] => not used
				                                [PROFILE] => not used
				                                [STATUS] => contact_new_status
				                                [WHENSPOKE] => contact_when_we_spoke
				                                [TYPE] => contact_type
				
				            
				*/
				$contact_batch[$row_count] = array('EMAIL' => stripslashes($row_contact['contact_email']), 'FNAME' => stripslashes($row_contact['contact_first']), 'LNAME' => stripslashes($row_contact['contact_last']), 'CEREBRO_ID' => $row_contact['contact_id'], 'TITLE' => $row_contact['contact_title'], 'DEPT' => $row_contact['contact_company'], 'PHONENUM' => $row_contact['contact_phone'], 'WEB' => $row_contact['contact_web'], 'HOWSPOKE' => $row_contact['contact_how_we_spoke'], 'LOCATION' => $row_contact['location_name'], 'SPOKETO' => $row_contact['user_name'], 'MEETING' => $row_contact['contact_specific_meeting'], 'STATUS' => $row_contact['contact_new_status'], 'WHENSPOKE' => $row_contact['contact_when_we_spoke'], 'TYPE' => $row_contact['contact_type']);
				//get custom fields for this contact
				/*record_set('lfields',"SELECT * FROM fields ORDER BY field_title ASC");
				
				            $i = 8; 
				            if ($totalRows_lfields) do {
				
				            record_set('cf',"SELECT * FROM fields_assoc WHERE cfield_contact = ".$row_contact['contact_id']." AND cfield_field = ".$row_lfields['field_id']."");
				
				            $contact_batch[$row_count] = array_merge($contact_batch[$row_count],array('MERGE'.$i => $row_cf['cfield_value']));
				
				            $i++; } while ($row_lfields = mysql_fetch_assoc($lfields));*/
				//
				/*
				            $api = new MCAPI($apikey);
				            
				            
				            $retval = $api->listMemberActivity( $listId, array($row_contact['contact_email']) );
				            // update  note history for each contact (if it exists)
				            
				            if ($api->errorCode){
				                $status .= "Unable to load listMemberActivity()!\n";
				                $status .= "\tCode=".$api->errorCode."\n";
				                $status .= "\tMsg=".$api->errorMessage."\n";
				            } else {
				
				                
				                foreach ($retval[data][0] as $key => $value) {
				                
				                $md5_note_hash = md5($value['action'].$value['timestamp'].$value['url']).'
				                ';
				                
				                record_set('note_hash',"SELECT * FROM notes WHERE note_hash = '$md5_note_hash'");
				                
				                    if ($totalRows_note_hash == 0) {
				        
				                    // convert timestamp
				                    $convert_note_date = strtotime($value['timestamp']);
				            
				                    // open, click, bounce, unsub, abuse, sent to
				            
				                    if ($value['action'] == 'sent') {
				                        $convert_note_type = 6;
				                        $campaign_details = $api->campaigns( array( 'campaign_id'=>$value['campaign_id'] ) );
				                        $campaign_subject = $campaign_details[data][0][subject];
				                        $convert_note_text = 'Sent the newsletter "'.$campaign_subject.'"';
				                
				                    } elseif ($value['action'] == 'open') {
				                        $convert_note_type = 5;
				                        $campaign_details = $api->campaigns( array( 'campaign_id'=>$value['campaign_id'] ) );
				                        $campaign_subject = $campaign_details[data][0][subject];
				                        $convert_note_text = 'Opened the newsletter "'.$campaign_subject.'"';			
				                    } elseif ($value['action'] == 'click') {
				                        $convert_note_type = 4;
				                        $campaign_details = $api->campaigns( array( 'campaign_id'=>$value['campaign_id'] ) );
				                        $campaign_subject = $campaign_details[data][0][subject];
				                        $convert_note_text = 'Clicked on "'.$value['url'].'" in the email "'.$campaign_subject.'"';			
				                    } elseif ($value['action'] == 'bounce') { 
				                        $convert_note_type = 3;
				                        $convert_note_text = 'An email sent to this contact bounced - perhaps its typed wrong?';
				                    } elseif ($value['action'] == 'unsub') { 
				                        $convert_note_type = 3;
				                        $convert_note_text = $row_contact['contact_first'].' has unsubscribed from our newsletters.';
				                    } elseif ($value['action'] == 'abuse') { 
				                        $convert_note_type = 3;
				                        $convert_note_text = $row_contact['contact_first'].' has reported us to Mailchimp for abuse!';
				                    }
				            
				                    //INSERT NOTE FOR CONTACT
				                mysql_query("INSERT INTO notes (note_contact, note_text, note_date, note_type, note_hash) VALUES 
				                    (
				                    ".$row_contact['contact_id'].",
				                    '".addslashes($convert_note_text)."',
				                    ".$convert_note_date.",
				                    $convert_note_type,
				                    '".$md5_note_hash."'
				                    )
				                ");
				
				                }
				                }
				            } */
				$row_count++;
			}
		};
		$api = new MCAPI($apikey);
		$retval = $api->listBatchSubscribe($listId, $contact_batch, false, true, true);
		//listMemberActivity( $listId, array($row_contact['contact_email']) );
		// update  each contact on mailchimp
		$status = '';
		if ($api->errorCode) {
			$status.= "Unable to subscribe people. ";
			$status.= "\tCode=" . $api->errorCode . "\n";
			$status.= "\tMsg=" . $api->errorMessage . "\n";
		} else {
			$status.= 'Successfully subscribed ' . count($contact_batch) . ' contacts.';
			$result = mysqli_query($connectionmanager->connection, "UPDATE mailchimp_api SET

            mailchimp_last_sync = '" . time() . "'

        WHERE 1=1");
		}
		return $status;
	}
	/* Code with hard values removed
	public function MailchimpUpdate()
	   {
		require_once 'includes/mailchimp/MCAPI.class.php';
	       
	       // TEMP SQL
	       global $connectionmanager;
	       $mailchimp = mysqli_query($connectionmanager -> connection, "SELECT * FROM mailchimp_api LIMIT 0,1");
	       
	       // Linked data
	       $apikey = $this->link('API')[0];
	       $email = $this->link('email');
	       $query = $this->link('query');
	       $merge = $this->link('merge');
	       
	       
	       while ($row_mailchimp = mysqli_fetch_assoc($mailchimp)) { 
	           if ($apikey == null) {
	                   $apikey = $row_mailchimp['apikey'];
	           }
	           $listId = $row_mailchimp['listId'];
	           $last_sync = $row_mailchimp['mailchimp_last_sync'];
	
	       };
	
	
	
	       //contacts to target
	       $contact = mysqli_query($connectionmanager -> connection, $query[0]);
	
	       $contact_batch = Array();
	       $row_count = 1;
	
	       while ($row_contact = mysqli_fetch_assoc($contact)) {
	           if (is_array($row_contact) && $row_contact[$email[0]] != null) {
	           
	           
	          
	               
	               $contact_batch[$row_count] = array('EMAIL'=>stripslashes($row_contact[$email[0]]));
	               foreach ($merge as $index => $val){
	               
	                   $contact_batch[$row_count][$index] = stripslashes($row_contact[$val]);
	               
	               }
	               
	           
	           $row_count++;
	           }
	       };
	
	
	       $api = new MCAPI($apikey);
	       $retval = $api->listBatchSubscribe($listId, $contact_batch, false, true, true);
	       // update  each contact on mailchimp
	       $status = '';
	       if ($api->errorCode){
	           $status .= "Unable to subscribe people. ";
	           $status .= "\tCode=".$api->errorCode."\n";
	           $status .= "\tMsg=".$api->errorMessage."\n";
	       } else {
	           $status .= 'Successfully subscribed '.count($contact_batch).' contacts.';
	           $result = mysqli_query($connectionmanager-> connection, "UPDATE mailchimp_api SET
	
	           mailchimp_last_sync = '".time()."'
	
	       WHERE 1=1");
	       }
	
	       return $status;
	
		
	
	}*/
	/**
	 * Check if a mail hash already exists and return true if it does
	 *
	 * @return boolean
	 */
	function CheckHashExists($string) {
		global $connectionmanager;
		$string = preg_replace('/\[.*?\] /', '', $string);
		$hash = md5($string);
		$query = mysqli_query($connectionmanager->connection, "SELECT COUNT(*) FROM global_notes WHERE note_hash = '" . $hash . "'");
		$hashcount = mysqli_fetch_row($query);
		if ($hashcount[0] > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function ContactUpdate() {
		require_once 'includes/mailchimp/MCAPI.class.php';
		global $item_id;
		$apikey = $this->link('API');
		$listId = $this->link('listID');
		$DB = $this->link('DB');
		$DBnote = $this->link('DBnote');
		global $ {
			'module_' . $DB[0]
		};
		$data = $ {
			'module_' . $DB[0]
		}->Query(array('contact_email'), array('contact_id' => $item_id), 'list', $order, 1, '');
		$api = new MCAPI($apikey[0]);
		$email = $data['result']['results'][0]['contact_email'];
		$status = '';
		$retval = $api->listMemberActivity($listId[0], array($email));
		if ($api->errorCode) {
			$status.= "Unable to load listMemberActivity()!\n";
			$status.= "\tCode=" . $api->errorCode . "\n";
			$status.= "\tMsg=" . $api->errorMessage . "\n";
		} else {
			foreach ($retval[data][0] as $key => $value) {
				$md5_note_hash = md5($value['action'] . $value['timestamp'] . $value['url']) . '
                    ';
				if ($this->CheckHashExists($md5_note_hash) !== true) {
					global $ {
						'module_' . $DBnote[0]
					};
					$data = $ {
						'module_' . $DBnote[0]
					}->Query(array('note_id'), array('note_hash' => $md5_note_hash), 'list', $order, 1, '');
					if ($data['result']['num_rows'] == 0) {
						//echo $value['action'].$value['title'];
						// convert timestamp
						$convert_note_date = strtotime($value['timestamp']);
						// open, click, bounce, unsub, abuse, sent to
						if ($value['action'] == 'sent') {
							$convert_note_type = 6;
							$campaign_details = $api->campaigns(array('campaign_id' => $value['campaign_id']));
							$campaign_subject = $campaign_details[data][0][subject];
							$convert_note_text = 'Sent the newsletter "' . $campaign_subject . '"';
						} elseif ($value['action'] == 'open') {
							$convert_note_type = 5;
							$campaign_details = $api->campaigns(array('campaign_id' => $value['campaign_id']));
							$campaign_subject = $campaign_details[data][0][subject];
							$convert_note_text = 'Opened the newsletter "' . $campaign_subject . '"';
						} elseif ($value['action'] == 'click') {
							$convert_note_type = 4;
							$campaign_details = $api->campaigns(array('campaign_id' => $value['campaign_id']));
							$campaign_subject = $campaign_details[data][0][subject];
							$convert_note_text = 'Clicked on <a href ="' . $value['url'] . '">a link</a> in the email "' . $campaign_subject . '"';
						} elseif ($value['action'] == 'bounce') {
							$convert_note_type = 3;
							$convert_note_text = 'An email sent to this contact bounced - perhaps its typed wrong?';
						} elseif ($value['action'] == 'unsub') {
							$convert_note_type = 3;
							$convert_note_text = 'Contact has unsubscribed from our newsletters.';
						} elseif ($value['action'] == 'abuse') {
							$convert_note_type = 3;
							$convert_note_text = 'Contact has reported us to Mailchimp for abuse!';
						}
						//INSERT NOTE FOR CONTACT
						global $connectionmanager;
						mysqli_query($connectionmanager->connection, "INSERT INTO notes (note_contact, note_text, note_date, note_type, note_hash) VALUES 
							(
							" . $item_id . ",
							'" . addslashes($convert_note_text) . "',
							" . $convert_note_date . ",
							$convert_note_type,
							'" . $md5_note_hash . "'
							)
						");
					}
				}
			}
			$status = 'successfully updated contact';
		}
		return '';
	}
}
?>
