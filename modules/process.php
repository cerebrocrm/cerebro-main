<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
error_reporting(1);
/**
 * Creates user editable workbooks.
 *
 * Tasks are assigned to workbooks, and workboks to teams.
 * Teams compete tasks to achieve levels within the scheme.
 *
 * @package cerebro
 * @subpackage process
 *
 */
class ProcessIncoming extends ActiveDirectory {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('actions' => 'How each type of incoming data will be handled', 'notes' => 'Notes to add according to each variable', 'projects' => 'Project ID numbers to link according to each variable', 'cc' => 'email address cc', 'api' => 'Mandrill API key');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('ProcessIncomingData' => 'Handle data in the current incoming queue');
	//Object functions and variables go here
	function Settings() {
		// global settings for this module
		return array('defaults' => '');
	}
	function ProcessIncomingData() {
		global $connectionmanager;
		$projects = $this->link('projects');
		$actions = $this->link('actions');
		$notes = $this->link('notes');
		$ccemail = $this->link('cc');
		global $dbmanager;
		$post = new Post;
		// TEMP SQL
		$tasks = mysqli_query($connectionmanager->connection, "SELECT * FROM incoming_data WHERE 1");
		while ($row = mysqli_fetch_assoc($tasks)) {
			//echo 'process 1<br>';
			if ($row['incoming_type'] == 'contact') {
				// now we process based upon the primary
				$postdata['id'] = null;
				$row['incoming_array'] = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", base64_decode($row['incoming_array']));
				$postdata = unserialize($row['incoming_array']);
				//echo $row['incoming_array'];
				//print_r($projects);
				//echo '<br>';
				$notearray = '';
				foreach ($postdata as $k => $v) {
					if ($k != 'contact_email' && $k != 'note' && $k != 'type' && $k != 'source' && $k != 'redirect' && $k != 'autoemail_subject' && $k != 'autoemail_from' && $k != 'autoemail_text' && $k != 'Send') $notearray.= '<p class="description">' . $k . ': ' . $v . '</p>';
					if (strpos($k, '_0') !== false && $k != 'link_0') {
						$postdata[str_replace('_0', '', $k) ] = $postdata[$k];
						unset($postdata[$k]);
					}
				}
				// link notes
				if (!isset($postdata['no_note'])) $postdata['note'] = '<p class="description">' . $notes[$row['incoming_source']] . ':
				
' . $postdata['note'] . '</p><p class="description">Form data ***** </p>' . $notearray;
				$ccnote = $postdata['note'];
				//check UUN or mail
				$mail = mysqli_real_escape_string($connectionmanager->connection, $row['incoming_primary']);
				if (strpos($mail, '@') !== false) {
					$check_contacts = $dbmanager->Query(array('id', 'name'), array(array('contact_email' => $mail)), null, 0, $this->id);
					//$check_contacts = mysqli_fetch_row(mysqli_query($connectionmanager ->connection ,'SELECT contact_id FROM contacts WHERE contact_email Like "'.$mail.'" AND contact_email != " " AND contact_email IS NOT NULL'));
					
				} else {
					$ad = new ActiveDirectory();
					$check = json_decode($ad->GetSecureJSON($mail));
					//add status check for existing and ID!! TODO
					$mail = $check->email;
					$check_contacts = $dbmanager->Query(array('id', 'name'), array(array('contact_email' => $mail)), null, 0, $this->id);
					//$check_contacts = mysqli_fetch_row(mysqli_query($connectionmanager ->connection ,'SELECT contact_id FROM contacts WHERE contact_email Like "'.$mail.'" AND contact_email != " " AND contact_email IS NOT NULL'));
					
				}
				if ($check_contacts['result_count'] > 0) {
					// duplicate
					$postdata['category'] = 1;
					$postdata['table'] = 'items';
					$postdata['contact_email'] = $mail;
					foreach ($check_contacts['result'] as $checkindex => $checkvalue) {
						$postdata['id'] = $checkvalue['id'];
						$contactname = $checkvalue['name'];
					}
				} else {
					$ad = new ActiveDirectory();
					$check = json_decode($ad->GetSecureJSON($mail));
					$postdata['category'] = 1;
					$postdata['table'] = 'items';
					// add contact
					//print_r($check);
					// check name
					if ($postdata['contact_last'] == null || $postdata['contact_first'] == null) {
						$name = explode(' ', $check->name);
						$postdata['contact_first'] = array_shift($name);
						$postdata['contact_last'] = trim(implode(' ', $name));
					}
					// check contact_type_0
					if ($postdata['contact_type'] == null) {
						if ($check->detail_type == 'Regular staff' || $check->detail_type == 'Staff visitor') {
							if (strpos($check->title, 'Lectur') !== false) {
								$postdata['contact_type'] = 1;
							} elseif (strpos($check->title, 'Prof') !== false) {
								$postdata['contact_type'] = 1;
							} elseif (strpos($check->title, 'Principal') !== false) {
								$postdata['contact_type'] = 1;
							} elseif (strpos($check->title, 'Chair') !== false) {
								$postdata['contact_type'] = 1;
							} elseif (strpos($check->title, 'Teach') !== false) {
								$postdata['contact_type'] = 1;
							} elseif (strpos($check->title, 'Research') !== false) {
								$postdata['contact_type'] = 1;
							} elseif (strpos($check->title, 'Fellow') !== false) {
								$postdata['contact_type'] = 1;
							} elseif (strpos($check->title, 'Lab Manager') !== false) {
								$postdata['contact_type'] = 2;
							} elseif (strpos($check->title, 'Technic') !== false) {
								$postdata['contact_type'] = 2;
							} else {
								$postdata['contact_type'] = 3;
							}
						} elseif ($check->detail_type == "Postgraduate student") {
							$postdata['contact_type'] = 4;
						} elseif ($check->detail_type == "Undergraduate student") {
							$postdata['contact_type'] = 5;
						} else {
							$postdata['contact_type'] = 7;
						}
					}
					// check contact_company
					if ($postdata['contact_company'] == null) {
						$postdata['contact_company'] = $check->org;
					}
					// check contact_email
					if ($postdata['contact_email'] == null) {
						$postdata['contact_email'] = $check->email;
					}
					// check contact_title
					if ($postdata['contact_title'] == null) {
						$postdata['contact_title'] = $check->title;
					}
					// check contact_when_we_spoke
					if ($postdata['contact_when_we_spoke'] == null) {
						$postdata['contact_when_we_spoke'] = date('Y-m-d');
					}
					// contact_specific_meeting
					if ($postdata['contact_specific_meeting'] == null) {
						$postdata['contact_specific_meeting'] = 60084;
					}
					// check contact_user
					if ($postdata['contact_user'] == null) {
						$postdata['contact_user'] = 20;
					}
				}
				//$postdata['contact_recieve_newsletters'] = 1;
				$postdata['redirect'] = '';
				// link projects
				$links = explode(',', $projects[$row['incoming_source']]);
				if (isset($actions[$row['incoming_source']])) $postdata['function'] = $actions[$row['incoming_source']];
				$j = 0;
				if (isset($postdata['link_0']) && $postdata['link_0'] != '') $j = 1;
				for ($i = $j;$i < count($links);$i++) {
					$postdata['link_' . $i] = $links[$i];
				}
				// send post
				//print_r($postdata);
				$_POST = $postdata;
				if ($_POST['contact_email'] == null || $_POST['contact_email'] == '') $_POST['contact_email'] = $row['incoming_primary'];
				if ($_POST['contact_first'] == null || $_POST['contact_first'] == '') $postdata['contact_first'] = $contactname;
				if (($_POST['contact_first'] == null || $_POST['contact_first'] == '') && !(isset($postdata['id']))) $_POST['contact_first'] = 'unknown';
				$_POST['name'] = $_POST['contact_first'] . ' ' . $_POST['contact_last'];
				if ($_POST['name'] == '') $_POST['name'] = $_POST['uun'];
				//print_r($_POST);
				//echo '<br>';
				$insert = $post->CheckPost();
				//echo 'process 2<br>';
				if (isset($ccemail[$row['incoming_source']])) $cc = $ccemail[$row['incoming_source']];
				else $cc = null;
				if ($postdata['link_0'] != '' && $postdata['link_0'] != 0) $this->email_notify($postdata['link_0'], $postdata['contact_first'] . ' ' . $postdata['contact_last'], $cc, $row['incoming_source'], $ccnote);
				// delete row and move on
				mysqli_query($connectionmanager->connection, "DELETE FROM incoming_data WHERE incoming_id = " . $row['incoming_id']);
			} // end contact -  add issue, project, etc types here
			
		} // end while
		return 'incoming data processed';
	} // end ProcessIncomingData
	function email_notify($insert, $contact, $cc, $source, $ccnote) {
		//file_put_contents('email_log.log', ' '.'test', FILE_APPEND | LOCK_EX);
		global $connectionmanager;
		global $dbmanager;
		$item = $dbmanager->Query(array('assigned_to'), array(array('id' => $insert)), null, 0, $this->id, 1);
		$user = $dbmanager->Query(array('user_email'), array(array('id' => $item['result'][$insert]['assigned_to'])), null, 0, $this->id);
		//require 'super-magic-email-sender.php';
		$instance = get_instance();
		$msg = $contact . ' has been assigned to your item: http://' . $instance . '?id=' . $insert;
		$newpost = new Post;
		$result = $newpost->Email(trim($user['result'][$item['result'][$insert]['assigned_to']]['user_email']), array('text' => $msg, 'subject' => 'New link with an item assigned to you'));
		if ($cc != null) {
			$message = mysqli_query($connectionmanager->connection, "SELECT * FROM auto_emails WHERE source LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $source . '_' . $cc) . "'");
			if (mysqli_num_rows($message) > 0) {
				$result = $newpost->Email(trim($cc), array('text' => $message['text'] . $ccnote, 'subject' => $message['subject']));
			}
		}
		//print_r($result);
		//file_put_contents('email_log.log', ' '.$msg, FILE_APPEND | LOCK_EX);
		return 1;
	}
} // end module
