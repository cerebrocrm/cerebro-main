<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Generates list of items of data
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class FreeList extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('query' => 'DB query', 'column_type' => 'column data format', 'fill_data' => 'column fill data', 'column_class' => 'column div class', 'options' => 'numeric relation options', 'header' => 'header link');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetList' => 'Returns list', 'GetCount' => 'Returns number of items in list');
	//Object functions and variables go here
	var $count;
	var $check;
	//Object functions and variables go here

	/**
	 * Gets number of items in list
	 *
	 * @return Count Returns count of items in the list
	 */
	public function GetCount() {
		if ($this->check == 1) return $this->count;
		else {
			$query = $this->link('query');
			global $connectionmanager;
			global $item_id;
			$query = mysqli_real_escape_string($connectionmanager->connection, str_replace('item_id', $item_id, $query[0]));
			$data = mysqli_query($connectionmanager->connection, $query);
			//Set count variable
			$resultcount = mysqli_query($connectionmanager->connection, "SELECT FOUND_ROWS()");
			$tempcount = mysqli_fetch_row($resultcount);
			$this->count = $tempcount[0];
			$this->check = 1;
			return $this->count;
		}
	}
	/**
	 * Generates list of items
	 *
	 * @return Content Returns html/scripting representation of list
	 */
	public function GetList() {
		//error_reporting(1);
		//Get setup data, query DBmanager
		$column_type = $this->link('column_type');
		$column_class = $this->link('column_class');
		$fill_data = $this->link('fill_data');
		$query = $this->link('query');
		$header = $this->link('header');
		$options = $this->link('options');
		$numfields = count($column_type);
		$listplace = 0;
		global $item_id;
		global $connectionmanager;
		global $page;
		$query = mysqli_real_escape_string($connectionmanager->connection, str_replace('item_id', $item_id, $query[0]));
		$data = mysqli_query($connectionmanager->connection, $query);
		//Set count variable
		$resultcount = mysqli_query($connectionmanager->connection, "SELECT FOUND_ROWS()");
		$tempcount = mysqli_fetch_row($resultcount);
		$this->count = $tempcount[0];
		$this->check = 1;
		$output = $header[0];
		$j = 0;
		$output.= '<div class="row info_subtab issues_display">';
		if (mysqli_num_rows($data) > 0) {
			while ($row = mysqli_fetch_assoc($data)) {
				if ($j == 0) {
					$output.= '<div class="row primary_item">';
					$j = 1;
				} else {
					$output.= '<div class="row primary_item even">';
					$j = 0;
				}
				for ($i = 0;$i <= $numfields - 1;$i++) {
					$function = $column_type[$i];
					$output.= '<div class="' . $column_class[$i] . '">';
					if (array_key_exists($fill_data[$i], $options)) {
						$optionlisttemp = explode(',', $options[$fill_data[$i]]);
						$optionlist = array();
						foreach ($optionlisttemp as $opt) {
							$temp = explode(':', $opt);
							$optionlist[$temp[0]] = $temp[1];
						}
						$row[$fill_data[$i]] = $optionlist[$row[$fill_data[$i]]];
					}
					$output.= $this->$function($row, $fill_data[$i]);
					$output.= '</div>';
				}
				$output.= "</div>";
				$k++;
			}
		}
		$output.= '</div>';
		return $output;
		if ($this->permissions['GetList'] <= Users::GetCurrentUser()['user_level']) {
			return $output;
		} else {
			return null;
		}
	}
	/**
	 * Format description type data
	 *
	 * @return Content Returns formatted data
	 */
	function description($row, $fill_data) {
		return stripslashes($row[$fill_data]);
	}
	/**
	 * Format detail type data
	 *
	 * @return Content Returns formatted data
	 */
	function detail($row, $fill_data) {
		$fill_data = explode(',', $fill_data);
		return '<p class="issue"><a href = "?page=' . $fill_data[2] . '&id=' . $row[$fill_data[1]] . '">' . stripslashes($row[$fill_data[0]]) . '</a></p>';
	}
}
?>
