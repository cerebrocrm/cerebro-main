<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-15 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates statistics for social media networks
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class SocialStats extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('twitter' => 'Twitter accounts: description -> account', 'facebook' => 'Facebook accounts: description -> ID', 'youtube' => 'Youtube accounts:  description -> short name', 'instagram' => 'Instagram accounts: description -> user name', 'facebook_appid' => 'Facebook App ID', 'facebook_secret' => 'Facebook app secret');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetSocialStats' => 'Get the stats', 'SocialGraphDaily' => 'Display a graph', 'ContactGraphDaily' => 'Display a graph of sign ups', 'GetContactStats' => 'Upload information abount the number of contacts by type.');
	//Object functions and variables go here
	function upload_stats($network, $account, $users) {
		global $connectionmanager;
		$network = mysqli_real_escape_string($connectionmanager->connection, $network);
		$account = mysqli_real_escape_string($connectionmanager->connection, $account);
		$users = mysqli_real_escape_string($connectionmanager->connection, $users);
		$do = mysqli_query($connectionmanager->connection, "INSERT INTO socialstats (network, account, users, date) VALUES
						(
						'" . $network . "',
						'" . $account . "',
						'" . $users . "',
						'" . date("Y-m-d") . "'
						)
					");
	}
	//Get Facebook Likes Count of a page
	function fbLikeCount($id, $appid, $appsecret) {
		//Construct a Facebook URL
		$json_url = 'https://graph.facebook.com/' . $id . '?access_token=' . $appid . '|' . $appsecret . '&fields=likes';
		$json = file_get_contents($json_url);
		$json_output = json_decode($json);
		//Extract the likes count from the JSON object
		if ($json_output->likes) {
			return $json_output->likes;
		} else {
			return 0;
		}
	}
	function GetSocialStats() {
		$totalfollowers = 0;
		$twitter = $this->link('twitter');
		$facebook = $this->link('facebook');
		$youtube = $this->link('youtube');
		$instagram = $this->link('instagram');
		$facebook_appid = $this->link('facebook_appid') [0];
		$facebook_secret = $this->link('facebook_secret') [0];
		// TWITTER
		foreach ($twitter as $n => $url) {
			$text = file_get_contents('http://www.twitter.com/' . $url);
			$doc = new DOMDocument();
			$doc->loadHTML($text);
			$results = $doc->getElementsByTagName('li');
			if ($results->length > 0) {
				foreach ($results as $result) {
					if (strpos($result->getAttribute('class'), 'ProfileNav-item--followers') > 0) {
						$data = preg_replace('/[^0-9]/', '', $result->textContent);
						$data = intval($data);
						$totalfollowers = $totalfollowers + $data;
						$twitter[$n] = $data;
						$this->upload_stats('twitter', $n, $data);
					}
				}
			}
		}
		// FACEBOOK
		foreach ($facebook as $n => $url) {
			//This Will return like count of the Facebook page
			$data = $this->fbLikeCount($url, $facebook_appid, $facebook_secret);
			$totalfollowers = $totalfollowers + $data;
			$facebook[$n] = $data;
			$this->upload_stats('facebook', $n, $data);
		}
		// YOUTUBE
		$n = 0;
		foreach ($youtube as $n => $url) {
			$text = file_get_contents('https://www.youtube.com/user/' . $url . '/about');
			$doc = new DOMDocument();
			$doc->loadHTML($text);
			$results = $doc->getElementsByTagName('span');
			if ($results->length > 0) {
				foreach ($results as $result) {
					if (strpos($result->getAttribute('class'), 'subscriber-count') > 0) {
						$data = preg_replace('/[^0-9]/', '', $result->textContent);
						$data = intval($data);
						$totalfollowers = $totalfollowers + $data;
						$youtube[$n] = $data;
						$this->upload_stats('youtube', $n, $data);
					}
				}
			}
		}
		// INSTAGRAM
		$n = 0;
		foreach ($instagram as $n => $url) {
			$text = file_get_contents('https://www.instagram.com/web/search/topsearch/?query=' . $url);
			$json_output = json_decode($text, true);
			foreach ($json_output['users'] as $user) {
				$data = ($user['user']['follower_count']);
				//Extract the likes count from the JSON object
				echo $data;
				if ($data) {
					$data = intval($data);
					$instagram[$n] = $data;
					$totalfollowers = $totalfollowers + $data;
					$this->upload_stats('instagram', $n, $data);
				}
			}
		}
		$results = Array('total' => $totalfollowers, 'twitter' => $twitter, 'facebook' => $facebook, 'youtube' => $youtube, 'instagram' => $instagram);
		$output = json_encode($results);
		return $output;
	}
	function count_contacts($type) {
		global $dbmanager;
		$thisr = 1010101;
		$_GET = Array('j_' . $thisr => 0, 'field_' . $thisr . '_0' => 'contact_type', 'op_' . $thisr . '_0' => 'equal', 'inf_' . $thisr . '_0' => 'reg_' . $thisr, 'parama_' . $thisr . '_0' => $type, 'and_' . $thisr . '_1' => 'AND', 'field_' . $thisr . '_1' => 'contact_retired', 'op_' . $thisr . '_1' => 'equal', 'inf_' . $thisr . '_1' => 'reg_' . $thisr, 'parama_' . $thisr . '_1' => 0,);
		$results = $dbmanager->Query(Array('id'), Array(Array('category' => 1)), null, 0, $thisr);
		return $results['result_count'];
	}
	function count_new_contacts($type, $id_number) {
		$count = 0;
		global $dbmanager;
		$thisr = 1010102;
		$_GET = Array('j_' . $thisr => 0, 'field_' . $thisr . '_0' => 'contact_type', 'op_' . $thisr . '_0' => 'equal', 'inf_' . $thisr . '_0' => 'reg_' . $thisr, 'parama_' . $thisr . '_0' => $type, 'and_' . $thisr . '_1' => 'AND', 'field_' . $thisr . '_1' => 'contact_retired', 'op_' . $thisr . '_1' => 'equal', 'inf_' . $thisr . '_1' => 'reg_' . $thisr, 'parama_' . $thisr . '_1' => 0,);
		$results = $dbmanager->Query(Array('id'), Array(Array('category' => 1)), null, 0, $thisr);
		foreach ($results['result'] as $result) {
			if ($result['id'] > $id_number) {
				$count++;
			}
		}
		return $count;
	}
	function GetContactStats() {
		global $dbmanager;
		$post = new Post();
		global $connectionmanager;
		$academics = $this->count_contacts(1);
		$technical = $this->count_contacts(2);
		$nontechnical = $this->count_contacts(3);
		$undergraduate = $this->count_contacts(4);
		$postgraduate = $this->count_contacts(5);
		$alumni = $this->count_contacts(6);
		$media = $this->count_contacts(7);
		$other = $this->count_contacts(0);
		$total = array_sum(array($academics, $technical, $nontechnical, $postgraduate, $undergraduate, $alumni, $media, $other));
		$students_total = ($undergraduate + $postgraduate);
		$staff_total = array_sum(array($academics, $technical, $nontechnical));
		$other_total = array_sum(array($other, $media, $alumni));
		$this->upload_stats('contact-stats', 'students_total', $students_total);
		$this->upload_stats('contact-stats', 'staff_total', $staff_total);
		$this->upload_stats('contact-stats', 'other_total', $other_total);
		// new contacts
		$new_contacts = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT COUNT(*) FROM items WHERE id > 85908 and category = 1 AND name NOT like '%test%' AND name NOT like '%unknown%' AND name is not null")) [0];
		$new_academics = $this->count_new_contacts(1, 85908);
		$new_technical = $this->count_new_contacts(2, 85908);
		$new_nontechnical = $this->count_new_contacts(3, 85908);
		$new_undergraduate = $this->count_new_contacts(4, 85908);
		$new_postgraduate = $this->count_new_contacts(5, 85908);
		$new_alumni = $this->count_new_contacts(6, 85908);
		$new_media = $this->count_new_contacts(7, 85908);
		$new_other = $this->count_new_contacts(0, 85908);
		$new_students_total = ($new_undergraduate + $new_postgraduate);
		$new_staff_total = array_sum(array($new_academics, $new_technical, $new_nontechnical));
		$new_other_total = array_sum(array($new_other, $new_media, $new_alumni));
		$this->upload_stats('new-contact-stats', 'students_total', $new_students_total);
		$this->upload_stats('new-contact-stats', 'staff_total', $new_staff_total);
		$this->upload_stats('new-contact-stats', 'other_total', $new_other_total);
		$results = Array('students' => $students_total, 'staff' => $staff_total, 'other' => $other_total, 'new_students' => $new_students_total, 'new_staff' => $new_staff_total, 'new_other' => $new_other_total);
		$output = json_encode($results);
		return $output;
	}
	function SocialGraphDaily() {
		global $connectionmanager;
		$twitter = $this->link('twitter');
		$facebook = $this->link('facebook');
		$youtube = $this->link('youtube');
		$instagram = $this->link('instagram');
		$output = '
		  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
		  <script type="text/javascript">
    google.load("visualization", "1", {packages: [\'annotationchart\']});
    google.setOnLoadCallback(drawSignUps);

    function drawSignUps()
    {';
		$chartdata = array();
		$accounts = array();
		foreach ($twitter as $n => $url) {
			$accounts[] = $n;
		}
		foreach ($facebook as $n => $url) {
			$accounts[] = $n;
		}
		foreach ($youtube as $n => $url) {
			$accounts[] = $n;
		}
		foreach ($instagram as $n => $url) {
			$accounts[] = $n;
		}
		$no_accounts = count($accounts);
		$data = mysqli_query($connectionmanager->connection, "SELECT network, account, users, date FROM socialstats WHERE network = 'youtube' OR network = 'facebook' OR network = 'twitter' OR network = 'instagram' ORDER BY date ASC");
		$current_row = '';
		while ($row = mysqli_fetch_assoc($data)) {
			$chartdata[$row['date']]['total'] = $chartdata[$row['date']]['total'] + $row['users'];
			$chartdata[$row['date']][$row['account']] = $row['users'];
		}
		$datatable = '["Date",';
		for ($i = 0;$i <= $no_accounts - 1;$i++) {
			$datatable.= '"' . $accounts[$i] . '"';
			$datatable.= ',';
		}
		$datatable.= '{type: \'string\', role: \'annotation\'}],
';
		$d = 0;
		$count_d = count($chartdata);
		foreach ($chartdata as $id => $value) {
			$usedate = 'new Date("' . $id . '")';
			$datatabletemp = '[' . $usedate . ',';
			$t = 0;
			for ($i = 0;$i <= $no_accounts - 1;$i++) {
				$datatabletemp.= $chartdata[$id][$accounts[$i]];
				$datatabletemp.= ',';
				$t = ($t + $chartdata[$id][$accounts[$i]]);
			}
			if ($d == 0 || ($d == round(($count_d / 2 - 1), 0) && $count_d > 10) || $d == $count_d - 1) {
				$datatabletemp.= '"' . $chartdata[$id]['total'] . '"],
';
			} else {
				$datatabletemp.= 'null],
';
			}
			if ($t > 0) {
				$datatable.= $datatabletemp;
			}
			$d++;
		}
		$datatable = rtrim($datatable, ',');
		$output.= "var data = google.visualization.arrayToDataTable([
" . $datatable . "
        ]);
        
        
        var options = {
        theme: 'maximized',
        chartArea: {width: '90%', height: '90%'},
        isStacked: true,
        displayZoomButtons: false,
        hAxis: {
          title: 'Date'
        },
        vAxis: {
          title: 'Number of users'
        }
      };

      var chart = new google.visualization.AnnotationChart(document.getElementById('chart_div'));
      chart.draw(data, options);
}
      </script>
      <div id=\"chart_div\" style=\"height: 500px;\"> </div>";
		return $output;
	}
	function ContactGraphDaily() {
		global $connectionmanager;
		$output = '
		  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
		  <script type="text/javascript">
    google.load("visualization", "1", {packages: [\'corechart\']});
    google.setOnLoadCallback(drawSignUps);

    function drawSignUps()
    {';
		$chartdata = array();
		$data = mysqli_query($connectionmanager->connection, "SELECT account, date, users as sum FROM socialstats WHERE network = 'contact-stats' ORDER BY date ASC
      	
      	");
		$current_row = '';
		$current_total = 0;
		while ($row = mysqli_fetch_assoc($data)) {
			$chartdata[$row['date']][$row['account']] = ($row['sum']);
		}
		$datatable = '["Date","Other","Staff","Students",';
		$datatable.= '{type: \'string\', role: \'annotation\'}],
';
		$d = 0;
		$count_d = count($chartdata);
		foreach ($chartdata as $id => $value) {
			$usedate = 'new Date("' . $id . '")';
			$datatable.= '[' . $usedate . ',' . $chartdata[$id]['other_total'] . ',' . $chartdata[$id]['staff_total'] . ',' . $chartdata[$id]['students_total'] . ',';
			if ($d == 0 || ($d == round(($count_d / 2 - 1), 0) && $count_d > 10) || $d == $count_d - 1) {
				$datatable.= '"' . array_sum($chartdata[$id]) . '"],
';
			} else {
				$datatable.= 'null],
';
			}
			$d++;
		}
		$datatable = rtrim($datatable, ',');
		$output.= "var data = google.visualization.arrayToDataTable([
" . $datatable . "
        ]);
        
        
        var options = {
        theme: 'maximized',
        chartArea: {width: '90%', height: '90%'},
        isStacked: true,
        hAxis: {
          title: 'Date'
        },
        vAxis: {
          title: 'Number of contacts'
        }
      };

      var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
      chart.draw(data, options);
}
      </script>
      <div id=\"chart_div\" style=\"height: 500px;\"> </div>";
		return $output;
	}
}
