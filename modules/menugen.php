<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates menu for search functionality
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class MenuGenRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('Tabs' => 'name of each tab', 'default' => 'default selected tab', 'List' => 'Listgen instance this list is linked to', 'cat' => 'DB cat');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetSearch' => 'returns tabbed search menu');
	//Object functions and variables go here
	
	/**
	 * Assemble full menu
	 *
	 * @return string HTML/Javascript representing full menu
	 */
	public function GetSearch() {
		global $page;
		$params = $this->link('Tabs');
		$list = $this->link('List');
		$default = $this->link('default');
		$tab = 'tabs_' . $this->id;
		$output = '		<div class = "row top_part main_search">
                        
                        <div class="seven columns alpha title_bar " >
                            <h2>Advanced search</h2>
                        </div>
						<div class="three columns omega action_bar">
								<div class=" advanced_search_toggle"></div>
						</div>
                        <div class = "row">
						<form action="" method="get">
						<input type = "hidden" name = "page" value = "' . $page . '">
                        <div class="row info_section search_results">
                            <div class="five columns alpha">
                                <h4>Search for...</h4>
                            </div>
                            <div class="ten columns omega sub_nav">
                                <div class="tabs">
                                ';
		$script = '<script>';
		$script2 = '<script>';
		$contentoutput = '';
		$menuoutput = '';
		//Hide box if query exists
		if (isset($_GET['tab'])) $script2.= '	//<![CDATA[
                    $(document).ready(function () {

                        var original_height = $(".main_search").height()

                        $(".main_search").children(".row").hide(0);
                        $(".advanced_search_toggle").html("<div class = \'button\'><img src=\'images/edit_icon.png\' />Edit</div>");

                        var var_state = 1;


                        $(".advanced_search_toggle").click(function (event) {


                            if (var_state == 1) {
                                event.preventDefault();
                                $(".main_search").animate({
                                    height: original_height
                                  }, 200, function () {
                                    $(".main_search").children(".row").fadeIn("fast");
                                    $(".main_search").css("height", "auto")
                                  });
                                var_state = 0;
                                $(".advanced_search_toggle").html("<div class = \'button\'><img src=\'images/close_icon.png\' />Close</div>");
                            } else {
                                event.preventDefault();
                                $(".main_search").children(".row").hide(0);
                                $(".advanced_search_toggle").html("<div class = \'button\'><img src=\'images/edit_icon.png\' />Edit</div>");
                                var_state = 1;

                            }
                        });

                    });

                    //]]>
                ';
		//Check for tab override
		if (isset($_GET['tab'])) {
			if (($key = array_search($_GET['tab'], $list)) !== false) {
				$default[0] = $params[$key];
			}
		}
		//Generate tab code
		for ($i = 0;$i < count($params);$i++) {
			//Start content fetch if query exists
			if (isset($_GET['tab'])) {
				global $ {
					'module_' . $list[$i]
				};
				$search = $ {
					'module_' . $list[$i]
				}->GetList();
				$count = $ {
					'module_' . $list[$i]
				}->GetCount();
			} else $search = null;
			if ($params[$i] == $default[0]) {
				$select = 'class="selected ' . $tab . '_button"';
				$script2.= '$(function () {
                    $( ".' . $tab . '" ).hide();
					$( ".searchitem" ).prop( "disabled", true );
					$( ".' . $list[$i] . '" ).prop( "disabled", false );
                    $( ".' . $params[$i] . '" ).show();
                });
                ';
			} else $select = 'class="' . $tab . '_button"';
			$script.= '$( "#' . $params[$i] . '_button" ).click(function (e) {
                            $( ".' . $tab . '_button" ).removeClass("selected");
                            $( ".' . $tab . '" ).hide();
							$( ".searchitem" ).prop( "disabled", true );
							$( ".' . $list[$i] . '" ).prop( "disabled", false );
                            $( "#' . $params[$i] . '_button" ).addClass("selected");
                            $( ".' . $params[$i] . '" ).show();
                            e.preventDefault();
                        });
                        ';
			$output.= '<a href="#" id= "' . $params[$i] . '_button" ' . $select . '>' . $params[$i] . '</a>';
			$menuoutput.= '<div class = "' . $params[$i] . ' ' . $tab . '">' . $this->GenMenu($list[$i], $i) . '</div>';
			if ($search != null && $_GET['tab'] == $list[$i]) $contentoutput.= '<div class = "' . $params[$i] . ' ' . $tab . ' row info_subtab issues_display"><div class ="row info_section"><div class="ten columns omega sub_nav"><div class="tabs"><a href="#" class="selected"><span class="big_number">' . $count . '</span>' . $params[$i] . '</a></div></div></div><div class = "row info_subtab issues_display">' . $search . '</div></div>';
		}
		$output.= '</div></div></div>
					' . $menuoutput;
		$output.= '
                    <div class="row">
                        <div class="ten columns omega action_bar">
                            <input type="submit" class="button" value = "search">
                        </div>
                    </div>
					</div>
					</form>
					</div>
                    ';
		$script.= '</script>';
		$script2.= '</script>';
		return $output . $contentoutput . $script . $script2;
	}
	/**
	 * Assemble set of menu elements
	 *
	 * @return string HTML/Javascript representing menu element set
	 */
	public function GenMenu($listparam, $j) {
		$list = array($listparam);
		$i = 0;
		global $page;
		$output = '<div class="search_' . $list[0] . '"><div class="row">
				
                    <div class="ten columns omega action_bar">
                    <a href="#" class="button add_criteria_' . $list[0] . '"><img src="images/add_icon.png" alt="Add"/> Criteria</a>
                    </div>
                </div>';
		if ($_GET['tab'] == $list[0]) {
			foreach ($_GET as $index => $value) {
				if (substr($index, 0, 5) == 'field') {
					$info = explode('_', $index);
					$output.= $this->GetMenu($info[2], $listparam, $j);
					$i++;
				}
			}
		}
		if ($i == 0) $output.= $this->GetMenu(0, $listparam, $j);
		if ($i > 0) $i--;
		$output.= '<input type="hidden" class ="searchitem ' . $list[0] . '" name="tab" value="' . $list[0] . '">';
		$output.= '</div><script>
                     $(document).ready(function () {
                        var counter_' . $list[0] . ' = ' . $i . ';
                        $("body").on("click",".add_criteria_' . $list[0] . '",function (event) {
                            counter_' . $list[0] . ' += 1;
                            $(".search_' . $list[0] . '").append($("<div>").load("?page=' . $page . '&instance=' . $this->id . '&list=' . $listparam . '&j_' . $list[0] . '=' . $j . '&method=GetMenu&i="+counter_' . $list[0] . ', function() {
                             $("select").chosen({disable_search_threshold: 5});
                            }));
                            
                            event.preventDefault();
                        });
                        
                        $("body").on("click",".remove_criteria_' . $this->id . '",function (event) {
                            $(this).parent().parent().remove();
                            counter_' . $this->id . ' -= 1;
                            event.preventDefault();
                        });
                    });
                    </script>';
		return $output;
	}
	/**
	 * Generate menu elements
	 *
	 * @return string HTML/Javascript representing menu elements
	 */
	public function GetMenu($i, $list, $j) {
		$list = array($list);
		if (isset($_GET['i'])) $i = $_GET['i'];
		if (isset($_GET['list'])) $list = array($_GET['list']);
		if (isset($_GET['j_' . $list[0]])) $j = $_GET['j_' . $list[0]];
		$DB = $list[0];
		global $page;
		$output = '<div class="row primary_item">';
		$DBinst = $list;
		global $dbmanager;
		$cat = $this->link('cat');
		$fields = $dbmanager->AllFields($cat[$j]);
		$catselect = $j;
		//List of each parameter type for scripting
		$option = '';
		$tiny = '';
		$date = '';
		//If not first criteria, add and/or field
		if ($i != 0) {
			$andget = '';
			$orget = '';
			if (isset($_GET['and_' . $DBinst[0] . '_' . $i])) {
				if ($_GET['and_' . $DBinst[0] . '_' . $i] == 'OR') $orget = ' selected ';
				else $andget = ' selected ';
			}
			$output.= '<div class="one column search_and alpha">
                        <select id="and_' . $DBinst[0] . '_' . $i . '" class="' . $list[0] . '" name="and_' . $DBinst[0] . '_' . $i . '">

                                <option value="AND"' . $andget . '>AND</option>
                                <option value="OR"' . $orget . '>OR</option>

                        </select>
                    </div>';
		} else {
			$output.= '<div class="one column search_and alpha">
                    <p> </p>
                    </div>';
		}
		//Generate main field list
		$output.= '<div class="three columns"><input type="hidden" name="j_' . $DBinst[0] . '" value ="' . $j . '"><select id = "field_' . $DBinst[0] . '_' . $i . '" class="searchitem ' . $list[0] . '" name = field_' . $DBinst[0] . '_' . $i . '>';
		$j = 0;
		foreach ($fields as $index => $value) {
			$type = 'default';
			if ($value['data_format'] == 1) $type = 'date';
			if ($value['data_format'] == 2) $value['friendly_name'] = 'Interest: ' . $value['friendly_name'];
			if (is_array($value['options'])) $type = 'option';
			if ($value['type'] == 3) $type = 'tiny';
			//Store scripting data
			switch ($type) {
				case 'option':
					$option.= ' case "' . $index . '":
                    ';
					$option.= '$("#ops_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = ops_' . $DBinst[0] . '_' . $i . '><div class=\'two columns\'><select id = op_' . $DBinst[0] . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' name = op_' . $DBinst[0] . '_' . $i . '><option value=equal>Equals</option><option value=not_equal>Doesn\'t equal</option></select></div></div> ");
                    $("#params_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = \'params_' . $DBinst[0] . '_' . $i . '\'><div class=\'three columns\'><input type=hidden class=\'searchitem ' . $list[0] . '\' name = \'inf_' . $DBinst[0] . '_' . $i . '\' value = \'reg_' . $DBinst[0] . '\'>';
					$option.= '<select class=\'searchitem ' . $list[0] . '\' id = parama_' . $DBinst[0] . '_' . $i . ' name = parama_' . $DBinst[0] . '_' . $i . '>';
					foreach ($value['options'] as $optindex => $optval) {
						$option.= '<option value =\'' . $optindex . '\'>' . $optval . '</option>';
					}
					$option.= '</select></div></div>");
                    ';
					$option.= '
                    $("select").chosen({disable_search_threshold: 5});
                    break;
                    ';
				break;
				case 'date':
					$date.= 'case "' . $index . '":
                    ';
				break;
				case 'tiny':
					$tiny.= 'case "' . $index . '":
                    ';
				break;
				default:
				break;
			}
			//If no option is preselected, generate further parts of menu from first item, else generate based upon GET
			if (($j == 0 && !(isset($_GET['field_' . $DBinst[0] . '_' . $i]))) || (isset($_GET['field_' . $DBinst[0] . '_' . $i]) && $_GET['field_' . $DBinst[0] . '_' . $i] == $index)) {
				$menuitems = $this->MenuOptions($type, $DBinst[0], $value['options'], $i, $list[0]);
				$output.= '<option value = "' . $index . '" selected>' . $value['friendly_name'] . '</option>';
			} else {
				$output.= '<option value = "' . $index . '">' . $value['friendly_name'] . '</option>';
			}
			$j++;
		}
		$output.= '</select></div>';
		//Add other menu items
		$output.= '<div id = ops_' . $DBinst[0] . '_' . $i . '><div class=\'two columns\'>' . $menuitems[0] . '</div></div>';
		$output.= '<div id = params_' . $DBinst[0] . '_' . $i . '>' . $menuitems[1] . '</div>';
		$output.= '<div class="one columns action_bar omega">
                        <a href="#" class="button remove_criteria_' . $this->id . '" ><img class="solo" src="images/remove_icon.png" alt="Remove criteria" title="Remove criteria"/></a>
                    </div>';
		$output.= '</div>';
		//Add scripting
		$output.= '<script>
            $( document ).ready(function() {
             var fieldtype = $("#field_' . $DBinst[0] . '_' . $i . '").val();
			$( "#parama_' . $DBinst[0] . '_' . $i . '" ).autocomplete({
				source: "?page=auto&type=' . $cat[$catselect] . '&field_1_0="+fieldtype,
				minLength:3
			});	
            
            $("#op_' . $DBinst[0] . '_' . $i . '").change(function () {
            
                                var $dropdown_' . $DBinst[0] . '_' . $i . '_3 = $(this);
                                var  key_' . $DBinst[0] . '_' . $i . '_3 = $dropdown_' . $DBinst[0] . '_' . $i . '_3.val();
                                var fieldtype = $("#field_' . $DBinst[0] . '_' . $i . '").val();
                                switch (fieldtype) {
                                case "undefineddate":
                                ' . $date . '
                                    if (key_' . $DBinst[0] . '_' . $i . '_3 == "between") {
                                    $("#params_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = params_' . $DBinst[0] . '_' . $i . '><input type=hidden class=\'searchitem ' . $list[0] . '\' name = inf_' . $DBinst[0] . '_' . $i . ' value = reg_' . $DBinst[0] . '><div class=\'one-point-five columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $DBinst[0] . '_' . $i . '\' name=\'parama_' . $DBinst[0] . '_' . $i . '\'  /></div><div class=\'one-point-five columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'paramb_' . $DBinst[0] . '_' . $i . '\' name=\'paramb_' . $DBinst[0] . '_' . $i . '\' /></div></div>");
                                    $( "#parama_' . $DBinst[0] . '_' . $i . '" ).datepicker({ dateFormat: "yy-mm-dd", autoSize: true });
                                    $( "#paramb_' . $DBinst[0] . '_' . $i . '" ).datepicker({ dateFormat: "yy-mm-dd", autoSize: true });
                                    } else {
                                    $("#params_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = params_' . $DBinst[0] . '_' . $i . '><input type=hidden class=\'searchitem ' . $list[0] . '\' name = inf_' . $DBinst[0] . '_' . $i . ' value = reg_' . $DBinst[0] . '><div class=\'three columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $DBinst[0] . '_' . $i . '\' name=\'parama_' . $DBinst[0] . '_' . $i . '\'  /></div></div>");
                                    $( "#parama_' . $DBinst[0] . '_' . $i . '" ).datepicker({ dateFormat: "yy-mm-dd", autoSize: true });
                                    }
                                break;
                                default:
                                break;
                                }
                                 
                            });
            $("#field_' . $DBinst[0] . '_' . $i . '").change(function () {
            var $dropdown_' . $DBinst[0] . '_' . $i . ' = $(this);
            var key_' . $DBinst[0] . '_' . $i . ' = $dropdown_' . $DBinst[0] . '_' . $i . '.val();
            ';
		$output.= 'switch (key_' . $DBinst[0] . '_' . $i . ') {
        ';
		$output.= $option;
		$output.= 'case "undefinedtinyint":
        ' . $tiny;
		$output.= '$("#ops_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = ops_' . $DBinst[0] . '_' . $i . '><div class=\'two columns\'><input type=hidden class=\'searchitem ' . $list[0] . '\' name = op_' . $DBinst[0] . '_' . $i . ' value = equal><select id = parama_' . $DBinst[0] . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' name = parama_' . $DBinst[0] . '_' . $i . '><option value=1>Yes</option><option value=0>No</option></select></div></div> ");
                    $("#params_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = params_' . $DBinst[0] . '_' . $i . '><input type=hidden name = inf_' . $DBinst[0] . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' value = reg_' . $DBinst[0] . '></div>");
                    $("select").chosen({disable_search_threshold: 5});
                    break;
                    ';
		$output.= 'case "undefineddate":
                    ' . $date;
		$output.= '$("#ops_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = ops_' . $DBinst[0] . '_' . $i . '><div class=\'two columns\'><select id = op_' . $DBinst[0] . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' name = op_' . $DBinst[0] . '_' . $i . '><option value=between>Between</option><option value=equal>On</option></select></div></div> ");
                $("#params_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = params_' . $DBinst[0] . '_' . $i . '><input type=hidden class=\'searchitem ' . $list[0] . '\' name = inf_' . $DBinst[0] . '_' . $i . ' value = reg_' . $DBinst[0] . '><div class=\'one-point-five columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $DBinst[0] . '_' . $i . '\' name=\'parama_' . $DBinst[0] . '_' . $i . '\'  /></div><div class=\'one-point-five columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'paramb_' . $DBinst[0] . '_' . $i . '\' name=\'paramb_' . $DBinst[0] . '_' . $i . '\' /></div></div>");
                $( "#parama_' . $DBinst[0] . '_' . $i . '" ).datepicker({ dateFormat: "yy-mm-dd", autoSize: true });
                $( "#paramb_' . $DBinst[0] . '_' . $i . '" ).datepicker({ dateFormat: "yy-mm-dd", autoSize: true });
                $("#op_' . $DBinst[0] . '_' . $i . '").change(function () {
                                var $dropdown_' . $DBinst[0] . '_' . $i . '_2 = $(this);
                                var  key_' . $DBinst[0] . '_' . $i . '_2 = $dropdown_' . $DBinst[0] . '_' . $i . '_2.val();
                                if (key_' . $DBinst[0] . '_' . $i . '_2 == "between") {
                                $("#params_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = params_' . $DBinst[0] . '_' . $i . '><input type=hidden class=\'searchitem ' . $list[0] . '\' name = inf_' . $DBinst[0] . '_' . $i . ' value = reg_' . $DBinst[0] . '><div class=\'one-point-five columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $DBinst[0] . '_' . $i . '\' name=\'parama_' . $DBinst[0] . '_' . $i . '\'  /></div><div class=\'one-point-five columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'paramb_' . $DBinst[0] . '_' . $i . '\' name=\'paramb_' . $DBinst[0] . '_' . $i . '\' /></div></div>");
                                $( "#parama_' . $DBinst[0] . '_' . $i . '" ).datepicker({ dateFormat: "yy-mm-dd", autoSize: true });
                                $( "#paramb_' . $DBinst[0] . '_' . $i . '" ).datepicker({ dateFormat: "yy-mm-dd", autoSize: true });
                                } else {
                                $("#params_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = params_' . $DBinst[0] . '_' . $i . '><input type=hidden name = inf_' . $DBinst[0] . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' value = reg_' . $DBinst[0] . '><div class=\'three columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $DBinst[0] . '_' . $i . '\' name=\'parama_' . $DBinst[0] . '_' . $i . '\'  /></div></div>");
                                $( "#parama_' . $DBinst[0] . '_' . $i . '" ).datepicker({ dateFormat: "yy-mm-dd", autoSize: true });
                                }
                            });
                $("select").chosen({disable_search_threshold: 5});            
                break;
                ';
		$output.= 'default:
                            $("#ops_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = ops_' . $DBinst[0] . '_' . $i . '><div class=\'two columns\'><select id = op_' . $DBinst[0] . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' name = op_' . $DBinst[0] . '_' . $i . '><option value=equal>Equals</option><option value=similar>Is similar to</option><option value=contain>Contains</option><option value=not_contain>Doesn\'t contain</option></select></div></div> ");
                            $("#params_' . $DBinst[0] . '_' . $i . '").replaceWith("<div id = params_' . $DBinst[0] . '_' . $i . '><input type=hidden name = inf_' . $DBinst[0] . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' value = zend_' . $DBinst[0] . '><div class=\'three columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $DBinst[0] . '_' . $i . '\' name=\'parama_' . $DBinst[0] . '_' . $i . '\' /></div></div>");
                            $("select").chosen({disable_search_threshold: 5});
                            var fieldtype = $("#field_' . $DBinst[0] . '_' . $i . '").val();
                            $( "#parama_' . $DBinst[0] . '_' . $i . '" ).autocomplete({
                                source: "?page=auto&type=' . $cat[$catselect] . '&field_1_0="+fieldtype,
                                minLength:3
                            });	
                            ';
		$output.= '}
                    })
                    });
            </script>';
		return $output;
	}
	function MenuOptions($type, $db, $options, $i, $list) {
		$list = array($list);
		//Check for preselected options/data
		$op0 = '';
		$op1 = '';
		$op2 = '';
		$op3 = '';
		if (isset($_GET['op_' . $db . '_' . $i])) {
			switch ($_GET['op_' . $db . '_' . $i]) {
				case 'equal':
					$op0 = ' selected';
				break;
				case 'similar':
				case 'between':
					$op1 = ' selected';
				break;
				case 'contain':
					$op2 = ' selected';
				break;
				case 'not_contain':
				case 'not_equal':
					$op3 = ' selected';
				break;
			}
		}
		$paramaget = '';
		$parambget = '';
		if (isset($_GET['parama_' . $db . '_' . $i])) $paramaget = $_GET['parama_' . $db . '_' . $i];
		if (isset($_GET['paramb_' . $db . '_' . $i])) $parambget = $_GET['paramb_' . $db . '_' . $i];
		//Generate relevant menu items based upon type
		switch ($type) {
			case "option":
				$menuitems[0] = '<select id = op_' . $db . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' name = op_' . $db . '_' . $i . '><option value=equal' . $op0 . '>Equals</option><option value=not_equal' . $op3 . '>Doesn\'t equal</option></select>';
				$menuitems[1] = '<div class=\'three columns\'><input type=hidden name = inf_' . $db . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' value = reg_' . $db . '><select id = "parama_' . $db . '_' . $i . '" class=\'searchitem ' . $list[0] . '\' name = "parama_' . $db . '_' . $i . '">';
				foreach ($options as $index => $value) {
					if (isset($_GET['parama_' . $db . '_' . $i]) && $_GET['parama_' . $db . '_' . $i] == $index) $menuitems[1].= '<option value = "' . $index . '" selected>' . $value . '</option>';
					else $menuitems[1].= '<option value = "' . $index . '">' . $value . '</option>';
				}
				$menuitems[1].= '</select></div>';
				break;
			case "date":
				$menuitems[0] = '<select id = op_' . $db . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' name = op_' . $db . '_' . $i . '><option value=between' . $op1 . '>Between</option><option value=equal' . $op0 . '>On</option></select>';
				if ($op1 == ' selected') $menuitems[1] = '<input type=hidden class=\'searchitem ' . $list[0] . '\' name = inf_' . $db . '_' . $i . ' value = reg_' . $db . '><div class=\'one-point-five columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $db . '_' . $i . '\' name=\'parama_' . $db . '_' . $i . '\' value = \'' . $paramaget . '\' /></div><div class=\'one-point-five columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'paramb_' . $db . '_' . $i . '\' name=\'paramb_' . $db . '_' . $i . '\' value = \'' . $parambget . '\' /></div>';
				else $menuitems[1] = '<input type=hidden name = inf_' . $db . '_' . $i . ' class=\'searchitem ' . $list[0] . '\' value = reg_' . $db . '><div class=\'three columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $db . '_' . $i . '\' name=\'parama_' . $db . '_' . $i . '\' value = \'' . $paramaget . '\' /></div>';
				break;
			case "tiny":
				$yes = ' selected';
				$no = '';
				if (isset($_GET['parama_' . $db . '_' . $i]) && $_GET['parama_' . $db . '_' . $i] == 0) {
					$yes = '';
					$no = ' selected';
				}
				$menuitems[0] = '<input type=hidden class=\'searchitem ' . $list[0] . '\' name = op_' . $db . '_' . $i . ' value = equal><select class=\'searchitem ' . $list[0] . '\' id = parama_' . $db . '_' . $i . ' name = parama_' . $db . '_' . $i . '><option value=1' . $yes . '>Yes</option><option value=0' . $no . '>No</option></select>';
				$menuitems[1] = '<input type=hidden class=\'searchitem ' . $list[0] . '\' name = inf_' . $db . '_' . $i . ' value = reg_' . $db . '>';
				break;
			default:
				$menuitems[0] = '<select class=\'searchitem ' . $list[0] . '\' id = op_' . $db . '_' . $i . ' name = op_' . $db . '_' . $i . '><option value=equal' . $op0 . '>Equals</option><option value=similar' . $op1 . '>Is similar to</option><option value=contain' . $op2 . '>Contains</option><option value=not_contain' . $op3 . '>Doesn\'t contain</option></select>';
				$menuitems[1] = '<input type=hidden class=\'searchitem ' . $list[0] . '\' name = inf_' . $db . '_' . $i . ' value = zend_' . $db . '><div class=\'three columns\'><input type=\'text\' class=\'searchitem ' . $list[0] . '\' id=\'parama_' . $db . '_' . $i . '\' name=\'parama_' . $db . '_' . $i . '\' value = \'' . $paramaget . '\' /></div>';
				break;
			}
			return $menuitems;
		}
	}
?>