<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
error_reporting(1);
/**
 * Creates user editable workbooks.
 *
 * Tasks are assigned to workbooks, and workboks to teams.
 * Teams compete tasks to achieve levels within the scheme.
 *
 * @package cerebro
 * @subpackage workbooks
 *
 */
class Accounts extends ListGenRev2 {
	//Object functions and variables go here
	function Settings() {
		// global settings for the workbook module
		return array('FY_start' => '01 April', 'FY_end' => '31 March', 'currency_symbol' => '£', 'decimal_symbol' => '.', 'thousands_separator' => ',');
	}
	function FormatNumber($number) {
		$settings = $this->Settings();
		return number_format($number, 2, $settings['currency_unit'], $settings['thousands_separator']);
	}
	function balance($field, $row, $sign = null) {
		$settings = $this->Settings();
		$number = $row[$field];
		if ($sign == null && $number < 0) {
			$sign = 'negative';
		} elseif ($sign == null && $number >= 0) {
			$sign = 'positive';
		}
		if ($sign == 'negative') {
			$opening = '(';
			$closing = ')';
		} else {
			$opening = '';
			$closing = '';
		}
		if ($number == 0) {
			$number = '-';
		} else {
			$number = $this->FormatNumber($number, $sign);
		}
		$output.= '
        <div class="balance">
            <div class="currency">' . $settings['currency_symbol'] . '</div>
            <div class="opening">' . $opening . '</div>
            <div class="number ' . $sign . '">' . $number . '</div>
            <div class="closing">' . $closing . '</div>
        </div>';
		return $output;
	}
}
