<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Note add with mentions
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class Notes extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('page' => 'item page', 'category' => 'page item category', 'notetype' => 'type of note displayed', 'header' => 'list header content', 'form' => 'form id');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('AddNote' => 'note box', 'AddMeeting' => 'add meeting', 'NotesList' => 'get list of notes', 'FormNote' => 'note box', 'NotesView' => 'Vue.js and Skeleton2 version of notes');
	//Object functions and variables go here

	/**
	 * Generates note add box
	 *
	 * @return Data Returns JSON autocomplete data
	 */
	public function FormNote() {
		global $page;
		global $item_id;
		global $connectionmanager;
		global $dbmanager;
		$form = $this->link('form');
		$txt = '';
		$txt = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_type=60 AND note_item=" . $item_id));
		$note_id = $txt['note_id'];
		$txt = $txt['note_text'];
		$check = array();
		$words = array();
		if (isset($_GET['id'])) {
			$links = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE note_id=" . $note_id);
			while ($row = mysqli_fetch_assoc($links)) {
				$namedata = $dbmanager->Query(array('name', 'id', 'category'), array(array('id' => $row['item1'])), null, 0, $this->id);
				foreach ($namedata['result'] as $result) {
					$name = $result['name'];
					if ($name != '') {
						if ($row['cat1'] == 99) $colour = '#E9F6DF';
						elseif ($row['cat1'] == 1) $colour = '#D8F7FF';
						else $colour = '#f4f4f4';
						if (!(array_key_exists($name, $check))) {
							$start = strpos($txt, $name) - 1;
							if ($start != - 1) {
								$words[] = array($result['name'], $result['id'], $result['category']);
								$start = strpos($txt, $name) - 1;
								$start2 = strlen($name) + 7;
								$end = stripos($txt, ')', $start2 + $start);
								$replace = substr($txt, $start, $end - $start + 1);
								$txt = str_replace($replace, $name, $txt);
								$check["$name"] = 1;
							}
						}
					}
				}
			}
		}
		$addnote = "

		function onsub(event) {
			$( '#note_add_button_" . $this->id . "' ).click();
		}
		function addnote(wordarray){
			var empty_flds = 0;
			$(\"." . $form[0] . "_req\").each(function () {
                            if (!$.trim($(this).val())) {
                            	$(this).parent().parent().append(\"<div class='validation four columns bad' role='alert'>This field is required.</div>\");
                                empty_flds++;
                            }
                          });
                          if (empty_flds != 0) {
                            alert(\"Please complete all required fields\");
                          } else {
			var note_text = $(\"#notebox_" . $this->id . "\").val();
			if (encodeURIComponent(note_text) != 'undefined'){
					var multi = ''
					for (var i = 0; i < wordarray.length; i++ )
					{
						note_text = note_text.replace(wordarray[i].name,'[' + wordarray[i].name + '](?id=' + wordarray[i].id +')');
						multi += wordarray[i].id + ',';
					}
					note_text = encodeURIComponent(note_text);

					$(\"#multinote\").val(note_text);
					$(\"#multilink\").val(multi);
				}
				 $('#cerebro_form').submit();
			}
		}

		$( document ).ready(function() {
			$(\"#submit\").click(onsub);
			$( '#init_button_" . $this->id . "' ).click();
		});

		";
		$extrawords = '';
		return $this->template($addnote, 0, $txt, $words);
	}
	public function AddNote() {
		global $page;
		global $item_id;
		global $connectionmanager;
		$category = $this->link('category');
		if (isset($_GET['id'])) $addnote = "

		 function addnote(wordarray) {
		 $('#note_add_button_" . $this->id . "').html('<img src=\"images/loading.gif\" alt=\"Loading...\" /> Please wait');
		var note_text = $(\"#notebox_" . $this->id . "\").val();
		var link_rem = note_text;
		if (encodeURIComponent(note_text) != 'undefined'){
				for (var i = 0; i < wordarray.length; i++ ) {
					note_text = note_text.replace(wordarray[i].name,'[' + wordarray[i].name + '](?id=' + wordarray[i].id +')');
					link_rem = link_rem.replace(wordarray[i].name,'');
				}
				note_text = encodeURIComponent(note_text);

				link_rem = link_rem.replace(/[and\W]/g, '');
				var itemcheck = ['init'];
				var dataString_note = \"note_text=\" + note_text + \"&note_item=" . $item_id . "&table=global_notes&note_user=" . Users::GetCurrentUser()['id'] . "&note_date=" . time() . "\";
				var linkcounter = 0;
				if (link_rem != ''){
					$.ajax({
					type: \"POST\",
					url: \"?page=post\",
					data: dataString_note,
					 success: function (result) {
						if (wordarray.length == 1){
							console.log(result);
							location.reload();
						}
						for (var i = 0; i < wordarray.length; i++ ) {
							var mainitemwords = encodeURIComponent(wordarray[i].name);
							for (var j = 0; j < wordarray.length; j++ ) {
								var itemwords = encodeURIComponent(wordarray[j].name);
								if (itemwords != mainitemwords && (itemcheck.indexOf(wordarray[j].name) == -1))
								{
									linkcounter++;
									var dataString_link = 'table=item_links&note_id=' + result + '&cat1=' + wordarray[i].category + '&item1=' + wordarray[i].id + '&cat2='+wordarray[j].category+'&item2='+wordarray[j].id;
									$.ajax({
										type: \"POST\",
										url: \"?page=post\",
										data: dataString_link,
										success: function (result) {
											linkcounter--;
                                            if (linkcounter == 0){
												console.log(result);
												location.reload();
											}
                                        }
									});
								}
							}
							itemcheck[itemcheck.length] = wordarray[i].name;
						}
					}
				});
				} else {
						for (var i = 0; i < wordarray.length; i++ ) {
							var mainitemwords = encodeURIComponent(wordarray[i].name);
								if (" . $item_id . " != wordarray[i].id && (itemcheck.indexOf(wordarray[i].name) == -1))
								{
									linkcounter++;
									var dataString_link = 'table=item_links&note_id=0&cat1=' + wordarray[i].category + '&item1=' + wordarray[i].id + '&cat2=" . $category[0] . "&item2=" . $item_id . "';
									$.ajax({
										type: \"POST\",
										url: \"?page=post\",
										data: dataString_link,
										success: function (result) {
											linkcounter--;
                                            if (linkcounter == 0){
												console.log(result);
												location.reload();
											}
                                        }
									});
								}
							itemcheck[itemcheck.length] = wordarray[i].name;
						}
				}


			}
		}
		";
		else $addnote = "

		 function addnote(wordarray) {
		 $('#note_add_button_" . $this->id . "').html('<img src=\"images/loading.gif\" alt=\"Loading...\" /> Please wait');
		var note_text = $(\"#notebox_" . $this->id . "\").val();
		if (encodeURIComponent(note_text) != 'undefined'){
				for (var i = 0; i < wordarray.length; i++ ) {
					note_text = note_text.replace(wordarray[i].name,'[' + wordarray[i].name + '](?id=' + wordarray[i].id +')');
				}
				note_text = encodeURIComponent(note_text);
				var itemcheck = ['init'];
				var linkcounter = 0;
				for (var i = 0; i < wordarray.length; i++ ) {
					var dataString_note = \"note_text=\" + note_text + \"&note_item=\" + wordarray[i].id + \"&table=global_notes&note_user=" . Users::GetCurrentUser()['id'] . "&note_date=" . time() . "\";
					$.ajax({
						type: \"POST\",
						url: \"?page=post\",
						indexValue: i,
						data: dataString_note,
						 success: function (result) {
							if (wordarray.length == 1){
							console.log(result);
							location.reload();
						}
							var mainitemwords = encodeURIComponent(wordarray[this.indexValue].name);
							for (var j = 0; j < wordarray.length; j++ ) {
								var itemwords = encodeURIComponent(wordarray[j].name);
								if (itemwords != mainitemwords && (itemcheck.indexOf(wordarray[j].name) == -1))
								{
									linkcounter++;
									var dataString_link = 'table=item_links&note_id='+result+'&cat1=' + wordarray[this.indexValue].category + '&item1=' + wordarray[this.indexValue].id + '&cat2='+wordarray[j].category+'&item2='+wordarray[j].id;
									$.ajax({
										type: \"POST\",
										url: \"?page=post\",
										data: dataString_link,
										success: function (result) {
											linkcounter--;
											if (linkcounter == 0){
												console.log(result);
												location.reload();
											}
										}
									});
								}
							}
							itemcheck[itemcheck.length] = wordarray[this.indexValue].name;


						}
					});
				}
			}
		}
		";
		return $this->template($addnote, 1, '', array());
	}
	public function template($addfunction, $addself, $text, $extra) {
		global $item_id;
		global $connectionmanager;
		$category = $this->link('category');
		if (isset($_GET['id'])) {
			$name = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . $item_id));
			$name = str_replace("'", "", $name['name']);
		}
		$output = '  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
	  <script src="js/mention.js"></script>
	  ';
		$output.= "<script>
  'use strict';
angular.module('example', ['ui.mention']).run(function (\$rootScope) {
  \$rootScope.post = {
    message: '" . $text . "'
  };
}).directive('mentionExample', function (\$http) {
  return {
    require: 'uiMention',
    link: function link(\$scope, \$element, \$attrs, uiMention) {
      /**
       * \$mention.findChoices()
       *
       * @param  {regex.exec()} match    The trigger-text regex match object
       * @todo Try to avoid using a regex match object
       * @return {array[choice]|Promise} The list of possible choices
       */
      uiMention.findChoices = function(match,symbol) {
		if (match[1].length > 2){
		  var params = {};
		  params['term'] = match[1];
		  params['page'] = 'auto';
		  if (symbol == '@') params['type'] = 99;
		  else if (symbol == '+') params['type'] = 1;
		  else params['set'] = 'items';
		  return \$http.get('index.php', { params: params })
			.then(function(response) {
			  return response.data;
			});
		}
		}
		uiMention.passVars = function(event) {
			event.preventDefault();
			postMentions(uiMention.mentions);
		}

		uiMention.setPrevious = function() {
			var previous = [];
			";
		foreach ($extra as $extrainfo) {
			$output.= "previous[previous.length] = {
					category: '" . $extrainfo[2] . "',
					id: '" . $extrainfo[1] . "',
					name: '" . $extrainfo[0] . "'
				};
				";
		}
		$output.= "

			for (var i = 0; i < previous.length; i++ ) {
				uiMention.mentions.push(previous[i]);
				uiMention.render();
			}
		}
    }
  };
});
var clicked = 0;
function postMentions(mentions){
	";
		if (isset($_GET['id']) && $addself == 1) $output.= "mentions[mentions.length] = {
		category: '" . $category[0] . "',
		id: '" . $item_id . "',
		name: '" . $name . "'
	  };";
		$output.= "
	if (clicked == 0) {
		clicked = 1;
		addnote(mentions);
	}

}
" . $addfunction . "
  </script>
  ";
		$output.= '<div id="mentions_' . $this->id . '" ng-app="example">
<div class="row">
	<div class="ten columns add_note">
	  <div class="mention-container">
		<textarea ui-mention mention-example style="height: 13em; font-size: 14px !important; font-family:\'HelveticaNeue\', \'Helvetica Neue\', Helvetica, Arial, sans-serif;"
		  ng-model="post.message"
		  ng-trim="false"
		  id="notebox_' . $this->id . '"></textarea>
		<div class="mention-highlight"></div>

	  </div>
</div>
<div class="row">
	<div class="ten columns note_actions ">
	<input type="hidden" name="note_type" value="60">
			<input type="hidden" name="multinote" id="multinote">
			<input type="hidden" name="multilink" id="multilink">
		  <div id ="link_menu">
			<div class="instructions" id="link_menu_instructions">Type to tag <strong class="users">@users</strong> <strong class="contacts">+contacts</strong> <strong class="tags">#issues</strong> <strong class="tags">#projects</strong> <strong class="tags">#locations</strong></div>
			<span id="pos_' . $this->id . '">
				<ul ng-if="$mention.choices.length">
				  <li ng-click="$mention.select(choice)" ng-repeat="choice in $mention.choices" ng-class="{active:$mention.activeChoice==choice}">
					  {{::choice.name}}
				  </li>
				</ul>
			</span>
			</div>
		</div>
		';
		if ($addself == 1) $output.= '<div class ="action_bar">
			<a href="#" ng-click="$mention.passVars($event)" class="button" id="note_add_button_' . $this->id . '"><img src="images/add_icon.png" alt="Add" /> Note</a>
		</div>
	';
		else $output.= '<a style = "display:none" href="#" ng-click="$mention.passVars($event)" class="button" id="note_add_button_' . $this->id . '"></a>';
		$output.= '<a style = "display:none" href="#" ng-click="$mention.setPrevious()" class="button" id="init_button_' . $this->id . '"></a></div>
</div>
</div>';
		return $output;
	}
	public function AddMeeting() {
		global $item_id;
		global $connectionmanager;
		$category = $this->link('category');
		$name = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . $item_id));
		$name = $name['name'];
		$output = "	<div class='row'>
		<div id='note_container_" . $this->id . "' class='ten columns add_note'>
			<textarea id='meeting_add'  name='contact_add_meeting' style='height: 3em;'> </textarea>
		</div>
	</div>
	<div class='row'>
		<div class='ten columns note_actions '>
			<a href='#' class='button' id='meeting_add_button'><img src='images/add_icon.png' alt='Add' /> Meeting</a>
		</div>
	</div>
	 <script>$(\"#meeting_add_button\").click(function (e) {
		var note_text = $(\"#meeting_add\").val();
		var dataString_meeting = \"note_text=\" + note_text + \"&note_type=10&note_item=" . $item_id . "&table=global_notes&note_user=" . Users::GetCurrentUser()['id'] . "&note_date=" . time() . "\";

		$.ajax({
			type: \"POST\",
			url: \"?page=post\",
			data: dataString_meeting,
			success: function (result) {
				var dataString_meeting_link = 'table=item_links&note_id=' + result + '&cat1=" . $category[0] . "&item1=" . $item_id . "&cat2=users&item2=" . Users::GetCurrentUser()['id'] . "';

				$.ajax({
					type: \"POST\",
					url: \"?page=post\",
					data: dataString_meeting_link,
					success: function (result) {
						location.reload();
					}
				});
			}
		});
		e.preventDefault();

	 });</script>
	 ";
		return $output;
	}
	public function NotesList() {
		$types = $this->link('notetype');
		$header = $this->link('header');
		$types = explode(',', $types[0]);
		$typeconds = '';
		foreach ($types as $type) {
			$typeconds.= "note_type=" . $type . " OR ";
		}
		$typeconds.= "note_type=null";
		global $connectionmanager;
		global $item_id;
		$output = $header[0] . '<div class="notes_all">';
		$notes = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $item_id . " AND (" . $typeconds . ") ORDER BY note_id DESC");
		$data = array();
		while ($row = mysqli_fetch_assoc($notes)) {
			if ($row['note_reply'] == 0) {
				$data[$row['note_id']][0] = $row;
			} else {
				if (array_key_exists($row['note_reply'], $data)) $data[$row['note_reply']][] = $row;
				else $data[$row['note_reply']][1] = $row;
			}
		}
		$output.= NoteThread($data);
		$output.= '</div>';
		$output.= $this->NoteScript();
		return $output;
	}
	public function NoteScript() {
		global $item_id;
		return '
                <script type="text/javascript">
					var noteclick = 0;
                    $( ".list_button" ).click(function (e) {
                        var content = $(this).parent().parent().children("div:first-child").children(".markdown").text();
                        var style = $(this).parent().parent().children("div:first-child").attr("class");
                        var noteid = $(this).attr("id");
                        var thisstyle = $(this).attr("class");
                        $(this).parent().parent().children("div:first-child").replaceWith( "<div class=\"" + style +"\"><textarea id=note_" + noteid + ">" + content + "</textarea></div>" );
                        $(this).replaceWith("<div class=\"" + thisstyle + "\"><a href=\"#\" id=" + noteid + " class=\"button note_edit\"><img src=\"images/add_icon.png\"  alt=\"Save\" title=\"Save\"/> Save</a> </div>");
                        e.preventDefault();
                        $( ".note_edit" ).click(function (e) {
                            var editid = $(this).attr("id");
                            var edited_text = $("textarea#note_" + editid).val();
                            var edited_text = encodeURIComponent(edited_text);
							var dataString_note_edit = "table=global_notes&note_text=" + edited_text + "&note_id=" + editid;
                            $.ajax({
                                type: "POST",
                                url: "?page=post",
                                data: dataString_note_edit,
                                 success: function (result) {
									location.reload();
                                }
                            });
                            e.preventDefault();
                        });
                    });

                    $( ".note_reply_textbox" ).hide();
                    $( ".note_reply" ).click(function (e) {
                        var note_reply_id = $(this).attr("id");
                        $("#area_" + note_reply_id).show();
                        $(this).replaceWith("<a href=\"#\" id=" + note_reply_id + " class=\"button note_reply\"><img src=\"images/add_icon.png\"  alt=\"Save\" title=\"Save\"/> Save</a> ");
                        e.preventDefault();
                        $( ".note_reply" ).click(function (e) {
							if (noteclick == 0){
								noteclick = 1;
								var reply_text_val = $("textarea#reply_" + note_reply_id).val();
								 var reply_text_val = encodeURIComponent(reply_text_val);
								var dataString_note_reply = "table=global_notes&note_text=" + reply_text_val + "&note_user=' . Users::GetCurrentUser()['id'] . '&note_reply=" + note_reply_id + "&note_item=' . $item_id . '&note_date=' . time() . '";
								$.ajax({
									type: "POST",
									url: "?page=post",
									data: dataString_note_reply,
									 success: function (e) {
										location.reload();
									}
								});
							}
                            e.preventDefault();
                        });
                    });

                </script>
                ';
	}
	public function NotesView() {
		$types = $this->link('notetype');
		$header = $this->link('header');
		$types = explode(',', $types[0]);
		$typeconds = '';
		foreach ($types as $type) {
			$typeconds.= "note_type=" . $type . " OR ";
		}
		$typeconds.= "note_type=null";
		global $connectionmanager;
		global $dbmanager;
		global $item_id;
		$output = '';
		$notes = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $item_id . " AND (" . $typeconds . ") ORDER BY note_id ASC");
		$data = array();
		while ($row = mysqli_fetch_assoc($notes)) {
			$namedata = $dbmanager->Query(array('name', 'id', 'category', 'user_email'), array(array('id' => $row['note_user'])), null, 0, $this->id);
			$row['gravitar_hash'] = md5(strtolower(trim($namedata['result'][$row['note_user']]['user_email'])));
			$row['user_name'] = $namedata['result'][$row['note_user']]['name'];
			$row['better_date'] = D('l, d F Y H:i:s T', $row['note_date']);
			$row['fuzzy_time'] = fuzzy_time($row['note_date']);
			$row['note_text'] = parse_markdown($row['note_text']);
			if ($row['note_reply'] == 0) {
				$data[$row['note_id']][0] = $row;
			} else {
				if (array_key_exists($row['note_reply'], $data)) $data[$row['note_reply']][] = $row;
				else $data[$row['note_reply']][1] = $row;
			}
		}
		$data2 = array('id' => $item_id, 'notes' => $data, 'user' => Users::GetCurrentUser()['id'], 'user_name' => Users::GetCurrentUser()['name'], 'user_gravitor_hash' => Users::GetCurrentUser()['gravatar_hash'], 'default_time_string' => T('just a moment ago'));
		$view = file_get_contents('views/notes.html');
		$view = str_replace("<<data>>", json_encode($data2), $view);
		$output.= $view;
		return $output;
	}
}
?>
