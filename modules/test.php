<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Note add with mentions
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class test extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('category' => 'page item category');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('AddNote' => 'note box', 'test' => 'test');
	//Object functions and variables go here
	public function test() {
		global $dbmanager;
		$dbmanager->Query(array('id'), array(array('category' => '1')), null, 0, 1);
	}
	/**
	 * Generates note add box
	 *
	 * @return Data Returns JSON autocomplete data
	 */
	public function AddNote() {
		global $page;
		global $connectionmanager;
		if (isset($_GET['id'])) {
			$category = $this->link('category');
			$cat = substr($category[0], 0, -1) . '_id';
			$name = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM " . $category[0] . " WHERE " . $cat . " = " . $_GET['id']));
			if ($category[0] == 'contacts') {
				$name = $name['contact_first'] . ' ' . $name['contact_last'];
			} else {
				$name = $name[substr($category[0], 0, -1) . '_name'];
			}
		}
		$output = "
	<div class='row'>
		<div id='note_container' class='ten columns add_note'>
			<textarea id='note_add0' class='sensor' name='contact_add_note' style='height: 13em;'> </textarea>
		</div>
	</div>
	<div class='row'>
		<div class='ten columns note_actions '>
			<a href='#' class='button' id='note_add_button'><img src='images/add_icon.png' alt='Add' /> Note</a>
			<div id ='link_menu'><div class='instructions' id='link_menu_instructions'>Type to tag <strong class='users'>@users</strong> <strong class='contacts'>+contacts</strong> <strong class='tags'>#issues</strong> <strong class='tags'>#projects</strong> <strong class='tags'>#locations</strong></div><div id ='loading'><img src='images/loading.gif'></div><span id='pos'></span></div>
		</div>
	</div>



		<script src='../js/jquery-migrate-1.2.1.min.js' type='text/javascript'></script>
  <script src='http://documentcloud.github.com/underscore/underscore-min.js' type='text/javascript'></script>
    <script src='../lib/jquery.events.input.js' type='text/javascript'></script>
    <script src='../lib/jquery.elastic.js' type='text/javascript'></script>
    <script src='../js/jquery.caret.1.02.js' type='text/javascript'></script>
    <script src='../js/jquery.highlighttextarea.js' type='text/javascript'></script>
    <script src='../js/bindWithDelay.js' type='text/javascript'></script>
	<link rel='stylesheet' href='../stylesheets/jquery.highlighttextarea.css'>

  <script src='../jquery.mentionsInput.js' type='text/javascript'></script><script>

	$(function(){

		$('#loading').hide();

		var checkdigit = 0;
		var symbol = 0;
		var loading = 0;";
		if (isset($_GET['id'])) $output.= "

		 function addnote(counter,wordarray,event) {
		 $(\"#note_add_button\").click(function () {
		var note_text = $(\"#note_add\"+counter).val();
		var link_rem = note_text;
		if (encodeURIComponent(note_text) != 'undefined'){
				$.each(wordarray, function(index,item)
				{
					if (note_text.indexOf(item.words) == -1){
						wordarray.splice(index, 1);
					}else{
						note_text = note_text.replace(item.words,'[' + item.words + '](?page=' + item.page +'&id=' + item.id +')');
						link_rem = link_rem.replace(item.words,'');
					}
				});
				note_text = encodeURIComponent(note_text);
				link_rem = link_rem.replace(/[and\W]/g, '');
				var itemcheck = ['init'];
				var dataString_note = \"note_text_0=\" + note_text + \"&note_item_0=" . $_GET['id'] . "&table=global_notes&note_table_0=" . $category[0] . "&note_user_0=" . Users::GetCurrentuser()['id'] . "&note_date_0=" . time() . "\";
				wordarray[wordarray.length] = {
												color: ['#E9F6DF','" . $_GET['id'] . "'],
												page: ['" . $category[0] . "'],
												category: ['" . $category[0] . "'],
												id: ['" . $_GET['id'] . "'],
												words: ['" . $name . "']
											  };
				if (link_rem != ''){
					$.ajax({
					type: \"POST\",
					url: \"?page=post\",
					data: dataString_note,
					async: false,
					 success: function (result) {

						$.each(wordarray, function(mainindex,mainitem)
						{
							var mainitemwords = encodeURIComponent(mainitem.words);
							 $.each(wordarray, function(index,item)
							{
								var itemwords = encodeURIComponent(item.words);
								if (itemwords != mainitemwords && (itemcheck.indexOf(item.words) == -1))
								{
									var dataString_link = 'table=item_links&note_id_0=' + result + '&cat1_0=' + mainitem.category + '&item1_0=' + mainitem.id + '&name1_0=' + mainitemwords + '&cat2_0='+item.category+'&item2_0='+item.id+'&name2_0='+itemwords;
									$.ajax({
										type: \"POST\",
										url: \"?page=post\",
										data: dataString_link,
										async: false
									});
								}
							});
							itemcheck[itemcheck.length] = mainitem.words;
						});
						location.reload();
					}
				});
				} else {

						$.each(wordarray, function(mainindex,mainitem)
						{
							var mainitemwords = encodeURIComponent(mainitem.words);
							var name2 = encodeURIComponent('" . $name . "');
								if (" . $_GET['id'] . " != mainitemwords && (itemcheck.indexOf(mainitem.words) == -1))
								{
									var dataString_link = 'table=item_links&note_id_0=0&cat1_0=' + mainitem.category + '&item1_0=' + mainitem.id + '&name1_0=' + mainitemwords + '&cat2_0=" . $category[0] . "&item2_0=" . $_GET['id'] . "&name2_0=' + name2;
									$.ajax({
										type: \"POST\",
										url: \"?page=post\",
										data: dataString_link,
										async: false
									});
								}
							itemcheck[itemcheck.length] = mainitem.words;
						});
						location.reload();
				}

			}
		});
		}
		";
		else $output.= "

		 function addnote(counter,wordarray,event) {
		 $(\"#note_add_button\").click(function () {
		var note_text = $(\"#note_add\"+counter).val();
		var link_rem = note_text;
		if (encodeURIComponent(note_text) != 'undefined'){
				$.each(wordarray, function(index,item)
				{
					if (note_text.indexOf(item.words) == -1){
						wordarray.splice(index, 1);
					}else{
						note_text = note_text.replace(item.words,'[' + item.words + '](?page=' + item.page +'&id=' + item.id +')');
						link_rem = link_rem.replace(item.words,'');
					}
				});
				note_text = encodeURIComponent(note_text);
				link_rem = link_rem.replace(/[and\W]/g, '');
				var itemcheck = ['init'];
				if (link_rem != ''){
					 $.each(wordarray, function(mainindex,mainitem)
					{
							var dataString_note = \"note_text_0=\" + note_text + \"&note_item_0=\" + mainitem.id + \"&table=global_notes&note_table_0=\" + mainitem.category + \"&note_user_0=" . Users::GetCurrentuser()['id'] . "&note_date_0=" . time() . "\";
							$.ajax({
							type: \"POST\",
							url: \"?page=post\",
							data: dataString_note,
							async: false,
							 success: function (result) {
								var mainitemwords = encodeURIComponent(mainitem.words);
								 $.each(wordarray, function(index,item)
								{
									var itemwords = encodeURIComponent(item.words);
									if (itemwords != mainitemwords && (itemcheck.indexOf(item.words) == -1))
									{
										var dataString_link = 'table=item_links&note_id_0='+result+'&cat1_0=' + mainitem.category + '&item1_0=' + mainitem.id + '&name1_0=' + mainitemwords + '&cat2_0='+item.category+'&item2_0='+item.id+'&name2_0='+itemwords;
										$.ajax({
											type: \"POST\",
											url: \"?page=post\",
											data: dataString_link,
											async: false
										});
									}
								});
								itemcheck[itemcheck.length] = mainitem.words;


							}
						});
					});
					location.reload();
				} else {

						 $.each(wordarray, function(mainindex,mainitem)
						{
							var mainitemwords = encodeURIComponent(mainitem.words);
							 $.each(wordarray, function(index,item)
							{
								var itemwords = encodeURIComponent(item.words);
								if (itemwords != mainitemwords && (itemcheck.indexOf(item.words) == -1))
								{
									var dataString_link = 'table=item_links&note_id_0=0&cat1_0=' + mainitem.category + '&item1_0=' + mainitem.id + '&name1_0=' + mainitemwords + '&cat2_0='+item.category+'&item2_0='+item.id+'&name2_0='+itemwords;
									$.ajax({
										type: \"POST\",
										url: \"?page=post\",
										data: dataString_link,
										async: false
									});
								}
							});
							itemcheck[itemcheck.length] = mainitem.words;
						});
						location.reload();
				}

			}
		});
		}
		";
		$output.= "
		function moveCaretToEnd(el,pos) {
			if (typeof el.selectionStart == 'number') {
				el.selectionStart = el.selectionEnd = pos;
			} else if (typeof el.createTextRange != 'undefined') {
				el.focus();
				var range = el.createTextRange();
				range.collapse(false);
				range.select();
			}
		}

		function checkCharacter(counter){

			var pos = $('.sensor').caret().start;
			var text = $( '#note_add'+counter ).val();
			var charval = text.charAt(pos-4);

			if (pos != checkdigit){

				if (charval.valueOf() == '@'){
					symbol = 1;
					searchUser(counter,pos-3);
				}

				if (charval.valueOf() == '+'){
					symbol = 2;
					searchContact(counter,pos-3);
				}

				if (charval.valueOf() == '#'){
					symbol = 3;
					searchItem(counter,pos-3);
				}
				checkdigit = pos;
			}

		}

		function searchUser(counter,pos){


			$( '#note_container' ).bindWithDelay('keyup',function appendlist() {
				if (symbol != 1)
				{
					return;
				}
				text = $( '#note_add'+counter ).val();
				var end = $('.sensor').caret().start;
				sub = text.substring(pos,end);

				if (sub.length > 2){
					var autostring = 'term='+sub;
					loading++;
					$('#loading').show();
					$.ajax({
                                    type: 'GET',
                                    url: 'index.php?page=advanced-search&instance=665&method=AutoAll',
                                    data: autostring,
									datatype: 'json',
                                     success: function (result) {
										loading--;
										if (loading == 0){
											$('#loading').hide();
										}
										 result = jQuery.parseJSON(result);
                                       var list = '<span id=\"pos\"><ul>';
										$.each(result, function(index,item)
										{
											list += '<li class = \"' + item.page +' ' + item.category + '\" id = \"' + item.id + '\">' + item.name + '</li>';
										});
										list += '</ul></span>';
										$( '#pos' ).replaceWith(list);
										$( '#pos li' ).click(function() {
											var itemComponents = $(this).attr('class').split(' ');
											wordarray[wordarray.length] = {
												color: ['#E9F6DF',$(this).attr('id')],
												page: [itemComponents[0]],
												category: [itemComponents[1].toLowerCase()],
												id: [$(this).attr('id')],
												words: [$(this).text()]
											  };
											$( '#pos' ).empty();
											$( '#note_container' ).off();
											var text = $( '#note_add'+counter ).val();
											text = text.replace('@'+sub,$(this).text());
											$( '#note_add'+counter ).val(text);

											highlighter(wordarray,counter);
											var textarea = document.getElementById('note_add'+(counter+1));
											var cursor = pos + $(this).text().length;
											moveCaretToEnd(textarea,cursor);
										});
                                    }
                                });



				}

			},500);



		}

		function searchContact(counter,pos){

			$( '#note_container' ).bindWithDelay('keyup',function appendlist() {
				if (symbol != 2)
				{
					return;
				}
				text = $( '#note_add'+counter ).val();
				var end = $('.sensor').caret().start;
				sub = text.substring(pos,end);

				if (sub.length > 2){
					loading++;
					$('#loading').show();
					var autostring = 'term='+sub;
					$.ajax({
                                    type: 'GET',
                                    url: 'index.php?page=advanced-search&instance=664&method=AutoAll',
                                    data: autostring,
									datatype: 'json',
                                     success: function (result) {
									 loading--;
										if (loading == 0){
											$('#loading').hide();
										}
									 result = jQuery.parseJSON(result);
                                       var list = '<span id=\"pos\"><ul>';
										$.each(result, function(index,item)
										{
											list += '<li class = \"' + item.page +' ' + item.category + '\" id = \"' + item.id + '\">' + item.name + '</li>';
										});
										list += '</ul></span>';
										$( '#pos' ).replaceWith(list);
										$( '#pos li' ).click(function() {
											var itemComponents = $(this).attr('class').split(' ');
											wordarray[wordarray.length] = {
												color: ['#D8F7FF',$(this).attr('id')],
												page: [itemComponents[0]],
												category: [itemComponents[1].toLowerCase()],
												id: [$(this).attr('id')],
												words: [$(this).text()]
											  };
											$( '#pos' ).empty();
											$( '#note_container' ).off();
											var text = $( '#note_add'+counter ).val();
											text = text.replace('+'+sub,$(this).text());
											$( '#note_add'+counter ).val(text);

											highlighter(wordarray,counter);
											var textarea = document.getElementById('note_add'+(counter+1));
											var cursor = pos + $(this).text().length;
											moveCaretToEnd(textarea,cursor);
										});
                                    }
                                });



				}

			},500);



		}

		function searchItem(counter,pos){

			$( '#note_container' ).bindWithDelay('keyup',function appendlist() {
				if (symbol != 3)
				{
					return;
				}
				text = $( '#note_add'+counter ).val();
				var end = $('.sensor').caret().start;
				sub = text.substring(pos,end);

				if (sub.length > 2){
					loading++;
					$('#loading').show();
					var autostring = 'term='+sub;
					$.ajax({
                                    type: 'GET',
									//Und wenn du lange in einen Abgrund blickst, blickt der Abgrund auch in dich hinein.
                                    url: 'index.php?page=advanced-search&instance=666&method=AutoAll',
                                    data: autostring,
									datatype: 'json',
                                     success: function (result) {
									 loading--;
										if (loading == 0){
											$('#loading').hide();
										}
									 result = jQuery.parseJSON(result);
                                       var list = '<span id=\"pos\"><ul>';
										$.each(result, function(index,item)
										{
											list += '<li class = \"' + item.page +' ' + item.category + '\" id = \"' + item.id + '\">' + item.name + '</li>';
										});
										list += '</ul></span>';
										$( '#pos' ).replaceWith(list);
										$( '#pos li' ).click(function() {
											var itemComponents = $(this).attr('class').split(' ');
											wordarray[wordarray.length] = {
												color: ['#f4f4f4',$(this).attr('id')],
												page: [itemComponents[0]],
												category: [itemComponents[1].toLowerCase()],
												id: [$(this).attr('id')],
												words: [$(this).text()]
											  };
											$( '#pos' ).empty();
											$( '#note_container' ).off();
											var text = $( '#note_add'+counter ).val();
											text = text.replace('#'+sub,$(this).text());
											$( '#note_add'+counter ).val(text);

											highlighter(wordarray,counter);
											var textarea = document.getElementById('note_add'+(counter+1));
											var cursor = pos + $(this).text().length;
											moveCaretToEnd(textarea,cursor);
										});
                                    }
                                });



				}

			},500);



		}
		function highlighter(wordarray,counter){

			var text = $( '#note_add'+counter ).val();
			$( '.highlightTextarea' ).remove();
			$( '#note_container' ).replaceWith( \"<div id='note_container' class='ten columns add_note alpha omega'><textarea id='note_add\"+(counter+1)+\"' class='sensor' name='contact_add_note' style='height: 13em;'>\"+ $.trim(text)+ \"</textarea></div>\" );
			counter++;
			addnote(counter,wordarray);
			$( '#note_add'+counter).highlightTextarea({
				 words: wordarray,
				 caseSensitive: false,
			});

			$( '#note_add'+counter ).focus();

			$( '#note_container' ).keyup(function() {

				$('.sensor').keyup(function(){
					checkCharacter(counter);
				});

				$( '#note_add'+counter).highlightTextarea({
					 words: wordarray,
					 caseSensitive: false
				});

				$( '#note_add'+counter ).focus();

			});

		}



		var counter = 0;
		var wordarray =  [];




		highlighter(wordarray,counter);

	});

  </script>";
		return $output;
	}
}
?>
