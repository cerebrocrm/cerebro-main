<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Collates module outputs for display
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class ViewControllerRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('output' => 'Content to be displayed', 'sidebarcontent' => 'sidebar content', 'specific' => 'specific page content', 'disable_divs' => 'Enter 1 at index 0 to disable DIVs between elements');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('Output' => 'Returns collation of html/javascript inputs', 'sidebar' => 'Returns sidebar items');
	//Object functions and variables go here

	/**
	 * Collates outputs from main modules
	 *
	 * @return Content Returns html/scripting content for main modules
	 */
	public function Output() {
		$output = $this->link('output');
		$specific = $this->link('specific');
		$disable_divs = $this->link('disable_divs');
		$content = '';
		// CACHE cache the output
		$cachename = get_instance() . '-output1-' . md5(serialize($output), serialize($specific));
		$content = apc_fetch($cachename); //get any cached variable we already have
		if ($content === false) {
			for ($i = 0;$i < count($output);$i++) {
				if ($specific[0] == '' && $disable_divs[0] !== '1') $content.= '<div>';
				$content.= $output[$i];
				if ($specific[0] == '' && $disable_divs[0] !== '1') $content.= '</div>';
			}
			//! clean up the URL
			apc_store($cachename, $content, 600); // save for 10 minutes

		}
		if (!isset($this->permissions['Output']))
			return $content;

		if ($this->permissions['Output'] <= Users::GetCurrentUser()['user_level']) {
			return $content;
		} else {
			return '';
		}
	}
	/**
	 * Collates outputs from sidebar
	 *
	 * @return Content Returns html/scripting content for sidebar modules
	 */
	public function Sidebar() {
		$sidebar = $this->link('sidebarcontent');
		$output = '';
		foreach ($sidebar as $content) {
			if ($content != '') $output.= '<div>
						' . $content . '
					</div>';
		}
		return $output;
	}
	/**
	 * Sets flag to determine whether template should be omitted when displaying page
	 *
	 * @return Flag returns boolean value to indicate whether template should be omitted
	 */
	public function Specific() {
		$specific = $this->link('specific');
		if ($specific[0] == '') return 0;
		else return 1;
	}
}
?>
