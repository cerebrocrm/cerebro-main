<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates summary of information held on a particular item
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class SummaryRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('vars' => 'list of summary variables', 'type' => 'item category', 'action' => 'action bar buttons', 'skeleton2_mode' => 'Enter 1 to get Skeleton 2 mode UI');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('Summary' => 'Returns summary of item');
	//Object functions and variables go here
	
	/**
	 * @return string HTML/Javascript summary panel using Skeleton 1 or 2 mode
	 */
	public function Summary() {
		$skeleton2_mode = $this->link('skeleton2_mode');
		if ($skeleton2_mode[0] == '1') {
			return $this->Summary_skeleton2();
		} else {
			return $this->Summary_skeleton1();
		}
	}
	/**
	 * @return string HTML/Javascript summary panel using Skeleton 1 mode
	 */
	public function Summary_skeleton1() {
		global $item_id;
		$vars = $this->link('vars');
		$type = $this->link('type');
		$action = $this->link('action');
		global $dbmanager;
		global $connectionmanager;
		$varlist = array();
		foreach ($vars as $var) {
			$var = explode(',', $var);
			$varlist[] = $var[0];
		}
		$data = $dbmanager->Query($varlist, array(array('id' => $item_id)), null, 0, $this->id, 1);
		$fields = $dbmanager->Fields($varlist, $type[0]);
		$page = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM categories WHERE cat_id=" . $type[0]));
		//Standard section of summary - name and edit/delete buttons
		$output = '<div class="row top_part"><div class="five columns alpha title_bar" ><h2>';
		$output.= stripslashes($data['result'][$item_id]['name']);
		$output.= '</h2></div>';
		$output.= '<div class="five columns omega action_bar">';
		foreach ($action as $bar) $output.= $bar;
		$output.= '<a href="?page=' . $page['cat_form'] . '&id=' . $item_id . '" class="button"><span class="fa fa-pencil" title="Edit"></span></a> <a href="#" class="button delete_item"><span class="fa fa-trash" title="Delete"></span></a>';
		$output.= '</div></div>';
		$summary = '';
		foreach ($varlist as $var) {
			//Quick edit option variable
			if ($fields[$var]['type'] == 2) {
				$icon = '';
				if ($fields[$var]['icon'] != null) $icon = 'fa fa-' . $fields[$var]['icon'] . ' fa-fw fa-2x';
				$summary.= '<p title="' . $fields[$var]['friendly_name'] . '"><span class="' . $icon . '" ></span><a href="#" id ="button_' . $var . '_' . $this->id . '" class="button borderless">' . $fields[$var]['options'][$data['result'][$item_id][$var]] . '</a></p>';
				$summary.= '<div id = ' . $var . '_' . $this->id . '>
						Assign new ' . $fields[$var]['friendly_name'] . ':
						<select id = "menu_' . $var . '_' . $this->id . '">';
				foreach ($fields[$var]['options'] as $optindex => $optvalue) {
					$isselected = '';
					if ($data['result'][$item_id][$var] == $optindex) $isselected = ' selected ';
					$summary.= '<option value="' . $optindex . '" ' . $isselected . '>' . $optvalue . '</option>';
				}
				$summary.= '</select></div>';
				$summary.= '<script type="text/javascript">
				$("#' . $var . '_' . $this->id . '").hide();
								$( "#button_' . $var . '_' . $this->id . '" ).click(function (e) {
									$("#' . $var . '_' . $this->id . '").toggle();
									e.preventDefault();
							});
							$("#menu_' . $var . '_' . $this->id . '").change(function () {
								var dropdown = $(this);
								var key = dropdown.val();
								var dataString = "category=' . $data['result'][$item_id]['category'] . '&id=' . $item_id . '&' . $var . '=" + key;
								$.ajax({
									type: "POST",
									url: "?page=post",
									data: dataString,
									 success: function () {
										location.reload();
									}
								});
							});
							</script>';
			} elseif ($fields[$var]['data_format'] == 1) {
				//Time format
				$icon = '';
				if ($fields[$var]['icon'] != null) $icon = 'fa fa-' . $fields[$var]['icon'] . ' fa-fw fa-2x';
				$summary.= '<span title = "' . $data['result'][$item_id][$var] . '"><p><span class="' . $icon . '" ></span>' . fuzzy_time($data['result'][$item_id][$var]) . '</p></span>';
			} else {
				//No formatting
				$icon = '';
				if ($fields[$var]['icon'] != null) $icon = 'fa fa-' . $fields[$var]['icon'] . ' fa-fw fa-2x';
				$summary.= '<p title="' . $fields[$var]['friendly_name'] . '"><span class="' . $icon . '" ></span>' . $data['result'][$item_id][$var] . '</p>';
			}
		}
		$output.= '<div class="five columns alpha summary" >';
		$output.= $summary;
		$output.= '</div>';
		$output.= '<script>
         $( ".delete_item" ).click(function (e) {
                                var remove_prompt = confirm("Are you sure you wish to delete this item?");
                                if (remove_prompt) {
                                    var dataString = "remove=item&id=' . $item_id . '";
                                    $.ajax({
                                        type: "POST",
                                        url: "?page=post",
                                        data: dataString,
                                         success: function () {
                                            window.location.assign("?page=' . $page['cat_name'] . '");
                                        }
                                    });
                                }
                                e.preventDefault();
                            });
                            </script>';
		return $output;
	}
	/**
	 * @return string HTML/Javascript summary panel using Skeleton 2 mode
	 */
	public function Summary_skeleton2() {
		global $item_id;
		$vars = $this->link('vars');
		$type = $this->link('type');
		$action = $this->link('action');
		global $dbmanager;
		global $connectionmanager;
		$varlist = array();
		foreach ($vars as $var) {
			$var = explode(',', $var);
			$varlist[] = $var[0];
		}
		$result = $dbmanager->Query($varlist, array(array('id' => $item_id)), null, 0, $this->id, 1);
		$fields = $dbmanager->Fields($varlist, $type[0]);
		$page = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM categories WHERE cat_id=" . $type[0]));
		$data = array('id' => $this->id, 'vars' => $varlist, 'type' => $type, 'result' => $result, 'fields' => $fields, 'page' => $page,);
		$actions = '';
		foreach ($action as $a) {
			$actions.= $a;
		}
		$view = file_get_contents('views/summary.html');
		$view = str_replace(array("<<data>>", "<<actions>>"), array(json_encode($data), $actions), $view);
		$output.= $view;
		$output.= '<script>
         $( ".delete_item" ).click(function (e) {
                                var remove_prompt = confirm("Are you sure you wish to delete this item?");
                                if (remove_prompt) {
                                    var dataString = "remove=item&id=' . $item_id . '";
                                    $.ajax({
                                        type: "POST",
                                        url: "?page=post",
                                        data: dataString,
                                         success: function () {
                                            window.location.assign("?page=' . $page['cat_name'] . '");
                                        }
                                    });
                                }
                                e.preventDefault();
                            });
                            </script>';
		return $output;
	}
}
?>