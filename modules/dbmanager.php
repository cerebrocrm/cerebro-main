<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Manages databases
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class DBmanagerRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('table' => 'Table this manager is linked with', 'permissions' => 'Table-wide permission level');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('Test' => 'test call', 'Optimize' => 'optimize zend indexes');
	//Object functions and variables go here
	var $primary;
	/**
	 * Table association link
	 *
	 * @return Data Returns name of table this manager is linked to
	 */
	public function GetTable() {
		return $table = $this->link('table') [0];
	}
	/**
	 * Optimize zend indexes
	 *
	 * @return void
	 */
	public function Optimize() {
		require_once 'Zend/Search/Lucene.php';
		$table = $this->link('table');
		$return = 0;
		foreach ($table as $type) {
			if ($type != null) {
				$return = 1;
			}
			$indexPath = ("./search/" . $type);
			$index = Zend_Search_Lucene::open($indexPath);
			$index->optimize();
		}
		return $return;
	}
	/**
	 * Get search results in appropriate form for autocomplete query
	 *
	 * @return Data Returns JSON search results
	 */
	public function Auto() {
		$return = array();
		$order = array(0);
		//perform query
		$vars = array($_GET['field_' . $this->id . '_0']);
		$_GET['parama_' . $this->id . '_0'] = $_GET['term'];
		$result = $this->Query($vars, '', 'list', $order, 5, '');
		foreach ($result['result']['results'] as $row) {
			$return[] = $row[$vars[0]];
		}
		//Return data in JSON format for autocomplete
		echo json_encode(array_values(array_unique($return)));
	}
	/**
	 * Pagination function
	 *
	 * @return Data Returns pagination segment of query
	 */
	function limit() {
		global $connectionmanager;
		if (isset($_GET[$this->id . '_place'])) {
			$place = $_GET[$this->id . '_place'] * 10;
			return ' LIMIT ' . mysqli_real_escape_string($connectionmanager->connection, $place) . ', 10';
		} else {
			return ' LIMIT 0, 10';
		}
	}
	/**
	 * Search op formatting function
	 *
	 * @return Data Returns op select portion of query
	 */
	function opselect($op, $parama, $paramb) {
		switch ($op) {
			case 'equal':
				$query = ' = "' . $parama . '"';
			break;
			case 'not_equal':
				$query = ' <> "' . $parama . '"';
			break;
			case 'contain':
				$query = ' LIKE "%' . $parama . '%"';
			break;
			case 'not_contain':
				$query = ' NOT LIKE "%' . $parama . '%"';
			break;
			case 'between':
				$query = ' BETWEEN "' . $parama . '" AND "' . $paramb . '"';
			break;
		}
		return $query;
	}
	/**
	 * Perform Zend search
	 *
	 * @return Data Returns search results in zend format
	 */
	public function zendsearch($field, $op, $parama, $paramb) {
		//error_reporting(1);
		$table = $this->link('table');
		if (isset($_GET['table'])) {
			if ($_GET['table'] != null) {
				$table = mysql_real_escape_string($_GET['table']);
			}
		}
		//Set up zend engine
		require_once 'Zend/Search/Lucene.php';
		$indexPath = ("./search/" . $table[0]);
		Zend_Search_Lucene_Analysis_Analyzer::setDefault(new Zend_Search_Lucene_Analysis_Analyzer_Common_TextNum_CaseInsensitive());
		$index = Zend_Search_Lucene::open($indexPath);
		Zend_Search_Lucene_Search_Query_Fuzzy::setDefaultPrefixLength(0);
		//Build query
		if ($field == 'all') $search = '"' . $parama . '~"';
		elseif ($op == 'equal') $search = $field . ': "' . $parama . '"';
		else $search = $field . ': ' . $parama . '~';
		//Perform query
		$hits = $index->find($search);
		return $hits;
	}
	/**
	 * Perform a general search
	 *
	 * @return Data Returns search query
	 */
	function search() {
		global $connectionmanager;
		$query = '';
		$table = $this->link('table');
		//Check if this (or every search) instance is being called
		if (isset($_GET['field_all_0'])) {
			$num = 'all';
		} else {
			$num = $this->id;
		}
		$i = 0;
		//Loop through each set of parameters
		while (isset($_GET['field_' . $num . '_' . $i])) {
			//Get search parameters
			$field = mysqli_real_escape_string($connectionmanager->connection, $_GET['field_' . $num . '_' . $i]);
			$op = mysqli_real_escape_string($connectionmanager->connection, $_GET['op_' . $num . '_' . $i]);
			$parama = mysqli_real_escape_string($connectionmanager->connection, $_GET['parama_' . $num . '_' . $i]);
			if (isset($_GET['paramb_' . $num . '_' . $i])) $paramb = mysqli_real_escape_string($connectionmanager->connection, $_GET['paramb_' . $num . '_' . $i]);
			else $paramb = '';
			$inf = mysqli_real_escape_string($connectionmanager->connection, $_GET['inf_' . $num . '_' . $i]);
			$info = explode('_', $inf);
			//Add query type for multiple criteria searches
			if ($i != 0) $query.= ' ' . mysqli_real_escape_string($connectionmanager->connection, $_GET['and_' . $num . '_' . $i]) . ' ';
			//Call zend search function, or build query using mysql, depending on data & op type
			if ($info[0] != 'zend' || ($op != 'equal' && $op != 'similar')) {
				global $modulemanager;
				$fields = mysqli_fetch_assoc(mysqli_query($modulemanager->connection, "SELECT requirements FROM field_data WHERE field_name = '" . $field . "'"));
				if ($fields['requirements'] != '') {
					$multifields = explode(',', $fields['requirements']);
					if ($multifields[0] == 'alias') $query.= '(( ' . $table[0] . '.' . $multifields[1] . ' ' . $this->opselect($op, $parama, $paramb) . ' ) OR ( ' . $table[0] . '.' . $multifields[2] . ' ' . $this->opselect($op, $parama, $paramb) . ' )OR ( concat(' . $multifields[1] . '," ",' . $multifields[2] . ') ' . $this->opselect($op, $parama, $paramb) . ' ))';
					else $query.= '( ' . $table[0] . '.' . $field . ' ' . $this->opselect($op, $parama, $paramb) . ' )';
				} else $query.= '( ' . $table[0] . '.' . $field . ' ' . $this->opselect($op, $parama, $paramb) . ' )';
			} else {
				//perform zend query
				$hits = $this->zendsearch($field, $op, $parama, $paramb);
				$numHits = count($hits);
				$primary = $this->primary;
				//Add zend results to query string
				$query.= '( ';
				if ($numHits > 0) {
					$zendquery = $hits[0]->$primary;
					$query.= $table[0] . '.' . $primary . ' = "' . $zendquery . '"';
					for ($j = 1;$j < $numHits;$j++) {
						$zendquery = $hits[$j]->$primary;
						$query.= ' OR ' . $table[0] . '.' . $primary . ' = "' . $zendquery . '"';
					}
				} else $query.= '0';
				$query.= ' )';
			}
			$i++;
		}
		return $query;
	}
	/**
	 * Execute MySql query
	 *
	 * @return Data Returns MySql result
	 */
	public function Query($vars, $conditions, $format, $order, $paginate, $freetext) {
		//error_reporting(1);
		//Check table-wide permissions, return if user is not allowed access
		$permissions = $this->link('permissions');
		if ($permissions[0] > Users::GetCurrentUser()['user_level']) {
			return null;
		}
		$table = $this->link('table');
		//Get database field info, storing relevant data and checking for joins and requirements
		$joins = array();
		$group = 0;
		$field_data = array();
		$requirement_list = array();
		$databases = array();
		global $modulemanager;
		global $connectionmanager;
		if ($freetext != '' && $freetext != null) {
			// free text cache
			$cachename = get_instance() . '-query-' . md5(Users::GetCurrentUser()['user_level'] . $table[0] . $freetext); // what the cache is called
			$results = apc_fetch($cachename); //get any cached variable we already have
			// check if cache exixts
			if ($results === false) {
				//Perform query, initialise results array and include results count. Store metadata for each variable
				error_reporting(1);
				$results = array();
				//echo $query;
				$tmpquery = mysqli_query($connectionmanager->connection, $freetext);
				// I'm PRE-STORING the data so that it can be accessed after caching
				$results['result'] = array('num_rows' => mysqli_num_rows($tmpquery), 'field_count' => mysqli_field_count($tmpquery));
				$i = 0;
				$results['result']['results'] = array();
				while ($row = mysqli_fetch_assoc($tmpquery)) {
					$results['result'][$i] = array();
					foreach ($row as $f => $v) {
						$results['result']['results'][$i][$f] = $v;
					}
					$i++;
				}
				// we also need to cache the primary for longer
				$primary = apc_fetch(get_instance() . '-' . $table[0] . '-primary');
				if ($primary === false) {
					$primary = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SHOW KEYS FROM " . $table[0] . " WHERE Key_name = 'PRIMARY'"));
					apc_store(get_instance() . '-' . $table[0] . '-primary', $primary, 18000); // cache for 5 hours

				}
				$this->primary = $primary[4];
				$count = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT FOUND_ROWS()"));
				$results['primary_key'] = $this->primary;
				$results['result_count'] = count($results['result']);
				$results['query'] = $freetext;
				foreach ($field_data as $index => $value) {
					$results[$index] = $value;
				}
				apc_store($cachename, $results, 60); //save the cache for 1 mins

			}
			return $results;
			break;
		}
		// dbmanager cache trick
		$cachename = get_instance() . '-query-' . md5(Users::GetCurrentUser()['user_level'] . $table[0] . serialize($_GET)); // what the cache is called
		echo 'test test test';
		$results = apc_fetch($cachename); //get any cached variable we already have
		if ($results === false) {
			// CACHE we also need to cache the fields for longer
			//$fields = apc_fetch(get_instance().'-fields');
			//if ($fields === false) {
			$fields = mysqli_query($modulemanager->connection, "SELECT * FROM field_data");
			//	apc_store(get_instance().'-fields',$fields,18000); // cache for 5 hours
			//}
			// we also need to cache the primary for longer
			$primary = apc_fetch(get_instance() . '-' . $table[0] . '-primary');
			if ($primary === false) {
				$primary = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SHOW KEYS FROM " . $table[0] . " WHERE Key_name = 'PRIMARY'"));
				apc_store(get_instance() . '-' . $table[0] . '-primary', $primary, 18000); // cache for 5 hours

			}
			$this->primary = $primary[4];
			while ($row = mysqli_fetch_assoc($fields)) {
				if (in_array($row['field_name'], $vars)) {
					$field_data[$row['field_name']] = $row;
					if ($row['requirements'] != '') {
						$requirements = explode(',', $row['requirements']);
						if ($requirements[0] == 'alias') {
							if (count($requirements) > 2) {
								$requirements[1] = 'concat(' . $requirements[1] . '," ",' . $requirements[2] . ')';
							}
							$arraykey = array_search($row['field_name'], $vars);
							if ($row['db'] != $table[0]) {
								$joinsearch = mysqli_fetch_assoc(mysqli_query($modulemanager->connection, "SELECT db1,db2 FROM db_links WHERE (db1 = '" . $table[0] . "' AND db2 = '" . $row['db'] . "') OR (db1 = '" . $row['db'] . "' AND db2 = '" . $table[0] . "')"));
								if ($joinsearch['db1'] == $table[0]) $vars[$arraykey] = $requirements[1] . ' AS ' . $row['field_name'];
								else {
									$vars[$arraykey] = 'COUNT(' . $row['db'] . '.' . $requirements[1] . ') AS ' . $row['field_name'];
									$group = 1;
								}
								$joins[] = array($row['db'], $requirements[1] . ' AS ' . $row['field_name']);
							} else {
								$vars[$arraykey] = $requirements[1] . ' AS ' . $row['field_name'];
							}
							$field_data[$row['field_name']] = $row;
						} else {
							if ($row['db'] != $table[0]) $joins[] = array($row['db'], $row['field_name']);
							if ($row['type'] == 'option') {
								$field_data[$row['field_name']]['options'] = array();
								$optionindexval = explode(',', $row['options']);
								$optionquery = mysqli_query($connectionmanager->connection, "SELECT " . $row['options'] . " FROM " . $row['requirements']);
								while ($optionvalrow = mysqli_fetch_assoc($optionquery)) {
									$field_data[$row['field_name']]['options'][$optionvalrow[$optionindexval[0]]] = $optionvalrow[$optionindexval[1]];
								}
							} else $field_data[$row['field_name']]['options'] = $row['requirements'] . ',' . $row['options'];
							$requirement_list[$row['field_name']] = $requirements;
							foreach ($requirements as $requirement) {
								if (!(in_array($requirement, $vars)) && $row['type'] != 'option') {
									$vars[] = $requirement;
									$field_data[$requirement] = mysqli_fetch_assoc(mysqli_query($modulemanager->connection, "SELECT * FROM field_data WHERE field_name LIKE '" . $requirement . "'"));
									$databases[$requirement] = $field_data[$requirement]['db'];
								} elseif ($row['type'] == 'option') {
									$add = explode(',', $row['options']);
									$vars[] = $row['requirements'] . '.' . $add[1] . ' AS ' . $row['field_name'];
									$joins[] = array($requirement, '');
								}
							}
						}
					} else {
						$databases[$row['field_name']] = $row['db'];
						if ($row['db'] != $table[0]) $joins[] = array($row['db'], $row['field_name']);
						if ($row['type'] == 'option' && $row['options'] != '') {
							$optionlist = array();
							$optionset = explode(',', $row['options']);
							foreach ($optionset as $set) {
								$splitset = explode(':', $set);
								$optionlist[$splitset[0]] = $splitset[1];
							}
							$field_data[$row['field_name']]['options'] = $optionlist;
						}
					}
				}
			}
			//Initialize query string
			$query = "SELECT SQL_CALC_FOUND_ROWS ";
			for ($i = 0;$i < count($vars) - 1;$i++) {
				if (!(array_key_exists($vars[$i], $requirement_list))) {
					if (array_key_exists($vars[$i], $databases)) $query.= $databases[$vars[$i]] . '.' . $vars[$i] . ',';
					else $query.= $vars[$i] . ',';
				}
			}
			if (array_key_exists($vars[$i], $databases)) $query.= $databases[$vars[$i]] . '.' . $vars[$i];
			else $query.= $vars[$i];
			$query.= ' FROM ' . $table[0];
			//Add required joins to query, get options for non-list formats
			foreach ($joins as $join) {
				$joinsearch = mysqli_query($modulemanager->connection, "SELECT field1,field2 FROM db_links WHERE (db1 = '" . $table[0] . "' AND db2 = '" . $join[0] . "') OR (db2 = '" . $table[0] . "' AND db1 = '" . $join[0] . "')");
				//echo "SELECT field1,field2 FROM db_links WHERE (db1 = '".$table[0]."' AND db2 = '".$join[0]."') OR (db2 = '".$table[0]."' AND db1 = '".$join[0]."')";
				if (mysqli_num_rows($joinsearch) != 0) {
					$joinparams = mysqli_fetch_assoc($joinsearch);
					if ($format == 'list') {
						$query.= ' LEFT JOIN ' . $join[0] . ' ON ' . $table[0] . '.' . $joinparams['field1'] . '=' . $join[0] . '.' . $joinparams['field2'];
						$field_data[$join[1]]['options'] = array();
					} else {
						$joinoptionlist = array();
						$joinoptions = mysqli_query($connectionmanager->connection, "SELECT " . $joinparams['field2'] . "," . $join[1] . " FROM " . $join[0]);
						while ($optionrow = mysqli_fetch_assoc($joinoptions)) {
							$joinoptionlist[$joinparams['field2']] = $join[1];
						}
						$field_data[$join[1]]['options'] = $joinoptionlist;
					}
				}
			}
			//Add conditions to query
			if ($conditions != null || isset($_GET['field_' . $this->id . '_0']) || isset($_GET['field_all_0'])) {
				$query.= ' WHERE ';
				if (isset($_GET['field_' . $this->id . '_0']) || isset($_GET['field_all_0'])) {
					$query.= $this->search();
				} else {
					$i = 0;
					foreach ($conditions as $condition => $value) {
						if ($i > 0) $query.= ' AND ';
						$query.= $table[0] . '.' . $condition . ' = "' . $value . '"';
						//echo $table[0].'.'.$condition.' = "'.$value.'"';
						$i++;
					}
				}
			}
			//Add ordering
			if (isset($_GET[$this->id . '_asc'])) {
				if (!(array_key_exists($_GET[$this->id . '_asc'], $requirement_list)) || count($requirement_list[$_GET[$this->id . '_asc']]) < 2) $query.= ' ORDER BY ' . mysqli_real_escape_string($connectionmanager->connection, $_GET[$this->id . '_asc']) . ' ASC';
				else $query.= ' ORDER BY ' . $requirement_list[$_GET[$this->id . '_asc']][0] . ' ASC';
			} elseif (isset($_GET[$this->id . '_desc'])) {
				if (!(array_key_exists($_GET[$this->id . '_desc'], $requirement_list)) || count($requirement_list[$_GET[$this->id . '_desc']]) < 2) $query.= ' ORDER BY ' . mysqli_real_escape_string($connectionmanager->connection, $_GET[$this->id . '_desc']) . ' DESC';
				else $query.= ' ORDER BY ' . $requirement_list[$_GET[$this->id . '_desc']][0] . ' DESC';
			} elseif (!(array_key_exists(0, $order))) {
				foreach ($order as $index => $value) $query.= ' ORDER BY ' . $index . ' ' . $value;
			}
			//If sum type join exists in query, add grouping
			if ($group == 1) $query.= ' GROUP BY ' . $table[0] . '.' . $this->primary;
			//Add pagination for lists
			if ($format == 'list' && $paginate != 0) {
				if (isset($_GET[$this->id . '_place'])) {
					$place = $_GET[$this->id . '_place'] * $paginate;
					$query.= ' LIMIT ' . mysqli_real_escape_string($connectionmanager->connection, $place) . ', ' . $paginate;
				} else {
					$query.= ' LIMIT 0, ' . $paginate;
				}
			}
			//Perform query, initialise results array and include results count. Store metadata for each variable
			error_reporting(1);
			$results = array();
			$tmpquery = mysqli_query($connectionmanager->connection, $query);
			// I'm PRE-STORING the data so that it can be accessed after caching
			$results['primary_key'] = $this->primary;
			$results['result'] = array('num_rows' => mysqli_num_rows($tmpquery), 'field_count' => mysqli_field_count($tmpquery));
			$i = 0;
			$results['result']['results'] = array();
			while ($row = mysqli_fetch_assoc($tmpquery)) {
				$results['result'][$i] = array();
				foreach ($row as $f => $v) {
					$results['result']['results'][$i][$f] = $v;
				}
				$i++;
			}
			$count = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT FOUND_ROWS()"));
			$results['result_count'] = count($results['result']);
			$results['query'] = $query;
			foreach ($field_data as $index => $value) {
				$results[$index] = $value;
			}
			apc_store($cachename, $results, 60); //save the cache for 1 mins

		} else {
			$results = $results;
		}
		return $results; //using either the cached or uncached version

	}
	/**
	 * Gets list of fields & metadata associated with table
	 *
	 * @return Data Returns array of fields plus associated data
	 */
	public function Fields() {
		$table = $this->link('table');
		global $modulemanager;
		//get a list of all fields associated with table
		$fields = mysqli_query($modulemanager->connection, "SELECT * FROM field_data WHERE db='" . $table[0] . "'");
		$result = array();
		while ($row = mysqli_fetch_assoc($fields)) {
			if ($row['type'] != 'option') {
				if ($row['requirements'] == '') $result[$row['field_name']] = $row;
			}
			//Get any options associated with field
			elseif ($row['requirements'] == '') {
				$options = array();
				$optionlist = explode(',', $row['options']);
				foreach ($optionlist as $index => $value) {
					$optiondetails = explode(':', $value);
					$options[$optiondetails[0]] = $optiondetails[1];
				}
				$result[$row['field_name']] = $row;
				$result[$row['field_name']]['options'] = $options;
			} else {
				$options = array();
				global $connectionmanager;
				$params = explode(',', $row['options']);
				$data = mysqli_query($connectionmanager->connection, "SELECT " . $params[0] . "," . $params[1] . " FROM " . $row['requirements']);
				while ($datarow = mysqli_fetch_assoc($data)) {
					$options[$datarow[$params[0]]] = stripslashes($datarow[$params[1]]);
				}
				$result[$row['field_name']] = $row;
				$result[$row['field_name']]['options'] = $options;
			}
		}
		return $result;
	}
	/**
	 * Performs count type query
	 *
	 * @return Data Returns query result
	 */
	public function GetCount($var) {
		$table = $this->link('table');
		global $modulemanager;
		global $connectionmanager;
		//get options for variable
		$fields = mysqli_fetch_assoc(mysqli_query($modulemanager->connection, "SELECT * FROM field_data WHERE db='" . $table[0] . "' AND field_name='" . $var . "'"));
		$optionlist = array();
		$result = array();
		$result['friendly_name'] = $fields['friendly_name'];
		if ($fields['type'] == 'tiny') {
			$result['options'] = array('No', 'Yes');
		} elseif ($fields['requirements'] == '') {
			$options = array();
			$optionlist = explode(',', $fields['options']);
			foreach ($optionlist as $index => $value) {
				$optiondetails = explode(':', $value);
				$options[$optiondetails[0]] = $optiondetails[1];
			}
			$result['options'] = $options;
		} else {
			$options = array();
			global $connectionmanager;
			$params = explode(',', $fields['options']);
			$data = mysqli_query($connectionmanager->connection, "SELECT " . $params[0] . "," . $params[1] . " FROM " . $fields['requirements']);
			while ($datarow = mysqli_fetch_assoc($data)) {
				$options[$datarow[$params[0]]] = stripslashes($datarow[$params[1]]);
			}
			$result['options'] = $options;
		}
		//Fetch data
		$primary = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SHOW KEYS FROM " . $table[0] . " WHERE Key_name = 'PRIMARY'"));
		$query = "SELECT " . $var . ", COUNT(" . $primary[4] . ") AS count FROM " . $table[0] . " WHERE ";
		if (isset($_GET['field_' . $this->id . '_0']) || isset($_GET['field_all_0'])) {
			$query.= $this->search() . " AND " . $var . " NOT LIKE '' GROUP BY " . $var;
		} else $query.= $var . " NOT LIKE '' GROUP BY " . $var;
		$data = mysqli_query($connectionmanager->connection, $query);
		$result['query'] = $query;
		$result['result'] = $data;
		return $result;
	}
}
?>
