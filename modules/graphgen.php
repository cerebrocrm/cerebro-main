<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Generates graph representations of data
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class GraphGenRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('DBmanager' => 'DBmanager providing data', 'var' => 'pie chart variable', 'linkvar' => 'link variable', 'query' => 'cloud query string', 'details' => 'cloud detail page string');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetPieChart' => 'Returns pie chart', 'GetTimeline' => 'Returns timeline', 'GetCloud' => 'Returns word cloud');
	//Object functions and variables go here

	/**
	 * Generates data cloud
	 *
	 * @return Content Returns html data for graph
	 */
	public function GetCloud() {
		$db = $this->link('DBmanager');
		$query = $this->link('query');
		$details = $this->link('details');
		global $ {
			'module_' . $db[0]
		};
		$data = $ {
			'module_' . $db[0]
		}->Query('', null, 'graph', array(), 0, $query[0]);
		$output = '<div>';
		while ($row_tag_cloud = mysqli_fetch_array($data)) {
			if ($row_tag_cloud['score'] > 0) {
				//make tag cloud
				if ($row_tag_cloud['score'] < 9 && $row_tag_cloud['score'] > 2) {
					$font_size = '9';
					$font_weight = 'font-weight:bold;';
				} elseif ($row_tag_cloud['score'] <= 2 && $row_tag_cloud['score'] > 1) {
					$font_size = '8';
					$font_weight = '';
				} elseif ($row_tag_cloud['score'] <= 1) {
					$font_size = '7';
					$font_weight = '';
				} else {
					$font_size = $row_tag_cloud['score'];
					$font_weight = '';
				}
				$output.= '<a href="' . $details[0] . '=' . $row_tag_cloud['id'] . '" class="tag" style="font-size: ' . $font_size . 'pt; color: #' . stringToColourCode($row_tag_cloud['title']) . ' !important;' . $font_weight . ' line-height: ' . $font_size . 'pt;" title="' . $row_tag_cloud['count'] . ' items">' . $row_tag_cloud['title'] . ' </a>';
			}
		}
		return $output . '</div>';
	}
	/**
	 * Generates pie chart
	 *
	 * @return Content Returns html data for chart
	 */
	public function GetPieChart() {
		$db = $this->link('DBmanager');
		$vars = $this->link('var');
		$link = $this->link('linkvar');
		$total = 0;
		global $item_id;
		if (isset($_GET['id'])) {
			$_GET['field_' . $db[0] . '_0'] = $link[0];
			$_GET['op_' . $db[0] . '_0'] = 'equal';
			$_GET['parama_' . $db[0] . '_0'] = $item_id;
			$_GET['inf_' . $db[0] . '_0'] = 'reg_' . $db[0];
			//global ${'module_' . $details[0]};
			//$titledata = ${'module_' . $details[0]}->Query(array($titlevars[0]),array($titlevars[1] => $_GET['id']),'list',$order,0,'');
			//$titledata = mysqli_fetch_assoc($titledata['result']);
			//$header = ' title: "'.$titledata[$titlevars[0]].'",';

		}
		global $ {
			'module_' . $db[0]
		};
		$countdata = $ {
			'module_' . $db[0]
		}->GetCount($vars[0]);
		$output = ' <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart()
      {
        var data = google.visualization.arrayToDataTable([

          ["item","value"]';
		while ($row = mysqli_fetch_array($countdata['result'])) {
			$total+= $row['count'];
			$output.= ', ["' . $countdata['options'][$row[$vars[0]]] . '",     ' . $row['count'] . ']';
		}
		$output.= ']);

        var options = {
          legend:"none",
         ' . $header . '
          chartArea: { left:0, right: 0, width:"100%" }
        };

            var chart = new google.visualization.PieChart(document.getElementById("' . $this->id . '"));
            chart.draw(data, options);
        }
        </script>';
		$output.= ' <div><div id="' . $this->id . '" style="width: 200px; height: 200px;"></div>';
		$output.= $total . ' total </div>';
		if ($this->permissions['GetPieChart'] <= Users::GetCurrentUser()['user_level']) {
			return $output;
		} else {
			return null;
		}
	}
	/**
	 * Generates timeline graph
	 *
	 * @return Content Returns html data for graph
	 */
	public function GetTimeline() {
		$db = $this->link('DBmanager');
		$query = $this->link('query');
		global $ {
			'module_' . $db[0]
		};
		$data = $ {
			'module_' . $db[0]
		}->Query('', null, 'graph', array(), 0, $query[0]);
		$total = 0;
		$output = '<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart","annotatedtimeline"]});
    public function drawSignUps()
    {
      var data = new google.visualization.DataTable();
      data.addColumn("date", "Date");
      data.addColumn("number", "data");
      data.addColumn("string", "title1");
      data.addColumn("string", "text1");
      data.addRows([';
		while ($row = mysqli_fetch_array($data)) {
			$total = $total + $row['total'];
			$date = explode('-', $row['selected']);
			if ($row['selected'] != '0000-00-00') {
				$output.= '[new Date(' . $date[0] . ',' . $date[1] . ',' . $date[2] . '), ' . $total . ', undefined, undefined],';
			}
		}
		$output.= ' ]);

      var g = new Dygraph.GVizChart(document.getElementById("' . $this->id . '"));
        g.draw(data, {
            displayAnnotations: true,
            labelsKMB: true,
            labelsDivStyles: { "textAlign": "left" },
            showRangeSelector: true
          });
    }
 google.setOnLoadCallback(drawSignUps);
  </script>';
		$output.= '<div id="' . $this->id . '" class="three columns"></div>';
		if ($this->permissions['GetTimeline'] <= Users::GetCurrentUser()['user_level']) {
			return $output;
		} else {
			return null;
		}
	}
}
?>
