<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Handles email functionality
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class Mail extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('service' => 'mandrill or ses', 'list' => 'list of contacts', 'vars' => 'name,email,id,mergevars', 'details' => 'link to details page', 'API' => 'API key', 'test_email' => 'Redirect all AutoEmails to this email address for testing');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetEmail' => 'Returns popup email', 'SendAutoEmails' => 'Send automatic emails according to search parameters');
	//Object functions and variables go here

	/**
	 * Update email templates
	 *
	 * @return ID Returns inserted template ID
	 */
	public function tempupdate() {
		global $connectionmanager;
		if (isset($_POST['template_id_0'])) $query = "UPDATE email_templates SET template_subject ='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['template_subject_0']) . "',template_text='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['template_text_0']) . "',user_id='" . mysqli_real_escape_string($connectionmanager->connection, $_POST['user_id_0']) . "' WHERE template_id = '" . mysqli_real_escape_string($connectionmanager->connection, $_POST['template_id_0']) . "'";
		else $query = "INSERT INTO email_templates (template_name,template_subject,template_text,user_id) VALUES ('" . mysqli_real_escape_string($connectionmanager->connection, $_POST['template_name_0']) . "','" . mysqli_real_escape_string($connectionmanager->connection, $_POST['template_subject_0']) . "','" . mysqli_real_escape_string($connectionmanager->connection, $_POST['template_text_0']) . "','" . mysqli_real_escape_string($connectionmanager->connection, $_POST['user_id_0']) . "')";
		mysqli_query($connectionmanager->connection, $query);
		$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM email_templates WHERE user_id =" . mysqli_real_escape_string($connectionmanager->connection, $_POST['user_id_0']) . " AND template_text LIKE '" . mysqli_real_escape_string($connectionmanager->connection, $_POST['template_text_0']) . "'"));
		$insert = $insert['template_id'];
		return $insert;
	}
	/**
	 * Generates email user interface
	 *
	 * @return Content Returns html/scripting for email interface
	 */
	public function GetEmail() {
		global $connectionmanager;
		global $page;
		$query = "SELECT * FROM  email_templates WHERE user_id = " . Users::GetCurrentUser()['id'];
		$result = mysqli_query($connectionmanager->connection, $query);
		$counter = 0;
		$first_template_contents = '';
		$first_template_subject = '';
		$templates = '';
		$output = '

                     <a href="#magnific_email_content" class="open-popup-link button magnific_email"><span class="fa fa-envelope" title="Send an email to these contacts"></a>

               	';
		$output.= '<div id ="magnific_email_content" class="white-popup mfp-hide"">
		 <style type="text/css">
                #template_selector_chosen {
                    width: 66% !important;
                    height: 40px;
                }

                #template_selector_chosen .chosen-drop {
                    margin-top: 15px;
                }
            </style>
            <div id ="email_div">
            <div class = "row action_bar">
            <a href="" class="button"><span class="fa fa-times fa-2x" aria-hidden="true"title="Close"></span> Close</a></div>
            <h3 style="margin-top: 14px;">Email contact</h3>

            <div class="row action_bar ten columns"><select name="template_selector"  style="width:130px;" id="template_selector">';
		while ($row = mysqli_fetch_array($result)) {
			$output.= '<option value="' . $row['template_id'] . '">' . stripslashes($row['template_name']) . '</option>';
			$templates.= '<div id ="content_' . $row['template_id'] . '" class="email_template">' . $row["template_text"] . '</div>
                            <div id ="subject_' . $row['template_id'] . '" class="email_template">' . $row["template_subject"] . '</div>';
			if ($counter == 0) {
				$first_template_contents = stripslashes($row["template_text"]);
				$first_template_subject = stripslashes($row["template_subject"]);
			}
			$counter++;
		}
		$id = '';
		$i = 0;
		foreach ($_GET as $index => $value) {
			$id.= '&' . $index . '=' . $value;
			$i++;
		}
		$output.= '</select>

                    <a class="button" href="#" id="save_template"><span class="fa fa-floppy-o fa-2x" aria-hidden="true" title="Save this template"></a> <a class="button" href="#" id="save_new_template"><span class="fa fa-plus fa-2x" aria-hidden="true"title="Add a new template"></span> New template</a> </div>
                <form action="" method="POST" enctype="multipart/form-data" name="email_contact" id="email_contact">
                <input type="hidden" name="send_email" value="1" />

                <p>Subject &nbsp;&nbsp;<input type="text" id="email_subject" style="width:85%;" name="email_subject" value="' . $first_template_subject . '"/></p>
                <textarea name="email_text" style="width:95% "rows="10" id="email_text" class="email_text"> ' . $first_template_contents . '</textarea>
                <p><small><strong>[contact_first]</strong> = First name, <strong>[contact_last]</strong> = Last name, <strong>[contact_when_we_spoke]</strong> = When we met (e.g. "last tuesday"), <br /><strong>[contact_location]</strong> = Location name</small></p>

                <p><br /></p>
                <p><strong>Attachments</strong></p>

                <input id="fileupload" type="file" name="files[]" data-url="server/php/" multiple>
                <div id="progress">
                    <p> </p>
                </div>
                <div id="files">

                </div>

                <p><br /></p>
                <p><strong>What are you emailing about?</strong></p>
                <textarea name="note_text" style="width:95% "rows="2" id="note_text">';
		if (isset($_GET['from_new'])) $output.= 'I emailed an introductory message.';
		else $output.= 'I emailed to...';
		$output.= '</textarea>

                <div class = "row action_bar">
                <input type="submit" name="Submit2" id="send_email" value="Send email" /></div>
                </form>
                <hr />
            </div></div>' . $templates . '<div class="email_template" id = "new_template">Enter a name for this template:</br><input type="text" id="template_name"></div>
            <script type="text/javascript">
            $(document).ready(function () {

               var i = 0;
                $(function () {
                    $("#fileupload").fileupload({
                        dataType: "json",
                        done: function (e, data) {
                            $.each(data.result.files, function (index, file) {

                                $("<input>").attr({
                                    type: "hidden",
                                    id: "upload_"+i,
                                    name: "upload_"+i,
                                    value: file.name,
                                }).appendTo("#files");

                                $("<p/>").text(file.name).appendTo("#files");
                                i = i + 1;

                            });
                        }
                    });
                });


                $(".email_template").hide();
                tinymce.init({ mode : "exact",
                    menubar:false,
                });
                $(".magnific_email").magnificPopup({
                  type:"inline",
                  showCloseBtn: false,
                  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don\'t provide alternative source in href.
                });
                $( ".magnific_email" ).click(function () {
                        tinyMCE.execCommand("mceToggleEditor", false, "email_text");
                });
                $( "#save_template" ).click(function (e) {
                        var editor = tinymce.get("email_text");
                        var save_template_content = editor.getContent();
                        var save_template_content = encodeURIComponent(save_template_content);
                        var save_template_subject = $("#email_subject").val();
                        var save_template_subject = encodeURIComponent(save_template_subject);
                        var save_template_id = $("#template_selector").val();
                        var save_dataString = "template_id_0="+save_template_id+"&template_subject_0="+save_template_subject+"&template_text_0="+save_template_content + "&user_id_0=' . Users::GetCurrentuser()['id'] . '";
                        $.ajax({
                            type: "POST",
                            url: "?page=' . $page . '&instance=' . $this->id . '&method=tempupdate",
                            data: save_dataString,
                             success: function () {
                                alert("Template saved!");
                                $("#subject_"+template_id).replaceWith( "<div id =\'subject_"+template_id+"\' class=\'email_template\'>"+template_subject+"</div>" );
                                $("#content_"+template_id).replaceWith( "<div id =\'content_"+template_id+"\' class=\'email_template\'>"+template_content+"</div>" );
                            }
                        });
                        e.preventDefault();
                });
                $( "#save_new_template" ).click(function (e) {
                        var save_temp_popup = prompt("Please enter a name for this template");
                        if (save_temp_popup) {
                            var editor = tinymce.get("email_text");
                            var save_template_content = editor.getContent();
                            var save_template_content = encodeURIComponent(save_template_content);
                            var save_template_subject = $("#email_subject").val();
                            var save_template_subject = encodeURIComponent(save_template_subject);
                            var save_template_id = $("#template_selector").val();
                            var save_dataString = "template_name_0="+save_temp_popup+"&template_subject_0="+save_template_subject+"&template_text_0="+save_template_content+"&user_id_0=' . Users::GetCurrentuser()['id'] . '";

                            $.ajax({
                                type: "POST",
                                url: "?page=' . $page . '&instance=' . $this->id . '&method=tempupdate",
                                data: save_dataString,
                                success: function (result) {
                                    $( "#template_selector" ).append( "<option value="+result+" selected>"+save_temp_popup+"</option>" );
                                    alert("Template saved!");
                                }
                            });
                        } else {
                            alert("Template not saved");
                        }
						 e.preventDefault();
                });
                $("#template_selector").change(function () {
                    var $dropdown_template = $(this);
                    var key_template = $dropdown_template.val();
                    var template_subject = $("#subject_"+key_template).text();
                    var template_content = $("#content_"+key_template).html();
                    $("#email_subject").replaceWith("<input type=\'text\' id=\'email_subject\' style=\'width:85%;\' name=\'email_subject\' value=\'"+template_subject+"\'/>");
                    var editor = tinymce.get("email_text");
                    var content = editor.setContent(template_content);
                });
                $( "#send_email" ).click(function (e) {
                        var editor = tinymce.get("email_text");
                        var save_template_content = editor.getContent();
                        var save_template_subject = $("#email_subject").val();
                        var save_template_subject = encodeURIComponent(save_template_subject);
                        var save_template_content = encodeURIComponent(save_template_content);
                        var note_text = $("#note_text").val();

                        var extra_dataString = "";
                        var i = 0;
                        $("#files input").each(function( index ) {
                            extra_dataString = extra_dataString+"&upload_"+i+"="+$(this).attr("value");
                            i = i+1;
                        });

                        var save_dataString = "note_text="+note_text+"&email_subject="+save_template_subject+"&email_text="+save_template_content+extra_dataString;
                        $.ajax({
                            type: "POST",
                            url: "?instance=' . $this->id . '&method=SendEmail' . $id . '",
                            data: (save_dataString),
                            success: function (result) {
								//alert(result);
								location.reload();
                            }
                        });
						e.preventDefault();
                });
            });
            </script>';
		return $output;
	}
	/**
	 * Processes email data according to service selection
	 *
	 * @return Checkdigit Returns 1 on success
	 */
	public function SendEmail() {
		global $connectionmanager;
		//Set up
		$service = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_service'"));
		if ($service['pref_value'] == 'ses') {
			return $this->ses_SendEmail();
		} elseif ($service['pref_value'] == 'sendgrid') {
			return $this->sendgrid_SendEmail();
		} else {
			return $this->mandrill_SendEmail();
		}
	}
	/**
	 * Processes email data with Amazon's Cloud Simple Email Service (SES)
	 *
	 * @return Checkdigit Returns 1 on sucess
	 */
	public function sendgrid_SendEmail() {
		global $connectionmanager;
		// include the SendGrid library
		require ("includes/vendor/autoload.php");
		//Set up
		$api1 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'sendgrid_api_key'"));
		$api = array(0 => $api1['pref_value'],);
		$list = $this->link('list');
		$vars = $this->link('vars');
		$return = 0;
		$for_session = '';
		$for_session_number = 1;
		$details = $this->link('details');
		global $dbmanager;
		$order = array('');
		$select = array();
		global $item_id;
		if ($list[0] != '') {
			foreach ($list as $subset) $select = array_merge($select, $subset);
		} elseif (isset($_GET['id'])) $select = array(array('id' => $item_id));
		//$data = ${'module_' . $DBmanager[0]}->Query($vars,null,'list',$order,0,'');
		$data = $dbmanager->Query($vars, $select, null, 0, $this->id);
		//Get non variable email parameters
		$message = urldecode(urldecode(insert('email_text')));
		$tempmessage = urldecode(urldecode(insert('email_text')));
		$subject = insert('email_subject');
		//Add campaign entry
		global $connectionmanager;
		mysqli_query($connectionmanager->connection, "INSERT INTO email_campaigns (campaign_subject, campaign_no_sent, campaign_full_text) VALUES
            (
            '$subject',
            " . $data['result_count'] . ",
            '$message'
            )
            ");
		$cid = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM email_campaigns WHERE campaign_subject LIKE '" . $subject . "' AND campaign_full_text LIKE '" . $message . "'"));
		$cid = $cid['campaign_id'];
		$from_email = str_replace('@', '_', Users::GetCurrentUser()['email']) . '@cerebro.org.uk';
		// loop through results (AND SEND EACH AND EVERY LOOP, BUILD ARRAY OF ERRORS AND SUCCESSES)
		// DIFFERENCE FROM MANDRILL - "We recommend that you call SendEmail once for every recipient."
		$success_emails = array();
		$fail_emails = array();
		$sendgrid = new SendGrid($api[0]);
		foreach ($data['result'] as $row) {
				$tempmessage2 = $tempmessage; // proecting against sideeffects
			if (filter_var(trim($row[$vars[1]]), FILTER_VALIDATE_EMAIL) && $row[$vars[1]] != null) {
				
				foreach ($vars as $var) {
					if ($data[$var]['data_format'] == 1) $row[$var] = fuzzy_time($row[$var]);
					$tempmessage2 = str_replace('[' . $var . ']', stripslashes($row[$var]), $tempmessage2);
				}
				$tempmessage2 = str_replace('<br />', ' <br />', $tempmessage2);
				$tempmessage2 = str_replace('<br>', ' <br />', $tempmessage2);
				$tempmessage2 = str_replace('</a>', ' </a>', $tempmessage2);
				$tempmessage2 = str_replace('</p>', ' </p>', $tempmessage2);
				$urls = convert_urls_to_tiny($tempmessage2, $row[$vars[2]], $subject, '1', $cid);
				$pre_msg = $urls['text'];
				unset($urls['text']);
				foreach ($urls as $id => $link) {
					$pre_msg = str_replace('*|' . $id . '|*', $link, $pre_msg);
				}
				$message = $pre_msg;
				$message = str_replace('< br/>', ' <br/>', $message);
				$message = str_replace('< /a>', ' </a>', $message);
				$message = str_replace('< /p>', ' </p>', $message);
				$message = '<html>
					<body>
					<img border="0" src="http://' . $_SERVER['SERVER_NAME'] . '/go/header.php?i=' . $cid . '&c=' . $row[$vars[2]] . '"/>
					<br />
					' . $message . '
					</body>
					</html>';
				$email = new SendGrid\Email();
				$email->addTo($row[$vars[1]])->setFrom(Users::GetCurrentUser()['email'])->setSubject(stripslashes($subject))->setHtml(stripslashes($message))->setFromName(Users::GetCurrentUser()['name'])->setReplyTo(Users::GetCurrentUser()['email']);
				// catch the error
				try {
					$sendgrid->send($email);
					$return = 1;
					$success_emails[] = $row[$vars[1]];
					echo 'Success!';
				}
				catch(\SendGrid\Exception $e) {
					if ($for_session_number == 1) {
						$for_session.= '<a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
						$for_session_number++;
					} else {
						$for_session.= ', <a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
					}
					echo $e->getCode();
					foreach ($e->getErrors() as $er) {
						echo $er;
					}
				}
				mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item, note_text, note_date,note_type,note_campaign) VALUES
					(
					" . $row[$vars[2]] . ",
					'" . addslashes($_POST['note_text']) . "',
					" . time() . ",
					6,
					" . $cid . "
					)
				");
			} else {
				if ($for_session_number == 1) {
					$for_session.= '<a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
					$for_session_number++;
				} else {
					$for_session.= ', <a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
					$fail_emails[] = $row[$vars[1]] . ' (Invalid email)';
				}
			}
			if ($for_session_number > 1) {
				$_SESSION['msg'] = Array('type' => 'warning', 'message' => 'The following contacts had invalid email addresses: ' . $for_session, 'id' => null, 'table' => null);
			} else {
				$_SESSION['msg'] = Array('type' => 'correct', 'message' => 'All your emails were successfully sent.', 'id' => null, 'table' => null);
			}
		}
		return $return;
	}
	/**
	 * Processes email data with Amazon's Cloud Simple Email Service (SES)
	 *
	 * @return Checkdigit Returns 1 on sucess
	 */
	public function ses_SendEmail() {
		global $connectionmanager;
		// include the Amazon library
		require_once ("includes/ses-utils.php");
		//Set up
		$api1 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'ses_key1'"));
		$api2 = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'ses_key2'"));
		$api = array(0 => $api1['pref_value'], 1 => $api2['pref_value']);
		$list = $this->link('list');
		$vars = $this->link('vars');
		$return = 0;
		$for_session = '';
		$for_session_number = 1;
		$details = $this->link('details');
		global $dbmanager;
		$order = array('');
		$select = array();
		global $item_id;
		if ($list[0] != '') {
			foreach ($list as $subset) $select = array_merge($select, $subset);
		} elseif (isset($_GET['id'])) $select = array(array('id' => $item_id));
		//$data = ${'module_' . $DBmanager[0]}->Query($vars,null,'list',$order,0,'');
		$data = $dbmanager->Query($vars, $select, null, 0, $this->id);
		//Get non variable email parameters
		$message = urldecode(urldecode(insert('email_text')));
		$tempmessage = urldecode(urldecode(insert('email_text')));
		$subject = insert('email_subject');
		//Add campaign entry
		global $connectionmanager;
		mysqli_query($connectionmanager->connection, "INSERT INTO email_campaigns (campaign_subject, campaign_no_sent, campaign_full_text) VALUES
            (
            '$subject',
            " . $data['result_count'] . ",
            '$message'
            )
            ");
		$cid = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM email_campaigns WHERE campaign_subject LIKE '" . $subject . "' AND campaign_full_text LIKE '" . $message . "'"));
		$cid = $cid['campaign_id'];
		$from_email = "" . Users::GetCurrentUser()['name'] . ' <' . str_replace('@', '_', Users::GetCurrentUser()['email']) . '@cerebro.org.uk>';
		// loop through results (AND SEND EACH AND EVERY LOOP, BUILD ARRAY OF ERRORS AND SUCCESSES)
		// DIFFERENCE FROM MANDRILL - "We recommend that you call SendEmail once for every recipient."
		$success_emails = array();
		$fail_emails = array();
		foreach ($data['result'] as $row) {
			if (filter_var(trim($row[$vars[1]]), FILTER_VALIDATE_EMAIL) && $row[$vars[1]] != null) {
				foreach ($vars as $var) {
					if ($data[$var]['data_format'] == 1) $row[$var] = fuzzy_time($row[$var]);
					$tempmessage = str_replace('[' . $var . ']', stripslashes($row[$var]), $tempmessage);
				}
				$tempmessage = str_replace('<br />', ' <br />', $tempmessage);
				$tempmessage = str_replace('<br>', ' <br />', $tempmessage);
				$tempmessage = str_replace('</a>', ' </a>', $tempmessage);
				$tempmessage = str_replace('</p>', ' </p>', $tempmessage);
				$urls = convert_urls_to_tiny($tempmessage, $row[$vars[2]], $subject, '1', $cid);
				$pre_msg = $urls['text'];
				unset($urls['text']);
				foreach ($urls as $id => $link) {
					$pre_msg = str_replace('*|' . $id . '|*', $link, $pre_msg);
				}
				$message = $pre_msg;
				$message = str_replace('< br/>', ' <br/>', $message);
				$message = str_replace('< /a>', ' </a>', $message);
				$message = str_replace('< /p>', ' </p>', $message);
				$message = '<html>
					<body>
					<img border="0" src="http://' . $_SERVER['SERVER_NAME'] . '/go/header.php?i=' . $cid . '&c=' . $row[$vars[2]] . '"/>
					<br />
					' . $message . '
					</body>
					</html>';
				$params = array("to" => $row[$vars[1]], "subject" => $subject, "message" => $message, "from" => $from_email, "replyTo" => Users::GetCurrentUser()['email'],);
				$aws = new SESUtils($api[0], $api[1]);
				$res = $aws->sendMail($params);
				if ($res->success == 1) {
					$return = 1;
					$success_emails[] = $row[$vars[1]];
					echo 'Success! - ' . $res->message_id;
				} else {
					if ($for_session_number == 1) {
						$for_session.= '<a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
						$for_session_number++;
					} else {
						$for_session.= ', <a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
					}
					$fail_emails[] = $row[$vars[1]] . ' (' . $res->result_text . ')';
					echo 'Fail! - ' . $res->result_text;
				}
				mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item, note_text, note_date,note_type,note_campaign) VALUES
					(
					" . $row[$vars[2]] . ",
					'" . addslashes($_POST['note_text']) . "',
					" . time() . ",
					6,
					" . $cid . "
					)
				");
			} else {
				if ($for_session_number == 1) {
					$for_session.= '<a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
					$for_session_number++;
				} else {
					$for_session.= ', <a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
					$fail_emails[] = $row[$vars[1]] . ' (Invalid email)';
				}
			}
			if ($for_session_number > 1) {
				$_SESSION['msg'] = Array('type' => 'warning', 'message' => 'The following contacts had invalid email addresses: ' . $for_session, 'id' => null, 'table' => null);
			} else {
				$_SESSION['msg'] = Array('type' => 'correct', 'message' => 'All your emails were successfully sent.', 'id' => null, 'table' => null);
			}
		}
		return $return;
	}
	/**
	 * Processes email data with mandrill
	 *
	 * @return Checkdigit Returns 1 on sucess
	 */
	public function mandrill_SendEmail() {
		global $connectionmanager;
		//Set up
		$api = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'mandrill_api_key'"));
		$list = $this->link('list');
		define('MANDRILL_API_KEY', $api['pref_value']);
		//$DBmanager = $this->link('DB');
		$vars = $this->link('vars');
		$details = $this->link('details');
		global $dbmanager;
		$order = array('');
		$select = array();
		global $item_id;
		if ($list[0] != '') {
			foreach ($list as $subset) $select = array_merge($select, $subset);
		} elseif (isset($_GET['id'])) $select = array(array('id' => $item_id));
		//$data = ${'module_' . $DBmanager[0]}->Query($vars,null,'list',$order,0,'');
		$data = $dbmanager->Query($vars, $select, null, 0, $this->id);
		//Get non variable email parameters
		$message = urldecode(urldecode(insert('email_text')));
		$tempmessage = urldecode(urldecode(insert('email_text')));
		$subject = insert('email_subject');
		//Add campaign entry
		global $connectionmanager;
		mysqli_query($connectionmanager->connection, "INSERT INTO email_campaigns (campaign_subject, campaign_no_sent, campaign_full_text) VALUES
            (
            '$subject',
            " . $data['result_count'] . ",
            '$message'
            )
            ");
		$cid = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM email_campaigns WHERE campaign_subject LIKE '" . $subject . "' AND campaign_full_text LIKE '" . $message . "'"));
		$cid = $cid['campaign_id'];
		$msgs = array();
		$msginfo = array();
		$for_session = '';
		$for_session_number = 1;
		//loop through results
		foreach ($data['result'] as $row) {
			if (filter_var(trim($row[$vars[1]]), FILTER_VALIDATE_EMAIL) && $row[$vars[1]] != null) {
				mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item, note_text, note_date,note_type,note_campaign) VALUES
                (
                " . $row[$vars[2]] . ",
                '" . addslashes($_POST['note_text']) . "',
                " . time() . ",
                6,
                " . $cid . "
                )
            ");
				$msgdata = array();
				foreach ($vars as $var) {
					if ($data[$var]['data_format'] == 1) $row[$var] = fuzzy_time($row[$var]);
					$msgdata[] = array('name' => $var, 'content' => stripslashes($row[$var]));
				}
				$urls = convert_urls_to_tiny($tempmessage, $row[$vars[2]], $subject, '1', $cid);
				$message = $urls['text'];
				foreach ($urls as $index => $value) {
					if ($index != 'text') $msgdata[] = array('name' => $index, 'content' => $value);
				}
				$msgs[] = array('rcpt' => stripslashes(trim($row[$vars[1]])), 'vars' => $msgdata);
				$msginfo[] = array('email' => stripslashes(trim($row[$vars[1]])), 'name' => stripslashes($row[$vars[0]]));
			} else {
				if ($for_session_number == 1) {
					$for_session.= '<a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
					$for_session_number++;
				} else {
					$for_session.= ', <a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
				}
			}
			if ($for_session_number > 1) {
				$_SESSION['msg'] = Array('type' => 'warning', 'message' => 'The following contacts had invalid email addresses: ' . $for_session, 'id' => null, 'table' => null);
			} else {
				$_SESSION['msg'] = Array('type' => 'correct', 'message' => 'All your emails were successfully sent.', 'id' => null, 'table' => null);
			}
		}
		$this->send_newsletter(Users::GetCurrentUser(), $msginfo, $subject, $message, $msgs, $cid);
		return 1;
	}
	/**
	 * Sends email data with mandrill
	 *
	 * @return void
	 */
	function send_newsletter($user, $msginfo, $subject, $text, $msgs, $campaign_id, $vars = null) {
		if (!isset($vars)) {
			$vars = $this->link('vars');
		}
		$user_name = $user['name'];
		$user_email = $user['email'];
		//SEND EMAIL
		// Joseph Farthing's Super Simple Emailer
		// a simple mailer script to send an email
		// grab the user email and friends email from the POST data
		$new_message = $text;
		// replace message variables
		$new_message = str_replace('[user_name]', $user_name, $new_message);
		$new_message = str_replace('[first_name]', '[contact_first]', $new_message);
		$new_message = str_replace('[last_name]', '[contact_last]', $new_message);
		$new_message = str_replace('[x_days_ago]', '[contact_when_we_spoke]', $new_message);
		$new_message = str_replace('[location_name]', '[contact_location]', $new_message);
		foreach ($vars as $var) {
			$new_message = str_replace('[' . $var . ']', '*|' . $var . '|*', $new_message);
		}
		// replace subject variables
		$subject = str_replace('[user_name]', $user_name, $subject);
		$subject = stripslashes($subject);
		foreach ($vars as $var) {
			$subject = str_replace('[' . $var . ']', '*|' . $var . '|*', $subject);
		}
		//strip possibly dangerous content from the strings
		$subject = str_replace(array('<', '>', "\r", "\n"), '', $subject);
		$new_message = stripslashes($new_message);
		$new_message = str_replace(array('
        '), '<br />', $new_message);
		$new_message = str_replace(' /p>', '</p>', $new_message);
		$new_message = str_replace("\\'", '\'', $new_message);
		$new_message = str_replace("\\\"", '"', $new_message);
		$new_message = str_replace("/cerebro/images", $_SERVER['SERVER_NAME'] . '/go/images', $new_message);
		$new_message = str_replace("youd", "you'd", $new_message);
		$new_message = str_replace("youre", "you're", $new_message);
		$new_message = str_replace("Universitys", "University's", $new_message);
		$new_message = str_replace(" cant ", " can't ", $new_message);
		$new_message = str_replace(" /span>", " ", $new_message);
		$new_message = '<html>
        <body>
        <img border="0" src="http://' . $_SERVER['SERVER_NAME'] . '/go/header.php?i=' . $campaign_id . '&c=*|' . $vars[2] . '|*"		/>
        <br />
        ' . $new_message . '
        </body>
        </html>';
		// attachments
		$i = 0;
		$attachments = Array();
		$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
		while (isset($_REQUEST['upload_' . $i])) {
			if (file_exists('./server/php/files/' . $_POST['upload_' . $i])) {
				$attachments[] = Array('type' => finfo_file($finfo, './server/php/files/' . $_POST['upload_' . $i]), 'name' => $_POST['upload_' . $i], 'content' => base64_encode(file_get_contents('./server/php/files/' . $_POST['upload_' . $i])));
				$i++;
				unlink('./server/php/files/' . $_POST['upload_' . $i]);
			}
		}
		$mandrill = new Mandrill();
		$message = array('subject' => $subject, 'from_email' => $user_email, 'html' => $new_message, 'merge_vars' => $msgs, 'tags' => array(get_instance()), 'attachments' => $attachments, 'to' => $msginfo);
		$result = $mandrill->messages->send($message);
		file_put_contents('email_log.php', '
        ' . $subject . serialize($result), FILE_APPEND | LOCK_EX);
	}
	/**
	 * Processes automated email data
	 *
	 * @return Checkdigit Returns 1 on sucess
	 */
	public function AutoEmail($table, $joins, $query, $message, $subject) {
		//Set up
		$api = $this->link('API');
		$vars = $this->link('vars');
		$test_email = $this->link('test_email');
		define('MANDRILL_API_KEY', $api[0]);
		//Get non variable email parameters
		$tempmessage = $message;
		global $connectionmanager;
		//build query
		$data = mysqli_query($connectionmanager->connection, "SELECT * FROM $table $joins WHERE $query");
		$msgs = array();
		$msginfo = array();
		//loop through results
		if (mysqli_num_rows($data) > 0) {
			while ($row = mysqli_fetch_array($data)) {
				if ($test_email[0] != null) {
					// test mode
					$row[$vars[1]] = $test_email[0];
					$row[1] = $test_email[0];
				}
				if (filter_var($row[$vars[1]], FILTER_VALIDATE_EMAIL)) {
					mysqli_query($connectionmanager->connection, "INSERT INTO global_notes (note_item, note_text, note_date, note_status,note_type) VALUES
                    (
                    " . $row[$vars[2]] . ",
                    'Sent the automatic email - **$subject**',
                    " . time() . ",
                    1,
                    6,
                    )
                ");
					$msgdata = array();
					$urls = convert_urls_to_tiny($tempmessage, $row[$vars[2]], $subject, '1', 0);
					$message = $urls['text'];
					foreach ($urls as $index => $value) {
						if ($index != 'text') $msgdata[] = array('name' => $index, 'content' => $value);
					}
					$msgs[] = array('rcpt' => $row[$vars[1]], 'vars' => $msgdata);
					$msginfo[] = array('email' => $row[$vars[1]], 'name' => $row[$vars[0]]);
				} else {
					if ($for_session_number == 1) {
						$for_session.= '<a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
						$for_session_number++;
					} else {
						$for_session.= ', <a href="?page=' . $details[0] . '&id=' . $row[$vars[2]] . '">' . $row[$vars[0]] . '</a>';
					}
				}
				if (isset($for_session_number)) {
					if ($for_session_number > 1) {
						$_SESSION['msg'] = Array('type' => 'warning', 'message' => 'The following contacts had invalid email addresses: ' . $for_session, 'id' => null, 'table' => null);
					} else {
						$_SESSION['msg'] = Array('type' => 'correct', 'message' => 'All your emails were successfully sent.', 'id' => null, 'table' => null);
					}
				}
			}
			$this->send_newsletter(Users::GetCurrentUser(), $msginfo, $subject, $message, $msgs, 0);
			return 1;
		} else {
			return 0;
		}
	}
	/**
	 * Sends automated emails
	 *
	 * @return void
	 */
	public function SendAutoEmails() {
		global $modulemanager;
		$auto_emails = mysqli_query($modulemanager->connection, "SELECT * FROM autoemails");
		$output = '';
		while ($row = mysqli_fetch_assoc($auto_emails)) {
			$output.= 'Sending "' . $row['autoemail_name'] . '"... ';
			$output.= $this->AutoEmail('contacts', $row['autoemail_joins'], $row['autoemail_query'], $row['autoemail_message'], $row['autoemail_subject']);
			$output.= '
';
		}
		return $output;
	}
}
?>
