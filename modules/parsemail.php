<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Handles email functionality
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class ParseMail extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('server' => 'pop3 mail server address', 'username' => 'username', 'password' => 'password');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('UpdateNotes' => 'Parse incoming mail and update notes', 'ContactFlag' => 'Flag unknown contacts', 'ResetLog' => 'Reset non-flagged error logs');
	//Object functions and variables go here
	
	/**
	 * Check if a mail hash already exists and return true if it does
	 *
	 * @return boolean
	 */
	function CheckHashExists($string) {
		global $connectionmanager;
		$string = preg_replace('/\[.*?\] /', '', $string);
		$hash = md5($string);
		$query = mysqli_query($connectionmanager->connection, "SELECT COUNT(*) FROM global_notes WHERE note_hash = '" . $hash . "'");
		$hashcount = mysqli_fetch_row($query);
		if ($hashcount[0] > 0) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * Parse incoming mail and update notes
	 *
	 * @return void
	 */
	public function ContactFlag() {
		global $connectionmanager;
		global $dbmanager;
		$users = array();
		$list = mysqli_query($connectionmanager->connection, "SELECT * FROM parsemail_new_contacts");
		while ($row = mysqli_fetch_assoc($list)) {
			if (array_key_exists($row['user_id'], $users)) $users[$row['user_id']][$row['email']] = 1;
			else $users[$row['user_id']] = array($row['email'] => 1);
		}
		print_r($users);
		$newpost = new Post;
		$subject = 'Cerebro - unknown contacts';
		foreach ($users as $user => $contacts) {
			$message = '<p>You were emailed by the following unknown addresses:</p><ul>';
			foreach ($contacts as $contact => $c) {
				$message.= '<li>' . $contact . '</li>';
			}
			$message.= '</ul><p>You are being sent this message because you are a user on the Cerebro contact management system.</p>
			<p><strong>If you are frequently emailing these people</strong>, why not add them as contacts?</p><p>That way, we can all benefit from knowing that you have a relationship.</p><p>Yours,</p><p>Cerebro</p>';
			$userdetails = $dbmanager->Query(array('user_email', 'user_name', 'user_level'), array(array('id' => $user)), null, 0, $this->id);
			foreach ($userdetails['result'] as $result) {
				print_r($result);
				$useremail = $result['user_email'];
				$username = $result['name'];
				$userlevel = $result['user_level'];
			}
			$message = '<p>Dear ' . $username . ', </p>' . $message;
			if ($userlevel !== 5) {
				$newpost->Email(trim(stripslashes($useremail)), array('text' => $message, 'subject' => $subject));
			}
		}
		mysqli_query($connectionmanager->connection, "DELETE FROM parsemail_new_contacts WHERE 1");
		return 1;
	}
	public function ResetLog() {
		global $connectionmanager;
		$yesterday = 'dotmailer_' . date('m_d_Y', time() - 24 * 60 * 60) . '.php';
		$start = strtotime(date('d-m-Y', time() - 24 * 60 * 60));
		$end = strtotime(date('d-m-Y', time()));
		$log = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM module_preferences WHERE pref_name LIKE 'email_log_count'"));
		$notes = mysqli_query($connectionmanager->connection, "SELECT note_id FROM global_notes WHERE (note_type=9 OR note_type=10) AND note_date > " . $start . " AND note_date < " . $end);
		if (mysqli_num_rows($notes) == $log['pref_value']) {
			unlink($yesterday);
			mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=0 WHERE pref_name LIKE 'email_log_count'");
		}
		return 1;
	}
	public function UpdateNotes() {
		require_once "ezc/Base/base.php";
		$loads = spl_autoload_functions();
		if (!(is_array($loads))) $loads = array();
		if (!(array_key_exists('my_autoloader', $loads))) {
			if (!(function_exists(my_autoloader))) {
				function my_autoloader($className) {
					ezcBase::autoload($className);
				}
			}
			spl_autoload_register('my_autoloader');
		}
		global $connectionmanager;
		global $dbmanager;
		$server = $this->link('server');
		$user = $this->link('username');
		$pass = $this->link('password');
		$imap = new ezcMailImapTransport($server[0]);
		$imap->authenticate($user[0], $pass[0]);
		$mailboxes = $imap->listMailboxes();
		$imap->selectMailbox('Inbox');
		$messages = $imap->listMessages();
		//print_r($messages);
		$parser = new ezcMailParser();
		$today = 'dotmailer_' . date('m_d_Y', time()) . '.php';
		$i = 0;
		foreach ($messages as $messageNr => $size) {
			$i++;
			if ($i > 100) break;
			if ($size > 20000) {
				try {
					$set = new ezcMailVariableSet($imap->top($messageNr, 5000));
					$mail = $parser->parseMail($set);
					$mail = $mail[0];
					$sensitivity = $mail->getHeader('sensitivity');
					$imap->delete($messageNr);
					if (stripos($sensitivity, 'confidential') === false && stripos($sensitivity, 'private') === false && stripos($sensitivity, 'personal') === false && stripos($mail->subject, 'private') === false && stripos($mail->subject, 'confidential') === false) {
						$contact = $dbmanager->Query(array('id'), array(array('contact_email' => $mail->from->email)), null, 0, $this->id, 1);
						if ($contact['result_count'] > 0) {
							$txt = '
						
						';
							$txt.= print_r($mail, true);
							//file_put_contents($today, $txt.PHP_EOL , FILE_APPEND);
							mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'email_log_count'");
							foreach ($contact['result'] as $result) $contactid = $result['id'];
							$init = 0;
							foreach ($mail->to as $to) {
								if ($init != 0) mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'email_log_count'");
								$user = $dbmanager->Query(array('id', 'name'), array(array('user_email' => $to->email), array('category' => 99)), null, 0, $this->id, 1);
								if ($user['result_count'] > 0) {
									foreach ($user['result'] as $result) {
										$userid = $result['id'];
										$username = $result['name'];
									}
									if ($this->CheckHashExists('9' . $mail->from->email . $userid . $mail->subject) !== true) {
										mysqli_query($connectionmanager->connection, 'INSERT INTO global_notes (note_item,note_text,note_date,note_user,note_type,note_hash) VALUES ("' . $contactid . '","Emailed [' . $username . '](?page=user-details&id=' . $userid . ') about **' . mysqli_real_escape_string($connectionmanager->connection, $mail->subject) . '**","' . time() . '","' . $userid . '","9","' . md5('9' . $mail->from->email . $userid . $mail->subject) . '")');
										$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $contactid . " AND note_hash LIKE '" . md5('9' . $mail->from->email . $userid . $mail->subject) . "'"));
										$insert = $insert['note_id'];
										mysqli_query($connectionmanager->connection, 'INSERT INTO item_links (cat1,item1,cat2,item2,note_id,link_date) VALUES (1,"' . $contactid . '",99,"' . $userid . '","' . $insert . '","' . time() . '")');
									}
								}
								$init++;
							}
						} else {
							foreach ($mail->to as $to) {
								$user = $dbmanager->Query(array('id'), array(array('user_email' => $to->email), array('category' => 99)), null, 0, $this->id, 1);
								if ($user['result_count'] > 0) {
									foreach ($user['result'] as $result) {
										$userid = $result['id'];
									}
									mysqli_query($connectionmanager->connection, 'INSERT INTO parsemail_new_contacts (user_id,email) VALUES (' . $userid . ',"' . $mail->from->email . '")');
								}
							}
						}
					}
				}
				catch(Exception $e) {
					$txt = '
						
						';
					$txt.= print_r($e, true);
					file_put_contents('dotmailer_errors.log', $txt . PHP_EOL, FILE_APPEND);
					$imap->delete($messageNr);
				}
			} else {
				try {
					$set = $imap->fetchByMessageNr($messageNr);
					$mail = $parser->parseMail($set);
					$mail = $mail[0];
					$parts = $mail->fetchParts();
					$text1 = 0;
					$text2 = 0;
					if (array_key_exists(1, $parts)) {
						$array = (array)$parts[1];
						foreach ($array as $arrindex => $arrpart) {
							if (strlen($arrindex) == 13) {
								$temparray = (array)$arrpart;
								if (array_key_exists('text', $temparray)) $text1 = 1;
							}
						}
					}
					if (array_key_exists(2, $parts)) {
						$array = (array)$parts[2];
						foreach ($array as $arrindex => $arrpart) {
							if (strlen($arrindex) == 13) {
								$temparray = (array)$arrpart;
								if (array_key_exists('text', $temparray)) $text2 = 1;
							}
						}
					}
					$sensitivity = $mail->getHeader('sensitivity');
					$imap->delete($messageNr);
					//echo 'from: '.$mail->from->email.' to: '.$mail->to[0]->email.' subject: **'.$mail->subject.'**</br>';
					if (stripos($sensitivity, 'confidential') === false && stripos($sensitivity, 'private') === false && stripos($sensitivity, 'personal') === false && stripos($mail->subject, 'private') === false && stripos($mail->subject, 'confidential') === false && stripos($mail->subject, 'confidence') === false) {
						// so this email is NOT confidential
						//if (stripos($mail->subject,'event') != false) echo $parts[1]->text.'test';
						//echo stripos($mail->subject ,'private').' - test ';
						if ($text1 != 0 && array_key_exists(1, $parts) && stripos($parts[1]->text, 'PARTSTAT=ACCEPTED') != false) {
							$start = stristr($parts[1]->text, 'DTSTART;');
							$start = substr($start, (stripos($start, ':') + 1));
							$contact = $dbmanager->Query(array('id'), array(array('contact_email' => $mail->from->email)), null, 0, $this->id, 1);
							if ($contact['result_count'] > 0) {
								$txt = '
						
						';
								$txt.= print_r($mail, true);
								//file_put_contents($today, $txt.PHP_EOL , FILE_APPEND);
								mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'email_log_count'");
								foreach ($contact['result'] as $result) $contactid = $result['id'];
								$init = 0;
								foreach ($mail->to as $to) {
									if ($init != 0) mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'email_log_count'");
									$user = $dbmanager->Query(array('id', 'name'), array(array('user_email' => $to->email), array('category' => 99)), null, 0, $this->id, 1);
									if ($user['result_count'] > 0) {
										foreach ($user['result'] as $result) {
											$userid = $result['id'];
											$username = $result['name'];
										}
										if ($this->CheckHashExists('10' . $mail->from->email . $userid . $mail->subject . $start) !== true) {
											mysqli_query($connectionmanager->connection, 'INSERT INTO global_notes (note_item,note_text,note_date,note_user,note_type,note_hash) VALUES ("' . $contactid . '","Meeting with [' . $username . '](?page=user-details&id=' . $userid . ') on ' . substr($start, 0, 4) . '-' . substr($start, 4, 2) . '-' . substr($start, 6, 2) . ': **' . mysqli_real_escape_string($connectionmanager->connection, $mail->subject) . '**","' . time() . '","' . $userid . '","10","' . md5('10' . $mail->from->email . $userid . $mail->subject . $start) . '")');
											$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $contactid . " AND note_hash LIKE '" . md5('10' . $mail->from->email . $userid . $mail->subject . $start) . "'"));
											$insert = $insert['note_id'];
											mysqli_query($connectionmanager->connection, 'INSERT INTO item_links (cat1,item1,cat2,item2,note_id,link_date) VALUES (1,"' . $contactid . '",99,"' . $userid . '","' . $insert . '","' . time() . '")');
										}
									}
									$init++;
								}
							} else {
								foreach ($mail->to as $to) {
									$user = $dbmanager->Query(array('id'), array(array('user_email' => $to->email), array('category' => 99)), null, 0, $this->id, 1);
									if ($user['result_count'] > 0) {
										foreach ($user['result'] as $result) {
											$userid = $result['id'];
										}
										mysqli_query($connectionmanager->connection, 'INSERT INTO parsemail_new_contacts (user_id,email) VALUES (' . $userid . ',"' . $mail->from->email . '")');
									}
								}
							}
						} elseif ($text2 != 0 && array_key_exists(2, $parts) && stripos($parts[2]->text, 'PARTSTAT=NEEDS-ACTION') != false) {
							$start = stristr($parts[2]->text, 'DTSTART;');
							$start = substr($start, (stripos($start, ':') + 1));
							//echo $parts[2]->text;
							$contact = $dbmanager->Query(array('id'), array(array('contact_email' => $mail->from->email)), null, 0, $this->id, 1);
							if ($contact['result_count'] > 0) {
								$txt = '
						
						';
								$txt.= print_r($mail, true);
								//file_put_contents($today, $txt.PHP_EOL , FILE_APPEND);
								mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'email_log_count'");
								foreach ($contact['result'] as $result) $contactid = $result['id'];
								$init = 0;
								foreach ($mail->to as $to) {
									if ($init != 0) mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'email_log_count'");
									$user = $dbmanager->Query(array('id', 'name'), array(array('user_email' => $to->email), array('category' => 99)), null, 0, $this->id, 1);
									if ($user['result_count'] > 0) {
										foreach ($user['result'] as $result) {
											$userid = $result['id'];
											$username = $result['name'];
										}
										if ($this->CheckHashExists('10' . $mail->from->email . $userid . $mail->subject . $start) !== true) {
											mysqli_query($connectionmanager->connection, 'INSERT INTO global_notes (note_item,note_text,note_date,note_user,note_type,note_hash) VALUES ("' . $contactid . '","Meeting with [' . $username . '](?page=user-details&id=' . $userid . ') on ' . substr($start, 0, 4) . '-' . substr($start, 4, 2) . '-' . substr($start, 6, 2) . ': **' . mysqli_real_escape_string($connectionmanager->connection, $mail->subject) . '**","' . time() . '","' . $userid . '","10","' . md5('10' . $mail->from->email . $userid . $mail->subject . $start) . '")');
											$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $contactid . " AND note_hash LIKE '" . md5('10' . $mail->from->email . $userid . $mail->subject . $start) . "'"));
											$insert = $insert['note_id'];
											mysqli_query($connectionmanager->connection, 'INSERT INTO item_links (cat1,item1,cat2,item2,note_id,link_date) VALUES (1,"' . $contactid . '",99,"' . $userid . '","' . $insert . '","' . time() . '")');
										}
									}
									$init++;
								}
							} else {
								foreach ($mail->to as $to) {
									$user = $dbmanager->Query(array('id'), array(array('user_email' => $to->email), array('category' => 99)), null, 0, $this->id, 1);
									if ($user['result_count'] > 0) {
										foreach ($user['result'] as $result) {
											$userid = $result['id'];
										}
										mysqli_query($connectionmanager->connection, 'INSERT INTO parsemail_new_contacts (user_id,email) VALUES (' . $userid . ',"' . $mail->from->email . '")');
									}
								}
							}
						} else {
							$contact = $dbmanager->Query(array('id'), array(array('contact_email' => $mail->from->email)), null, 0, $this->id, 1);
							if ($contact['result_count'] > 0) {
								$txt = '
						
						';
								$txt.= print_r($mail, true);
								//file_put_contents($today, $txt.PHP_EOL , FILE_APPEND);
								mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'email_log_count'");
								foreach ($contact['result'] as $result) $contactid = $result['id'];
								$init = 0;
								foreach ($mail->to as $to) {
									if ($init != 0) mysqli_query($connectionmanager->connection, "UPDATE module_preferences SET pref_value=pref_value+1 WHERE pref_name LIKE 'email_log_count'");
									$user = $dbmanager->Query(array('id', 'name'), array(array('user_email' => $to->email), array('category' => 99)), null, 0, $this->id, 1);
									foreach ($user['result'] as $result) {
										$userid = $result['id'];
										$username = $result['name'];
									}
									if ($user['result_count'] > 0) {
										if ($this->CheckHashExists('9' . $mail->from->email . $userid . $mail->subject) !== true) {
											mysqli_query($connectionmanager->connection, 'INSERT INTO global_notes (note_item,note_text,note_date,note_user,note_type,note_hash) VALUES ("' . $contactid . '","Emailed [' . $username . '](?page=user-details&id=' . $userid . ') about **' . mysqli_real_escape_string($connectionmanager->connection, $mail->subject) . '**","' . time() . '","' . $userid . '","9","' . md5('9' . $mail->from->email . $userid . $mail->subject) . '")');
											$insert = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item=" . $contactid . " AND note_hash LIKE '" . md5('9' . $mail->from->email . $userid . $mail->subject) . "'"));
											$insert = $insert['note_id'];
											mysqli_query($connectionmanager->connection, 'INSERT INTO item_links (cat1,item1,cat2,item2,note_id,link_date) VALUES (1,"' . $contactid . '",99,"' . $userid . '","' . $insert . '","' . time() . '")');
										}
									}
									$init++;
								}
							} else {
								foreach ($mail->to as $to) {
									$user = $dbmanager->Query(array('id'), array(array('user_email' => $to->email), array('category' => 99)), null, 0, $this->id, 1);
									if ($user['result_count'] > 0) {
										foreach ($user['result'] as $result) {
											$userid = $result['id'];
										}
										mysqli_query($connectionmanager->connection, 'INSERT INTO parsemail_new_contacts (user_id,email) VALUES (' . $userid . ',"' . $mail->from->email . '")');
									}
								}
							}
						}
					}
				}
				catch(Exception $e) {
					$txt = '
						
						';
					$txt.= print_r($e, true);
					file_put_contents('dotmailer_errors.log', $txt . PHP_EOL, FILE_APPEND);
					$imap->delete($messageNr);
				}
			}
		}
		$imap->expunge();
		return 'mail parsing complete';
	}
}
?>
