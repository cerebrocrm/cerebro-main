<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   Copyright 2014 Left Join Ltd.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Homepage widgets
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class HomeContent extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('linked_table' => 'type the name of the linked table e.g. contacts ', 'notes' => 'notes module');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetExample' => 'Prints the homepage', 'GetHomeMenu' => 'Prints the action bar for the home page', 'GetHomeButton' => 'Displays a button that adds the current object to the home screen.');
	//Object functions and variables go here
	public $widget_count = 2;
	// Translate the number into a type
	public function GetNoteType($note_type) {
		$icon = 'info';
		switch ($note_type) {
			case '0':
				$icon = 'info';
			break;
			case '1':
				$icon = 'technical';
			break;
			case '2':
				$icon = 'question';
			break;
			case '3':
				$icon = 'warning';
			break;
		}
		return $icon;
	}
	// TEMP SQL - these should be configured from the DB
	public function GetStatusText($type, $status_id) {
		$output = null;
		switch ($type) {
			case 'Issue':
				switch ($status_id) {
					case '1':
						$output = 'New issue';
					break;
					case '2':
						$output = 'Assigned to project';
					break;
					case '3':
						$output = 'Resolved';
					break;
					default:
						$output = 'Dropped';
				}
			break;
			case 'Location': //0:Prospective,1:Active,2:Finished,3:Stalled,4:Dropped
				switch ($status_id) {
					case '1':
						$output = 'Active';
					break;
					case '2':
						$output = 'Finished';
					break;
					case '3':
						$output = 'Stalled';
					break;
					case '4':
						$output = 'Dropped';
					break;
					default:
						$output = 'Prospective';
				}
			break;
			case 'Project':
				switch ($status_id) {
					case '1':
						$output = 'Prospective';
					break;
					case '2':
						$output = 'New';
					break;
					case '3':
						$output = 'In progress';
					break;
					case '4':
						$output = 'Stalled';
					break;
					default:
						$output = 'Dropped';
				}
			break;
		}
		return $output;
	}
	public function GetHomeMenu() {
		$output = '
        <div class="5 columns action_bar">
                <a href="#magnific_widgets" class="button magnific"><span class="fa fa-plus" title="Add"></span>Widget</a>
            </div>
        ';
		return $output;
	}
	public function GetHomeButton() {
		global $item_id;
		$table = $this->link('linked_table');
		$output = '
                <a href="home_update.php?action=addWidget&amp;type=' . $table[0] . '&amp;subject_id=' . $item_id . '" class="button"><span class="fa fa-home fa-fw fa-lg" title="Add to home" ></span><span class="fa fa-plus-circle" title="Add to home"></span></a>
        ';
		return $output;
	}
	public function WidgetList() {
		// TEMP SQL get the user settings
		global $connectionmanager;
		$widgets = mysqli_query($connectionmanager->connection, "SELECT * FROM homepage_widgets WHERE 1");
		$output = '';
		$row_count = 1;
		while ($row_widgets = mysqli_fetch_array($widgets)) {
			if ($row_count % 2) {
				$output.= '<div class="row primary_item">';
			} else {
				$output.= '<div class="row primary_item even">';
			}
			$output.= '
                <div style="width:100%;">
                   <strong>' . $row_widgets['widget_name'] . '</strong>
                </div>
                <div class="two columns action_bar omega">
                    <a class="button" href="http://' . get_instance() . '/home_update.php?action=addWidget&type=' . $row_widgets['widget_id'] . '&subject_id=X"><span class="fa fa-plus" title="Add to homepage"></span></a>
                </div>

            </div> <!-- widget_item -->';
			$row_count++;
		}
		return $output;
	}
	public function GetExample() {
		// TEMP SQL get the user settings
		global $connectionmanager;
		global $dbmanager;
		$data = $dbmanager->Query(array('user_home'), array(array('id' => Users::GetCurrentUser()['id'])), null, 0, $this->id);
		$user_homepage = $data['result'][Users::GetCurrentUser()['id']]['user_home'];
		$output = '

        <script type="text/javascript">
        function getBoxElementData()
        {
                var dataString = "";

                var totalItems = $("li.mainitem");
                for (var i = 0; i < totalItems.length; i++) {
                    var item = totalItems[i];

                    var id = $(item).attr("id");
                    var index = totalItems.index($(item));

                    dataString += id+"="+index;

                    // Formatting string
                    if (i < totalItems.length-1) {
                        dataString += "&"
                    }
                }

                return dataString;
            }

        $(function () {
            // Zero-index amount of box elements on screen.
            // NOTE: This is just given the 2 boxes currently being tested with, "welcome" and "your home"
            // 		 You will need to adjust this according to how many default elements they are - Niall
            var totalBoxes = 1;

            $("body").on("click", ".hider", function (e) {
                // "Hide me" button used - remove box element

                // Saving the numerical ID, e.g. 3 for "boxelement_3" to compare
                var id = $(e.target).parents(".mainitem").attr("id");
                var numericalId = parseInt(id.substring(id.indexOf("_") + 1));


                $(e.target).parents(".mainitem").remove();

                totalBoxes--;

                // Update other box IDs for the element data return


                var totalItems = $("li.mainitem");

                for (var i = 0; i < totalItems.length; i++) {
                    var item = totalItems[i];

                    // - shift all boxes that had a higher ID than the removed box down by 1

                    var itemId = $(item).attr("id");
                    var itemNumericalId = parseInt(itemId.replace("boxelement_", ""));

                    if (itemNumericalId > numericalId) {
                        $(item).attr("id", "boxelement_"+(itemNumericalId-1));
                    }
                }


                $.ajax({
                        type: "POST",
                        url: "home_update.php?method=post&action=updateHomePage",
                        data: getBoxElementData(),
                         success: function () {
                            /* success */
                        }
                    });

                e.preventDefault();
            });


            $("#newBoxer").click(function () {
                totalBoxes++;

                var newBoxString = "boxelement_"+totalBoxes;

                $("#sortable").append("<li class=\"five columns newelement"+totalBoxes+" mainitem\" id=\""+newBoxString+"\"></li>");
                $("#"+newBoxString).append("<div class=\"action_bar hider\"><a href=\"#\" class=\"button\"><span class=\"fa fa-times\" title=\"Remove this widget\"></span> Hide me</a></div>");


                var content;
                $.get("?page=Issues&instance=98&method=GetContent", function (data) {
                    content = data;
                    $("#"+newBoxString).append("<div>"+content+"</div>");
                });

            });

            $( "#sortable" ).sortable({
                update: function (event, ui) {
                    $.ajax({
                        type: "POST",
                        url: "home_update.php?method=post&action=updateHomePage",
                        data: getBoxElementData(),
                         success: function () {
                            /* success */
                        }
                    });
                }
            });
            $( "#sortable" ).disableSelection();
          });
        </script>';
		$output.= '

        <ul id="sortable">
        ';
		// CACHE cache the homepage widgets
		$cache_content = apc_fetch(get_instance() . '-home-' . md5($_SESSION['user'] . $user_homepage)); //get any cached variable we already have
		if ($cache_content === false) {
			// cache homepage content
			$cache_content = '<!-- loading homepage from 5 min cache at ' . date(DATE_RFC2822) . '--> ';
			parse_str($user_homepage, $user_homepage_array);
			foreach ($user_homepage_array as $user_homepage_item => $position) {
				$starttime = time();
				$user_homepage_item = explode('_', $user_homepage_item);
				//$user_homepage_id = $user_homepage_item[1];
				$user_homepage_item = explode('-', $user_homepage_item[0]);
				$user_homepage_subject = $user_homepage_item[1];
				$user_homepage_item = $user_homepage_item[0];
				if ($user_homepage_item == 'HelpCerebroIntro') {
					$cache_content.= $this->RenderWidget('HelpCerebroIntro', '', 'welcome', 'ten');
				} elseif ($user_homepage_item == 'HelpHome') {
					$cache_content.= $this->RenderWidget('HelpHome', '', 'help');
				} elseif ($user_homepage_item == 'Note') {
					$cache_content.= $this->RenderWidget('Note', '', '', 'ten');
				} else {
					if ($user_homepage_subject == 'X' && ($user_homepage_item == 'Contact' || $user_homepage_item == 'Location')) {
						$cache_content.= $this->RenderWidget($user_homepage_item, $user_homepage_subject, 'help');
					} else {
						$cache_content.= $this->RenderWidget($user_homepage_item, $user_homepage_subject);
					}
				}
			}
			apc_store(get_instance() . '-home-' . md5($_SESSION['user'] . $user_homepage), (string)$cache_content, 600); // save 10 mins
		}
		$output.= $cache_content . '</ul>';
		$output.= '
        <script type="text/javascript">
        $(document).ready(function () {
                    $( ".magnific" ).click(function () {
                        var currentId = $(this).attr("id");
                         $(".magnific_content").attr("id", currentId)
                    });
                    $(".magnific").magnificPopup({
                      type:"inline",
                      showCloseBtn: false,
                      midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don\'t provide alternative source in href.
                    });
                });
                </script><div id="magnific_widgets" class="white-popup mfp-hide"><div class="row">
                    <div class="ten columns omega action_bar">

                        <a href="" class="button"><img src="images/close_icon.png" alt="Close"/> Close</a>
                    </div>
                    <div class = "magnific_content">

                    ' . $this->WidgetList() . '

                    </div>
                </div></div>
        ';
		$output.= '<style>
.add_note div {
    margin: 0 !important;
}
		</style>';
		return $output;
	}
	// function to generate a your contacts box
	public function RenderWidget($widget_type, $content_id = null, $classes = null, $size = 'five') {
		// functions here get the value of which widget type you're using
		// this data gets passed back as an array of blob of code
		$this->widget_count++;
		if (method_exists($this, 'widget_' . $widget_type)) {
			// so we generate the widget from that function
			$widget_contents = $this->{'widget_' . $widget_type}($content_id);
		} else {
			echo $widget_type;
			$widget_contents = '

            <div >
                <h3>Test widget</h3>
                <p>This is <strong>a test widget</strong>.</p>
            </div>

            ';
		}
		$output = '
        <li class="' . $size . ' columns mainitem ' . $classes . '" id="' . $widget_type . '-' . $content_id . '_' . $this->widget_count . '">
                <div class="action_bar hider omega">
                    <a href="#" class="button borderless"><span class="fa fa-times" title="Remove this widget"></span></a>
                </div>
                ' . $widget_contents . '
            </li>
        ';
		return $output;
	}
	// function to display note-add box
	public function widget_Note() {
		$notes = $this->link('notes');
		global $ {
			'module_' . $notes[0]
		};
		$output = '


			<div ><h3>Notepad</h3></div>
			' . str_replace('class="row"', 'class="notrow"', $ {
			'module_' . $notes[0]
		}->AddNote());
		return $output;
	}
	// function to generate a your contacts box
	// this finds the last 4 notes from contact associated with the user ID
	// in the future this could be replaced with the message centre
	public function widget_YourContacts() {
		// TEMP SQL get the user id
		global $connectionmanager;
		global $dbmanager;
		$user_id = Users::GetCurrentUser()['id'];
		$query = $dbmanager->Query(array('id'), array(array('assigned_to' => $user_id, 'category' => 1)), null, 0, 0);
		// build look up table
		$ids1 = '';
		$ids2 = '';
		$ids = Array();
		foreach ($query['result'] as $result) {
			$ids[$result['id']] = $result['name'];
			if ($ids1 == '') {
				$ids1.= 'note_item = ' . $result['id'] . ' ';
			} else {
				$ids1.= ' OR note_item = ' . $result['id'] . ' ';
			}
		}
		// get notes for just those IDs
		$YourContacts_data = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE ($ids1) ORDER BY note_id DESC LIMIT 0,4");
		$output = '

         <div >
           <h3>Your contacts</h3>
        ';
		// TEMP SQL iterate through the array and print the notes
		// This really needs to be generated from the note info
		while ($row = mysqli_fetch_array($YourContacts_data)) {
			$output.= '<p class="info"><strong><a href="?page=contact-details&id=' . $row['note_item'] . '">' . $ids[$row['note_item']] . '</a></strong> ' . shorten_string($row['note_text'], 140) . '</p>';
		}
		$output.= '<p><a href="?page=simple-search&field_all_0=assigned_to&op_all_0=equal&inf_all_0=zend_all&parama_all_0=' . $user_id . '">View your contacts</a></p></div>

        ';
		return $output;
	}
	// function to generate a your contacts box
	// this finds the last 4 notes from contact associated with the user ID
	// in the future this could be replaced with the message centre
	public function widget_YourMentions() {
		// TEMP SQL get the user id
		global $connectionmanager;
		global $categories;
		$user_id = Users::GetCurrentUser()['id'];
		// TEMP SQL get the contacts assocaited with this user ID
		$YourContacts_data = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links LEFT JOIN global_notes ON global_notes.note_id=item_links.note_id LEFT JOIN items ON (id=item1 OR id=item2) WHERE ((item1 = $user_id AND item2=id) OR (item2 =$user_id AND item1=id)) AND (note_type=1 OR note_type=2 OR note_type=3) ORDER BY global_notes.note_id DESC LIMIT 0,4");
		$output = '

            <div >
                    <h3>Your mentions</h3>
        ';
		$catlist = array();
		$cats = mysqli_query($connectionmanager->connection, "SELECT * FROM categories");
		while ($row = mysqli_fetch_array($cats)) {
			$catid = $row['cat_id'];
			$catlist["$catid"] = $row['cat_page'];
		}
		while ($row = mysqli_fetch_array($YourContacts_data)) {
			$output.= '<p class="info"><strong><a href="?page=' . $catlist[$row['category']] . '&id=' . $row['id'] . '">' . $row['name'] . '</a></strong> ' . shorten_string($row['note_text'], 38) . '</p>';
		}
		return $output;
	}
	// function to generate a web form sign ups box
	public function widget_WebFormSignUps() {
		// TEMP SQL get the contacts assocaited with this user ID
		global $connectionmanager;
		global $dbmanager;
		$data = $dbmanager->Query(array('name', 'id', 'contact_when_we_spoke', 'contact_updated'), array(array('contact_how_we_spoke' => 4)), array('contact_updated' => 'DESC'), 7, $this->id);
		//$WebFormSignUps_data = mysqli_query($connectionmanager ->connection,"SELECT * FROM contacts WHERE contact_how_we_spoke = 4 GROUP BY contact_id ORDER BY contact_updated DESC LIMIT 0,7");
		$output = '

            <div >
                <h3>Web form sign ups</h3>
                <ul class="square">
        ';
		// TEMP SQL iterate through the array and print the notes
		foreach ($data['result'] as $row) {
			$output.= '<li><a href="?page=contact-details&id=' . $row['id'] . '">' . $row['name'] . '</a> (' . fuzzy_time($row['contact_when_we_spoke']) . ')</li>';
		}
		$output.= '		</ul>

            </div>

        ';
		return $output;
	}
	// function to generate a contact summary
	public function widget_Contact($contact_id) {
		if ($contact_id > 0) {
			if ($contact_id < 10000) $contact_id = $contact_id + 10000;
			global $dbmanager;
			$data = $dbmanager->Query(array('name', 'id', 'contact_location', 'contact_status', 'contact_user'), array(array('id' => $contact_id)), null, 0, $this->id);
			// TEMP SQL get the contact
			global $connectionmanager;
			$Contact_data = mysqli_query($connectionmanager->connection, "SELECT * FROM contacts LEFT JOIN locations ON contact_location = location_id LEFT JOIN users ON user_id = contact_user WHERE contact_id = $contact_id GROUP BY contact_id LIMIT 1");
			$Contact_notes = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item = $contact_id GROUP BY note_id ORDER BY note_id DESC LIMIT 2");
			// TEMP SQL iterate through the array and print the notes
			// This really needs to be generated from the note info
			$row = $data['result'][$contact_id];
			$output = '

			<div class="summary" >
					<h3><a href="?page=contact-details&id=' . $row['id'] . '">' . $row['name'] . '</a></h3>';
			if ($row['contact_location'] != null) {
				$output.= '<p class="contact_location"><img src="images/location_icon.png" alt="Location:" title="Location" />' . $row['contact_location'] . '</p>';
			}
			$output.= '<p class="contact_status"><img src="images/status_icon.png" alt="Status:" title="Status" />' . $row['contact_status'] . '</p>
					<p class="contact_user"><img src="images/user_icon.png" alt="Spoken to by:" title="Spoken to by" />' . $row['contact_user'] . '</p>
					<hr />
			';
			while ($row = mysqli_fetch_array($Contact_notes)) {
				$icon = $this->GetNoteType($row['note_type']);
				$output.= '<p class="' . $icon . '"><strong>' . fuzzy_time(date('F d, Y', $row['note_date'])) . '</strong> ' . shorten_string($row['note_text'], 140) . '</p>';
			}
			$output.= '

					</div>
			';
		} else {
			$output = '

            <div class="summary" >
                    <h3>Add a contact</h3>
                    <img src="images/help_add_widget.png" alt="The add to home button is immediately after the title of any contact or location." />
                    <p>To add any contact to the home screen, just click the <strong>Add to home</strong> button on any contact page.</p>
            </div>

            ';
		}
		return $output;
	}
	// function to generate a contact summary
	public function widget_Location($location_id) {
		if ($location_id > 0) {
			if ($location_id < 20000) $location_id = $location_id + 20000;
			// TEMP SQL get the contact
			global $connectionmanager;
			global $dbmanager;
			$data = $dbmanager->Query(array('name', 'id', 'location_status', 'location_assigned_to'), array(array('id' => $location_id)), null, 0, $this->id);
			$Location_notes = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes WHERE note_item = $location_id GROUP BY note_id ORDER BY note_id DESC LIMIT 2");
			// TEMP SQL iterate through the array and print the notes
			// This really needs to be generated from the note info
			$row = $data['result'][$location_id];
			$output = '

			<div class="summary" >
					<h3><a href="?page=location-details&id=' . $row['id'] . '">' . $row['name'] . '</a></h3>';
			$output.= '<p class="contact_status"><img src="images/status_icon.png" alt="Status:" title="Status" />' . $this->GetStatusText('Location', $row['location_status']) . '</p>
					<p class="contact_user"><img src="images/user_icon.png" alt="Spoken to by:" title="Spoken to by" />' . $row['location_assigned_to'] . '</p>
					<hr />
			';
			while ($row = mysqli_fetch_array($Location_notes)) {
				$icon = $this->GetNoteType($row['note_type']);
				$output.= '<p class="' . $icon . '"><strong>' . fuzzy_time(date('F d, Y', $row['note_date'])) . '</strong> ' . shorten_string($row['note_text'], 38) . '</p>';
			}
			$output.= '

					</div>
			';
		} else {
			$output = '

            <div class="summary" >
                    <h3>Add a location</h3>
                    <img src="images/help_add_widget.png" alt="The add to home button is immediately after the title of any contact or location." />
                    <p>To add any location to the home screen, just click the <strong>Add to home</strong> button on any location page.</p>
            </div>

            ';
		}
		return $output;
	}
	// simple homepage box
	public function widget_HelpHome() {
		$output = '

            <div >
                <h3>Your home</h3>
                <p>This is <strong>your page</strong>. You can use it to keep in touch with a particular contact, or watch several locations at once.</p>
                <ol>
                    <li>Click and drag widgets to move them around</li>
                    <li>Click the X to remove a widget</a></li>
                    <li>Click "Add widget" to get more</li>

                </ol>

            </div>

            ';
		return $output;
	}
	// basic Cerebro help introduction for new users
	// <p><button type="button" id="newBoxer">Click Me!</button></p>
	/*

	   <p style="font-weight: bold">Let\'s get started</p>
	   <ol>
	       <li><a href="#">Learn how to add a contact</a></li>
	       <li><a href="#">Discover something about a relationship</a></li>
	       <li><a href="#">Search for something</a></li>

	   </ol>

	*/
	public function widget_HelpCerebroIntro() {
		$output = '

            <div>
                    <img src="images/hello.png" alt="Hello!" style="float: left; margin: 2px 29px 15px 5px" />
                    <h3>Welcome to Cerebro</h3>
                    <p>Cerebro is a tool to help you organise and share information about relationships.</p>

                    <p style="font-weight: bold">Please report anything broken</p>
                    <p>Please bear with us, things aren\'t quite perfect yet.</p>
                    <p>If you see something broken, please <a href="http://edin.ac/GVpf16">report it!</a></p>

                </div>

            ';
		return $output;
	}
	public function widget_Buglist() {
		// Bug list RSS feed, show 5 items as bullet points
		// https://www.debugle.com/projects/rss/8b48df2f082749143b143199f769eb29d53d3ae3
		/*$rsscontents = '';
		      $limit = 5;

		      $rss = new DOMDocument();
		      $rss->load('https://www.debugle.com/projects/rss/8b48df2f082749143b143199f769eb29d53d3ae3');

		      $feed = array();
		      foreach ($rss->getElementsByTagName('item') as $node) {
		          $item = array (
		              'title' => $node->getElementsByTagName('title')->item(0)->nodeValue,
		              'desc' => $node->getElementsByTagName('description')->item(0)->nodeValue,
		              'link' => $node->getElementsByTagName('link')->item(0)->nodeValue,
		              'date' => $node->getElementsByTagName('pubDate')->item(0)->nodeValue,
		              );
		          array_push($feed, $item);
		      }
		      for($x=0;$x<$limit;$x++) {
		          $title = str_replace(' & ', ' &amp; ', $feed[$x]['title']);
		          $link = $feed[$x]['link'];
		          $description = $feed[$x]['desc'];
		          $date = date('l F d, Y', strtotime($feed[$x]['date']));
		          $rsscontents .= '<li><a href="'.$link.'" title="'.$title.'">'.$title.'</a></li>';

		      }
		      $output = '

		              <div>
		                      <h3>Development news</h3>

		                      <ul class="square">'.$rsscontents.'</ul>

		                      <p style="font-weight: bold">Please report anything broken</p>
		                      <p>Please bear with us, things aren\'t quite perfect yet. If you see something broken, please <a href="https://www.debugle.com/projects/9131/">report it!</a></p>

		                  </div>

		              ';
		*/
		return '';
	}
	/**
	* Display social media statistics on the homepage
	*
	* @global Object @connectionmanager
	* @return string HTML output
	*/

	public function widget_SocialStat() {
		global $connectionmanager;
		$data = mysqli_query($connectionmanager->connection, "SELECT date,SUM(users) as total FROM `socialstats` WHERE (network = 'twitter' OR network = 'facebook' OR network = 'youtube' OR network = 'instagram') GROUP BY date ORDER BY date DESC LIMIT 30");
		$total = 0;
		$data_array = Array();
		$minimum = 999999999999999999999999999;
		while ($row = mysqli_fetch_assoc($data)) {
			$data_array[] = $row;
			if ($row['total'] < $minimum) {
				$minimum = $row['total'];
			}
		}
		foreach ($data_array as $n => $row) {
			if ($n == 0) {
				$sparkdata = ($row['total'] - $minimum);
				$total = $row['total'];
			} else {
				$sparkdata = ($row['total'] - $minimum) . ',' . $sparkdata;
			}
		}
		// TREND
		$trendtext = '';
		if ($data_array[0] > $data_array[(count($data_array) - 1) ]) {
			$trendtext = round((($data_array[0]['total'] - $data_array[(count($data_array) - 1) ]['total']) / $data_array[(count($data_array) - 1) ]['total']) * 100, 0) . '% increase';
		} else {
			$trendtext = round((($data_array[0]['total'] - $data_array[(count($data_array) - 1) ]['total']) / $data_array[0]['total']) * 100, 0) . '% decrease';
		}
		$output = '

            <div>

                    <h3>Social media</h3>
                    <img src="./sparkline.php?size=340x150&data=' . $sparkdata . '&line=87a9ad&fill=E9F6DF" style="vertical-align: middle; margin-right: 10px; width: 100%"/>
                    <p><strong>' . $total . ' followers</strong></p>
                    <p>' . $trendtext . ' over the last 30 days</p>
                    <p><a href="/?page=social-media">View report</a></p>

                </div>

            ';
		return $output;
	}
	public function widget_SecReport() {
		global $connectionmanager;
		$data = mysqli_query($connectionmanager->connection, "SELECT date,SUM(users) as total FROM `socialstats` WHERE (network = 'secreport') GROUP BY date ORDER BY date DESC LIMIT 30");
		$total = 0;
		$data_array = Array();
		while ($row = mysqli_fetch_assoc($data)) {
			$data_array[] = $row;
		}
		foreach ($data_array as $n => $row) {
			if ($n == 0) {
				$sparkdata = ($row['total']);
			} else {
				$sparkdata = ($row['total']) . ',' . $sparkdata;
			}
			$total = $row['total'] + $total;
		}
		$output = '

            <div>

                    <h3>Security reports</h3>
                    <img src="./sparkline.php?size=340x150&data=' . $sparkdata . '&line=87a9ad&fill=E9F6DF" style="vertical-align: middle; margin-right: 10px; width: 100%" />
                    <p><strong>' . $total . ' reports</strong> over the last 30 days</p>
                    <p><a href="/?page=security-reports">View report</a></p>

                </div>

            ';
		return $output;
	}
	public function widget_ContactReport() {
		global $connectionmanager;
		global $dbmanager;
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1)), null, 0, 0);
		$total_active = $active_contacts['result_count'];
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1, 'contact_type' => 1)), null, 0, 0);
		$active_academics = $active_contacts['result_count'];
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1, 'contact_type' => 2)), null, 0, 0);
		$active_technical = $active_contacts['result_count'];
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1, 'contact_type' => 3)), null, 0, 0);
		$active_non_technical = $active_contacts['result_count'];
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1, 'contact_type' => 4)), null, 0, 0);
		$active_undergrad = $active_contacts['result_count'];
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1, 'contact_type' => 5)), null, 0, 0);
		$active_postgrad = $active_contacts['result_count'];
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1, 'contact_type' => 6)), null, 0, 0);
		$active_alumni = $active_contacts['result_count'];
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1, 'contact_type' => 7)), null, 0, 0);
		$active_media = $active_contacts['result_count'];
		$active_contacts = $dbmanager->Query(array('contact_id'), array(array('contact_is_active' => 1, 'contact_type' => 0)), null, 0, 0);
		$active_zero = $active_contacts['result_count'];
		$active_staff = $active_academics + $active_non_technical + $active_technical;
		$active_students = $active_undergrad + $active_postgrad;
		$active_other = $active_alumni + $active_media + $active_zero;
		$data = mysqli_query($connectionmanager->connection, "SELECT date,SUM(users) as total FROM `socialstats` WHERE (network = 'contact-stats') GROUP BY date ORDER BY date DESC LIMIT 30");
		$total = 0;
		$data_array = Array();
		$minimum = 999999999999999999999999999;
		while ($row = mysqli_fetch_assoc($data)) {
			$data_array[] = $row;
			if ($row['total'] < $minimum) {
				$minimum = $row['total'];
			}
		}
		foreach ($data_array as $n => $row) {
			if ($n == 0) {
				$sparkdata = ($row['total'] - $minimum);
				$total = $row['total'];
			} else {
				$sparkdata = ($row['total'] - $minimum) . ',' . $sparkdata;
			}
		}
		// TREND
		$trendtext = '';
		if ($data_array[0] > $data_array[(count($data_array) - 1) ]) {
			$trendtext = round((($data_array[0]['total'] - $data_array[(count($data_array) - 1) ]['total']) / $data_array[(count($data_array) - 1) ]['total']) * 100, 0) . '% increase';
		} else {
			$trendtext = round((($data_array[0]['total'] - $data_array[(count($data_array) - 1) ]['total']) / $data_array[0]['total']) * 100, 0) . '% decrease';
		}
		$data2 = mysqli_query($connectionmanager->connection, "SELECT date,users,account FROM `socialstats` WHERE (network = 'contact-stats') GROUP BY date,account ORDER BY date DESC LIMIT 3");
		$contact_stats = array();
		while ($row = mysqli_fetch_assoc($data2)) {
			$contact_stats[$row['account']] = $row['users'];
		}
		$new_contact_stats = array();
		$new_total = 0;
		$data3 = mysqli_query($connectionmanager->connection, "SELECT date,users,account FROM `socialstats` WHERE (network = 'new-contact-stats') GROUP BY date,account ORDER BY date DESC LIMIT 3");
		while ($row = mysqli_fetch_assoc($data3)) {
			$new_contact_stats[$row['account']] = $row['users'];
			$new_total = $new_total + $row['users'];
		}
		$output = '

            <div>

                    <h3>Contacts</h3>
                    <img src="./sparkline.php?size=340x150&data=' . $sparkdata . '&line=87a9ad&fill=E9F6DF" style="vertical-align: middle; margin-right: 10px; width: 100%"/>

                    <p><strong>' . $total . ' contacts</strong> <em>' . round($total_active / $total * 100) . '% active</em></p>
                    <p>' . $trendtext . ' over the last 30 days</p>
                    <ul class="square">
                        <li>' . $contact_stats['students_total'] . ' students (' . (round($contact_stats['students_total'] / $total, 2) * 100) . '%) <em>' . round($active_students / $contact_stats['students_total'] * 100) . '% active, ' . $active_students . ' ppl</em></li>
                        <li>' . $contact_stats['staff_total'] . ' staff (' . (round($contact_stats['staff_total'] / $total, 2) * 100) . '%) <em>' . round($active_staff / $contact_stats['staff_total'] * 100) . '% active, ' . $active_staff . ' ppl</em></li>
                        <li>' . $contact_stats['other_total'] . ' other (' . (round($contact_stats['other_total'] / $total, 2) * 100) . '%) <em>' . round($active_other / $contact_stats['other_total'] * 100) . '% active, ' . $active_other . ' ppl</em></li>
                    </ul>

                    <p><strong>' . $new_total . ' new contacts</strong></p>
                    <ul class="square">
                        <li>' . $new_contact_stats['students_total'] . ' students (' . (round($new_contact_stats['students_total'] / $new_total, 2) * 100) . '%)</li>
                        <li>' . $new_contact_stats['staff_total'] . ' staff (' . (round($new_contact_stats['staff_total'] / $new_total, 2) * 100) . '%)</li>
                        <li>' . $new_contact_stats['other_total'] . ' other (' . (round($new_contact_stats['other_total'] / $new_total, 2) * 100) . '%)</li>
                    </ul>

                    <p><a href="/?page=contact-graph">View report</a></p>

                </div>

            ';
		return $output;
	}
}
