<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates instance configuration page
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
include ('instance_dropdown.php');
//include('functions.php');
$mlinkdata = array();
$clinkdata = array();
$instance = mysqli_real_escape_string($modulemanager->connection, $_GET['instance']);
$page = mysqli_real_escape_string($modulemanager->connection, $_GET['page']);
$modules = mysqli_query($modulemanager->connection, "SELECT * FROM modules WHERE page='" . $page . "' ORDER BY instance ASC");
$access = mysqli_query($modulemanager->connection, "SELECT * FROM user_access WHERE instance='" . $instance . "'");
$i = 0;
$init = 0;
$description = '';
//Get access levels
$accesslevels = array();
while ($row = mysqli_fetch_array($access)) {
	$accesslevels[$row['function']] = array($row['id'], $row['level']);
}
//Get list of modules associated with this page
while ($row = mysqli_fetch_array($modules)) {
	if ($row['include'] != null) {
		include ($row['include'] . '.php');
	}
	//Get first module associated with page (used to build menus)
	if ($i == 0) {
		$init = $row['instance'];
	}
	$i++;
}
$update_cache = 0;
//Carry out module updates based on POST data
if (isset($_POST['description'])) {
	mysqli_query($modulemanager->connection, "UPDATE modules SET description='" . mysqli_real_escape_string($modulemanager->connection, $_POST['description']) . "' WHERE instance = '" . $instance . "'");
	$update_cache = 1;
}
//Get list of links for this module
$module = mysqli_query($modulemanager->connection, "SELECT * FROM modules WHERE instance ='" . $instance . "'");
while ($row = mysqli_fetch_array($module)) {
	$object = new $row['type'](0);
	$mod_description = $row['description'];
}
$link = $object->Links();
$functions = $object->Functions();
$special_editor = $object->special_editor;
if (count($special_editor) == 0) {
	$special_editor = array();
}
//Carry out link updates based on POST data
foreach ($link as $name => $description) {
	$i = 0;
	if (array_key_exists($name, $special_editor) && $special_editor[$name] == 'wysiwyg') {
		echo '<!-- wysiwyg editor processing -->';
		if (isset($_POST[$name . '_textarea'])) {
			//save the textarea to a new linkid
			// we delete the old ones to clean up first
			$inputs = $_POST[$name . '_textarea'];
			$i = 0;
			while (isset($_POST[$name . '_linkid_special_' . $i])) {
				mysqli_query($modulemanager->connection, "DELETE FROM modlinks WHERE linkid = '" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_linkid_special_' . $i]) . "'");
				$i++;
			}
			if ($inputs != null) {
				$inputs = str_replace('<ul>', '<ul class="square">', $inputs);
				mysqli_query($modulemanager->connection, "INSERT INTO modlinks (instance, calledinstance, variable, varname, varposition) VALUES 
				(
					'" . $instance . "',
					NULL,
					'" . mysqli_real_escape_string($modulemanager->connection, $inputs) . "',
					'" . $name . "',
					0
				)
				");
			}
		}
	}
	while (isset($_POST[$name . '_' . $i])) {
		if (isset($_POST[$name . '_linkid_' . $i])) {
			if (isset($_POST[$name . '_remove_' . $i])) {
				mysqli_query($modulemanager->connection, "DELETE FROM modlinks WHERE linkid = '" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_linkid_' . $i]) . "'");
			} elseif ($_POST[$name . '_' . $i] == 'constant') {
				mysqli_query($modulemanager->connection, "UPDATE modlinks SET calledinstance=NULL, variable='" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_variable_' . $i]) . "', varposition='" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_index_' . $i]) . "' WHERE linkid=" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_linkid_' . $i]) . "");
			} else {
				mysqli_query($modulemanager->connection, "UPDATE modlinks SET calledinstance='" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_link_' . $i]) . "', variable='" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_method_' . $i]) . "', varposition='" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_index_' . $i]) . "' WHERE linkid=" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_linkid_' . $i]) . "");
			}
		} else {
			if ($_POST[$name . '_' . $i] == 'constant') {
				mysqli_query($modulemanager->connection, "INSERT INTO modlinks (instance, calledinstance, variable, varname, varposition) VALUES 
				(
					'" . $instance . "',
					NULL,
					'" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_variable_' . $i]) . "',
					'" . $name . "',
					'" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_index_' . $i]) . "'
				)
				");
			} else {
				mysqli_query($modulemanager->connection, "INSERT INTO modlinks (instance, calledinstance, variable, varname, varposition) VALUES 
				(
					'" . $instance . "',
					'" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_link_' . $i]) . "',
					'" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_method_' . $i]) . "',
					'" . $name . "',
					'" . mysqli_real_escape_string($modulemanager->connection, $_POST[$name . '_index_' . $i]) . "'
				)
				");
			}
		}
		$i++;
	}
}
//Carry out access level updates
foreach ($functions as $key => $value) {
	if (isset($_POST[$key])) {
		if (!array_key_exists($key, $accesslevels)) {
			$query = "INSERT INTO user_access (instance, function, level,page) VALUES 
						(
							'" . $instance . "',
							'" . $key . "',
							'" . mysqli_real_escape_string($modulemanager->connection, $_POST[$key]) . "',
							'" . $page . "'
						)
						";
			mysqli_query($modulemanager->connection, $query);
		} else {
			mysqli_query($modulemanager->connection, "UPDATE user_access SET level=" . mysqli_real_escape_string($modulemanager->connection, $_POST[$key]) . " WHERE id=" . $accesslevels[$key][0]);
		}
		$accesslevels[$key] = array(0, $_POST[$key]);
	}
}
// clear cache
if ($update_cache == 1) {
	apc_clean_cache(); //clear out the caches
	
}
//Get links to current instance
$links = mysqli_query($modulemanager->connection, "SELECT * FROM modlinks WHERE instance ='" . $instance . "'");
while ($row = mysqli_fetch_array($links)) {
	if ($row['calledinstance'] != null) {
		$mlinkdata[$row['varname']][$row['varposition']] = array($row['variable'], $row['calledinstance'], $row['linkid']);
	} else {
		$clinkdata[$row['varname']][$row['varposition']] = array($row['variable'], $row['linkid']);
	}
}
//Generate menu
$menu = '<script src="http://code.jquery.com/jquery-1.8.3.js"></script>
 <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script><form action="instance_config.php?page=' . $page . '&instance=' . $instance . '" method="post">
 <a href="page_config.php?page=' . $page . '"> Return to module list</a><br>Module description: <input type="text" name="description" value="' . $mod_description . '"><br>';
foreach ($link as $key => $description) {
	//Run code for special editors (e.g. tinyMCE)
	/* Editor types:
	 * wysiwyg = tinyMCE
	 * formeditor = jquery form editor (TODO)
	*/
	if (array_key_exists($key, $special_editor) && $special_editor[$key] == 'wysiwyg') {
		$menu.= '<div id="' . $key . '">';
		$menu.= $key . ': ' . $description . '<br />';
		$menu.= '
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>

		
		<script type="text/javascript">
        $(document).ready(function () {
                tinymce.init({  selector: "textarea",
                    plugins: "link code",
					style_formats: [
						{ title: "Lead text", selector: "p", classes: "lead" }
					  ],
                    menubar:false,
                    toolbar:"bold italic underline strikethrough alignleft aligncenter alignright bullist numlist link formatselect styleselect removeformat | code",
                });
            });
		
		</script>
		
		<textarea style="width:100%; min-height:15em;" name="' . $key . '_textarea">
		
		';
		$hiddenvars = '';
		$i = 0;
		foreach ($clinkdata[$key] as $index => $value) {
			$hiddenvars.= '<input type="hidden" name="' . $key . '_linkid_special_' . $i . '" value = "' . $clinkdata[$key][$index][1] . '">';
			$menu.= $value[0] . '
';
			$i++;
		}
		$menu.= '</textarea><br />
		' . $hiddenvars . '
		</div>';
	} else if (array_key_exists($key, $clinkdata)) {
		//Generate menu items if linked constants currently exists
		$i = 0;
		$menu.= '<div id="' . $key . '">';
		$menu.= $key . ': ' . $description . '<br>';
		foreach ($clinkdata[$key] as $index => $value) {
			$menu.= '<input type="hidden" name="' . $key . '_linkid_' . $i . '" value = "' . $clinkdata[$key][$index][1] . '">';
			$menu.= generatemenu($modulemanager->connection, $page, $key, $i, $index, $init, $clinkdata[$key][$index][0]);
			$i++;
		}
		$menu.= '</div>
		<a id="remove_' . $key . '" href="#"> - </a> <a id="add_' . $key . '" href="#"> +</a><br>		
		<script>
			$(document).ready(function(){
				$("#remove_' . $key . '").hide();
			});	
				var ' . $key . '_counter = ' . $i . ';
				$("#add_' . $key . '").click(function () {
					$("#' . $key . '").append($("<div>").load("instance_dropdown.php?call=menu&page=' . $page . '&key=' . $key . '&index=" + ' . $key . '_counter + "&i=" + ' . $key . '_counter));
					if (' . $key . '_counter == ' . $i . '){
						$("#remove_' . $key . '").show();
					}
					' . $key . '_counter ++;
					event.preventDefault()
				});
				$("#remove_' . $key . '").click(function () {
					$("#' . $key . ' div").last().remove();
					' . $key . '_counter --;
					if (' . $key . '_counter == ' . $i . '){
						$("#remove_' . $key . '").hide();
					}
					event.preventDefault()
				});
			</script>		';
		//Generate menu items if linked methods currently exists
		
	} elseif (array_key_exists($key, $mlinkdata)) {
		$i = 0;
		$menu.= '<div id="' . $key . '">';
		$menu.= $key . ': ' . $description . '<br>';
		foreach ($mlinkdata[$key] as $index => $value) {
			$menu.= '<input type="hidden" name="' . $key . '_linkid_' . $i . '" value = "' . $mlinkdata[$key][$index][2] . '">';
			$menu.= '
			<script>
			$(document).ready(function(){	
				$("#' . $key . '_variable_' . $i . '").hide();
				$("#' . $key . '_' . $i . '").change(function() {
					var $dropdown_' . $key . '_' . $i . ' = $(this);
					var key_' . $key . '_' . $i . ' = $dropdown_' . $key . '_' . $i . '.val();
					switch(key_' . $key . '_' . $i . ') {
						case "constant":
							$("#' . $key . '_l_' . $i . '").hide();
							$("#' . $key . '_method_' . $i . '").hide();
							$("#' . $key . '_variable_' . $i . '").show();
						break;
						case "method":
						$("#' . $key . '_l_' . $i . '").show();
						$("#' . $key . '_method_' . $i . '").show();
						$("#' . $key . '_variable_' . $i . '").hide();
						break;
						default:
						break;
					}
				});	
				$("#' . $key . '_link_' . $i . '").change(function(){
					var $function_dropdown_' . $key . '_' . $i . ' = $(this);
					var function_key_' . $key . '_' . $i . ' = $function_dropdown_' . $key . '_' . $i . '.val();
					$("#' . $key . '_method_' . $i . '").load("instance_dropdown.php?call=method&page=' . $page . '&link=' . $key . '&i=' . $i . '&instance=" + function_key_' . $key . '_' . $i . ');
				});				
			});
			
			</script>
			';
			$menu.= 'index: <input type="text" name="' . $key . '_index_' . $i . '" value="' . $index . '">';
			$menu.= '<select id="' . $key . '_' . $i . '" name="' . $key . '_' . $i . '">
			<option value="constant">Constant</option>
			<option value="method"selected>Method</option></select><br>';
			$menu.= '<span id="' . $key . '_l_' . $i . '">';
			$menu.= modulemenu($modulemanager->connection, $page, $i, $key, $mlinkdata[$key][$index][1]);
			$menu.= '</span>';
			$menu.= '<span id="' . $key . '_method_' . $i . '">';
			$menu.= methodmenu($modulemanager->connection, $mlinkdata[$key][$index][1], $i, $key, $mlinkdata[$key][$index][0]);
			$menu.= '</span>';
			$menu.= '<input type="text" id="' . $key . '_variable_' . $i . '" name="' . $key . '_variable_' . $i . '" value=""><input type="checkbox" name="' . $key . '_remove_' . $i . '">Remove<br>';
			$i++;
		}
		$menu.= '</div>
		<a id="remove_' . $key . '" href="#"> - </a> <a id="add_' . $key . '" href="#"> +</a><br>		
		<script>
			$(document).ready(function(){
				$("#remove_' . $key . '").hide();
			});	
				var ' . $key . '_counter = ' . $i . ';
				$("#add_' . $key . '").click(function () {
					$("#' . $key . '").append($("<div>").load("instance_dropdown.php?call=menu&page=' . $page . '&key=' . $key . '&index=" + ' . $key . '_counter + "&i=" + ' . $key . '_counter));
					if (' . $key . '_counter == ' . $i . '){
						$("#remove_' . $key . '").show();
					}
					' . $key . '_counter ++;
					event.preventDefault()
				});
				$("#remove_' . $key . '").click(function () {
					$("#' . $key . ' div").last().remove();
					' . $key . '_counter --;
					if (' . $key . '_counter == ' . $i . '){
						$("#remove_' . $key . '").hide();
					}
					event.preventDefault()
				});
			</script>		';
		//Generate menu items if no links currently exist
		
	} else {
		$menu.= '<div id="' . $key . '">';
		$menu.= $key . ': ' . $description . '<br>';
		$menu.= generatemenu($modulemanager->connection, $page, $key, 0, 0, $init, '');
		$menu.= '</div>
			<a id="remove_' . $key . '" href="#"> - </a><a id="add_' . $key . '" href="#"> + </a><br>		
		<script>
			$(document).ready(function(){
				$("#remove_' . $key . '").hide();
			});	
				var ' . $key . '_counter = 1;
				$("#add_' . $key . '").click(function () {
					$("#' . $key . '").append($("<div>").load("instance_dropdown.php?call=menu&page=' . $page . '&key=' . $key . '&index=" + ' . $key . '_counter + "&i=" + ' . $key . '_counter));
					if (' . $key . '_counter == 1){
						$("#remove_' . $key . '").show();
					}
					' . $key . '_counter ++;
					event.preventDefault()
				});
				$("#remove_' . $key . '").click(function () {
					$("#' . $key . ' div").last().remove();
					' . $key . '_counter --;
					if (' . $key . '_counter == 1){
						$("#remove_' . $key . '").hide();
					}
					event.preventDefault()
				});
			</script>		';
	}
}
//Generate User Access menu
$menu.= '<br><br>User Access Level<br><br>';
foreach ($functions as $key => $value) {
	$data = '';
	if (array_key_exists($key, $accesslevels)) $data = $accesslevels[$key][1];
	$menu.= $key . ' <input type="text" id="' . $key . '" name="' . $key . '" value="' . $data . '"><br>';
}
$menu.= '<input type="submit" name="Update" value="Update"></input></form>';
echo $menu;
?>
