<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Tiny URL generation
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
require_once ('../config.php');
function do_query($sql, $line) {
	$client = "your name"; // Client Name
	$email = "your email"; // Email to notify on error
	$result = @mysql_query($sql);
	$total = @mysql_num_rows($result);
	if (@mysql_error() <> "") {
		echo " <br><font face=\"Verdana\" size=\"1\"><small><b><p align=\"center\">Sorry, there has been an unexpected database error. The webmaster has been informed of this error.</p></b></small></font>";
		// Error number
		$error_message = "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" style=\"border: 1px solid #bbbbbb;\" bgcolor=\"#ffffff\" width=\"80%\" align=\"center\"><tr><td align=\"right\" width=\"25%\"><font face=\"Verdana\" size=\"1\"><small><b>Error Number:</b></small></font></td><td width=\"75%\"><font face=\"Verdana\" size=\"1\"><small>" . @mysql_errno() . "</small></font></td></tr>";
		// Error Description
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Error Description:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . @mysql_error() . "</small></font></td></tr>";
		// Error Date / Time
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Error Time:</b></small></font></td><td><font face='Verdana' size='1'><small>" . date("H:m:s, jS F, Y") . "</small></font></td></tr>";
		// Client
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Client:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . $client . "</small></font></td></tr>";
		// Script
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Script:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . $_SERVER["SCRIPT_NAME"] . "</small></font></td></tr>";
		// Line Number
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Line:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . $line . "</small></font></td></tr></table>";
		// SQL
		$error_message.= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" style=\"border: 1px solid #bbbbbb;\" bgcolor=\"#ffffff\" width=\"80%\" align=\"center\"><tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Query:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . $sql . "</small></font></td></tr>";
		$error_message.= "<tr><td align=\"right\" valign=\"top\" width=\"25%\"><font face=\"Verdana\" size=\"1\"><small><b>Processes:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>";
		$result = @mysql_list_processes();
		while ($row = @mysql_fetch_assoc($result)) {
			$error_message.= $row["Id"] . " " . $row["Command"] . " " . $row["Time"] . "<br>";
		}
		@mysql_free_result($result);
		$error_message.= "</small></font></td></tr></table>";
		$headers = "From: \"MySQL Debug\" <" . $email . ">\r\n";
		$headers.= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		echo $error_message;
		//mail($email, "[MySQL Error] ". $client, $error_message, $headers);
		die();
	}
	if ($_GET["debug"] == "1") {
		echo "<div width=\"100%\" style=\"margin: 10px; margin-bottom: 0; padding: 3px; border: 1px solid #ff0000; background-color: #ffffff; font: 10px verdana; overflow: auto;\">" . $sql . "</div><div width=\"100%\" style=\"margin: 10px; margin-top: 0; padding: 3px; border: 1px solid #ff0000; background-color: #ff0000; font: 10px verdana; color: #ffffff; font-weight: bold;\">" . $total . " rows found.</div>";
	}
	return $result;
}
// sanitise inputs (I can't BELIEVE this wasn't done already)
function sane_get($get) {
	$var = preg_replace('/[^0-9]/', '', $_GET[$get]);
	return $var;
}
if (isset($_GET['i'])) {
	$link = do_query('SELECT * FROM short_urls WHERE short_url_id = ' . sane_get('i'), __LINE__);
	$row_count = 1;
	do {
		if ($row_link['short_url_contact'] != null) {
			// add to notes
			do_query("INSERT INTO notes (note_contact, note_text, note_date, note_status, note_type, note_campaign) VALUES
        (
        " . $row_link['short_url_contact'] . ",
        '" . addslashes('Clicked on "' . $row_link['short_url_destination'] . '" in the email "' . $row_link['short_url_campaign']) . '"' . "',
        " . time() . ",
        1,
        4,
        " . $row_link['short_url_campaign_id'] . "
        )
        ", __LINE__);
		}
		header('Location: ' . str_replace('"', '', $row_link['short_url_destination']));
	} while ($row_link = mysql_fetch_assoc($link));
}

