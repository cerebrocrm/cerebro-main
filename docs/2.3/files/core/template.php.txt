<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<!-- <?php echo round((microtime(true) - $time_start), 2) . ' seconds'; ?>-->
<head>
    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>Cerebro | <?php echo $page; ?></title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
    <link rel="stylesheet" href="stylesheets/base.css">
    <link rel="stylesheet" href="stylesheets/skeleton.css">
    <link rel="stylesheet" href="stylesheets/layout.css">
    <link rel="stylesheet" href="stylesheets/cerebro.min.css">
    <link rel="stylesheet" href="stylesheets/magnific-popup.css">
    <link rel="stylesheet" href="stylesheets/south-street/jquery-ui-1.8.21.custom.css">
    <link rel="stylesheet" href="stylesheets/goalProgress.css">
	<link rel="stylesheet" href="stylesheets/iosOverlay.css">
    <link rel="stylesheet" href="chosen/chosen.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">



    <!-- Print CSS -->
    <link rel="stylesheet" href="stylesheets/cerebro-print.css" type="text/css" media="print" />

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="js/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/magnific_popup.js" ></script>
    <script type="text/javascript" src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script type="text/javascript" src="chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.complexify.js"></script>
	<script type="text/javascript" src="js/cerebro.js"></script>
	<script type="text/javascript" src="js/goalProgress.min.js"></script>
    <script type="text/javascript" src="js/iosOverlay.js"></script>
	<script type="text/javascript" src="js/spin.min.js"></script>
	<script type="text/javascript" src="js/cookies.js"></script>
	<script type="text/javascript" src="js/jquery.countdown.min.js"></script>
	<script type="text/javascript" src="js/vue.min.js"></script>
    <script src="js/jquery.fileupload.js"></script>


	<?php
if (isset($user[7])) {
	if ($user[7] == '1') {
		echo "
				<script type=\"text/javascript\">
					$(document).ready(function () {
						$.magnificPopup.open({
						  items: {
							  src: '#password-reminder',
							  type: 'inline'
						  }
						});
					});

				</script>
					";
	}
}
if (isset($_GET['print'])) {
	echo "
				<script type=\"text/javascript\">
					window.print();
				</script>
					";
}
global $connectionmanager;
$update_password_warning = mysqli_query($connectionmanager->connection, "UPDATE users SET user_first_run = 0 WHERE user_id = '" . $user[1] . "'");
?>

    </head>
<body>

    <!-- Primary Page Layout
    ================================================== -->

    <!-- Delete everything in this .container and get started on your own site! -->
    <div class="header">
        <div class="container">

            <div class="sixteen columns">
                <a href="index.php"><img src="images/header.png" alt="Cerebro"/></a>

            </div>

            <div class="two-thirds column global_nav">
                <div class="tabs">

                     <a href="?page=home" class="home_tab <?php if ($_GET['page'] == 'home' || $_GET['page'] == null) echo 'selected'; ?>" title="Home"><span class="fa fa-home fa-fw fa-lg"></span></a>

                    <a href="?page=advanced-search" class="search_tab <?php if ($_GET['page'] == 'advanced-search' || $_GET['page'] == 'simple-search') echo 'selected'; ?>" title="Advanced search"><span class="fa fa-search fa-fw fa-lg"></a>

                    <?php echo $menu ?>
                </div>

            </div>

            <div class="one-third column">

                <div class="search">
                    <form action="" method="get">
                    <input type="hidden" name="page" value="simple-search"/>
                    <input type="hidden" name="field_all_0" value="name"/>
                    <input type="hidden" name="op_all_0" value="contain"/>
                    <input type="hidden" name="inf_all_0" value="zend_all"/>

                    <input type="text" name="parama_all_0" id="simplesearchbox"/>
                    </form>
                </div>

            </div>
        </div><!-- container -->
    </div><!-- header -->
	<script>
   $(function() {
   $.widget( "custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        if ( item.catname != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category'>" + item.catname + "</li>" );
          currentCategory = item.catname;
        }
        that._renderItemData( ul, item );
      });
    }
  });

    function loadpage( ui ) {

		window.location.replace("index.php?page="+ui.item.page+"&id="+ui.item.id);

	  }

    $( "#simplesearchbox" ).catcomplete({
      source: "index.php?page=auto&set=all",
      minLength: 3,
	  autoFocus: true,
      select: function( event, ui ) {
        loadpage( ui );
      }
    });
  });
</script>
    <div class="container">
        <div class="two-thirds column content">

            <?php
if (isset($_SESSION['msg'])) {
	if ($_SESSION['hidemsg'] == true) {
		$_SESSION['hidemsg'] == false;
	} elseif ($_SESSION['msg'] != null) {
		$jscript = '';
		if ($_SESSION['msg']['type'] == 'info' || $_SESSION['msg']['type'] == null) {
			echo '<div class="message ten columns">';
			echo '<p class="info">';
			echo $_SESSION['msg']['message'];
			echo '</p>';
			echo '</div>';
		} elseif ($_SESSION['msg']['type'] == 'warning') {
			$jscript = 'iosOverlay({
										text: "Error",
										duration: 2e3,
										icon: "images/cross.png"
									});
									return false';
			echo '<div class="message ten columns">';
			echo '<p class="warning">';
			echo $_SESSION['msg']['message'];
			echo '</p>';
			echo '</div>';
		} elseif ($_SESSION['msg']['type'] == 'correct') {
			$jscript = 'iosOverlay({
										text: "Done!",
										duration: 2e3,
										icon: "images/check.png"
									});
									return false';
		}
		if (isset($_SESSION['msg']['id'])) {
			echo '<!-- ID: ' . $_SESSION['msg']['id'] . '--> ';
		}
		$_SESSION['msg'] = null;
		echo '<script type="text/javascript">
									$(document).ready(function() {
										' . $jscript . '
									});
								</script>';
	}
}
?>
			<!-- <?php echo round((microtime(true) - $time_start), 2) . ' seconds'; ?>-->
            <?php echo $variablecontent ?>
			<!-- <?php echo round((microtime(true) - $time_start), 2) . ' seconds'; ?>-->
        </div>

        <div class="one-third column sidebar">
                <div class="login_details">
                    <h2><?php echo $user[0] ?></h2>
                    <?php if ($accesslevel == 1) {
	echo '<a href="?page=users" class="button">Admin settings</a>';
}
?>
					<a href="?page=help" class="button">Help</a>
                    <a href="?page=user-details&id=<?php echo $user[1]; ?>" class="button">Edit your details</a>
                    <a href="logout.php" class="button">Log out</a>

                </div>

                <?php echo $sidebarcontent; ?>

                <?php if (get_instance() == 'edinburgh.cerebro.org.uk' || get_instance() == 'www.edinburgh.cerebro.org.uk') { ?>

                    <div class="action_bar">
                        <a href="?page=contact-add-edit" class="button">Add contact</a>
                        <a href="?page=location-add-edit" class="button">Add location</a>
                        <a href="?page=issue-add-edit" class="button">Add issue</a>
                        <a href="?page=project-add-edit" class="button">Add project</a>
                        <a href="?page=event-add-edit" class="button">Add event</a>
                        <a href="?page=esa-team-add-edit" class="button">Add team</a>

                    </div>

                <?php
} else { ?>

                    <div class="action_bar">
                        <a href="?page=contact-add-edit" class="button">Add contact</a>
                        <a href="?page=location-add-edit" class="button">Add studio</a>
                        <a href="?page=issue-add-edit" class="button">Add issue</a>

                    </div>

                <?php
} ?>

                <div class="print_only">
                <strong>Jump to this page - </strong> <div id="bitly"> </div>

                <h4>Office use only</h4>
                <p class="print_box">
                <strong>Action required</strong>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                </p>
                <p class="print_box">
                <strong>Priority</strong> <br />
                <span class="print_choice">Low</span> <span class="print_choice">Med</span>  <span class="print_choice">High</span>
                </p>
                <p class="print_box">
                <strong>Handed over</strong>
                <?php echo date("m/d/y"); ?>
                </p>

                <h4>Volunteer use only</h4>

                <p class="print_box">
                <strong>Audited?</strong> <br />
                <span class="print_choice">No</span>  <span class="print_choice">Yes</span>
                </p>

                <p class="print_box">
                <strong>Notes</strong>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                </p>

                <p class="print_box">
                <strong>Signed (volunteer)</strong>
                <br />
                <br />
                <br />
                </p>

                <p class="print_box">
                <strong>Date</strong>
                <br />
                <br />
                </p>

                <h4>Signed off</h4>

                <p class="print_box">
                <strong>Signed (supervisor)</strong>
                <br />
                <br />
                <br />
                </p>

                <p class="print_box">
                <strong>Date</strong>
                <br />
                <br />
                </p>

                </div>
        </div>

    </div><!-- container -->

	<div id="password-reminder" class="white-popup mfp-hide">
	  <h2>It's time to change your password</h2>
	  <p>Please change your password right now.</p>
	  <a href="?page=user-details&id=<?php echo $user[1]; ?>" class="button">Change my password</a>
	</div>

	<div id="logged-out" class="white-popup mfp-hide">
	  <h2>Your session has timed out</h2>
	  <p>After 15 minutes of inactivity, your session will log out.
	  <p>To avoid losing any work you were just doing, please log in again.</p>

		<form method="POST" action="login.php" target="_blank">
		  <div class="form_area">
		  <?php echo $error; ?>
			  <div class="row primary_item">

					  <label for="email" class="required"><img src="images/form_email_icon.png" alt="Type the email in this box" /> <span class="form_label_text" style="display:none">Email address</span> </label>
					  <input type="text" name="email" id="email" class="has_image required" tabindex="1" autocomplete="off" placeholder="Email address"/>



			  </div>



			  <div class="row primary_item">


					  <label for="password" class="required"><span class="form_label_text" style="display:none;">Password</span> </label>
					  <input type="password" name="password" id="password" tabindex="2" autocomplete="off" placeholder="Password" />

			  </div>

			  <div class="row primary_item">


					  <a href="forgot_password.php" class="button borderless" style="padding:15px;">Forgot your password?</a>



					  <input type="submit" class="button" style="height: 40px;" tabindex="3" value="Login">


			  </div>
		  </div> <!-- form_area -->
	  </form>

	</div>

<!-- End Document
================================================== -->
<div class="container">
    <div class="row sixteen columns">
            <?php
$mem = memory_get_peak_usage(true);
$loadtime = round((microtime(true) - $time_start), 2);
//execution time and memory of the script
echo '<p style="color: gray;font-size: 80%;"><strong>Cerebro ' . $version . '</strong> made this page in ' . $loadtime . ' seconds with ' . round($mem / 1024 / 10124, 2) . 'MiB memory. Cerebro is open source, licenced under the <a href="https://www.apache.org/licenses/">Apache License</a>.</p>';
?>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-3347190-3', 'auto');
ga('send', 'pageview');
ga('send', 'timing', 'PHP', 'Load time', <?php echo ($loadtime * 1000) ?>, '<?php echo get_instance() ?>');
ga('set', 'dimension1', '<?php echo get_instance() ?>');

</script>

    </div>
</div>
</body>
</html>

