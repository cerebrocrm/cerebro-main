<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates data input form
 *
 *
 * @package cerebro
 * @subpackage modules
 *
 */
class FormGenRev2 extends Module {
	//Define any linked variables in format ('var1' => 'brief description of var1', 'var2' => 'brief description of var2')
	public $links = array('images' => 'some content', 'extra' => 'extra content to be injected into form', 'filter' => 'filter title', 'function' => 'triggered post function', 'header' => 'section title', 'section_switches' => 'only show specific sections according to the variable in a select box - use the format section => variable_name,value[,value2,value3] e.g. 2 => contact_type,1,2', 'vars' => 'form variables', 'title' => 'form title', 'table' => 'category number', 'skeleton2_mode' => 'Set to 1 for Skeleton 2 form mode');
	//Define any public functions in format ('funct1' => 'brief description of what funct1 returns', 'funct2' => 'etc')
	public $functions = array('GetForm' => 'Returns form', 'MultiSelect' => 'multiselect links', 'ItemImport' => 'Returns import button', 'GetImport' => 'Returns batch import form', 'GetEventImageUpload' => 'event_image uplaod form');
	//Object functions and variables go here
	
	/**
	 * @return string HTML/Javascript representation of form
	 */
	public function GetForm() {
		global $page;
		$imglist = array();
		$elements = array();
		$table = $this->link('table');
		$function = $this->link('function');
		$extra = $this->link('extra');
		global $connectionmanager;
		$img = $this->link('images');
		$header = $this->link('header');
		$section_switches = $this->link('section_switches');
		$switch_vars = array();
		$vars = $this->link('vars');
		$title = $this->link('title');
		$parameters = array();
		// for newer versions of Skeleton, we need to rework many custom features
		$skeleton2_mode = $this->link('skeleton2_mode') [0];
		//fetch field type information
		foreach ($vars as $index => $value) {
			if (strpos($index, ',')) {
				$data = explode(',', $value);
				$parameters[] = $data[0];
			}
		}
		global $item_id;
		global $dbmanager;
		$fields = $dbmanager->Fields($parameters, $table[0]);
		if ($skeleton2_mode == '1') {
			$output = '<div><div class="row">
                <div class="title_bar" >
                    <h2>' . $title[0] . '</h2>
                </div>
            </div>';
		} else {
			$output = '<div class="row form_display"><div class="row top_part">

                <div class="six columns alpha title_bar" >
                    <h2>' . $title[0] . '</h2>
                </div>

            </div>';
		}
		$output.= '<p>Required fields are in <strong>bold with an asterisk *</strong></p>
                    <form action="?page=post" method="post" enctype="multipart/form-data" id="cerebro_form" >';
		//store images for latter append
		foreach ($img as $index => $value) {
			if (strpos($index, ',')) {
				$pos = explode(',', $index);
				$imglist[$pos[0]][$pos[1]][$pos[2]] = $value;
			}
		}
		//inject any extra content
		foreach ($extra as $index => $value) {
			if (strpos($index, ',')) {
				$pos = explode(',', $index);
				$elements[$pos[0]][$pos[1]][$pos[2]] = $value;
			}
		}
		//section headers
		foreach ($header as $index => $value) {
			if ($value != '') {
				$imgdata = '';
				$hdata = explode(',', $value);
				if (array_key_exists($index, $imglist) && array_key_exists(0, $imglist[$index]) && array_key_exists(0, $imglist[$index][0])) $imgdata = '<img src="' . $imglist[$index][0][0] . '" />';
				if ($skeleton2_mode == '1') {
					$element = '
					 <div class="twelve columns"> <h3>' . $imgdata . $hdata[1] . '</h3>
                            </div>';
				} else {
					$element = '<div class="' . $hdata[0] . '">
                                <h4>' . $imgdata . $hdata[1] . '</h4>
                            </div>';
				}
				$elements[$index][0][0] = $element;
			}
		}
		//Get fill values if applicable
		if (isset($_GET['id'])) {
			$filldata = $dbmanager->Query($parameters, array(array('id' => $item_id)), null, 0, $this->id, 1);
			foreach ($filldata['result'] as $result) {
				$fillvars = $result;
			}
		}
		//Add each variable to form
		foreach ($vars as $index => $value) {
			if (strpos($index, ',')) {
				$pos = explode(',', $index);
				$data = explode(',', $value);
				$req_string = '';
				switch ($fields[$data[0]]['type']) {
					case 3:
						//checkbox field
						if (is_array($fillvars) && $fillvars[$data[0]] == 1) $fillval = ' checked ';
						else $fillval = '';
						$element = '
						<input type="hidden" name="' . $data[0] . '" id="' . $data[0] . '_"  value="0">';
						if ($skeleton2_mode == '1') {
							$element.= '
								<label class="%_WIDTH_%">
									<input class= name="' . $data[0] . '" type="checkbox" id="' . $data[0] . '" value="1" ' . $fillval . '/>
									<span class="label-body">' . $fields[$data[0]]['friendly_name'] . '</span>
								</label>
							';
						} else {
							$element.= '<div class="' . $data[1] . ' checkbox">
                                <label for="' . $data[0] . '" ><span class="form_label_text">' . $fields[$data[0]]['friendly_name'] . '</label>
                                <input class="u-full-width" name="' . $data[0] . '" type="checkbox" id="' . $data[0] . '" value="1" ' . $fillval . '/>
                            </div>';
						}
						break;
						//select field
						
					case 2:
						if ($skeleton2_mode == '1') {
							$element = '<div class="%_WIDTH_% ' . $this->CleanSkeletonWidths($data[1]) . '">
								<label for="' . $data[0] . '" >' . $fields[$data[0]]['friendly_name'] . '</label>
								<select class="u-full-width" id="' . $data[0] . '" name="' . $data[0] . '" data-placeholder="' . $fields[$data[0]]['friendly_name'] . '">';
							$element.= '<option value="0"></option>';
						} else {
							$element = '<div class="' . $data[1] . '"> 
								<select id="' . $data[0] . '" name="' . $data[0] . '" data-placeholder="' . $fields[$data[0]]['friendly_name'] . '">';
							$element.= '<option value="0"></option>';
						}
						foreach ($fields[$data[0]]['options'] as $optindex => $optvalue) {
							$isselected = '';
							if ((is_array($fillvars) && $fillvars[$data[0]] == $optindex)) $isselected = ' selected ';
							$element.= '<option value="' . $optindex . '" ' . $isselected . '>' . $optvalue . '</option>';
						}
						$element.= '</select></div>';
						break;
					default:
						//text field
						$img1 = '';
						$img2 = '';
						if (is_array($fillvars)) $fillval = $fillvars[$data[0]];
						else $fillval = '';
						//add css if field is mandatory
						if (array_key_exists(2, $data) && ($data[2] == 1 || $data[2] == 'yes')) {
							$class = 'required';
							$class2 = $this->id . '_req field_req';
							$req_string = '<span title="This field is required.">*</span>';
						} else {
							$class = '';
							$class2 = '';
						}
						if ($fields[$data[0]]['data_format'] == 3) {
							//password type input
							$element = '
                                       
						<!-- password box area -->
							<div class="six columns alpha" id="' . $data[0] . '"><h3>Change password</h3></div>
							<div class="four columns action_bar omega"><a href="#" class="button" id="password_random">Random password</a></div>
							<div class="six columns alpha" id="password">
								<input type="password" name="' . $data[0] . '" id="' . $data[0] . '"  class="u-full-width ' . $class2 . '" value="" placeholder="' . $fields[$data[0]]['friendly_name'] . '"/>
							</div>
							<div class="four columns omega"  id="password_validate">
								<p class="validation normal">Type a new password</p>
							</div>
							<div class="six columns alpha">                        
								<label for="password_repeat"><span class="form_label_text">Repeat password</span> </label>
								<input type="password" name="password_repeat" id="password_repeat" class="u-full-width" />
							</div>
							<div class="four columns omega" id="password_validate_repeat">
								
							</div>
							
							<!-- end of password box area -->
				         ';
						} else {
							if (array_key_exists(3, $data) && ($data[3] == 1 || $data[3] == 'yes')) {
								//textarea type input
								if ($skeleton2_mode == '1') {
									$element = '<div class="%_WIDTH_%  ' . $this->CleanSkeletonWidths($data[1]) . '" id="' . $data[0] . '_div">
									   <label for="' . $data[0] . '" >' . $fields[$data[0]]['friendly_name'] . ' ' . $req_string . '</label>
										<textarea class="u-full-width" name="' . $data[0] . '" id="' . $data[0] . '">' . $fillval . '</textarea>
										</div>';
								} else {
									$element = '<div class="' . $data[1] . '" id="' . $data[0] . '_div">
										<span class="form_label_text">' . $fields[$data[0]]['friendly_name'] . ' ' . $req_string . '</span><br/>
										<textarea class="u-full-width" name="' . $data[0] . '" id="' . $data[0] . '">' . $fillval . '</textarea>
									</div>
									';
								}
							} else {
								//standard html input
								if (array_key_exists($pos[0], $imglist) && array_key_exists($pos[1], $imglist[$pos[0]]) && array_key_exists($pos[2], $imglist[$pos[0]][$pos[1]])) {
									$img1 = '<img src="' . $imglist[$pos[0]][$pos[1]][$pos[2]] . '" alt="' . $fields[$data[0]]['friendly_name'] . '" />';
									$img2 = ' has_image ';
								}
								if ($skeleton2_mode == '1') {
									$element = '<div class="%_WIDTH_%  ' . $this->CleanSkeletonWidths($data[1]) . '" id="' . $data[0] . '_div">
									   <label for="' . $data[0] . '" >' . $fields[$data[0]]['friendly_name'] . ' ' . $req_string . '</label>
										<input type="text" name="' . $data[0] . '" id="' . $data[0] . '" ' . $img2 . ' class="u-full-width ' . $class2 . $img2 . '" value="' . $fillval . '" placeholder="' . $fields[$data[0]]['friendly_name'] . '"/>
									</div>
									';
								} else {
									$element = '<div class="' . $data[1] . '" id="' . $data[0] . '_div">
										<label for="' . $data[0] . '" class="' . $class . '">' . $img1 . ' ' . $req_string . '</span> </label>
										<input type="text" name="' . $data[0] . '" id="' . $data[0] . '" ' . $img2 . ' class="u-full-width ' . $class2 . $img2 . '" value="' . $fillval . '" placeholder="' . $fields[$data[0]]['friendly_name'] . '"/>
									</div>
									';
								}
							}
						}
						break;
					}
					$elements[$pos[0]][$pos[1]][$pos[2]] = $element;
				}
			}
			//Assemble elements into complete form
			//print_r($elements);
			for ($i = 0;$i < count($elements);$i++) {
				$extra_class = '';
				$data_vars = '';
				if (isset($section_switches[$i]) && $section_switches[$i] != null) {
					$extra_class = 'form_switch';
					$switch_on = explode(',', $section_switches[$i]);
					$switch_variable = array_shift($switch_on);
					$data_vars = ' data-variable="' . $switch_variable . '" data-choices="' . htmlspecialchars(json_encode($switch_on)) . '" ';
					$switch_vars[$switch_variable] = 1;
				}
				if ($skeleton2_mode != '1') {
					if (!($i % 2)) {
						$output.= '<div class="form_area ' . $extra_class . ' " ' . $data_vars . '>';
					} else {
						$output.= '<div class="form_area even ' . $extra_class . ' " ' . $data_vars . '>';
					}
				} else {
					$output.= '<div class="leftjoin_form_area ' . $extra_class . ' " ' . $data_vars . '>';
				}
				for ($j = 0;$j < count($elements[$i]);$j++) {
					$output.= '<div class="row primary_item">';
					$width_check = count($elements[$i][$j]);
					for ($k = 0;$k < count($elements[$i][$j]);$k++) {
						if (!($width_check % 2)) {
							// half width
							$output.= str_replace('%_WIDTH_%', 'six columns', $elements[$i][$j][$k]);
						} else {
							// full width
							$output.= str_replace('%_WIDTH_%', 'twelve columns', $elements[$i][$j][$k]);
						}
					}
					$output.= '</div>';
				}
				$output.= '</div>';
			}
			//add hidden fields
			$output.= '<input type="hidden" name="redirect" id="redirect" value="?id=">';
			$output.= '<input type="hidden" name="table" id="table" value="items">';
			$output.= '<input type="hidden" name="category" id="category" value="' . $table[0] . '">';
			if ($function[0] != '') $output.= '<input type="hidden" name="function" id="function" value="' . $function[0] . '">';
			if (isset($_GET['id'])) $output.= '<input type="hidden" name="id" id="id" value="' . $item_id . '">';
			// here's custom javascript that hides certain fields according to the section_switch parameters
			$switch_script = '';
			if (count($switch_vars) > 0) {
				$switch_script.= '
				
				$(".form_switch").each(function(index) {
					
					var variable = $(this).data("variable");
					var choices = $(this).data("choices");
					var comparison = $("#"+variable).val();
					
					console.log(index + " " + variable + " " + choices );
					
				
					
					if (choices.indexOf(comparison) == -1) {
						$(this).hide();
					} else {
						$(this).show();
					}
				});
			
			';
				foreach ($switch_vars as $var => $binary) {
					$switch_script.= '
					$("#' . $var . '").change(function(e) {
						$(".form_switch").each(function(index) {
					
							var variable = $(this).data("variable");
							var choices = $(this).data("choices");
							var comparison = $("#"+variable).val();
							
							console.log(index + " " + variable + " " + choices );
							
							if (choices.indexOf(comparison) == -1) {
								$(this).hide();
							} else {
								$(this).fadeIn("slow");
							}
						});
					});
				';
				}
			}
			//add scripting
			$script = '<script type="text/javascript">

        $(function () {
					var form_local = new Array();
					
					' . $switch_script . '
					
					function saveFormLocal (e) {
						var temp_array = new Array();
						$("#cerebro_form :input:visible").each(function () {
							if ( $(this).is(":checkbox") && $(this).prop( "checked" ) !== true ) {
								
							} else {
								temp_array[$(this).attr("name")] = $(this).val();
							}
							
						});
						console.debug(temp_array);
						return true;
					}
					
					window.setInterval(function(){
						saveFormLocal();
					}, 30000);
		
                    $("#submit").click(function (e) {
                        var empty_flds = 0;
                          $(".' . $this->id . '_req").each(function () {
                            if (!$.trim($(this).val())) {
							
								var this_children = $(this).parent().parent().children().length;
								$(this).addClass("has_bad");
								if (this_children == 1) {
									$(this).parent().parent().append("<div class=\'validation four columns bad \' role=\'alert\'>This field is required.</div>");
								} else if (this_children == 2) {
									$(this).parent().parent().append("<div class=\'validation four columns bad \' style=\'width: 48% !important;\' role=\'alert\'>This field is required.</div>");
								} else if (this_children == 3) {
									$(this).parent().parent().append("<div class=\'validation four columns bad \' style=\'width: 48% !important; margin-left: 4% !important;\' role=\'alert\'>This field is required.</div>");
								}
								
                                empty_flds++;
                            }
                          });
                          if (empty_flds != 0) {
							$(this).parent().parent().prepend("<div class=\'validation_alert ten columns bad \' role=\'alert\'>Please complete all required fields.</div>");
                          } else {
						 $("#cerebro_form").submit();
                        }
						e.preventDefault();
                    });
                });</script>';
			$output.= '<div class="row"><div class="two columns omega action_bar">
                    <a href="#" class="button primary" id="submit">Save</a>
					</div>
                </div></form></div>';
			$final = $script . $output;
			global $accesslevel;
			if ($this->permissions['GetForm'] <= $accesslevel) {
				return $final;
			} else {
				return null;
			}
		}
		/**
		 * @return string HTML item import form
		 */
		public function GetImport() {
			$this->CheckImport();
			// Form to upload stuff
			$output.= '
		 <h3>Upload your spreadsheet</h3>
		 <p>Please use the CSV format - <a href="http://edinburgh.cerebro.org.uk/sample-export.csv">download a sample</a>.</p>
		 
		 <form name="0" enctype="multipart/form-data" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">			 
		 
			<div class="row form_display">
				<input type="hidden" id="import_mode" name="import_mode" value="submitted_file" />
				<div class="row">
					<input type="file" name="csv" id="csv">
				</div>
				<div class="row">
					<input type="submit" name="submit" value="Import" />
				</div>
			</div>
		 
		 </form>
		
		';
			return $output;
		}
		/**
		 * @return string HTML popup form
		 */
		public function ItemImport() {
			$this->CheckImport();
			// Popup to upload stuff
			$output = '<a href="#import_form" class="open-popup-link button import_popup"><span class="fa fa-upload" title="Import contacts"></span> Import</a>
			<script>$( document ).ready(function() {
		   
				$(\'.import_popup\').magnificPopup({
				  type:"inline",
                  showCloseBtn: false,
                  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don\'t provide alternative source in href.				  
				});
			});
			</script>';
			$output.= '
		<div id ="import_form" class="white-popup mfp-hide"">
		 <h3>Upload your spreadsheet</h3>
		 <p>Please use the CSV format - <a href="http://edinburgh.cerebro.org.uk/sample-export.csv">download a sample</a>.</p>
		 
		 <form name="0" enctype="multipart/form-data" action="' . $_SERVER['REQUEST_URI'] . '" method="POST">			 
		 
			<div class="row form_display">
				<input type="hidden" id="import_mode" name="import_mode" value="submitted_file" />
				<div class="row">
					<input type="file" name="csv" id="csv">
				</div>
				<div class="row">
					<input type="submit" name="submit" value="Import" />
				</div>
			</div>
		 
		 </form>
		 </div>
		
		';
			return $output;
		}
		/**
		 * Processes csv file into items and calls Post module to store each of those items
		 *
		 * @return null
		 */
		public function CheckImport() {
			include_once 'modules/activedirectory.php';
			include_once 'functions/event_import.php';
			global $connectionmanager;
			global $dbmanager;
			$function = $this->link('function');
			$mode = mysqli_real_escape_string($connectionmanager->connection, $_POST['import_mode']);
			$ad = new ActiveDirectory();
			$output = '';
			$keys = array();
			$row = 0;
			$email_column = null;
			$skipped = '';
			$done = '';
			$skipped_no = 0;
			$done_no = 0;
			$query_adds = '';
			if ($mode == 'submitted_file' && !empty($_FILES['csv'])) {
				// Received a file for upload and process it
				$post = new Post;
				$handle = fopen($_FILES['csv']['tmp_name'], "r");
				while ($data = fgetcsv($handle)) {
					$_POST = array();
					$_POST['table'] = 'items';
					$_POST['category'] = 1;
					if ($function[0] != null) $_POST['function'] = $function[0];
					// first process array keys
					if ($row == 0) {
						foreach ($data as $key => $value) {
							$keys[$key] = trim(strtolower($value));
						}
						$email_column = array_search('contact_email', $keys);
					} elseif ($email_column !== null) {
						//check duplicates
						$check = $dbmanager->Query(array('id'), array(array('contact_email' => $data[$email_column])), null, 0, $this->id);
						if ($check['result_count'] > 0) {
							$skipped_no++;
							$skipped.= $data[$email_column];
							foreach ($check['result'] as $resindex => $result) {
								$_POST["id"] = $resindex;
								$_POST["name"] = $result['name'];
							}
						} else {
							$done.= $data[$email_column] . ' ';
							$done_no++;
						}
						// add the contact, build the query
						foreach ($data as $key => $value) {
							$field = $keys[$key];
							$_POST["$field"] = $value;
						}
						if (isset($_GET['id'])) {
							$l = 0;
							while (isset($_POST["link_" . $l])) $l++;
							$_POST["link_" . $l] = $_GET['id'];
						}
						if ((!(isset($_POST["id"])) || $_POST["name"] == '') && !(isset($_POST["contact_first"]))) {
							$check = json_decode($ad->GetSecureJSON($data[$email_column]));
							$_POST['name'] = $check->name;
							$name = explode(' ', $check->name);
							$_POST['contact_first'] = array_shift($name);
							$_POST['contact_last'] = trim(implode(' ', $name));
							// check contact_type_0
							if ($_POST['contact_type'] == null) {
								if ($check->detail_type == 'Regular staff' || $check->detail_type == 'Staff visitor') {
									if (strpos($check->title, 'Lectur') !== false) {
										$_POST['contact_type'] = 1;
									} elseif (strpos($check->title, 'Prof') !== false) {
										$_POST['contact_type'] = 1;
									} elseif (strpos($check->title, 'Principal') !== false) {
										$_POST['contact_type'] = 1;
									} elseif (strpos($check->title, 'Chair') !== false) {
										$_POST['contact_type'] = 1;
									} elseif (strpos($check->title, 'Teach') !== false) {
										$_POST['contact_type'] = 1;
									} elseif (strpos($check->title, 'Research') !== false) {
										$_POST['contact_type'] = 1;
									} elseif (strpos($check->title, 'Fellow') !== false) {
										$_POST['contact_type'] = 1;
									} elseif (strpos($check->title, 'Lab Manager') !== false) {
										$_POST['contact_type'] = 2;
									} elseif (strpos($check->title, 'Technic') !== false) {
										$_POST['contact_type'] = 2;
									} else {
										$_POST['contact_type'] = 3;
									}
								} elseif ($check->detail_type == "Postgraduate student") {
									$_POST['contact_type'] = 4;
								} elseif ($check->detail_type == "Undergraduate student") {
									$_POST['contact_type'] = 5;
								} else {
									$_POST['contact_type'] = 7;
								}
							}
							// check contact_company
							if ($_POST['contact_company'] == null) {
								$_POST['contact_company'] = $check->org;
							}
							// check contact_email
							if ($_POST['contact_email'] == null) {
								$_POST['contact_email'] = $check->email;
							}
							// check contact_title
							if ($_POST['contact_title'] == null) {
								$_POST['contact_title'] = $check->title;
							}
							// check contact_when_we_spoke
							if ($_POST['contact_when_we_spoke'] == null) {
								$_POST['contact_when_we_spoke'] = date('Y-m-d');
							}
						}
						$insert = $post->CheckPost();
						event_import($insert, $_POST);
					}
					$row++;
				}
				$_SESSION['msg'] = Array('type' => 'info', 'message' => 'We added ' . $done_no . ' contacts: ' . $done . ' and updated ' . $skipped_no . ': ' . $skipped . '.');
			}
			return null;
		}
		/**
		 * @return string HTML/Javascript item link filter box
		 */
		public function MultiSelect() {
			global $dbmanager;
			global $connectionmanager;
			global $item_id;
			$filter = $this->link('filter');
			$table = $this->link('table');
			$output = '<div id="selectlist"></div><div class = "row primary_item"><div class = "six columns" id="div_filter_' . $this->id . '"><input type = "hidden" name="filter_cat" value="' . $table[0] . '"><select multiple style="width:150px;" id="filter_' . $this->id . '" data-placeholder="' . $filter[0] . '">';
			$output.= '<option value = "none"></option>';
			$output.= '<optgroup>';
			$selected = array();
			//Fetch preexisting associations
			if (isset($_GET['id'])) {
				$prev = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE (cat1 = " . $table[0] . " AND item2=" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . ") OR (cat2 = " . $table[0] . " AND item1=" . mysqli_real_escape_string($connectionmanager->connection, $item_id) . ")");
				while ($row = mysqli_fetch_assoc($prev)) {
					if ($row['item1'] == $item_id) $key = $row['item2'];
					else $key = $row['item1'];
					$selected[$key] = 1;
				}
			}
			//Fetch menu items
			$tags = mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE category = " . $table[0]);
			while ($row = mysqli_fetch_assoc($tags)) {
				if (array_key_exists($row['id'], $selected)) $output.= '<option value = ' . $row['id'] . ' selected>' . $row['name'] . '</option>';
				else $output.= '<option value = ' . $row['id'] . '>' . $row['name'] . '</option>';
			}
			$output.= '</optgroup>';
			$output.= '</select></div></div>';
			$output.= '<script>
		$( document ).ready(function() {
			$("#filter_' . $this->id . '").chosen().change(function(e, params){
				 values = $("#filter_' . $this->id . '").chosen().val();
				 string = "<input type=\'hidden\' name=\'filter_trigger\' value=\'1\'>";
				 link_i=0;
				if (values != null){
					 $.each(values, function( index, value ) {
					  string += "<input type=hidden name=\'link_"+link_i+"\' value=\'"+value+"\'>";
					  link_i++;
					});
				}
				 $("#selectlist").html(string);
			 
			});
		});	
		
		</script>';
			return $output;
		}
		/**
		 * @return string HTML for event image upload
		 */
		public function GetEventImageUpload() {
			global $dbmanager;
			global $connectionmanager;
			global $item_id;
			$parameters = array(0 => 'id', 1 => 'event_image');
			$fillvars = array();
			//fetch field type information
			//Get fill values if applicable
			if (isset($_GET['id'])) {
				$filldata = $dbmanager->Query($parameters, array(array('id' => $item_id)), null, 0, $this->id, 1);
				foreach ($filldata['result'] as $result) {
					$fillvars = $result;
				}
			}
			// Form to upload stuff
			if (isset($fillvars['event_image']) && file_exists('api/exports/images/' . $fillvars['event_image'])) {
				$output.= '<div class="ten columns"><h5>Current image</h5></div>';
				$output.= '
			 
				
					<input type="hidden" id="event_image" name="event_image" value="' . $fillvars['event_image'] . '" />
					
					
					
					<div class="row">
						<div class="six columns">
							<img class="image_preview" src="/api/exports/images/' . $fillvars['event_image'] . '" style="width:100%; height: auto;" />
						</div>
					</div>
					<div class="row">
						<div class="six columns checkbox">
							<label for="event_image_delete"><span class="form_label_text">Delete this image</span></label>
							<input name="event_image_delete" type="checkbox" id="event_image_delete" value="1" >
						</div>
					</div>
					
			';
			}
			$output.= '
				<div class="ten columns"><h5>Upload a new image</h5></div>
				<div class="row">
					<div class="ten columns">
						<script type="text/javascript">
						
						$( document ).ready(function() {
							
							$(document).on("change", "#event_image_upload", function () {
								var file = this.files[0];
								if (file.size > 2*1024*1024) {
									alert("That pictuer is huge! Only images smaller than 2MiB can be uploaded.");
									$(this).replaceWith(\'<input type="file" name="event_image_upload" id="event_image_upload">\');
									return false;
								}
							});
						
						});
						
						</script>
						<input type="file" name="event_image_upload" id="event_image_upload">
						<p>Images must be less than 2MiB - ideally only about 500KiB.</p>
					</div>
				</div>
		
		';
			return $output;
		}
		/**
		 * @return Clean string of all skeleton width mentions
		 */
		function CleanSkeletonWidths($string) {
			return str_replace(array('one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'column', 'columns'), '', $string);
		}
	}
?>

