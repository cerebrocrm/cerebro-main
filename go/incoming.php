<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-15 The University of Edinburgh
   Copyright 2014-15 Left Join Ltd.
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Incoming data to processing queue - cleaning up and using POST or GET data
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
error_reporting(1);
include 'microindex.php';
$connectionmanager = new TableConnect();
$connectionmanager->connect();
$incoming = $_REQUEST;
$special = Array();
// safety steps
foreach ($incoming as $c => $d) {
	$incoming[$c] = mysqli_real_escape_string($connectionmanager->connection, $d);
	if (0 === strpos($c, 'special_')) {
		// It starts with 'special_'
		$special[str_replace('special_', '', $c) ] = $incoming[$c];
	}
}
if ($incoming['has_captcha'] == '1' && $incoming['detail_check'] != 2) {
	die('Please complete the maths question.');
}
if ($incoming['type'] == 'contact') {
	// identifying primary key
	if ($incoming['uun'] !== null) {
		$primary = $incoming['uun'];
	} elseif ($incoming['contact_email'] !== null) {
		if (filter_var($incoming['contact_email'], FILTER_VALIDATE_EMAIL)) {
			$primary = $incoming['contact_email'];
		} else {
			die('Please provide a real email address.'); // make pretty
			
		}
	} else {
		die('No email address provided.'); // make pretty
		
	}
	// identifying source
	if ($incoming['source'] !== null) {
		$source = $incoming['source'];
	} else {
		$source = 'default'; // no project link, just add contact
		
	}
	// identifying source
	if ($incoming['note'] !== null) {
		$note = $incoming['note'];
	} else {
		$note = ''; // no note, just add contact
		
	}
	foreach ($special as $key => $s) {
		if ($s != null) {
			$note.= '  

- ' . $key . ':	' . $s;
		}
	}
	// preparing data for saving
	$data_array = base64_encode(serialize($incoming));
	// do database query
	mysqli_query($connectionmanager->connection, "INSERT INTO incoming_data
			(incoming_id, incoming_date, incoming_type, incoming_source, 				 			
			incoming_primary, incoming_array, incoming_note) VALUES (NULL, CURRENT_TIMESTAMP, '" . $incoming['type'] . "','" . $source . "','" . $primary . "', '" . $data_array . "', '" . $note . "' )");
	// redirects
	
} else {
	// add more types here
	die("Type not supported");
}
// remove this once we have new data structure finished
if ($incoming['autoemail_text'] !== null && $incoming['contact_email'] !== null && $incoming['autoemail_subject'] !== null) {
	if ($incoming['autoemail_from'] !== null) {
		$email_from = $incoming['autoemail_from'];
	} else {
		$email_from = 'noreply@cerebro.org.uk';
	}
	/*
	if ($incoming['autoemail_image'] !== null) {
	   	$email_from = $incoming['autoemail_from'];
	   } else {
	   	$email_from = 'noreply@cerebro.org.uk';
	   }
	*/
	$text = stripslashes($incoming['autoemail_text']);
	$text = str_replace('_NL', '  
', $text);
	$text = stripslashes(parse_markdown($text));
	$newpost = new Post;
	$result = $newpost->Email(trim($incoming['contact_email']), array('text' => $text, 'subject' => $incoming['autoemail_subject'], 'from_email' => $email_from));
	file_put_contents('email_log.log', '
	' . serialize($result), FILE_APPEND | LOCK_EX);
}
if ($incoming['redirect'] !== null) {
	header("Location: " . $incoming['redirect']);
} else {
	die(fail_error('Your information has been received and we\'ll be in touch soon.', 'Thanks for your information'));
}
