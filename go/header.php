<?php 

include '../microindex.php';


if (isset($_GET['i']) && isset($_GET['c']) && $_GET['i'] !== '' && $_GET['c'] !== '') {

	$campaign = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection,"SELECT * FROM email_campaigns WHERE campaign_id = ".mysqli_real_escape_string($connectionmanager ->connection,$_GET['i'])));
    // add to notes
    mysqli_query($connectionmanager->connection,"INSERT INTO global_notes (note_item, note_text, note_date, note_status, note_type,note_campaign) VALUES
    (
    ".mysqli_real_escape_string($connectionmanager ->connection,$_GET['c']).",
    '".mysqli_real_escape_string($connectionmanager ->connection,'Opened the email **'.$campaign['campaign_subject'].'**')."',
    ".time().",
    1,
    5,
    ".mysqli_real_escape_string($connectionmanager ->connection,$_GET['i'])."
    )
    ");	

}

header('Location: http://'.$_SERVER['HTTP_HOST'].'/go/1x1.gif');
