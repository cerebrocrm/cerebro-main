<?php

include '../microindex.php';




// sanitise inputs (I can't BELIEVE this wasn't done already)
function sane_get($get)
{
    $var = preg_replace('/[^0-9]/', '', $_GET[$get]);

    return $var;
}

if (isset($_GET['i'])) {

    $link = do_query('SELECT * FROM short_urls WHERE short_url_id = '.sane_get('i'),__LINE__) ;
    $row_count = 1;
    do {
        
        $row_link['short_url_destination'] = trim($row_link['short_url_destination']);

        if ($row_link['short_url_contact'] != null) {
        // add to notes
      
        mysqli_query($connectionmanager->connection,"INSERT INTO global_notes (note_item, note_text, note_date, note_status, note_type,note_campaign) VALUES
            (
            ".mysqli_real_escape_string($connectionmanager ->connection,$row_link['short_url_contact']).",
            '".mysqli_real_escape_string($connectionmanager ->connection,'Clicked on '.$row_link['short_url_destination'].' in the email **'.$row_link['short_url_campaign'])."**,
            ".time().",
            1,
            4,
            ".mysqli_real_escape_string($connectionmanager ->connection,$row_link['short_url_campaign_id'])."
            )
        ");
        }
        
            //header('Location: '. str_replace('"','',$row_link['short_url_destination']."?utm_source=".get_instance()."&utm_medium=Email&utm_campaign=Cerebro"));
            header('Location: '. str_replace('"','',$row_link['short_url_destination']));
            
    } while ($row_link = mysqli_fetch_assoc($link));

} else {
    die(fail_error('No link has been specified.','Please enter a link'));
}
