# README #

This is the source repository of the Cerebro CRM project.

### What is Cerebro? ###

Cerebro is an open source contact relationship manager (CRM) with a difference. Unlike other CRMs that are wedded to a particular management style - and so require extensive and costly customisation before you can use them - Cerebro is designed from the start to be flexible and modular.

This means that every Cerebro installation can be completely different.

Most Cerebro installations are designed to:

* Record the contacts you meet, and record as much as possible about your meeting.
* Automatically sign them up for your ongoing newsletters (powered by Mailchimp).
* Keep track of your contacts issues and ideas, the projects they take part in.
* Monitor how active people are – watching how often they open their emails, click the links you send them and how frequently they attend the events you create (powered by Eventbrite).
* And more…

In Cerebro, we expect you'll be going out to meet people, tell them about your organisation, and then sign them up as participants, volunteers or sales leads.

Each person will tell you their ideas and problems (issues). You'll keep in touch with each person – emailing and calling, sending out regular newsletters and social updates.

Eventually, as you build a relationship with each person, you'll involve them in projects that will either contribute to your organisation's goals or help to answer your contact's issues.

Cerebro was originally based on SimpleCustomer, but has long since lost virtually all SimpleCustomer code.

### How do I get set up? ###

To write!

### Documentation ###

The technical documentation for the system can be found at http://cerebro.org.uk/docs/

### Contribution guidelines ###

Please just go ahead and submit patches.

### Who do I talk to? ###

**Repository manager & lead developer** - Finn Tessier-Lavigne

**Developer** - Joseph Farthing