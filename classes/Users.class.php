<?php

class Users {

	private static $cUser;

	/**
	 * Setup the initial session information, mainly the time when the session
	 * will expire, this is called on each page load.
	 */
	public static function SetupSession() {
		// if the PHP session hasn't already started, start it
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
			$_SESSION['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
		}

		// Increase the expiry time of the session if the time the session is due
		// to expire has not passed
		$ExpiresAt = isset($_SESSION['expires']) ? $_SESSION['expires'] : 0;
		if (!self::ShouldSessionExpire())
			$_SESSION['expires'] = time() + 1800;
	}

	/**
	 * Get the unix timestamp when the current session should expire.
	 *
	 * @return an int representing the time stamp or 0 if the system never set
	 * an expiry time
	 */
	public static function GetSessionExpiry() {
		return isset($_SESSION['expires']) ? $_SESSION['expires'] : 0;
	}

	/**
	 * Identifies if the current session should expire or not, note: this
	 * function just tests logic, it does not actually expire a session (aka
    * logout the user), if you would like to log the user out call
	 * Users::DeAuthenticate()
	 *
	 * @return a boolean, true if the session should expire, false if it should
	 * not
	 */
	public static function ShouldSessionExpire() {
		$ExpiresAt = self::GetSessionExpiry();
		if ($ExpiresAt == 0) return false;
	 	return (time() >= $ExpiresAt);
	}

	/**
	 * Gets an associative array representing the currently logged in user or
	 * null if there is currently no user logged in
	 *
	 * @return associative array representing current user or null
	 */
   public static function GetCurrentUser() {
		self::SetupSession();

		if (self::ShouldSessionExpire() || $_SESSION['REMOTE_ADDR'] != $_SERVER['REMOTE_ADDR']) {
			self::DeAuthenticate();
			return null;
		}  // if the session should expire or a change in REMOTE_ADDR occurred,
		// expire the session and return null

		if (!isset($_SESSION['uid']))
			return null; // user is not logged in, return null

		if (!isset(self::$cUser)) // if $cUser has not been set, set it
			self::$cUser = self::GetUser($_SESSION['uid']);

		return self::$cUser;
   }

	/**
	 * Gets an associative array representing the requrested user or
	 * null if no user could be found
	 *
	 * @return associative array representing requrested user or null
	 */
   public static function GetUser($id) {
		global $dbmanager;
		$userdata = $dbmanager->Query(array('user_email', 'contact_email', 'user_level', 'name'), array(array('id' => $id)), null, 0, 999, 1);

		if (count($userdata['result']) == 0)
			return null; // no user returned, return null

		$result = array_values($userdata['result'])[0]; // get first user returned

		$ghash = md5(strtolower(trim($result['user_email'])));
		$email = '';
		$email = $result['user_email'] != '' ? $result['user_email'] : '';
		$email = $result['contact_email'] != '' ? $result['contact_email'] : '';
		if ($email == '')	return null;

		return array(
				'id' => $result['id'],
				'category' => $result['category'],
				'name' => $result['name'],
				'email' => $email,
				'user_level' => $result['user_level'],
				'gravatar_hash' => $ghash,
				'avatar_url' => 'https://www.gravatar.com/avatar/' . $ghash,
		);
	}

	/**
	 * Destroy the currently active session, forcing the user to be logged out
	 */
	public static function DeAuthenticate() {
		session_start();
		unset($_SESSION['uid']);
		unset($_SESSION['expires']);
		unset($_SESSION['REMOTE_ADDR']);
		session_destroy();
	}

}

?>
