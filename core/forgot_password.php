<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates page to deal with lost passwords
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
include 'microindex.php';
$post = new Post();
function createPassword($length) {
	$chars = "234567890abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$i = 0;
	$password = "";
	while ($i <= $length) {
		$password.= $chars{mt_rand(0, strlen($chars)) };
		$i++;
	}
	return $password;
}
if (isset($_POST['email'])) {
	$data = $dbmanager->Query(array('name', 'id', 'category'), array(array('user_email' => $_POST['email'])), null, 0, 0);
	if ($data['result_count'] < 1) {
		$content = '<br>Email address not recognised<br>
    <div class="row form_display">

                <form method="POST" action="">
                    <div class="form_area">
                        <div class="row primary_item">
                            <div class="six columns alpha">
                                <label for="email" class="required"><img src="images/form_email_icon.png" alt="Type the email in this box" /> <span class="form_label_text">Please enter your email address</span> </label>
                                <input type="text" name="email" id="email" class="has_image required" tabindex="0" autocomplete="off"/>
                            </div>
                            <div class="two columns omega action_bar">
                                <input type="submit" class="button" style="height: 40px;" tabindex="3" value="Submit">
                            </div>

                        </div>






                    </div> <!-- form_area -->
                </form>

            </div>';
	} else {
		foreach ($data['result'] as $result) {
			$user = $result;
		}
		$password = createPassword(8);
		$result = $post->Email(trim($_POST['email']), array('text' => '<p>Your new password is: ' . $password . '</p>', 'subject' => 'Password reset'));
		$_POST['id'] = $user['id'];
		$_POST['category'] = $user['category'];
		$_POST['table'] = 'items';
		$_POST['user_password'] = $password;
		$post->CheckPost();
		//$logincheck = mysqli_query($connectionmanager ->connection ,"UPDATE extra_char SET val = '".genpwd($password)."' WHERE item_id = ".$user['id']." AND meta_id=604");
		$content = '<br><br><strong>A new password has been emailed to you.</strong>';
	}
} else {
	$content = '<div class="row form_display">

                <form method="POST" action="">
                    <div class="form_area">
                        <div class="row primary_item">
                            <div class="six columns alpha">
                                <label for="email" class="required"><img src="images/form_email_icon.png" alt="Type the email in this box" /> <span class="form_label_text">Please enter your email address</span> </label>
                                <input type="text" name="email" id="email" class="has_image required" tabindex="0" autocomplete="off"/>
                            </div>
                            <div class="two columns omega action_bar">
                                <input type="submit" class="button" style="height: 40px;" tabindex="3" value="Submit">
                            </div>

                        </div>






                    </div> <!-- form_area -->
                </form>

            </div>';
}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>Cerebro</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
    <link rel="stylesheet" href="stylesheets/base.css">
    <link rel="stylesheet" href="stylesheets/skeleton.css">
    <link rel="stylesheet" href="stylesheets/layout.css">
    <link rel="stylesheet" href="stylesheets/cerebro.css">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://www.cerebro.org.uk/core/includes/src/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/magnific_popup.js"></script>

    <script type="text/javascript">	//<![CDATA[
    $(document).ready(function () {

        $('.open-popup-link').magnificPopup({
          type:'inline',
          showCloseBtn: false,
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });

        $(':text').each(function (index) {
            if ( $(this).val() ) {
                $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").hide(0);
            }

        });

        $(':text').focus(function () {
            $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").hide(0);
        });

        $(':text').blur(function () {
            if ( !$(this).val() ) {
                $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").show(0);
            }
        });

        $(':password').focus(function () {
            $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").hide(0);
        });

        $(':password').blur(function () {
            if ( !$(this).val() ) {
                $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").show(0);
            }
        });
    });

    //]]>
    </script>


</head>
<body>
    <!-- Primary Page Layout
    ================================================== -->

    <!-- Delete everything in this .container and get started on your own site! -->
    <div class="header">
        <div class="container">

            <div class="sixteen columns" style="display: block; height: 200px !important;">
                <!-- special login page header -->
                <p><br /></p>
            </div>
            <div class="sixteen columns">
                <img src="images/header.png" alt="Cerebro"/>

            </div>


        </div><!-- container -->
    </div><!-- header -->
    <div class="container">
        <div class="sixteen columns content">

        <?php echo $content; ?>

        </div>

    </div><!-- container --></body>
</html>
