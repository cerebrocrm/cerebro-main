<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Runs scheduled tasks
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
if (isset($_GET['freq'])) {
	switch ($_GET['freq']) {
		case 'daily':
			$method = 'Daily';
		break;
		case 'weekly':
			$method = 'Weekly';
		break;
		case 'monthly':
			$method = 'Monthly';
		break;
		case 'hourly':
			$method = 'Hourly';
		break;
		case 'minutes':
			$method = 'Minutes';
		break;
		default:
			$method = 'Daily';
		break;
	}
} else {
	$method = 'Daily';
}
include 'microindex.php';
$scheduled_module = mysqli_query($modulemanager->connection, "SELECT * FROM `modules` WHERE `type` = 'Scheduled' LIMIT 0,1");
while ($row = mysqli_fetch_assoc($scheduled_module)) {
	$scheduled_tasks_instance = $row['instance'];
	$page = $row['page'];
	global $accesslevel;
	$user = array('Web Form', '20', '99999');
	$accesslevel = '99999';
	$instances = array();
	$mlinkdata = array();
	$clinkdata = array();
	$viewcontroller = null;
	$access = array();
	//Get all modules and links
	$modules = mysqli_query($modulemanager->connection, "SELECT * FROM modules LEFT JOIN modlinks ON modules.instance=modlinks.instance WHERE page='" . $page . "' ORDER BY `modules`.`instance` ASC");
	$accesslevels = mysqli_query($modulemanager->connection, "SELECT * FROM user_access  WHERE page='" . $page . "'");
	//add access levels to array
	while ($row = mysqli_fetch_array($accesslevels)) {
		$access[$row['instance']][$row['function']] = $row['level'];
	}
	//Instantiate each unique module instance, storing the instance number in an array, and all link data in seperate arrays
	while ($row = mysqli_fetch_array($modules)) {
		//Set viewcontroller instance number
		if ($row['type'] == 'ViewController' || $row['type'] == 'ViewControllerRev2') {
			$viewcontroller = $row['instance'];
		}
		if (!array_key_exists($row['instance'], $instances)) {
			if ($row['include'] != null && $row['include'] != '') {
				include ($row['include'] . '.php');
			}
			$ {
				'module_' . $row['instance']
			} = new $row['type']($row['instance']);
			$instances[$row['instance']] = 1;
		}
		if ($row['calledinstance'] != null) {
			$mlinkdata[] = array($row['instance'], $row['varname'], $row['varposition'], $row['variable'], $row['calledinstance']);
		} else {
			$clinkdata[] = array($row['instance'], $row['varname'], $row['varposition'], $row['variable']);
		}
	}
	//Assign each link to an array based upon the instance it belongs to, in format $module_123['variable'][variable_position]
	//For constants
	foreach ($clinkdata as $x => $x_value) {
		$ {
			'clinks_' . $x_value[0]
		}
		[$x_value[1]][$x_value[2]] = $x_value[3];
	}
	foreach ($instances as $x => $x_value) {
		if (isset($ {
			'clinks_' . $x
		})) {
			$ {
				'module_' . $x
			}->cLink($ {
				'clinks_' . $x
			});
		}
		//Add user acces levels
		if (array_key_exists($x, $access)) $ {
			'module_' . $x
		}->SetAccess($access[$x]);
	}
	//For method calls
	foreach ($mlinkdata as $x => $x_value) {
		$ {
			'mlinks_' . $x_value[0]
		}
		[$x_value[1]][$x_value[2]] = array($x_value[4], $x_value[3]);
	}
	foreach ($instances as $x => $x_value) {
		if (isset($ {
			'mlinks_' . $x
		})) {
			$ {
				'module_' . $x
			}->mLink($ {
				'mlinks_' . $x
			});
		}
	}
	// test to see whether the "Specific()" method is set in viewcontroller
	// and then remove
	$method = 'Run' . $method . 'Tasks';
	echo $ {
		'module_' . $scheduled_tasks_instance
	}->$method('');
}
