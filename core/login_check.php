<?php
// return logged in => 1 if user is logged in or 0 if not
include ('microindex.php');
session_start();
if (!isset($_SESSION['user']) || $_SESSION['user'] == null) {
	$return = array('logged_in' => '0');
	echo json_encode($return);
	die();
} else {
	$return = array('logged_in' => '1');
	echo json_encode($return);
	die();
}
