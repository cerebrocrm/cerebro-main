<?php /*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-15 Joseph Farthing / The University of Edinburgh
   Copyright 2012 Joseph Farthing

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
// includes
include 'microindex.php';
include 'modules/activedirectory.php';
$overlay = '';
$userstring = '';
foreach ($users as $index => $val) {
	$userstring.= '<option value="' . $index . '" ' . $selected . '>' . $val . '</option>';
}
$event = null;
if (isset($_GET['event_id'])) {
	// add kiosk to database
	$event = $dbmanager->Query(array('name', 'event_type'), array(array('id' => $_GET['event_id'])), null, 0, 0);
	$event = $event['result'][$_GET['event_id']];
} else {
	// add cookie check
	die('Sorry, an error has occurred. Please speak to a member of staff.');
}
if (isset($_POST['contact_email'])) {
	$overlay = 'iosOverlay({
					text: "Thanks!",
					duration: 7e2,
					icon: "images/check.png"
				});';
	//print_r($_POST);
	$note = $_POST['note'];
	$meta = mysqli_query($connectionmanager->connection, "SELECT * FROM meta WHERE cat_id=5");
	$heard = array();
	while ($row = mysqli_fetch_assoc($meta)) {
		$ind = $row['name'];
		$heard["$ind"] = $row['id'];
	}
	$ind = $_POST['how_heard_val'];
	$check = mysqli_query($connectionmanager->connection, "SELECT * FROM extra_int WHERE meta_id=" . $heard[$ind] . " AND item_id=" . mysqli_real_escape_string($connectionmanager->connection, $_GET['event_id']));
	if (mysqli_num_rows($check) > 0) mysqli_query($connectionmanager->connection, "UPDATE extra_int SET val=(val+1) WHERE meta_id=" . $heard[$ind] . " AND item_id=" . mysqli_real_escape_string($connectionmanager->connection, $_GET['event_id']));
	else mysqli_query($connectionmanager->connection, "INSERT INTO extra_int (meta_id,item_id,val) VALUES (" . $heard[$ind] . "," . mysqli_real_escape_string($connectionmanager->connection, $_GET['event_id']) . ",1)");
	$post = new Post;
	if (isset($_POST['contact_existing_id_0']) && $_POST['contact_existing_id_0'] != 'NONE') {
		$postdata = array();
		$postdata['table'] = 'global_notes';
		$userid = $_POST['assigned_to'];
		$attend = mysqli_query($connectionmanager->connection, "SELECT * FROM global_notes LEFT JOIN item_links ON global_notes.note_id=item_links.note_id WHERE note_item=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['contact_existing_id_0']) . " AND (note_type = 52) AND (item1=" . mysqli_real_escape_string($connectionmanager->connection, $_GET['event_id']) . " OR item2 = " . mysqli_real_escape_string($connectionmanager->connection, $_GET['event_id']) . ")");
		if (mysqli_num_rows($attend) > 0) {
			$attend = mysqli_fetch_assoc($attend);
			$type = 53;
			$postdata['note_type'] = $type;
			$postdata['note_id'] = $attend['note_id'];
			$text = $_POST['note'];
			$itemid = $_POST['contact_existing_id_0'];
			$_POST = $postdata;
			$post->CheckPost();
			$_POST = array();
			$_POST['table'] = 'global_notes';
			$_POST['note_id'] = $attend['note_id'];
			if ($attend['note_text'] != '') $text = $attend['note_text'] . ' ---- ' . $text;
			$_POST['note_text'] = $text;
			$post->CheckPost();
			if ($_POST['note_text'] != '') {
				$_POST = array();
				$_POST['table'] = 'global_notes';
				$_POST['note_type'] = 0;
				$_POST['note_text'] = $text;
				$_POST['note_item'] = $itemid;
				$_POST['note_date'] = time();
				$_POST['autoemail'] = 'kiosk_autoemail';
				$insert = $post->CheckPost();
			}
			$_POST = array();
			$_POST['table'] = 'items';
			$_POST['category'] = 1;
			$_POST['id'] = $itemid;
			$_POST['assigned_to'] = $userid;
			$post->CheckPost();
		} else {
			$type = 51;
			$postdata['note_type'] = $type;
			$postdata['note_text'] = $_POST['note'];
			$postdata['note_item'] = $_POST['contact_existing_id_0'];
			$postdata['note_date'] = time();
			$itemid = $_POST['contact_existing_id_0'];
			$_POST = $postdata;
			$insert = $post->CheckPost();
			if ($note != '') {
				$_POST['autoemail'] = '';
				$_POST['note_type'] = 0;
				$post->CheckPost();
			}
			$postlink = array();
			$postlink['table'] = 'item_links';
			$postlink['note_id'] = $insert;
			$postlink['item1'] = $itemid;
			$postlink['item2'] = $_GET['event_id'];
			$_POST = $postlink;
			$post->CheckPost();
			$_POST = array();
			$_POST['table'] = 'items';
			$_POST['category'] = 1;
			$_POST['id'] = $itemid;
			$_POST['assigned_to'] = $userid;
			$_POST['autoemail'] = 'kiosk_autoemail';
			$post->CheckPost();
		}
	} else {
		$userid = $_POST['assigned_to'];
		$_POST['table'] = 'items';
		$_POST['category'] = 1;
		$_POST['autoemail'] = 'kiosk_autoemail';
		$itemid = $post->CheckPost();
		$postdata = array();
		$type = 51;
		$postdata['table'] = 'global_notes';
		$postdata['note_type'] = $type;
		$postdata['note_text'] = $_POST['note'];
		$postdata['note_item'] = $itemid;
		$postdata['note_date'] = time();
		$_POST = $postdata;
		$insert = $post->CheckPost();
		if ($note != '') {
			$_POST['note_type'] = 0;
			$post->CheckPost();
		}
		$postlink = array();
		$postlink['table'] = 'item_links';
		$postlink['note_id'] = $insert;
		$postlink['item1'] = $itemid;
		$postlink['item2'] = $_GET['event_id'];
		$_POST = $postlink;
		$post->CheckPost();
	}
	if ($note != '') email_notify($itemid, $note);
}
function email_notify($insert, $note) {
	//file_put_contents('email_log.log', ' '.'test', FILE_APPEND | LOCK_EX);
	global $connectionmanager;
	global $dbmanager;
	$item = $dbmanager->Query(array('assigned_to'), array(array('id' => $insert)), null, 0, 0, 1);
	$user = $dbmanager->Query(array('user_email'), array(array('id' => $item['result'][$insert]['assigned_to'])), null, 0, 0);
	//require 'super-magic-email-sender.php';
	$instance = get_instance();
	$msg = 'New note added to your item: http://' . $instance . '?id=' . $insert . ' :' . $note;
	$newpost = new Post;
	$result = $newpost->Email(trim($user['result'][$item['result'][$insert]['assigned_to']]['user_email']), array('text' => $msg, 'subject' => 'New note added to an item assigned to you'));
	return 1;
}
// get event info
$event_name = $event['name'];
$event_type = $event['event_type'];
if (isset($_GET['submit'])) {
	header("Pragma: no-cache");
?>
	<head>
<title>Registration form</title>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">
	<link rel="stylesheet" href="stylesheets/layout.css">
	<link rel="stylesheet" href="stylesheets/cerebro.css">
	<link rel="stylesheet" href="stylesheets/magnific-popup.css">
	<link rel="stylesheet" href="stylesheets/south-street/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" href="stylesheets/goalProgress.css">
	<link rel="stylesheet" href="stylesheets/iosOverlay.css">
	<link rel="stylesheet" href="chosen/chosen.min.css">


	<!-- Print CSS -->
	<link rel="stylesheet" href="stylesheets/cerebro-print.css" type="text/css" media="print" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	 <script type="text/javascript" src="js/magnific_popup.js" ></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://www.cerebro.org.uk/core/includes/src/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.complexify.js"></script>
	<script type="text/javascript" src="js/cerebro.js"></script>
	<script type="text/javascript" src="js/goalProgress.min.js"></script>
    <script type="text/javascript" src="js/iosOverlay.js"></script>
	<script type="text/javascript" src="js/spin.min.js"></script>
	
	<style type="text/css">
	
	.fb_iframe_widget {
		transform: scale(2.5);
		-ms-transform: scale(2.5); 
		-webkit-transform: scale(2.5); 
		-o-transform: scale(2.5); 
		-moz-transform: scale(2.5); 
		transform-origin: bottom left;
		-ms-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
		-moz-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
	}
	
	</style>
	
</head>

<body>


	
<div class="header">
		<div class="container">
		
			
			<div class="sixteen columns " style="padding-top: 5px; padding-bottom: 5px; background: none;">
				<img src="images/logos.png" /> 
				
			</div>

		</div><!-- container -->
	</div><!-- header -->
<div class="container">
	<div class="sixteen columns content">
		
		<div class="row primary_item alpha even">
			<h3>Thank you for registering your interest.</h3>
			<p>We'll be in touch soon.</p>
		</div>
		</br>
		</br>
		<a href= "kiosk.php?form=<? echo $form_id; ?>" class="button">Sign up here</a>

	</div>
</div>
<?php
} else {
?>
<!DOCTYPE html>
<html>
<head>
<title>Registration form</title>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">
	<link rel="stylesheet" href="stylesheets/layout.css">
	<link rel="stylesheet" href="stylesheets/cerebro.css">
	<link rel="stylesheet" href="stylesheets/magnific-popup.css">
	<link rel="stylesheet" href="stylesheets/south-street/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" href="stylesheets/goalProgress.css">
	<link rel="stylesheet" href="stylesheets/iosOverlay.css">
	<link rel="stylesheet" href="chosen/chosen.min.css">


	<!-- Print CSS -->
	<link rel="stylesheet" href="stylesheets/cerebro-print.css" type="text/css" media="print" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	 <script type="text/javascript" src="js/magnific_popup.js" ></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://www.cerebro.org.uk/core/includes/src/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.complexify.js"></script>
	<script type="text/javascript" src="js/cerebro.js"></script>
	<script type="text/javascript" src="js/goalProgress.min.js"></script>
    <script type="text/javascript" src="js/iosOverlay.js"></script>
	<script type="text/javascript" src="js/spin.min.js"></script>
	 
	<style type="text/css">
	
	.fb_iframe_widget {
		transform: scale(1.8);
		-ms-transform: scale(1.8); 
		-webkit-transform: scale(1.8); 
		-o-transform: scale(1.8); 
		-moz-transform: scale(1.8); 
		transform-origin: bottom left;
		-ms-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
		-moz-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
	}
	#contact_email {
		background-position: 96%;
		background-repeat: no-repeat;
	}
	
	#how_heard a {
		min-height: 2.2em;
		margin-top: 1em;
	}
	
	
	</style>
</head>
<body>
<script>
$(document).ready(function () {
var check = 0;
<?php echo $overlay; ?>
	$( "#reg_form" ).click(function(e) {
	  e.preventDefault();
	  $( "#footer" ).slideUp();
	  $( "#hidden_form" ).slideDown();
	});
	$( "#cancel" ).click(function(e) {
	  e.preventDefault();
	  $( "#footer" ).show();
	  $( "#hidden_form" ).hide();
	});
	
	$( "#hidden_2" ).hide();
	$( "#hidden_3" ).hide();
	
	$( "#office" ).click(function(e) {
		e.preventDefault();
		$( "#hidden_3" ).slideDown('slow');	
	});
	
	$( "#submit" ).click(function(e) {
	e.preventDefault();
	var invert = $("input#contact_recieve_newsletters_inv:checked").val();
	if (invert != 1){
		$("input#contact_recieve_newsletters").val("1");
	}
	preEmail(e, 1);
	
	
	});
	
	
		var preEmail = function (e, callback_submit) {
			if (typeof callback_submit === 'undefined') { callback_submit= '0'; }
			check = 1;
			var pre_email = $("#contact_email").val();
			if (pre_email == '') {
				return true;
			}
			
			
			
			
			var current_id = "";
			
			if (current_id > 0) {
				/* don't load db */
				return false;
			} else {
				/* get ajax */
				$.getJSON( "http://edinburgh.cerebro.org.uk/ad-json.php?mail="+pre_email, function( data ) {
					
					var items = [];

					var status = data['status'];
					
					
					if (status == 'left' && pre_email.indexOf("@ed.ac.uk") > -1 && pre_email.indexOf("s") == 0)  {														 
					   $("#contact_email").val(pre_email.replace('@ed.ac.uk','@sms.ed.ac.uk'));
					   	preEmail(e, callback_submit);
					   	return true;
					}
					
					if (status == 'existing') {
						$("#contact_email").css("background-image","url(images/correct.png)");
						$("#contact_existing_id_0").val(data['id']);
					} else if (status != 'left') {
					
						$("#contact_email").css("background-image","url(images/correct.png)");
				  
						if ( data['email'] != '') {
							$("#contact_email").val(data['email']);
							$("label[for='contact_email']").children(".form_label_text").hide();
						}
						if ( data['title'] != '') {
							$("#contact_title").val(data['title']);
							$("label[for='contact_title']").children(".form_label_text").hide();
						}
						
						if ( data['org'] != '') {
							$("#contact_company").val(data['org']);
							$("label[for='contact_company']").children(".form_label_text").hide();
						}
						/* choose type */
						
						if (data['title'] != '') {
							var title = data['title'];
						} else {
							var title = '';
						}
						var type = data['detail_type'];
						if (type == "Regular staff") {
						
							if (title.indexOf("Lectur") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Prof") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Principal") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Chair") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Teach") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Research") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Fellow") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Lab Manager") > -1) {
								$("#contact_type").val('2');
							} else if (title.indexOf("Technic") > -1) {
								$("#contact_type").val('2');
							} else {
								$("#contact_type").val('3');
							}
							
						} else if (type == "Staff visitor") {
							if (title.indexOf("Lectur") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Prof") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Principal") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Chair") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Teach") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Research") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Fellow") > -1) {
								$("#contact_type").val('1');
							} else if (title.indexOf("Lab Manager") > -1) {
								$("#contact_type").val('2');
							} else if (title.indexOf("Technic") > -1) {
								$("#contact_type").val('2');
							} else {
								$("#contact_type").val('3');
							}
						} else if (type == "Postgraduate student") {
							$("#contact_type").val('4');
						} else if (type == "Undergraduate student") {
							$("#contact_type").val('5');
						} else {
							$("#contact_type").val('7');
						}
						$("#contact_type").trigger("chosen:updated");
						
						/* name */
						var contact_first = data['first'];
						var contact_last = data['last'];
						var fullName = data['first']+' '+data['last'];
						
						
						$("#contact_first").val(contact_first);
						$("label[for='contact_first']").children(".form_label_text").hide();
						$("#contact_last").val(contact_last);
						$("label[for='contact_last']").children(".form_label_text").hide();
											
						if ($("#position_div").children().length < 4) {
							$("#position_div").append(' <div class="five columns validation meh"><p>Did we get these right?</p></div>	');
						}
						
						
					} else {
						$( "#hidden_2" ).slideDown('slow');
						$("#contact_email").val(pre_email);
						$("label[for='contact_email']").children(".form_label_text").hide();
					}
					
					if (callback_submit == 1) {
						if  (!$.trim(fullName) && !$.trim($("#contact_first").val()) && status != 'existing') {
							alert('Please enter your name!');
						} else {
							$( "#interest_form" ).submit();
						}
					}
				});
			}
		}
	
	$("#contact_email").blur(preEmail);
	
	$("#how_heard a").click(function(e) {
		e.preventDefault();
		var how_heard = $(this).attr("id");
		$("#how_heard a").css("background-color","#fff");
		$(this).css("background-color","#c7ebab");
		$("#how_heard_val").val(how_heard);
	
	});
	
 });


	

</script>
<div class="header">
		<div class="container">
		
			
			<div class="sixteen columns " style="padding-top: 5px; padding-bottom: 5px; background: none;">
				<img src="images/logos.png" /> 
				
			</div>

		</div><!-- container -->
	</div><!-- header -->
<div class="container">
	<div class="sixteen columns content">
		<p><br /></p>
		<div class="sixteen columms alpha omega">
			
			<div class="two-thirds column alpha">
			
				<h1>Sign in to <?php echo $event_name; ?></h1>
			</div>
					</div>
		<p> <br /></p>
		<div class = "row form_display">
			<form name="interest_form" id="interest_form" method="POST" action="kiosk.php?event_id=<?php echo $_GET['event_id']; ?>"> 
				<div class="form_area">
				
							<div class="row primary_item even">
							
								<div class="fifteen columns alpha" id="contact_email_div"></br>
									<label for="contact_email" class="required"><img src="images/form_email_icon.png" alt="Email" /><span class="form_label_text">Your University Email (or personal email)</span> </label>
									<input type="text" name="contact_email" id="contact_email"   class="req has_image" value="" autocomplete="off"/>
								</div>
								
							</div>
							
							<div class="row primary_item even">
							
								<div class="fifteen columns alpha" id="how_heard">
									<h3>I heard about it by...</h3>
									<input type="hidden" name="how_heard_val" id="how_heard_val" value="NOT_SELECTED" />
									
									<a href="#" id="heard_uni_website" class="two columns alpha button">University website</a>
									<a href="#" id="heard_other_website" class="two columns button">Another website</a>
									<a href="#" id="heard_email_invitation" class="two columns button">Email or invitation</a>
									<a href="#" id="heard_social_media" class="two columns button">Social media</a>
									<a href="#" id="heard_flyer_poster" class="two columns button">Flyer or poster</a>
									<a href="#" id="heard_word_mouth" class="two columns button">Word of mouth</a>
									<a href="#" id="heard_other" class="one column omega button">Other</a>
								</div>
								
							</div>	
					<p> <br /> </p>
					
					<div id="hidden_2">
					
					<div class="row primary_item">
					
						<div class="sixteen columns alpha">
                             <h4> We just need to ask a couple of extra things... </h4>
                        </div>
					</div>
					
					<div class="row primary_item even">
						<div class="six columns alpha" id="contact_first1">
							<label for="contact_first" class="required"><span class="form_label_text">First name</span> </label>
							<input type="text" name="contact_first" id="contact_first"  class="req" value=""/>
						</div>
						<div class="six columns omega" id="contact_last1">
							<label for="contact_last" class="required"><span class="form_label_text">Last name</span> </label>
							<input type="text" name="contact_last" id="contact_last"  class="req" value=""/>
						</div>
					</div>
					
					<div class="row primary_item">
							<div class="four columns alpha">
                            <select id="contact_type" name="contact_type" data-placeholder="Select type"><option value=""></option>
							<option value="5" >Undergraduate Student</option><option value="4" >Postgraduate Student</option>
							<option value="1" >Academic Staff</option><option value="2" >Technical Support Staff</option><option value="3" >Administrative / Managerial Support Staff</option><option value="6" >Alumnus</option><option value="7" >Media contact</option><option value="0">Member of the public</option></select></div>
				
							<div class="three column alpha" id="contact_title1">
                                <label for="contact_title" class=""><span class="form_label_text">Job Title</span> </label>
                                <input type="text" name="contact_title" id="contact_title"  class="" value=""/>
                            </div>
                            <div class="three columns omega" id="contact_company1">
                                <label for="contact_company" class=""><span class="form_label_text">Department/Company</span> </label>
                                <input type="text" name="contact_company" id="contact_company"  class="" value=""/>
                            </div>
						</div>
					</div>
					
				
					</div>
					<p><br /><br /></p>
					
					<div class="row primary_item" style="background-color: #f4f4f4 !important; padding: 1em !important;">
						<div class="fifteen columns alpha checkbox">
							<label for="contact_recieve_newsletters_inv" ><span class="form_label_text">Sign up to newsletter?</label>
							<input name="contact_recieve_newsletters_inv" type="checkbox" id="contact_recieve_newsletters_inv" value="1" checked="" />
						</div>
						<div class="fifteen columns alpha" style="background-color: #f4f4f4 !important;">
							<h3>What happens to my data?</h3>
							<p><strong>Don't worry.</strong> As a University, we're committed to the highest possible standards of data protection. We keep your details in order to let you know about what's happening, and in order to learn which things interest people the most.</p>
							<ul class="circle">
								<li><strong>We won't bother you too much</strong>. We send about one email a month.</li>
								<li><strong>You can unsubscribe whenever you like</strong>. Just click the link at the bottom of the emails we send you, or get in touch.</li>
								<li><strong>Your personal information is safe</strong>. We won't sell your data to others and <strong>we use University-approved systems to send our emails</strong>.</li>
								
							</ul>
						</div>
					</div>
					<div id="hidden_3">
						<div class="row primary_item">
							<div class = "ten columns">
								<h3>Add note</h3>
							</div>
						</div>
					
						<div class="row primary_item">
							<div class = "ten columns add_note alpha omega">
								<textarea id = "note" name = "note"></textarea>
							</div>	
						</div>
						<div class="row primary_item">
							<div class="four columns alpha">
								<select id="assigned_to" name="assigned_to" data-placeholder="Select user"><option value=""></option>
								<?php echo $userstring; ?>
								</select>
							</div>
						</div>
				
						
					</div>
				
					
					

						<input type="hidden" name="contact_specific_meeting" id="contact_specific_meeting" value="<?php echo $_GET['event_id']; ?>" />
						
						<input type="hidden" name="contact_existing_id_0" id="contact_existing_id_0" value="NONE" />
						
						<input type="hidden" name="contact_recieve_newsletters" id="contact_recieve_newsletters" value="0" />
						
						<input type="hidden" name="contact_when_we_spoke" id="contact_when_we_spoke" value="<?php echo date('Y-m-d'); ?>" />

						<input type="hidden" name="contact_how_we_spoke" id="contact_how_we_spoke" value="<?php echo $event_type; ?>" />
						
						
																			
						<div class="row primary_item" style="background-color: #f4f4f4 !important;">
							<div class="two columns alpha">
									<a href="#" class="button" id="office"><img src="images/fold_down.png" alt="Add">Office use</a>
							</div>		
							<div class="thirteen columns omega action_bar">
									<a href="#" class="button" id="submit">Submit</a>

							</div>							
						</div>
					
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<?php
}
