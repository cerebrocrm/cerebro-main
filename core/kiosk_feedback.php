<?php /*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-15 Joseph Farthing / The University of Edinburgh
   Copyright 2012 Joseph Farthing

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
// includes
include 'functions.php';
include 'db_config.php';
global $connectionmanager;
$connectionmanager = new TableConnect();
$connectionmanager->connect();
// sanitise inputs
function sane_get($get) {
	global $connectionmanager;
	return mysqli_real_escape_string($connectionmanager->connection, $_GET[$get]);
}
// build kiosk
if (isset($_POST['event_id']) && $_POST['type'] == 'feedback_note') {
	$form_id = mysqli_real_escape_string($connectionmanager->connection, $_POST['event_id']);
	$note = mysqli_real_escape_string($connectionmanager->connection, $_POST['note']);
	$check = mysqli_query($connectionmanager->connection, "
		INSERT INTO global_notes (note_table, note_item, note_text, note_date ) VALUES
			('events',
			" . $form_id . ",
			'**Received anonymous feedback:**

" . $note . "',
			" . time() . "
			)");
} elseif (isset($_GET['event_id'])) {
	$form_id = sane_get('event_id');
} elseif (isset($_POST['event'])) {
	$type = array(0, 619, 620, 621);
	$ind = $_POST['feedback'];
	$check = mysqli_query($connectionmanager->connection, "SELECT * FROM extra_int WHERE meta_id=" . $type[$ind] . " AND item_id=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['event']));
	if (mysqli_num_rows($check) > 0) mysqli_query($connectionmanager->connection, "UPDATE extra_int SET val=(val+1) WHERE meta_id=" . $type[$ind] . " AND item_id=" . mysqli_real_escape_string($connectionmanager->connection, $_POST['event']));
	else mysqli_query($connectionmanager->connection, "INSERT INTO extra_int (meta_id,item_id,val) VALUES (" . $type[$ind] . "," . mysqli_real_escape_string($connectionmanager->connection, $_POST['event']) . ",1)");
} elseif (isset($_GET['event']) && $_GET['email'] == 1) {
	$form_id = sane_get('event');
	$type = array(0, 619, 620, 621);
	$ind = $_GET['feedback'];
	$check = mysqli_query($connectionmanager->connection, "SELECT * FROM extra_int WHERE meta_id=" . $type[$ind] . " AND item_id=" . mysqli_real_escape_string($connectionmanager->connection, $_GET['event']));
	if (mysqli_num_rows($check) > 0) mysqli_query($connectionmanager->connection, "UPDATE extra_int SET val=(val+1) WHERE meta_id=" . $type[$ind] . " AND item_id=" . mysqli_real_escape_string($connectionmanager->connection, $_GET['event']));
	else mysqli_query($connectionmanager->connection, "INSERT INTO extra_int (meta_id,item_id,val) VALUES (" . $type[$ind] . "," . mysqli_real_escape_string($connectionmanager->connection, $_GET['event']) . ",1)");
} else {
	// add cookie check
	die('Sorry, an error has occurred. Please speak to a member of staff.');
}
// get event info
if (isset($_GET['submit']) || isset($_POST['type'])) {
	header("Pragma: no-cache");
?>
	<head>
<title>Registration form</title>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">
	<link rel="stylesheet" href="stylesheets/layout.css">
	<link rel="stylesheet" href="stylesheets/cerebro.css">
	<link rel="stylesheet" href="stylesheets/magnific-popup.css">
	<link rel="stylesheet" href="stylesheets/south-street/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" href="stylesheets/goalProgress.css">
	<link rel="stylesheet" href="stylesheets/iosOverlay.css">
	<link rel="stylesheet" href="chosen/chosen.min.css">


	<!-- Print CSS -->
	<link rel="stylesheet" href="stylesheets/cerebro-print.css" type="text/css" media="print" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	 <script type="text/javascript" src="js/magnific_popup.js" ></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://www.cerebro.org.uk/core/includes/src/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.complexify.js"></script>
	<script type="text/javascript" src="js/cerebro.js"></script>
	<script type="text/javascript" src="js/goalProgress.min.js"></script>
    <script type="text/javascript" src="js/iosOverlay.js"></script>
	<script type="text/javascript" src="js/spin.min.js"></script>

	<style type="text/css">

	.fb_iframe_widget {
		transform: scale(2.5);
		-ms-transform: scale(2.5);
		-webkit-transform: scale(2.5);
		-o-transform: scale(2.5);
		-moz-transform: scale(2.5);
		transform-origin: bottom left;
		-ms-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
		-moz-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
	}

	</style>

</head>

<body>



<div class="header">
		<div class="container">


			<div class="sixteen columns " style="padding-top: 5px; padding-bottom: 5px; background: none;">
				<img src="images/logos.png" />

			</div>

		</div><!-- container -->
	</div><!-- header -->
<div class="container">
	<div class="sixteen columns content">

		<div class="row primary_item alpha even">
			<h3>Thank you for giving feedback.</h3>
			<p>You can close this tab.</p>
		</div>
		</br>
		</br>

	</div>
</div>
<?php
} elseif (isset($_GET['email'])) {
	header("Pragma: no-cache");
?>
	<head>
<title>Registration form</title>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">
	<link rel="stylesheet" href="stylesheets/layout.css">
	<link rel="stylesheet" href="stylesheets/cerebro.css">
	<link rel="stylesheet" href="stylesheets/magnific-popup.css">
	<link rel="stylesheet" href="stylesheets/south-street/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" href="stylesheets/goalProgress.css">
	<link rel="stylesheet" href="stylesheets/iosOverlay.css">
	<link rel="stylesheet" href="chosen/chosen.min.css">


	<!-- Print CSS -->
	<link rel="stylesheet" href="stylesheets/cerebro-print.css" type="text/css" media="print" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	 <script type="text/javascript" src="js/magnific_popup.js" ></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://www.cerebro.org.uk/core/includes/src/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.complexify.js"></script>
	<script type="text/javascript" src="js/cerebro.js"></script>
	<script type="text/javascript" src="js/goalProgress.min.js"></script>
    <script type="text/javascript" src="js/iosOverlay.js"></script>
	<script type="text/javascript" src="js/spin.min.js"></script>

	<script type="text/javascript">
		$( document ).ready(function() {
			$("#submit").prop( "disabled", true );
			$('#note').keyup(function (e) {
				if ($(this).val() !== '') {
					$('#submit').prop( "disabled", false );
				} else {
					$('#submit').prop( "disabled", true );
				}
			});
		});
	</script>

	<style type="text/css">

	.fb_iframe_widget {
		transform: scale(2.5);
		-ms-transform: scale(2.5);
		-webkit-transform: scale(2.5);
		-o-transform: scale(2.5);
		-moz-transform: scale(2.5);
		transform-origin: bottom left;
		-ms-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
		-moz-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
	}

	</style>

</head>

<body>



<div class="header">
		<div class="container">


			<div class="sixteen columns " style="padding-top: 5px; padding-bottom: 5px; background: none;">
				<img src="images/logos.png" />

			</div>

		</div><!-- container -->
	</div><!-- header -->
<div class="container">
	<div class="sixteen columns content">

		<div class="row primary_item alpha even">
			<p><br /></p>
			<h1>Thanks, your feedback really helps</h1>
			<p>Do you have any anything you want to add?</p>
		</div>
		</br>
		</br>

			<form action="kiosk_feedback.php?submit" method="POST">  <div class="form_area">
				<input type="hidden" name="type" value="feedback_note"/>
				<input type="hidden" name="event_id" value="<?php echo $form_id; ?>" />
				<textarea id="note" name="note" placeholder="Please leave your anonymous comments here." style="font-size: 120%; width:100%; min-height: 15em;"></textarea>

			</div>

			<div class="two columns alpha">
				<input type="submit" name="submit" value="Submit" id="submit"/>
			</div>

		     </form>

	</div>
</div>
<?php
} else {
?>
<!DOCTYPE html>
<html>
<head>
<title>Registration form</title>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">
	<link rel="stylesheet" href="stylesheets/layout.css">
	<link rel="stylesheet" href="stylesheets/cerebro.css">
	<link rel="stylesheet" href="stylesheets/magnific-popup.css">
	<link rel="stylesheet" href="stylesheets/south-street/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" href="stylesheets/goalProgress.css">
	<link rel="stylesheet" href="stylesheets/iosOverlay.css">
	<link rel="stylesheet" href="chosen/chosen.min.css">


	<!-- Print CSS -->
	<link rel="stylesheet" href="stylesheets/cerebro-print.css" type="text/css" media="print" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	 <script type="text/javascript" src="js/magnific_popup.js" ></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://www.cerebro.org.uk/core/includes/src/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.complexify.js"></script>
	<script type="text/javascript" src="js/cerebro.js"></script>
	<script type="text/javascript" src="js/goalProgress.min.js"></script>
    <script type="text/javascript" src="js/iosOverlay.js"></script>
	<script type="text/javascript" src="js/spin.min.js"></script>
	<script type="text/javascript" src="js/jquery.foggy.js"></script>

	<style type="text/css">

	.fb_iframe_widget {
		transform: scale(1.8);
		-ms-transform: scale(1.8);
		-webkit-transform: scale(1.8);
		-o-transform: scale(1.8);
		-moz-transform: scale(1.8);
		transform-origin: bottom left;
		-ms-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
		-moz-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
	}
	#contact_email_0 {
		background-position: 96%;
		background-repeat: no-repeat;
	}




	</style>
</head>
<body>
<script>
$(document).ready(function () {
	var wait = 0;

	$( "#choose_green" ).click(function () {

		if (wait == 0){

			var dataString = 'feedback=1&event=<? echo $_GET['event_id']; ?>';
			$.ajax({
				type: "POST",
				url: "kiosk_feedback.php",
				data: dataString,
			});

			wait = 1;

			$('.container').foggy();

			var opts = {
				lines: 13, // The number of lines to draw
				length: 11, // The length of each line
				width: 5, // The line thickness
				radius: 17, // The radius of the inner circle
				corners: 1, // Corner roundness (0..1)
				rotate: 0, // The rotation offset
				color: '#FFF', // #rgb or #rrggbb
				speed: 1, // Rounds per second
				trail: 60, // Afterglow percentage
				shadow: false, // Whether to render a shadow
				hwaccel: false, // Whether to use hardware acceleration
				className: 'spinner', // The CSS class to assign to the spinner
				zIndex: 2e9, // The z-index (defaults to 2000000000)
				top: 'auto', // Top position relative to parent in px
				left: 'auto' // Left position relative to parent in px
			};
			var target = document.createElement("div");
			document.body.appendChild(target);
			var spinner = new Spinner(opts).spin(target);
			iosOverlay({
				duration: 7e2,
				spinner: spinner
			});
			window.setTimeout(function() {
				iosOverlay({
					text: "Thanks!",
					duration: 7e2,
					icon: "images/check.png"
				});
			}, 7e2);

			window.setTimeout(function() {
				$('.container').foggy(false);
				wait = 0;
			}, 7e2);
		}

	});

	$( "#choose_amber" ).click(function () {

		if (wait == 0){

			var dataString = 'feedback=2&event=<? echo $_GET['event_id']; ?>';
			$.ajax({
				type: "POST",
				url: "kiosk_feedback.php",
				data: dataString,
			});

			wait = 1;

			$('.container').foggy();

			var opts = {
				lines: 13, // The number of lines to draw
				length: 11, // The length of each line
				width: 5, // The line thickness
				radius: 17, // The radius of the inner circle
				corners: 1, // Corner roundness (0..1)
				rotate: 0, // The rotation offset
				color: '#FFF', // #rgb or #rrggbb
				speed: 1, // Rounds per second
				trail: 60, // Afterglow percentage
				shadow: false, // Whether to render a shadow
				hwaccel: false, // Whether to use hardware acceleration
				className: 'spinner', // The CSS class to assign to the spinner
				zIndex: 2e9, // The z-index (defaults to 2000000000)
				top: 'auto', // Top position relative to parent in px
				left: 'auto' // Left position relative to parent in px
			};
			var target = document.createElement("div");
			document.body.appendChild(target);
			var spinner = new Spinner(opts).spin(target);
			iosOverlay({
				duration: 7e2,
				spinner: spinner
			});
			window.setTimeout(function() {
				iosOverlay({
					text: "Thanks!",
					duration: 7e2,
					icon: "images/check.png"
				});
			}, 7e2);

			window.setTimeout(function() {
				$('.container').foggy(false);
				wait = 0;
			}, 7e2);
		}

	});

	$( "#choose_red" ).click(function () {

		if (wait == 0){

			var dataString = 'feedback=3&event=<? echo $_GET['event_id']; ?>';
			$.ajax({
				type: "POST",
				url: "kiosk_feedback.php",
				data: dataString,
			});

			wait = 1;

			$('.container').foggy();

			var opts = {
				lines: 13, // The number of lines to draw
				length: 11, // The length of each line
				width: 5, // The line thickness
				radius: 17, // The radius of the inner circle
				corners: 1, // Corner roundness (0..1)
				rotate: 0, // The rotation offset
				color: '#FFF', // #rgb or #rrggbb
				speed: 1, // Rounds per second
				trail: 60, // Afterglow percentage
				shadow: false, // Whether to render a shadow
				hwaccel: false, // Whether to use hardware acceleration
				className: 'spinner', // The CSS class to assign to the spinner
				zIndex: 2e9, // The z-index (defaults to 2000000000)
				top: 'auto', // Top position relative to parent in px
				left: 'auto' // Left position relative to parent in px
			};
			var target = document.createElement("div");
			document.body.appendChild(target);
			var spinner = new Spinner(opts).spin(target);
			iosOverlay({
				duration: 7e2,
				spinner: spinner
			});
			window.setTimeout(function() {
				iosOverlay({
					text: "Thanks!",
					duration: 7e2,
					icon: "images/check.png"
				});
			}, 7e2);

			window.setTimeout(function() {
				$('.container').foggy(false);
				wait = 0;
			}, 7e2);
		}

	});




 });




</script>
<div class="header">
		<div class="container">


			<div class="sixteen columns " style="padding-top: 5px; padding-bottom: 5px; background: none;">
				<img src="images/logos.png" />

			</div>

		</div><!-- container -->
	</div><!-- header -->
<div class="container">
	<div class="sixteen columns content">
		<p><br /></p>
		<div class="sixteen columms alpha omega" style="text-align: center">


				<h1>How was this event?</h1>
				<p>Please choose the face that most closely represents how you feel.</p>
				<p><br /></p>

				<a href="#" id="choose_green"><img src="images/green-large.jpg" /></a>
				<a href="#" id="choose_amber"><img src="images/amber-large.jpg" /></a>
				<a href="#" id="choose_red"><img src="images/red-large.jpg" /></a>
					</div>
		<p> <br /></p>




						<input type="hidden" name="contact_specific_meeting_0" id="contact_specific_meeting_0" value="<?php echo $event_name; ?>" />

						<input type="hidden" name="contact_existing_id_0" id="contact_existing_id_0" value="NONE" />

						<input type="hidden" name="contact_when_we_spoke_0" id="contact_when_we_spoke_0" value="<?php echo date('Y-m-d'); ?>" />

						<input type="hidden" name="contact_how_we_spoke_0" id="contact_how_we_spoke_0" value="<?php echo $event_type; ?>" />


						<input type="hidden" name="contact_user_0" id="contact_user_0" value="<?php echo $currentuser[1]; // not defined? ?>" />


				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<?php
}
