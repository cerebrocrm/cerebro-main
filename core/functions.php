<?php
function NoteThread($notes) {
	$output = '';
	global $users;
	foreach ($notes as $row) {
		if ($j == 0) {
			$even = '';
			$j = 1;
		} else {
			$even = ' even ';
			$j = 0;
		}
		$output.= '<div class="row note_item ' . $even . '" id="' . $row[0]['note_id'] . '">';
		$output.= '<div class="two columns alpha note_info">';
		$output.= '<p class="user">' . $users[$row[0]['note_user']] . '</p>';
		$output.= '<span title ="' . D('F d, Y', $row[0]['note_date']) . '"><p class="date">' . fuzzy_time($row[0]['note_date']) . '</p></span>';
		$output.= '</div>';
		$output.= '<div>';
		$output.= '<div class="seven columns note_text">';
		$output.= str_replace('<p>', '<p class="description">', parse_markdown(stripslashes($row[0]['note_text']))) . '<div class="markdown" style="display:none">' . stripslashes($row[0]['note_text']) . '</div>';
		$output.= '</div>';
		$output.= '<div class="row eight columns ' . $even . ' mini_note_actions action_bar">
						<a href="#" id="' . $row[0]['note_id'] . '" class="button borderless list_button"><img src="images/edit_icon.png" alt="Edit" title="Edit" class="solo"/></a>
					</div>';
		$output.= '</div>';
		$k = $j;
		for ($i = count($row) - 1;$i > 0;$i--) {
			if ($k == 0) {
				$subeven = '';
				$k = 1;
			} else {
				$subeven = ' even ';
				$k = 0;
			}
			$output.= '<div class="eight columns note_item ' . $subeven . ' alpha omega">';
			$output.= '<div class="two columns alpha note_info">';
			$output.= '<p class="user">' . $users[$row[$i]['note_user']] . '</p>';
			$output.= '<span title ="' . D('F d, Y', $row[$i]['note_date']) . '"><p class="date">' . fuzzy_time($row[0]['note_date']) . '</p></span>';
			$output.= '</div>';
			$output.= '<div>';
			$output.= '<div class="five columns note_text">';
			$output.= str_replace('<p>', '<p class="description">', parse_markdown(stripslashes($row[$i]['note_text']))) . '<div class="markdown" style="display:none">' . stripslashes($row[$i]['note_text']) . '</div>';
			$output.= '</div>';
			$output.= '<div class="row seven columns ' . $subeven . ' mini_note_actions action_bar">
							<a href="#" id="' . $row[$i]['note_id'] . '" class="button borderless list_button"><img src="images/edit_icon.png" alt="Edit" title="Edit" class="solo"/></a>
						</div>';
			$output.= '</div>';
			$output.= '</div>';
		}
		$output.= '<div class="row eight columns omega ' . $even . ' mini_note_actions action_bar">
					<div id="area_' . $row[0]['note_id'] . '" class="row seven columns note_text note_reply_textbox' . $even . '"><textarea id="reply_' . $row[0]['note_id'] . '"></textarea></div>
					<a href="#" id="' . $row[0]['note_id'] . '" class="button note_reply"><img src="images/reply_icon.png" alt="Reply" title="Reply"/> Reply</a>
				</div>';
		$output.= '</div>';
	}
	return $output;
}
function gets() {
	$string = '';
	foreach ($_GET as $index => $value) {
		$string.= '&' . $index . '=' . $value;
	}
	return $string;
}
function mini_template($content, $modal_mode = 0) {
	if ($modal_mode == 1) {
		$bgcolour = '#f4f4f4';
	} else {
		$bgcolour = '#fff';
	}
	return '
	<!DOCTYPE html><html lang="en"> <head><!-- Basic Page Needs==================================================--><meta charset="utf-8"><title>' . $title . '</title><meta name="description" content=""><meta name="author" content=""><!-- Mobile Specific Metas==================================================--><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><!-- CSS==================================================--><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/base.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/skeleton.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/layout.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/cerebro.css"><!--[if lt IE 9]><script src="http://cerebro.org.uk/errors/http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--><!-- Favicons==================================================--><link rel="shortcut icon" href="http://cerebro.org.uk/errors/images/favicon.ico">
<style>
 body {
	background-color: ' . $bgcolour . ' !important;
 }
</style>
	</head><body><!-- Primary Page Layout==================================================-->' . $content . '<!-- End Document==================================================--></body></html>';
}
function fail_error($message, $title = 'Something went wrong') {
	return '
	<!DOCTYPE html><html lang="en"> <head><!-- Basic Page Needs==================================================--><meta charset="utf-8"><title>' . $title . '</title><meta name="description" content=""><meta name="author" content=""><!-- Mobile Specific Metas==================================================--><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><!-- CSS==================================================--><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/base.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/skeleton.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/layout.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/cerebro.css"><!--[if lt IE 9]><script src="http://cerebro.org.uk/errors/http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--><!-- Favicons==================================================--><link rel="shortcut icon" href="http://cerebro.org.uk/errors/images/favicon.ico"></head><body><!-- Primary Page Layout==================================================--><div class="header"><div class="container"><div class="sixteen columns" style="display: block; height: 200px !important;"> <p><br/></p></div><div class="sixteen columns"><img src="http://cerebro.org.uk/errors/images/header.png" alt="Cerebro"/></div></div></div><div class="container"><div class="sixteen columns content"><h2>' . $title . '</h2><p>' . $message . '</p></div></div><!--- Unfortunately, Microsoft has added a clever new- "feature" to Internet Explorer. If the text of- an error\'s message is "too small", specifically- less than 512 bytes, Internet Explorer returns- its own error message. You can turn that off,- but it\'s pretty tricky to find switch called- "smart error messages". That means, of course,- that short error messages are censored by default.- IIS always returns error messages that are long- enough to make Internet Explorer happy. The- workaround is pretty simple: pad the error- message with a big comment like this to push it- over the five hundred and twelve bytes minimum.- Of course, that\'s exactly what you\'re reading- right now.--><script>(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\'); ga(\'create\', \'UA-3347190-3\', \'auto\'); ga(\'send\', \'pageview\'); ga(\'send\', \'\event\', \'error\', "' . $title . '", window.location.href);</script><!-- End Document==================================================--></body></html>';
}
global $lang;
global $timezone;
$lang = array('CODE' => 'en_GB', 'TIMEZONE' => 'Europe/London', 1 => 'one', 2 => 'two', 3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six', 7 => 'seven', 8 => 'eight', 9 => 'nine', 10 => 'ten', 11 => 'eleven', 12 => 'twelve', 13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen', 16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen', 19 => 'nineteen', 20 => 'twenty', 'month' => 'month', 'months' => 'months', 'ago' => 'ago', 'in' => 'in', 'about' => 'about', 'just a moment' => 'just a moment', 'a few minutes' => 'a few minutes', 'less than an hour' => 'less than an hour', 'a little while ago' => 'a little while ago', 'today' => 'today', 'yesterday' => 'yesterday', 'on' => 'on', 'welcome' => 'Welcome', 'log in' => 'Log in', 'log out' => 'Log out', 'user details' => 'User details', 'event' => 'event', 'events' => 'events', 'upcoming events' => 'Upcoming events', 'past events' => 'Past events', 'booked' => 'booked', 'cancelled' => 'cancelled', 'new event' => 'New event',);
/*
$lang = array (
	'CODE' => 'nl_NL',
	'TIMEZONE' => 'Europe/Amsterdam', // these should be set by another value, but this acts as the default
	1 => 'één',
	2 => 'twee',
	3 => 'drie',
	4 => 'vier',
	5 => 'vijf',
	6 => 'zes',
	7 => 'zeven',
	8 => 'acht',
	9 => 'negen',
	10 => 'tien',
	11 => 'elf',
	12 => 'twaalf',
	13 => 'dertien',
	14 => 'viertien',
	15 => 'vijvtien',
	16 => 'zestien',
	17 => 'zeventien',
	18 => 'achtien',
	19 => 'negentien',
	20 => 'twintig',
	'month' => 'maand',
	'months' => 'maanden',
	'ago' => 'geleden',
	'in' => 'in',
	'about' => 'ongeveer',
	'just a moment' => 'een moment',
	'a few minutes' => 'een paar minuuten',
	'less than an hour' => 'minder dan één uur',
	'a little while ago' => 'een tijdje geleden',
	'more than ten years' => 'meer dan tien jaar',
	'today' => 'vandaag',
	'yesterday' => 'gisteren',
	'on' => 'op',
	'welcome' => 'Welkom',
	'log in' => 'Inloggen',
	'log out' => 'Uitloggen',
	'user details' => 'Profiel',
	'event' => 'evenement',
	'events' => 'evenementen',
	'upcoming events' => 'Aankomende evenementen',
	'past events' => 'Oude evenementen',
	'booked' => 'geboekt',
	'cancelled' => 'geannuleerd',
	'new event' => 'Niewe evenement',
	'about a week' => 'ongeveer een week',
	'about an hour' => 'ongeveer een urr',
	'just a moment' => 'een par seconden',
	'a few minutes' => '',
	'about two weeks' => 'ongeveer twee weken',
	'about three weeks' => 'ongeveer drie weken',
	'reply to this post' => 'Antwoord om dit bericht',
	'type your notes here' => 'Schrijft uw notities hier'

);
*/
$timezone = $lang['TIMEZONE']; //needs to be a server set variable
// Translations
function T($text, $s = null) {
	global $lang;
	$translation = $lang[$text];
	if ($text == '') {
		return null; // add better English fail safe

	}
	if ($translation == '') {
		$translation = $lang[trim(strtolower($text)) ];
		if ($translation == '') {
			return $text;
		}
	}
	if ($s !== null) {
		$s = explode($s);
		foreach ($s as $num => $value) {
			$translation = str_replace('%' . $num, $value, $translation);
		}
	}
	return $translation;
}
// Translated dates - same syntax as Date() but provides correct dates on demand.
// We do it this way because PHP switches locale per PROCESS not per THREAD.
// That means otherwise one user might suddenly find themselves using another language.
function D($format, $timestamp = null) {
	global $lang;
	setlocale(LC_TIME, $lang['CODE']);
	$time_format = date_format_to($format, 'strf');
	if ($time_format == $format) {
		return date($format, $timestamp);
	} else {
		return strftime($time_format, $timestamp);
	}
}
function isValidTimeStamp($timestamp) {
	return ((string)(int)$timestamp === $timestamp) && ($timestamp <= PHP_INT_MAX) && ($timestamp >= ~ PHP_INT_MAX);
}
// Get a fuzzy time, including translation
function fuzzy_time($time) {
	if (!defined('NOW')) {
		define('NOW', time());
	}
	if (!defined('ONE_MINUTE')) {
		define('ONE_MINUTE', 60);
	}
	if (!defined('ONE_HOUR')) {
		define('ONE_HOUR', 3600);
	}
	if (!defined('ONE_DAY')) {
		define('ONE_DAY', 86400);
	}
	if (!defined('ONE_WEEK')) {
		define('ONE_WEEK', ONE_DAY * 7);
	}
	if (!defined('ONE_MONTH')) {
		define('ONE_MONTH', ONE_WEEK * 4);
	}
	if (!defined('ONE_YEAR')) {
		define('ONE_YEAR', ONE_MONTH * 12);
	}
	if (!isValidTimeStamp($time)) {
		if (($time = strtotime($time)) == false) {
			return T('a little while ago');
		}
	} else {
		//echo 'valid timestamp';

	}
	// sod = start of day :)
	$sod = mktime(0, 0, 0, date('m', $time), date('d', $time), date('Y', $time));
	$sod_now = mktime(0, 0, 0, date('m', NOW), date('d', NOW), date('Y', NOW));
	//echo $sod.' '.$sod_now;
	if ($sod_now < $sod) {
		//echo 'future';
		$time_string = str_replace(T('in') . ' ' . T('on'), T('on') . ' ', T('in') . ' ' . detail_fuzzy_time($time, $sod_now, $sod));
	} else {
		//echo 'past';
		$time_string = detail_fuzzy_time($time, $sod, $sod_now);
		if (strpos($time_string, T('yesterday')) === false && strpos($time_string, T('on') . ' ') === false && strpos($time_string, T('today')) === false) {
			$time_string.= ' ' . T('ago');
		}
	}
	return $time_string;
}
function detail_fuzzy_time($time, $sod, $sod_now) {
	if (!defined('NOW')) {
		define('NOW', time());
	}
	if (!defined('ONE_MINUTE')) {
		define('ONE_MINUTE', 60);
	}
	if (!defined('ONE_HOUR')) {
		define('ONE_HOUR', 3600);
	}
	if (!defined('ONE_DAY')) {
		define('ONE_DAY', 86400);
	}
	if (!defined('ONE_WEEK')) {
		define('ONE_WEEK', ONE_DAY * 7);
	}
	if (!defined('ONE_MONTH')) {
		define('ONE_MONTH', ONE_WEEK * 4);
	}
	if (!defined('ONE_YEAR')) {
		define('ONE_YEAR', ONE_MONTH * 12);
	}
	// today
	if ($sod_now == $sod) {
		if ($time > NOW - (ONE_MINUTE * 3)) {
			return T('just a moment');
		} elseif ($time > NOW - (ONE_MINUTE * 7)) {
			return T('a few minutes');
		} elseif ($time > NOW - (ONE_HOUR)) {
			return T('less than an hour');
		}
		return T('today');
	}
	// yesterday
	if (($sod_now - $sod) <= ONE_DAY) {
		if (date('i', $time) > (ONE_MINUTE + 30)) {
			$time+= ONE_HOUR / 2;
		}
		return T('yesterday');
	}
	// within the last 5 days
	if (($sod_now - $sod) <= (ONE_DAY * 5)) {
		$str = D('l', $time);
		$hour = D('G', $time);
		return T('on') . ' ' . $str;
	}
	// number of weeks (between 1 and 3)...
	if (($sod_now - $sod) < (ONE_WEEK * 3.5)) {
		if (($sod_now - $sod) < (ONE_WEEK * 1.5)) {
			return T('about a week');
		} elseif (($sod_now - $sod) < (ONE_DAY * 2.5)) {
			return T('about two weeks');
		} else {
			return T('about three weeks');
		}
	}
	// number of months (between 1 and 11)...
	if (($sod_now - $sod) < (ONE_MONTH * 11.5)) {
		for ($i = (ONE_WEEK * 3.5), $m = 0;$i < ONE_YEAR;$i+= ONE_MONTH, $m++) {
			if (($sod_now - $sod) <= $i) {
				return T('about') . ' ' . T($m) . ' ' . T('month' . (($m > 1) ? 's' : ''));
			}
		}
	}
	// number of years...
	for ($i = (ONE_MONTH * 11.5), $y = 0;$i < (ONE_YEAR * 10);$i+= ONE_YEAR, $y++) {
		if (($sod_now - $sod) <= $i) {
			if (array_key_exists($y, $convert)) {
				return T('about') . ' ' . T($y) . ' ' . T('year' . (($m > 1) ? 's' : ''));
			}
		}
	}
	// more than ten years...
	return T('more than ten years');
}
// GENERATE PASSWORD
function genpwd($password) {
	return hash('sha256', '5932446259422767385121652b226e527328577e2a476c7433245b5636' . $password . '3459df' . $password);
}
function stringToColourCode($str) {
	$code = dechex(crc32($str));
	$code = substr($code, 0, 6);
	$code = str_replace('e', '2', $code);
	$code = str_replace('f', '2', $code);
	$code = str_replace('d', '2', $code);
	return $code;
}
function insert($post) {
	$post = addslashes($_POST[$post]);
	return $post;
}
// PARSE A STRING FOR URLS
// Convert the URLs into Tiny URLs
function convert_urls_to_tiny($text, $contact_id, $campaign, $medium, $campaign_id) {
	$text = urldecode(urldecode($text));
	$text = str_replace('&amp;', '&', $text);
	$text = str_replace('&nbsp;', ' ', $text);
	$regex = "
  #((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie";
	$m = preg_match_all($regex, $text, $match);
	$linkarray = array();
	if ($m) {
		$links = $match[0];
		for ($j = 0;$j < $m;$j++) {
			$linkarray['link_' . $j] = create_tiny_url($links[$j], $contact_id, $campaign, $medium, $campaign_id);
			$text = str_replace($links[$j], '*|link_' . $j . '|* ', $text);
		}
	}
	$linkarray['text'] = $text;
	return $linkarray;
}
// CREATE A TINY URL FROM A URL
// Return the new tiny URL id
function create_tiny_url($old_url, $contact_id, $campaign, $medium = 1, $campaign_id) {
	$trailing_char = '';
	$check_last_char = substr($old_url, -1);
	if ($check_last_char == '<') {
		$trailing_char = '<';
	} elseif ($check_last_char == '"') {
		$trailing_char = '"';
	}
	$old_url = str_replace('<', '', $old_url);
	$old_url = str_replace('&nbsp;', '', $old_url);
	$old_url = mb_convert_encoding($old_url, 'HTML-ENTITIES', 'UTF-8');
	global $connectionmanager;
	$random = openssl_random_pseudo_bytes(5);
	mysqli_query($connectionmanager->connection, "INSERT INTO short_urls (short_url_destination, short_url_contact, short_url_medium, short_url_campaign, short_url_campaign_id, random_hash) VALUES


    (
        '" . $old_url . "',
        '" . $contact_id . "',
        '" . $medium . "',
        '" . $campaign . "',
        '" . $campaign_id . "',
		'" . $random . "'

    )

    ");
	$get_id = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT short_url_id FROM short_urls WHERE random_hash = '" . $random . "' AND short_url_destination = '" . $old_url . "'"));
	$sid = $get_id[0];
	$new_url = 'http://' . $_SERVER['SERVER_NAME'] . '/go/?i=' . $sid;
	return $new_url . $trailing_char . ' ';
}
function shorten_string($string, $limit = 100) {
	$string = parse_markdown($string);
	// Return early if the string is already shorter than the limit
	if (strlen($string) < $limit) {
		return $string;
	}
	$regex = "/(.{1,$limit})\b/";
	preg_match($regex, $string, $matches);
	return closetags($matches[1]) . '...';
}
// close opened html tags
function closetags($html) {
	#put all opened tags into an array
	preg_match_all("#<([a-z]+)( .*)?(?!/)>#iU", $html, $result);
	$openedtags = $result[1];
	#put all closed tags into an array
	preg_match_all("#</([a-z]+)>#iU", $html, $result);
	$closedtags = $result[1];
	$len_opened = count($openedtags);
	# all tags are closed
	if (count($closedtags) == $len_opened) {
		return $html;
	}
	$openedtags = array_reverse($openedtags);
	# close tags
	for ($i = 0;$i < $len_opened;$i++) {
		if (!in_array($openedtags[$i], $closedtags)) {
			$html.= "</" . $openedtags[$i] . ">";
		} else {
			unset($closedtags[array_search($openedtags[$i], $closedtags) ]);
		}
	}
	return $html;
}
// close opened html tags
function get_instance() {
	$var = '';
	if ($_SERVER['SERVER_NAME'] !== 'server1.cerebro.org.uk') {
		$var = $_SERVER['SERVER_NAME'];
	} else {
		$var = $_SERVER['SERVER_NAME'] . substr(getcwd(), strrpos(getcwd(), '/'));
	}
	return $var;
}
function check_date_against_today($date) {
	$opening_date = new DateTime($date);
	$current_date = new DateTime();
	$interval = date_diff($opening_date, $current_date);
	$interval = $interval->format('%a days');
	if (filter_var($interval, FILTER_SANITIZE_NUMBER_INT) > 5000) {
		$interval = 'years';
	}
	if ($opening_date > $current_date) {
		return '<img src="images/correct.png" alt="OK" title="OK"> <span>Expires in ' . $interval . '</span>';
	} else {
		return '<img src="images/issue_icon.png" alt="Wwarning" title="Warning"> <span class="expired">Expired ' . $interval . ' ago</span>';
	}
}
function check_arts_location_compliance($location_id, $location_pat_test_expires, $location_fire_safety_expires) {
	// we need to update the status of this location
	/*
	 * 0 Empty
	 * 1 Closed for maintenance
	 * 4 Moving in
	 * 5.1 Changing tenant
	 * 5.2 Moving out
	 * 10.0 Occupied - all OK
	 * 10.1 Occupied - Behind compliance
	 * 10.2 Occupied - Behind rent
	 * 10.3 Occupied - Behind rent and compliance
	*/
	$check_compliance = 0;
	// TEMP SQL get the contacts
	global $connectionmanager;
	$Contact_invalid = mysqli_query($connectionmanager->connection, "SELECT contact_id, contact_first, contact_last, contact_insurace_file, contact_insurance_expires FROM contacts WHERE contact_location = " . $location_id . " AND contact_insurance_expires <= CURDATE() GROUP BY contact_id");
	$invalid_contacts = mysqli_num_rows($Contact_invalid);
	if ($invalid_contacts > 0) {
		$check_compliance++;
	}
	while ($row = mysqli_fetch_array($Contact_invalid)) {
		if ($row['contact_insurace_file'] == '') {
			$check_compliance++;
		}
		$check_result = check_date_against_today($location_fire_safety_expires);
		if (strpos($check_result, 'ago') == true) {
			$check_compliance++;
		}
		if ($check_compliance > 0) {
			$change = mysqli_query($connectionmanager->connection, "
            UPDATE contacts
            SET contact_new_status = 'Current occupant - no insurance'
            WHERE contact_id = " . $row['contact_id'] . " AND contact_new_status Like 'Current%';");
			zend_update('contacts', $row['contact_id']);
		}
	}
	$check_result = check_date_against_today($location_pat_test_expires);
	if (strpos($check_result, 'ago') == true) {
		$check_compliance++;
	}
	if ($check_compliance > 0) {
		$change = mysqli_query($connectionmanager->connection, "
        UPDATE locations
        SET location_status = '10.1 Occupied - Behind compliance'
        WHERE location_id = $location_id AND location_stauts Like '%Occupied%';");
		return 'is behind compliance';
		zend_update('locations', $location_id);
	} else {
		$change = mysqli_query($connectionmanager->connection, "
        UPDATE locations
        SET location_status = '10.0 Occupied - all OK'
        WHERE location_id = $location_id AND location_stauts Like '%Occupied%';");
		return 'is up to date';
		zend_update('locations', $location_id);
	}
}
//! Zend update
function zend_update($table, $id) {
	require_once 'Zend/Search/Lucene.php';
	global $connectionmanager;
	$primary = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'"));
	$fields = mysqli_query($connectionmanager->connection, "SELECT * FROM $table WHERE " . $primary[4] . " = $id");
	$names = mysqli_query($connectionmanager->connection, "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $table . "'");
	$zendfields = array();
	$rows = array();
	echo mysqli_error($connectionmanager->connection);
	//! iterate over fields list to build Zend search fields
	while ($row = mysqli_fetch_array($fields)) {
		while ($name = mysqli_fetch_array($names)) {
			$name1 = $name['COLUMN_NAME'];
			$zendfields[$name1] = $row[$name1];
			//echo $name1." - ".$row[$name1];

		}
	}
	//! connect to the index
	$indexPath = ("search/$table");
	Zend_Search_Lucene_Analysis_Analyzer::setDefault(new Zend_Search_Lucene_Analysis_Analyzer_Common_TextNum_CaseInsensitive());
	if (file_exists($indexPath)) $index = Zend_Search_Lucene::open($indexPath);
	//! delete old documents from the index
	$hits = $index->find($primary[4] . ':' . $id);
	foreach ($hits as $hit) {
		//echo 'Deleting '.$hit->id.' ';
		$index->delete($hit->id);
	}
	//! add updated data to the index
	$doc = new Zend_Search_Lucene_Document();
	$doc->addField(Zend_Search_Lucene_Field::Text($primary[4], $id));
	foreach ($zendfields as $arrayindex => $value) {
		if ($arrayindex != $primary[4]) {
			$doc->addField(Zend_Search_Lucene_Field::Unstored($arrayindex, $value));
		}
	}
	if (file_exists($indexPath)) $index->addDocument($doc);
	return $index->numDocs();
}
//! reindex an entire table
function zend_reindex($table, $also_optimise = null) {
	global $connectionmanager;
	$primary = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SHOW KEYS FROM $table WHERE Key_name = 'PRIMARY'"));
	$all_data = mysqli_query($connectionmanager->connection, "SELECT " . $primary[4] . " FROM $table WHERE 1");
	while ($row = mysqli_fetch_array($all_data)) {
		$try = zend_update($table, $row[$primary[4]]);
		if (!$try > 0) {
			die('Indexing failed');
		}
	}
	if ($also_optimise !== null) {
		$indexPath = ("search/$table");
		Zend_Search_Lucene_Analysis_Analyzer::setDefault(new Zend_Search_Lucene_Analysis_Analyzer_Common_TextNum_CaseInsensitive());
		if (file_exists($indexPath)) $index = Zend_Search_Lucene::open($indexPath);
		$index->optimize();
	}
	return $try;
}
//! make a URL small with Bitly - used to give quick access to on printable pages
function make_bitly_url($url, $login = 'arvendui', $appkey = 'R_9c9e858e488c521b47763fc564d4a677', $version = '2.0.1') {
	// CACHE cache the url
	$shorturl = apc_fetch('bitly2-' . md5($url . $login . $appkey . $version)); //get any cached variable we already have
	if ($shorturl === false) {
		//! clean up the URL
		if (strpos($url, 'http://') === false && strpos($url, 'https://') === false) {
			$url = 'http://' . $url;
		}
		//! create the URL
		$bitly = 'https://api-ssl.bitly.com/shorten?version=' . $version . '&longUrl=' . urlencode($url) . '&login=' . $login . '&apiKey=' . $appkey . '&format=xml';
		//! get the url
		//!could also use cURL here
		$response = file_get_contents($bitly);
		$xml = simplexml_load_string($response);
		$shorturl = $xml->results->nodeKeyVal->shortUrl;
		apc_store('bitly2-' . md5($url . $login . $appkey . $version), (string)$shorturl); // save for ever

	}
	return $shorturl;
}
//! convert a number into a string with an ordinal suffix (e.g. turn 1 into 1st)
//! based on code from wikipedia - http://en.wikipedia.org/wiki/English_numerals#Ordinal_numbers
function number_to_ordinal_string($number) {
	if (is_numeric($number)) {
		$ends = array('th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th');
		if (($number % 100) >= 11 && ($number % 100) <= 13) {
			$number = $number . 'th';
		} else {
			$number = $number . $ends[$number % 10];
		}
	}
	return $number;
}
//! tidy html content
function get_tidy_html($html) {
	//! Specify configuration
	$config = array('indent' => true, 'output-xhtml' => true, 'wrap' => 200, 'show-body-only' => true);
	//! Tidy
	$tidy = new tidy;
	$tidy->parseString($html, $config, 'utf8');
	$tidy->cleanRepair();
	return $tidy;
}
/**
 * Get a web file (HTML, XHTML, XML, image, etc.) from a URL.  Return an
 * array containing the HTTP server response header fields and content.
 */
function get_web_page($url) {
	$options = array(CURLOPT_RETURNTRANSFER => true, //! return web page
	CURLOPT_HEADER => false, //! don't return headers
	CURLOPT_FOLLOWLOCATION => true, //! follow redirects
	CURLOPT_ENCODING => "", //! handle all encodings
	CURLOPT_USERAGENT => "University of Edinburgh Search Engine", //! who am i
	CURLOPT_AUTOREFERER => true, //! set referer on redirect
	CURLOPT_CONNECTTIMEOUT => 120, //! timeout on connect
	CURLOPT_TIMEOUT => 120, //! timeout on response
	CURLOPT_MAXREDIRS => 10, //! stop after 10 redirects
	);
	$ch = curl_init($url);
	curl_setopt_array($ch, $options);
	$content = curl_exec($ch);
	$err = curl_errno($ch);
	$errmsg = curl_error($ch);
	$header = curl_getinfo($ch);
	curl_close($ch);
	if ($content == '') {
		// retry!
		$ch = curl_init($url);
		curl_setopt_array($ch, $options);
		$content = curl_exec($ch);
		$err = curl_errno($ch);
		$errmsg = curl_error($ch);
		$header = curl_getinfo($ch);
		curl_close($ch);
	}
	$header['errno'] = $err;
	$header['errmsg'] = $errmsg;
	$header['content'] = $content;
	return $header;
}
//! get help files
function parse_help($page) {
	// get a copy of the webpage
	$request = get_web_page($page);
	return get_tidy_html($request['content']);
}
function random_password($len = 10) {
	/* Programmed by Christian Haensel
	 ** christian@chftp.com
	 ** http://www.chftp.com
	 **
	 ** Exclusively published on weberdev.com.
	 ** If you like my scripts, please let me know or link to me.
	 ** You may copy, redistribute, change and alter my scripts as
	 ** long as this information remains intact.
	 **
	 ** Modified by Josh Hartman on 12/30/2010.
	*/
	if (($len % 2) !== 0) { // Length paramenter must be a multiple of 2
		$len = 8;
	}
	$length = $len - 2; // Makes room for the two-digit number on the end
	$conso = array('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z', 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z');
	$vocal = array('a', 'e', 'i', 'o', 'u', '4', '3', '1', '0');
	$punct = array('!', '?', '#', '$', '£');
	$password = '';
	srand((double)microtime() * 1000000);
	$max = $length / 2;
	for ($i = 1;$i <= $max;$i++) {
		$password.= $conso[rand(0, 39) ];
		$password.= $vocal[rand(0, 8) ];
	}
	$password.= $punct[rand(0, 4) ];
	$password.= rand(10, 99);
	$password.= $punct[rand(0, 4) ];
	$newpass = $password;
	return $newpass;
}
//! parse text in the markdown format
function parse_markdown($text) { #
	require_once 'Parsedown.php';
	$result = correct_encoding($result);
	$result = Parsedown::instance()->parse($text);
	$result = str_replace('\r\n', '<br />', $result);
	return $result;
}
//! parse text to convert to the correct character encoding!
function correct_encoding($text) {
	$text = iconv(mb_detect_encoding($text, 'UTF-8, ISO-8859-1'), 'UTF-8', $text);
	$text = str_replace(array('£', '‘', '’'), array('&pound;', '\'', '\''), $text);
	return $text;
}
//legacy functions
function record_set($name, $query) {
	global $contacts;
	global $ {
		"query_$name"
	};
	global $ {
		"$name"
	};
	global $ {
		"row_$name"
	};
	global $ {
		"totalRows_$name"
	};
	$ {
		"query_$name"
	} = "$query";
	$ {
		"$name"
	} = mysql_query($ {
		"query_$name"
	}, $contacts) or die(mysql_error());
	$ {
		"row_$name"
	} = mysql_fetch_assoc($ {
		"$name"
	});
	$ {
		"totalRows_$name"
	} = mysql_num_rows($ {
		"$name"
	});
	$ {
		"row_$name"
	} = stripslashes_deep($ {
		"row_$name"
	});
}
function stripslashes_deep($value) {
	$value = is_array($value) ? array_map('stripslashes_deep', $value) : stripslashes($value);
	return $value;
}
function do_query($sql, $line, $cm = null) {
	if ($cm == null) {
		global $connectionmanager;
		$cm = $connectionmanager;
	}
	$client = "Joseph Farthing"; // Client Name
	$result = @mysqli_query($cm->connection, $sql);
	$total = @mysqli_num_rows($result);
	if (@mysqli_error() <> "") {
		// Error number
		$error_message = "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" style=\"border: 1px solid #bbbbbb;\" bgcolor=\"#ffffff\" width=\"80%\" align=\"center\"><tr><td align=\"right\" width=\"25%\"><font face=\"Verdana\" size=\"1\"><small><b>Error Number:</b></small></font></td><td width=\"75%\"><font face=\"Verdana\" size=\"1\"><small>" . @mysqli_errno($cm->connection) . "</small></font></td></tr>";
		// Error Description
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Error Description:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . @mysqli_error($cm->connection) . "</small></font></td></tr>";
		// Error Date / Time
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Error Time:</b></small></font></td><td><font face='Verdana' size='1'><small>" . D("H:m:s, jS F, Y") . "</small></font></td></tr>";
		// Client
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Client:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . $client . "</small></font></td></tr>";
		// Script
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Script:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . $_SERVER["SCRIPT_NAME"] . "</small></font></td></tr>";
		// Line Number
		$error_message.= "<tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Line:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . $line . "</small></font></td></tr></table>";
		// SQL
		$error_message.= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\" style=\"border: 1px solid #bbbbbb;\" bgcolor=\"#ffffff\" width=\"80%\" align=\"center\"><tr><td align=\"right\"><font face=\"Verdana\" size=\"1\"><small><b>Query:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>" . $sql . "</small></font></td></tr>";
		$error_message.= "<tr><td align=\"right\" valign=\"top\" width=\"25%\"><font face=\"Verdana\" size=\"1\"><small><b>Processes:</b></small></font></td><td><font face=\"Verdana\" size=\"1\"><small>";
		$result = @mysqli_list_processes();
		while ($row = @mysqli_fetch_assoc($result)) {
			$error_message.= $row["Id"] . " " . $row["Command"] . " " . $row["Time"] . "<br>";
		}
		@mysqli_free_result($result);
		$error_message.= "</small></font></td></tr></table>";
		$headers = "From: \"MySQL Debug\" <" . $email . ">\r\n";
		$headers.= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		echo fail_error($error_message, 'Database error');
		die();
	}
	return $result;
}
function apc_clean_cache($item = null) {
	if ($item) {
		$search = get_instance() . '-' . $item;
	} else {
		$search = get_instance();
	}
	$iterator = new APCIterator('user', '#^' . $search . '#', APC_ITER_KEY);
	foreach ($iterator as $entry_name) {
		apc_delete($entry_name['key']);
	}
	return true;
}
function arrayRecursiveDiff($aArray1, $aArray2) {
	$aReturn = array();
	foreach ($aArray1 as $mKey => $mValue) {
		if (array_key_exists($mKey, $aArray2)) {
			if (is_array($mValue)) {
				$aRecursiveDiff = arrayRecursiveDiff($mValue, $aArray2[$mKey]);
				if (count($aRecursiveDiff)) {
					$aReturn[$mKey] = $aRecursiveDiff;
				}
			} else {
				if ($mValue != $aArray2[$mKey]) {
					$aReturn[$mKey] = $mValue;
				}
			}
		} else {
			$aReturn[$mKey] = $mValue;
		}
	}
	return $aReturn;
}
function data_change_summary($before, $after, $username) {
	$difference = arrayRecursiveDiff($after, $before);
	if (count($difference) > 1) {
		$return = "$username made the following changes:

";
		foreach ($difference as $name => $changed_to) {
			$name = str_replace('_', ' ', $name);
			$name = ucwords($name);
			if ($changed_to != '') {
				$return.= '- **' . $name . '** changed to **"' . $changed_to . '"**
';
			} else {
				$return.= '- **' . $name . '** deleted
';
			}
		}
	} elseif (count($difference) > 0) {
		$return = $changed_to . "$username changed ";
		foreach ($difference as $name => $changed_to) {
			$name = str_replace('_', ' ', $name);
			$name = ucwords($name);
			if ($changed_to != '') {
				$return = "$username changed ";
				$return.= '**' . $name . '** to **"' . $changed_to . '"**
';
			} else {
				$return = "$username deleted ";
				$return.= '**' . $name . '**
';
			}
		}
	} else {
		$return = null;
	}
	return $return;
}
/**
 * Convert date/time format between `date()` and `strftime()` by mcaskill
 * https://gist.github.com/mcaskill/02636e5970be1bb22270
 * Timezone conversion is done for Unix. Windows users must exchange %z and %Z.
 *
 * Unsupported date formats : S, n, t, L, B, G, u, e, I, P, Z, c, r
 * Unsupported strftime formats : %U, %W, %C, %g, %r, %R, %T, %X, %c, %D, %F, %x
 *
 * @example Convert `%A, %B %e, %Y, %l:%M %P` to `l, F j, Y, g:i a`, and vice versa for "Saturday, March 10, 2001, 5:16 pm"
 * @link http://php.net/manual/en/function.strftime.php#96424
 *
 * @param string $format The format to parse.
 * @param string $syntax The format's syntax. Either 'strf' for `strtime()` or 'date' for `date()`.
 * @return bool|string Returns a string formatted according $syntax using the given $format or `false`.
 */
function date_format_to($format, $syntax) {
	// http://php.net/manual/en/function.strftime.php
	$strf_syntax = [
	// Day - no strf eq : S (created one called %O)
	'%O', '%d', '%a', '%e', '%A', '%u', '%w', '%j',
	// Week - no date eq : %U, %W
	'%V',
	// Month - no strf eq : n, t
	'%B', '%m', '%b', '%-m',
	// Year - no strf eq : L; no date eq : %C, %g
	'%G', '%Y', '%y',
	// Time - no strf eq : B, G, u; no date eq : %r, %R, %T, %X
	'%P', '%p', '%l', '%I', '%H', '%M', '%S',
	// Timezone - no strf eq : e, I, P, Z
	'%z', '%Z',
	// Full Date / Time - no strf eq : c, r; no date eq : %c, %D, %F, %x
	'%s'];
	// http://php.net/manual/en/function.date.php
	$date_syntax = ['S', 'd', 'D', 'j', 'l', 'N', 'w', 'z', 'W', 'F', 'm', 'M', 'n', 'o', 'Y', 'y', 'a', 'A', 'g', 'h', 'H', 'i', 's', 'O', 'T', 'U'];
	switch ($syntax) {
		case 'date':
			$from = $strf_syntax;
			$to = $date_syntax;
		break;
		case 'strf':
			$from = $date_syntax;
			$to = $strf_syntax;
		break;
		default:
			return false;
	}
	$pattern = array_map(function ($s) {
		return '/(?<!\\\\|\%)' . $s . '/';
	}, $from);
	return preg_replace($pattern, $to, $format);
}
