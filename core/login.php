<?php /*
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Generates login page
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */

/**
 * @todo needs updating to use new Users class, but need to decide on implimentation first
 */
if (isset($_COOKIE["usertype"]) && $_COOKIE["usertype"] == 6 && !(isset($_POST['email']))) {
	header(sprintf('Location: %s', 'http://www.sustainability.ed.ac.uk/awards/login/'));
	die;
}

// fix www -> non www
if (substr($_SERVER['SERVER_NAME'], 0, 4) === 'www.') {
	$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL.= substr($_SERVER['SERVER_NAME'], 4) . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
	} else {
		$pageURL.= substr($_SERVER['SERVER_NAME'], 4) . $_SERVER["REQUEST_URI"];
	}
	str_replace('login.php', 'index.php', $pageURL);
	header('Location: ' . $pageURL);
}
include 'microindex.php';
$pagetitle = 'Login';
$error = '';
if (isset($_POST['email']) && isset($_POST['password'])) {
	$data = $dbmanager->Query(array('user_bad_login_count', 'user_first_bad_login'), array(array('user_email' => $_POST['email'])), null, 0, 0);
	foreach ($data['result'] as $result) {
		$badcount = $result['user_bad_login_count'];
		$badid = $result['id'];
		$badcat = $result['category'];
		if ($result['user_bad_login_count'] > 3 && (time() - $result['user_first_bad_login']) < 600) die('<!DOCTYPE html><html lang="en"><head><!-- Basic Page Needs==================================================--><meta charset="utf-8"><title>Your account has been locked</title><meta name="description" content=""><meta name="author" content=""><!-- Mobile Specific Metas==================================================--><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><!-- CSS==================================================--><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/base.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/skeleton.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/layout.css"><link rel="stylesheet" href="http://cerebro.org.uk/errors/stylesheets/cerebro.css"><!--[if lt IE 9]><script src="http://cerebro.org.uk/errors/http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]--><!-- Favicons==================================================--><link rel="shortcut icon" href="http://cerebro.org.uk/errors/images/favicon.ico"></head><body><!-- Primary Page Layout==================================================--><div class="header"><div class="container"><div class="sixteen columns" style="display: block;height: 200px !important;"><p><br/></p></div><div class="sixteen columns"><img src="http://cerebro.org.uk/errors/images/header.png" alt="Cerebro"/></div></div></div><div class="container"><div class="sixteen columns content"><h2>Your account has been locked</h2><p>You tried to log in too many times with a bad password. To protect this system, your account has been locked. Please contact your system administrator or consultant.</p></div></div><!--- Unfortunately, Microsoft has added a clever new- "feature" to Internet Explorer. If the text of- an error\'s message is "too small", specifically- less than 512 bytes, Internet Explorer returns- its own error message. You can turn that off,- but it\'s pretty tricky to find switch called- "smart error messages". That means, of course,- that short error messages are censored by default.- IIS always returns error messages that are long- enough to make Internet Explorer happy. The- workaround is pretty simple: pad the error- message with a big comment like this to push it- over the five hundred and twelve bytes minimum.- Of course, that\'s exactly what you\'re reading- right now.--><!-- End Document==================================================--></body></html>');
	}
	$data = $dbmanager->Query(array('contact_email', 'user_password', 'user_level'), array(array('user_email' => $_POST['email']), array('user_password' => genpwd($_POST['password']))), null, 0, 0);
	$fail = false;
	if (count($data['result']) == 1) {
		foreach ($data['result'] as $index => $val) {
			$item = $index;
			$category = $val['category'];
		}
		$_SESSION['uid'] = $item;
		$_SESSION['logintype'] = 'normal';
		if (isset($_SESSION['login_temp'])) $redirect = $_SESSION['login_temp'];
		else $redirect = 'index.php';
		header(sprintf('Location: %s', $redirect));
		die;
		$_POST = array();
		$_POST['table'] = 'items';
		$_POST['category'] = $category;
		$_POST['id'] = $item;
		$_POST['user_bad_login_count'] = 0;
		if ($item != null) {
			$post = new Post;
			$post->CheckPost();
		} else {
			$fail = true;
		}
	}
	if (count($data['result']) < 1 || $fail === true) {
		$_POST = array();
		$_POST['table'] = 'items';
		$_POST['category'] = $badcat;
		$_POST['id'] = $badid;
		$_POST['user_bad_login_count'] = $badcount + 1;
		if ($badcount == 0) $_POST['user_first_bad_login'] = time();
		if ($badid != null) {
			$post = new Post;
			$post->CheckPost();
		}
		$logline = time() . ' ' . $_POST['email'] . ' ' . $_POST['password'];
		foreach ($_SERVER as $key => $item) {
			$logline.= " $key: $item,";
		}
		file_put_contents('invalid_password_attempts.php', $logline, FILE_APPEND | LOCK_EX);
		$error = '<strong>Invalid email or password!<strong>';
	}
}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>Login | Cerebro</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
    <link rel="stylesheet" href="stylesheets/base.css">
    <link rel="stylesheet" href="stylesheets/skeleton.css">
    <link rel="stylesheet" href="stylesheets/layout.css">
    <link rel="stylesheet" href="stylesheets/cerebro.css">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://www.cerebro.org.uk/core/includes/src/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/magnific_popup.js"></script>

    <script type="text/javascript">	//<![CDATA[
    $(document).ready(function () {

        $('.open-popup-link').magnificPopup({
          type:'inline',
          showCloseBtn: false,
          midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        });


    });

    //]]>
    </script>


</head>
<body>
    <!-- Primary Page Layout
    ================================================== -->

    <!-- Delete everything in this .container and get started on your own site! -->
    <div class="header">
        <div class="container">

            <div class="sixteen columns" style="display: block; height: 200px !important;">
                <!-- special login page header -->
                <p><br /></p>
            </div>
            <div class="sixteen columns">
                <img src="images/header.png" alt="Cerebro"/>

            </div>


        </div><!-- container -->
    </div><!-- header -->
    <div class="container">
        <div class="sixteen columns content">




            <div class="row form_display">

                <form method="POST" action="">
                    <div class="form_area">
                    <?php echo $error; ?>
                        <div class="row primary_item">
                            <div class="six columns alpha">
                                <label for="email" class="required"><img src="images/form_email_icon.png" alt="Type the email in this box" /> <span class="form_label_text" style="display:none">Email address</span> </label>
                                <input type="text" name="email" id="email" class="has_image required" tabindex="1" autocomplete="off" placeholder="Email address"/>
                            </div>


                        </div>



                        <div class="row primary_item">

                            <div class="six columns alpha">
                                <label for="password" class="required"><span class="form_label_text" style="display:none;">Password</span> </label>
                                <input type="password" name="password" id="password" tabindex="2" autocomplete="off" placeholder="Password" />
                            </div>
                        </div>

                        <div class="row primary_item">

                            <div class="four columns alpha">
                                <a href="forgot_password.php" class="button borderless" style="padding:15px;">Forgot your password?</a>
                            </div>

                            <div class="two columns omega action_bar">
                                <input type="submit" class="button" style="height: 40px;" tabindex="3" value="Login">
                            </div>

                        </div>
                    </div> <!-- form_area -->
                </form>

            </div>
        </div>

    </div><!-- container --></body>
</html>
