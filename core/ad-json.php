<?php
// testing time start
global $time_start;
$time_start = microtime(true);
header("Access-Control-Allow-Origin: *");
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	exit(0);
}
// version
$version = '2.1.0';
include 'microindex.php';
$legacy = array();
$legacy['contact-details'] = 1;
$legacy['issue-details'] = 4;
$legacy['project-details'] = 3;
$legacy['location-details'] = 2;
$legacy['event-details'] = 5;
$legacy['contact-add-edit'] = 1;
$legacy['issue-add-edit'] = 4;
$legacy['project-add-edit'] = 3;
$legacy['location-add-edit'] = 2;
$legacy['event-add-edit'] = 5;
$legacy['user-add-edit'] = 6;
$legacy['user-details'] = 6;
if (isset($_GET['id'])) {
	if ($_GET['id'] < 10000 && array_key_exists($_GET['page'], $legacy)) $item_id = ($legacy[$_GET['page']] * 10000) + $_GET['id'];
	else $item_id = $_GET['id'];
}
include './modules/activedirectory.php';
$ad = new ActiveDirectory();
//echo $ad->GetActiveDirectory();
$data = $dbmanager->Query(array('id'), array(array('contact_email' => $_GET['mail'])), null, 0, 0);
foreach ($data['result'] as $result) $check_contacts = $result;
if ($data['result_count'] > 0 && !(isset($_GET['ping']))) {
	// duplicate
	$return = array('status' => 'existing', 'id' => $check_contacts['id']);
	echo json_encode($return);
} else {
	$return = $ad->GetSecureJSON($_GET['mail']);
	$check = json_decode($return);
	$data = $dbmanager->Query(array('id'), array(array('contact_email' => $check->email)), null, 0, 0);
	foreach ($data['result'] as $result) $check_contacts = $result;
	if ($data['result_count'] > 0 && !(isset($_GET['ping']))) {
		// duplicate
		$return = array('status' => 'existing', 'id' => $check_contacts['id']);
		echo json_encode($return);
	} else {
		echo $return;
	}
}
