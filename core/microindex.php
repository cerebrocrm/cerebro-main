<?php
require_once 'classes/Users.class.php';
Users::SetupSession();
include 'functions.php';
include 'modules/post.php';
require 'vendor/autoload.php';
require 'super-magic-email-sender.php';
class DBconnect {
	public $connection;
	public function connect($config) {
		set_time_limit(20000);
		$hostname_contacts = $config['back_host'];
		$database_contacts = $config['back_database']; //The name of the database
		$username_contacts = $config['back_user']; //The username for the database
		$password_contacts = $config['back_password']; // The password for the database
		$this->connection = mysqli_connect("$hostname_contacts", "$username_contacts", "$password_contacts", "$database_contacts");
		if (mysqli_connect_errno($this->connection)) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
}
class TableConnect {
	public $connection;
	public function connect($config) {
		set_time_limit(20000);
		$hostname_contacts = $config['front_host'];
		$database_contacts = $config['front_database']; //The name of the database
		$username_contacts = $config['front_user']; //The username for the database
		$password_contacts = $config['front_password']; // The password for the database
		$this->connection = mysqli_connect($hostname_contacts, $username_contacts, $password_contacts, $database_contacts);
		if (mysqli_connect_errno($this->connection)) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}
}
class DBManagerNew {
	var $config;
	function SetConfig($config) {
		$this->config = $config;
	}
	//Get items associated via shared links
	function Links($id, $thing1, $thing2, $type = 0) {
		//0 = item,cat,cat; 1 = item,cat,primary_cat; 2 = item,cat,item; 3 = item,primary_cat,item
		global $connectionmanager;
		$coords = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links WHERE (item1 = " . $id . " AND cat2 = $thing1) OR (item2=" . $id . " AND cat1 = $thing1)");
		$query1 = "";
		$query2 = "";
		$i = 0;
		while ($row = mysqli_fetch_assoc($coords)) {
			$or = " OR ";
			if ($i == 0) $or = "";
			if ($row['cat1'] == $thing1) $item = $row['item1'];
			else $item = $row['item2'];
			$query1.= $or . "item1 = " . $item;
			$query2.= $or . "item2 = " . $item;
			$i++;
		}
		if ($type == 3) {
			$meta = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . $thing2));
			$meta = $meta['category'];
		}
		if ($type == 0) $query = "SELECT * FROM item_links WHERE ((" . $query1 . ") AND cat2 = $thing2) OR ((" . $query2 . ") AND cat1 = $thing2)";
		elseif ($type == 1) $query = "SELECT * FROM item_links WHERE ((" . $query1 . ") AND primary_meta = $thing2)";
		elseif ($type == 2) $query = "SELECT * FROM item_links WHERE ((" . $query1 . ") AND item2 = $thing2) OR ((" . $query2 . ") AND item1 = $thing2)";
		else $query = "SELECT * FROM item_links WHERE ((" . $query1 . ") AND item2 = $thing2 AND primary_meta = $meta)";
		$results = array();
		$result = mysqli_query($connectionmanager->connection, $query);
		while ($row = mysqli_fetch_assoc($result)) {
			if ($type == 0) {
				if ($row['cat2'] == $thing2) {
					$index = $row['item2'];
					$item = $row['item1'];
				} else {
					$index = $row['item1'];
					$item = $row['item2'];
				}
			} elseif ($type == 2) {
				if ($row['item2'] == $thing2) {
					$index = $row['item2'];
					$item = $row['item1'];
				} else {
					$index = $row['item1'];
					$item = $row['item2'];
				}
			} else {
				$index = $row['item2'];
				$item = $row['item1'];
			}
			$results[$index][] = $item;
		}
		if ($type == 2 || $type == 3) $results = array_unique($results[$thing2]);
		else {
			foreach ($results as $index => $val) $results[$index] = array_unique($val);
		}
		return $results;
	}
	//Get meta information for fields
	function Fields($vars, $category) {
		global $connectionmanager;
		$meta = array();
		$results = array();
		$fieldquery = 'SELECT * FROM meta WHERE (';
		foreach ($vars as $var) {
			$fieldquery.= 'name LIKE "' . $var . '" OR ';
		}
		$fieldquery.= 'null) AND cat_id=' . $category;
		$fieldquery = mysqli_query($connectionmanager->connection, $fieldquery);
		while ($row = mysqli_fetch_assoc($fieldquery)) {
			$meta[$row['name']] = $row;
		}
		foreach ($meta as $index => $val) {
			$results[$index]['css'] = $val['css'];
			$results[$index]['friendly_name'] = $val['friendly_name'];
			$results[$index]['type'] = $val['type'];
			$results[$index]['linkid'] = $val['linkid'];
			$results[$index]['data_format'] = $val['data_format'];
			$results[$index]['icon'] = $val['icon'];
			if ($val['options'] != '') {
				$options = array();
				$optionlist = explode(',', $val['options']);
				foreach ($optionlist as $subvalue) {
					$optiondetails = explode(':', $subvalue);
					$options[$optiondetails[0]] = $optiondetails[1];
				}
				$results[$index]['options'] = $options;
			}
			if ($val['linkid'] != 0) {
				$options = array();
				$query = mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE category=" . $val['linkid']);
				while ($row = mysqli_fetch_assoc($query)) {
					$options[$row['id']] = $row['name'];
				}
				$results[$index]['options'] = $options;
			}
		}
		//$results['name'] = array('friendly_name'=>'Name','type'=>1);
		$results['name']['css'] = 'three columns';
		if (!(isset($results['name']['friendly_name']))) $results['name']['friendly_name'] = 'Name';
		$results['name']['type'] = 1;
		$results['name']['linkid'] = 0;
		$results['name']['data_format'] = 4;
		return $results;
	}
	//Get meta info for every field associated with a category (TODO merge with 'Fields' - no need to be 2 separate functions )
	function AllFields($cat) {
		global $connectionmanager;
		$meta = array();
		$results = array();
		$fieldquery = 'SELECT * FROM meta WHERE cat_id=' . $cat;
		$fieldquery = mysqli_query($connectionmanager->connection, $fieldquery);
		while ($row = mysqli_fetch_assoc($fieldquery)) {
			$meta[$row['name']] = $row;
		}
		$results['name'] = array('css' => 'three columns', 'linkid' => 0, 'friendly_name' => 'Name', 'type' => 1, 'data_format' => 0);
		foreach ($meta as $index => $val) {
			$results[$index]['css'] = $val['css'];
			$results[$index]['linkid'] = $val['linkid'];
			$results[$index]['friendly_name'] = $val['friendly_name'];
			$results[$index]['type'] = $val['type'];
			$results[$index]['data_format'] = $val['data_format'];
			if ($val['options'] != '') {
				$options = array();
				$optionlist = explode(',', $val['options']);
				foreach ($optionlist as $subvalue) {
					$optiondetails = explode(':', $subvalue);
					$options[$optiondetails[0]] = $optiondetails[1];
				}
				$results[$index]['options'] = $options;
			}
			if ($val['linkid'] != 0) {
				$options = array();
				$query = mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE category=" . $val['linkid']);
				while ($row = mysqli_fetch_assoc($query)) {
					$options[$row['id']] = $row['name'];
				}
				$results[$index]['options'] = $options;
			}
		}
		return $results;
	}
	//Query the dataset
	//$link: module calling database (used to check for GET variables affecting searching/sorting)
	//$match: whether to return the matched value, or the label value (eg $status = (1:yes, 2:no), return 1 vs return 'yes')
	function Query($vars, $extraconditions, $order, $paginate, $link, $match = 0) {
		$config = array('adapteroptions' => array('host' => $this->config['solr_host'], 'port' => $this->config['solr_port'], 'path' => $this->config['solr_path'], 'core' => $this->config['solr_core']));
		$client = new Solarium_Client($config);
		global $connectionmanager;
		$meta = array();
		$results = array();
		$results['result_count'] = 0;
		$fieldquery = 'SELECT * FROM meta WHERE (';
		if (!is_array($vars)) {
			$varlist = mysqli_query($connectionmanager->connection, "SELECT * FROM meta WHERE cat_id=" . $vars);
			$vars = array();
			while ($row = mysqli_fetch_assoc($varlist)) $vars[] = $row['name'];
		}
		foreach ($vars as $var) {
			$fieldquery.= 'name LIKE "' . $var . '" OR ';
		}
		$items = array();
		//Extra variables are called via the method argument or using GET variables for more advanced queries (see advanced search page for example).
		//TODO remove use of GET variables - instead use only method arguments to avoid messiness!
		$extraconditionslist = array_chunk($extraconditions, 200);
		if ($extraconditionslist[0] == null) $extraconditions['noconds'] = 1;
		$ex = 0;
		foreach ($extraconditionslist as $extracondsindex => $extraconditions) {
			if ($extraconditions != null || isset($_GET['field_' . $link . '_0']) || isset($_GET['field_all_0'])) {
				foreach ($extraconditions as $extracon) {
					foreach ($extracon as $index => $val) {
						if (!(array_key_exists($index, $vars)) && $ex == 0) $fieldquery.= 'name LIKE "' . $index . '" OR ';
					}
				}
				$i = 0;
				if (isset($_GET['field_' . $link . '_' . $i])) $trigger = $link;
				else $trigger = 'all';
				while (isset($_GET['field_' . $trigger . '_' . $i])) {
					if ($ex == 0) $fieldquery.= 'name LIKE "' . mysqli_real_escape_string($connectionmanager->connection, $_GET['field_' . $trigger . '_' . $i]) . '" OR ';
					$i++;
				}
			}
			if ($ex == 0) {
				$fieldquery.= 'null)';
				$fieldquery = mysqli_query($connectionmanager->connection, $fieldquery);
				while ($row = mysqli_fetch_assoc($fieldquery)) {
					if (array_key_exists($row['name'], $meta)) {
						$meta[$row['name'] . '_1'] = $row;
						$vars[] = $row['name'] . '_1';
					} else $meta[$row['name']] = $row;
				}
				foreach ($meta as $index => $val) {
					$results[$index]['css'] = $val['css'];
					$results[$index]['friendly_name'] = $val['friendly_name'];
					if ($val['options'] != '') {
						$options = array();
						$optionlist = explode(',', $val['options']);
						foreach ($optionlist as $subvalue) {
							$optiondetails = explode(':', $subvalue);
							$options[$optiondetails[0]] = $optiondetails[1];
						}
						$meta[$index]['options'] = $options;
						$results[$index]['options'] = $options;
					}
				}
			}
			//Generate the query string to pass to SOLR
			$solr = '(id:0';
			if ($extraconditions != null || isset($_GET['field_' . $link . '_0']) || isset($_GET['field_all_0'])) {
				$s = 0;
				$exc = 0;
				$extrafields = array();
				foreach ($extraconditions as $extracon) {
					foreach ($extracon as $index => $val) {
						if ($exc == 0 || array_key_exists($index, $extrafields)) $exval = ' OR ';
						else $exval = ' ) AND (';
						$extrafields["$index"] = 1;
						if ($index != 'id' && $index != 'name' && $index != 'category') {
							if ($meta[$index]['type'] == 1) $field = $index . '_char';
							elseif ($meta[$index]['type'] == 2) $field = $index . '_int';
							else $field = $index . '_tiny';
							$solr.= $exval . $field . ': "' . $val . '"';
						} else $solr.= $exval . $index . ': "' . $val . '"';
						$exc++;
					}
				}
				$solr.= ')';
				if (isset($_GET['field_' . $link . '_0'])) $searchtype = $link;
				else $searchtype = 'all';
				if (isset($_GET['field_' . $searchtype . '_0'])) {
					$inv = '';
					if ($_GET['field_' . $searchtype . '_' . $s] != 'id' && $_GET['field_' . $searchtype . '_' . $s] != 'name' && $_GET['field_' . $searchtype . '_' . $s] != 'category') {
						if ($meta[$_GET['field_' . $searchtype . '_' . $s]]['type'] == 1) $searchfield = $_GET['field_' . $searchtype . '_' . $s] . '_char';
						elseif ($meta[$_GET['field_' . $searchtype . '_' . $s]]['type'] == 2) $searchfield = $_GET['field_' . $searchtype . '_' . $s] . '_int';
						else $searchfield = $_GET['field_' . $searchtype . '_' . $s] . '_tiny';
					} else $searchfield = $_GET['field_' . $searchtype . '_' . $s];
					$_GET['and_' . $searchtype . '_' . $s] = '';
					if ($_GET['op_' . $searchtype . '_' . $s] == 'not_equal' || $_GET['op_' . $searchtype . '_' . $s] == 'not_contain') $solr.= ' NOT (';
					elseif ($_GET['op_' . $searchtype . '_' . $s] == 'equal' && $_GET['parama_' . $searchtype . '_' . $s] == '0') {
						$solr.= ' AND -(' . $searchfield . ':[* TO *] OR ';
						$inv = '-';
					} else $solr.= ' AND (';
					if ($_GET['op_' . $searchtype . '_' . $s] == 'equal') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': "' . $_GET['parama_' . $searchtype . '_' . $s] . '"';
					elseif ($_GET['op_' . $searchtype . '_' . $s] == 'similar') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': ' . $_GET['parama_' . $searchtype . '_' . $s] . '~';
					elseif ($_GET['op_' . $searchtype . '_' . $s] == 'contain') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': *' . $_GET['parama_' . $searchtype . '_' . $s] . '*';
					elseif ($_GET['op_' . $searchtype . '_' . $s] == 'not_equal') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': "' . $_GET['parama_' . $searchtype . '_' . $s] . '"';
					elseif ($_GET['op_' . $searchtype . '_' . $s] == 'not_contain') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': *' . $_GET['parama_' . $searchtype . '_' . $s] . '*';
					elseif ($_GET['op_' . $searchtype . '_' . $s] == 'between') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': ["' . $_GET['parama_' . $searchtype . '_' . $s] . '" TO "' . $_GET['paramb_' . $searchtype . '_' . $s] . '"]';
					$s++;
					while (isset($_GET['field_' . $searchtype . '_' . $s])) {
						if ($_GET['op_' . $searchtype . '_' . $s] == 'not_equal' || $_GET['op_' . $searchtype . '_' . $s] == 'not_contain') $_GET['and_' . $searchtype . '_' . $s] = 'NOT';
						if ($_GET['field_' . $searchtype . '_' . $s] != 'id' && $_GET['field_' . $searchtype . '_' . $s] != 'name' && $_GET['field_' . $searchtype . '_' . $s] != 'category') {
							if ($meta[$_GET['field_' . $searchtype . '_' . $s]]['type'] == 1) $searchfield = $_GET['field_' . $searchtype . '_' . $s] . '_char';
							elseif ($meta[$_GET['field_' . $searchtype . '_' . $s]]['type'] == 2) $searchfield = $_GET['field_' . $searchtype . '_' . $s] . '_int';
							else $searchfield = $_GET['field_' . $searchtype . '_' . $s] . '_tiny';
						}
						if ($_GET['op_' . $searchtype . '_' . $s] == 'equal' && $_GET['parama_' . $searchtype . '_' . $s] == '0') {
							$_GET['and_' . $searchtype . '_' . $s] = ' ) ' . $_GET['and_' . $searchtype . '_' . $s] . ' -( ' . $searchfield . ':[* TO *] OR ';
							$inv = '-';
						} elseif ($_GET['and_' . $searchtype . '_' . $s] == 'AND' || ($_GET['and_' . $searchtype . '_' . $s] == 'NOT')) {
							$_GET['and_' . $searchtype . '_' . $s] = ' ) ' . $_GET['and_' . $searchtype . '_' . $s] . ' ( ';
							$inv = '';
						}
						if ($_GET['op_' . $searchtype . '_' . $s] == 'equal') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': "' . $_GET['parama_' . $searchtype . '_' . $s] . '"';
						elseif ($_GET['op_' . $searchtype . '_' . $s] == 'similar') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': ' . $_GET['parama_' . $searchtype . '_' . $s] . '~';
						elseif ($_GET['op_' . $searchtype . '_' . $s] == 'contain') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': *' . $_GET['parama_' . $searchtype . '_' . $s] . '*';
						elseif ($_GET['op_' . $searchtype . '_' . $s] == 'not_equal') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': "' . $_GET['parama_' . $searchtype . '_' . $s] . '"';
						elseif ($_GET['op_' . $searchtype . '_' . $s] == 'not_contain') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': *' . $_GET['parama_' . $searchtype . '_' . $s] . '*';
						elseif ($_GET['op_' . $searchtype . '_' . $s] == 'between') $solr.= ' ' . $_GET['and_' . $searchtype . '_' . $s] . ' ' . $inv . $searchfield . ': ["' . $_GET['parama_' . $searchtype . '_' . $s] . '" TO "' . $_GET['paramb_' . $searchtype . '_' . $s] . '"]';
						$s++;
					}
					$solr.= ')';
				}
			}
			$solrquery = $client->createSelect();
			$solrquery->setQuery($solr);
			$solrquery->setStart(0)->setRows(1000000);
			if ($paginate != 0) {
				if (isset($_GET[$link . '_place'])) {
					$place = $_GET[$link . '_place'] * $paginate;
					$solrquery->setStart($place)->setRows($paginate);
				} else {
					$solrquery->setStart(0)->setRows($paginate);
				}
			}
			//Add sorting based on either GET vars (as used by lists) or as method argument
			if (isset($_GET[$link . '_asc'])) {
				if ($_GET[$link . '_asc'] == 'id' || $_GET[$link . '_asc'] == 'name' || $_GET[$link . '_asc'] == 'category') $appended = $_GET[$link . '_asc'];
				elseif ($meta[$_GET[$link . '_asc']]['type'] == 1) $appended = $_GET[$link . '_asc'] . '_char';
				elseif ($meta[$_GET[$link . '_asc']]['type'] == 2) $appended = $_GET[$link . '_asc'] . '_int';
				else $appended = $_GET[$link . '_asc'] . '_tiny';
				$solrquery->addSort($appended, $solrquery::SORT_ASC);
			} elseif (isset($_GET[$link . '_desc'])) {
				if ($_GET[$link . '_desc'] == 'id' || $_GET[$link . '_desc'] == 'name' || $_GET[$link . '_desc'] == 'category') $appended = $_GET[$link . '_desc'];
				elseif ($meta[$_GET[$link . '_desc']]['type'] == 1) $appended = $_GET[$link . '_desc'] . '_char';
				elseif ($meta[$_GET[$link . '_desc']]['type'] == 2) $appended = $_GET[$link . '_desc'] . '_int';
				else $appended = $_GET[$link . '_desc'] . '_tiny';
				$solrquery->addSort($appended, $solrquery::SORT_DESC);
			} elseif ($order != null && !(array_key_exists(0, $order))) {
				foreach ($order as $index => $value) {
					if ($index == 'id' || $index == 'name' || $index == 'category') $appended = $index;
					elseif ($meta[$index]['type'] == 1) $appended = $index . '_char';
					elseif ($meta[$index]['type'] == 2) $appended = $index . '_int';
					else $appended = $index . '_tiny';
					if (strtolower($value) == 'asc') $ordertype = $solrquery::SORT_ASC;
					else $ordertype = $solrquery::SORT_DESC;
					$solrquery->addSort($appended, $ordertype);
				}
			}
			$resultset = $client->execute($solrquery);
			$results['result_count'] = $results['result_count'] + $resultset->getNumFound();
			foreach ($resultset as $document) {
				$items[$document['id']] = array('name' => $document['name'], 'category' => $document['category'], 'id' => $document['id']);
			}
			$ex++;
		}
		$extraquery = ' (item_id=0';
		foreach ($items as $index => $val) {
			$extraquery.= ' OR item_id=' . $index;
		}
		$extraquery.= ')';
		//Once matching items are found pull requested extra variables from database
		foreach ($vars as $var) {
			if ($var != 'id' && $var != 'category' && $var != 'name') {
				if ($meta[$var]['linkid'] == 0) {
					if ($meta[$var]['type'] == 1) $extra = mysqli_query($connectionmanager->connection, "SELECT * FROM extra_char WHERE " . $extraquery . " AND meta_id=" . $meta[$var]['id']);
					elseif ($meta[$var]['type'] == 2) $extra = mysqli_query($connectionmanager->connection, "SELECT * FROM extra_int WHERE " . $extraquery . " AND meta_id=" . $meta[$var]['id']);
					else $extra = mysqli_query($connectionmanager->connection, "SELECT * FROM extra_tiny WHERE " . $extraquery . " AND meta_id=" . $meta[$var]['id']);
					if (mysqli_num_rows($extra) > 0) {
						while ($row = mysqli_fetch_assoc($extra)) {
							while ((stripos($var, '_1')) !== false) $var = substr($var, 0, -2);
							if (is_array($meta[$var]['options']) && $match == 0) {
								$items[$row['item_id']][$var] = $meta[$var]['options'][$row['val']];
							} else {
								$items[$row['item_id']][$var] = $row['val'];
							}
						}
					}
				} else {
					$linkquery = str_replace('item_id', 'item1', $extraquery);
					$extras = mysqli_query($connectionmanager->connection, "SELECT * FROM item_links LEFT JOIN items ON item_links.item2=items.id WHERE " . $linkquery . " AND link_level=" . $meta[$var]['link_level'] . " AND primary_meta=" . $meta[$var]['linkid']);
					/*$links = 'SELECT * FROM items WHERE (id= 0';
					while ($row = mysqli_fetch_assoc($extra)){

						$links .= " OR id=".$row['val'];
						$items[$row['item_id']][$var] = $row['val'];

					}
					$links .= ')';
					$links = mysqli_query($connectionmanager ->connection ,$links);*/
					$extra = array();
					while ($row = mysqli_fetch_assoc($extras)) {
						if ($match == 0) $extra[$row['item1']][$var] = $row['name'];
						else $extra[$row['item1']][$var] = $row['id'];
					}
					if ($match != 0) {
						$extralist = mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE category=" . $meta[$var]['linkid']);
						while ($row = mysqli_fetch_assoc($extralist)) {
							$results[$var]['options'][$row['id']] = $row['name'];
						}
					}
					foreach ($items as $index => $val) {
						$items[$index][$var] = $extra[$val['id']][$var];
					}
				}
			}
		}
		$results['result'] = $items;
		return $results;
	}
}
class Module {
	public $id;
	public $clinks = array();
	public $mlinks = array();
	public $permissions = array();
	public function __construct($id) {
		$this->id = $id;
	}
	public function cLink($links) {
		$this->clinks = $links;
	}
	public function mLink($links) {
		$this->mlinks = $links;
	}
	public function Links() {
		return $this->links;
	}
	public function Functions() {
		return $this->functions;
	}
	public function SetAccess($levels) {
		$this->permissions = $levels;
	}
	public function Instance() {
		return $this->id;
	}
	//Pass linked data to module
	public function link($link) {
		if (array_key_exists($link, $this->clinks)) {
			return $this->clinks[$link];
		} elseif (array_key_exists($link, $this->mlinks)) {
			foreach ($this->mlinks[$link] as $key => $value) {
				global $ {
					'module_' . $value[0]
				};
				$modinst = $ {
					'module_' . $value[0]
				};
				$modfunction = $value[1];
				$data[$key] = $modinst->$modfunction();
			}
			return $data;
		} else return array('');
	}
}
require_once ('config.php');
$dbmanager = new DBmanagerNew();
$dbmanager->SetConfig($config);
$connectionmanager = new TableConnect();
$modulemanager = new DBconnect();
$modulemanager->connect($config);
$connectionmanager->connect($config);
//Set commonly used data as globals
$users = array();
$userlist = mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE category = 99");
while ($row = mysqli_fetch_assoc($userlist)) {
	$users[$row['id']] = $row['name'];
}
$catlist = mysqli_query($connectionmanager->connection, "SELECT * FROM categories");
$categories = array();
while ($row = mysqli_fetch_assoc($catlist)) {
	$categories[$row['cat_id']] = $row['cat_name'];
	$categories[$row['cat_name']] = $row['cat_id'];
}
