<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-14 The University of Edinburgh
   
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
/**
 * Generates dropdown menus for instance configuration page
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
include 'microindex.php';
$structure = $modulemanager->connection;
//Return select list of module functions
function methodmenu($structure, $instance, $i, $link, $selected) {
	$modules = mysqli_query($structure, "SELECT * FROM modules WHERE instance='" . $instance . "'");
	$row = mysqli_fetch_array($modules);
	$calledmodule = new $row['type'](0);
	$functions = $calledmodule->Functions();
	$menu = '<select id = "' . $link . '_method_' . $i . '" name = "' . $link . '_method_' . $i . '">';
	$menu.= '<option value = "Instance">Get instance for this module</option>';
	foreach ($functions as $key => $value) {
		$menu.= '<option value = "' . $key . '"';
		if ($key == $selected) {
			$menu.= 'selected';
		}
		$menu.= '>' . $key . ' (' . $value . ')</option>';
	}
	$menu.= '</select>';
	return $menu;
}
//Return select list of modules
function modulemenu($structure, $page, $i, $link, $selected) {
	$modules = mysqli_query($structure, "SELECT * FROM modules WHERE page='" . $page . "' ORDER BY instance ASC");
	$menu = '<select id = "' . $link . '_link_' . $i . '" name = "' . $link . '_link_' . $i . '">';
	while ($row = mysqli_fetch_array($modules)) {
		$menu.= '<option value = "' . $row['instance'] . '"';
		if ($row['instance'] == $selected) {
			$menu.= 'selected';
		}
		$menu.= '>' . $row['type'] . ' (' . $row['description'] . ')</option>';
	}
	$menu.= '</select>';
	return $menu;
}
//Function to generate menu
function generatemenu($structure, $page, $key, $i, $index, $init, $value) {
	$menu = '
            <script>
            $(document).ready(function () {
                $("#' . $key . '_l_' . $i . '").hide();
                $("#' . $key . '_method_' . $i . '").hide();
                $("#' . $key . '_' . $i . '").change(function () {
                    var $dropdown_' . $key . '_' . $i . ' = $(this);
                    var key_' . $key . '_' . $i . ' = $dropdown_' . $key . '_' . $i . '.val();
                    switch (key_' . $key . '_' . $i . ') {
                        case "constant":
                            $("#' . $key . '_l_' . $i . '").hide();
                            $("#' . $key . '_method_' . $i . '").hide();
                            $("#' . $key . '_variable_' . $i . '").show();
                        break;
                        case "method":
                        $("#' . $key . '_l_' . $i . '").show();
                        $("#' . $key . '_method_' . $i . '").show();
                        $("#' . $key . '_variable_' . $i . '").hide();
                        break;
                        default:
                        break;
                    }
                });
                $("#' . $key . '_link_' . $i . '").change(function () {
                    var $function_dropdown_' . $key . '_' . $i . ' = $(this);
                    var function_key_' . $key . '_' . $i . ' = $function_dropdown_' . $key . '_' . $i . '.val();
                    $("#' . $key . '_method_' . $i . '").load("instance_dropdown.php?call=method&page=' . $page . '&link=' . $key . '&i=' . $i . '&instance=" + function_key_' . $key . '_' . $i . ');
                });
            });
            </script>
            ';
	$menu.= 'index: <input type="text" name="' . $key . '_index_' . $i . '" value="' . $index . '">';
	$menu.= '<select id="' . $key . '_' . $i . '" name="' . $key . '_' . $i . '">
            <option value="constant"selected>Constant</option>
            <option value="method">Method</option></select><br>';
	$menu.= '<span id="' . $key . '_l_' . $i . '">';
	$menu.= modulemenu($structure, $page, $i, $key, '');
	$menu.= '</span>';
	$menu.= '<span id="' . $key . '_method_' . $i . '">';
	$menu.= methodmenu($structure, $init, $i, $key, '');
	$menu.= '</span>';
	$menu.= '<input type="text" id="' . $key . '_variable_' . $i . '" name="' . $key . '_variable_' . $i . '" value="' . $value . '"><input type="checkbox" name="' . $key . '_remove_' . $i . '">Remove<br>';
	return $menu;
}
//Respond to page calls
if (isset($_GET['call'])) {
	//Get required includes
	$page = mysqli_real_escape_string($modulemanager->connection, $_GET['page']);
	$modules = mysqli_query($structure, "SELECT * FROM modules WHERE page='" . $page . "' ORDER BY instance ASC");
	$i = 0;
	$init = 0;
	while ($row = mysqli_fetch_array($modules)) {
		if ($row['include'] != null) {
			include ($row['include'] . '.php');
		}
		//Get first module associated with page (used to initialise second dropdown menu)
		if ($i == 0) {
			$init = $row['instance'];
		}
		$i++;
	}
	$call = $_GET['call'];
	//Determine the call response required
	if ($call == 'module') {
		$i = mysqli_real_escape_string($modulemanager->connection, $_GET['i']);
		$link = mysqli_real_escape_string($modulemanager->connection, $_GET['link']);
		echo modulemenu($structure, $page, $i, $link, '');
	}
	if ($call == 'method') {
		$i = mysqli_real_escape_string($modulemanager->connection, $_GET['i']);
		$link = mysqli_real_escape_string($modulemanager->connection, $_GET['link']);
		$instance = mysqli_real_escape_string($modulemanager->connection, $_GET['instance']);
		echo methodmenu($structure, $instance, $i, $link, '');
	}
	if ($call == 'menu') {
		$i = mysqli_real_escape_string($modulemanager->connection, $_GET['i']);
		$index = mysqli_real_escape_string($modulemanager->connection, $_GET['index']);
		$key = mysqli_real_escape_string($modulemanager->connection, $_GET['key']);
		echo generatemenu($structure, $page, $key, $i, $index, $init, '');
	}
}
