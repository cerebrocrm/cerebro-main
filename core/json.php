<?php
/** JSON contact checker
*
* Checks the database to see if the given contact already exists
*
* 
* @author Joseph Farthing, Left Join Ltd.
* @author University of Edinburgh
* @version 1.0
* @package cerebro
* @subpackage core
*/

// testing time start
global $time_start;
$time_start = microtime(true);
header("Access-Control-Allow-Origin: *");
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
	if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
	exit(0);
}
// version
$version = '2.1.0';
include 'microindex.php';

if (isset($_GET['id'])) {
	if ($_GET['id'] < 10000 && array_key_exists($_GET['page'], $legacy)) $item_id = ($legacy[$_GET['page']] * 10000) + $_GET['id'];
	else $item_id = $_GET['id'];
}

/**
* Query for the given email
*
* @todo Also check postal addresses and phone numbers
*/
$data = $dbmanager->Query(array('id'), array(array('contact_email' => $_GET['mail'])), null, 0, 0);
foreach ($data['result'] as $result) $check_contacts = $result;
if ($data['result_count'] > 0 && !(isset($_GET['ping']))) {
	// duplicate
	$return = array('status' => 'existing', 'id' => $check_contacts['id']);
	echo json_encode($return);
} else {
	$return = array('status' => 'no_results');
	echo json_encode($return);
}
