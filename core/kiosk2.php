<?php /*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-16 Joseph Farthing / The University of Edinburgh
   Copyright 2012 Joseph Farthing

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
   
*/
// Offline storage version of kiosk
// Joseph Farthing for University of Edinburgh, 2016
// includes
include 'microindex.php';
include 'modules/activedirectory.php';
$overlay = '';
$userstring = '';
foreach ($users as $index => $val) {
	$userstring.= '<option value="' . $index . '" ' . $selected . '>' . $val . '</option>';
}
if (isset($_GET['event_id'])) {
	// add kiosk to database
	$event = $dbmanager->Query(array('name,event_type'), array(array('id' => $_GET['event_id'])), null, 0, 0);
	$event = $event['result'][$_GET['event_id']];
} else {
	// add cookie check
	die('Sorry, an error has occurred. Please speak to a member of staff.');
}
// get event info
$event_name = $event['event_name'];
$event_type = $event['event_type'];
?>
<!DOCTYPE html>
<html>
<head>
<title>OFFLINE Registration form</title>
<meta charset="UTF-8">
<meta name="apple-mobile-web-app-capable" content="yes">
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">
	<link rel="stylesheet" href="stylesheets/layout.css">
	<link rel="stylesheet" href="stylesheets/cerebro.css">
	<link rel="stylesheet" href="stylesheets/magnific-popup.css">
	<link rel="stylesheet" href="stylesheets/south-street/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" href="stylesheets/goalProgress.css">
	<link rel="stylesheet" href="stylesheets/iosOverlay.css">
	<link rel="stylesheet" href="chosen/chosen.min.css">


	<!-- Print CSS -->
	<link rel="stylesheet" href="stylesheets/cerebro-print.css" type="text/css" media="print" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	 <script type="text/javascript" src="js/magnific_popup.js" ></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="http://www.cerebro.org.uk/core/includes/src/dygraph-combined.js"></script>
    <script type="text/javascript" src="js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.complexify.js"></script>
	<script type="text/javascript" src="js/cerebro.js"></script>
	<script type="text/javascript" src="js/goalProgress.min.js"></script>
    <script type="text/javascript" src="js/iosOverlay.js"></script>
	<script type="text/javascript" src="js/spin.min.js"></script>
	 
	<style type="text/css">
	
	.fb_iframe_widget {
		transform: scale(1.8);
		-ms-transform: scale(1.8); 
		-webkit-transform: scale(1.8); 
		-o-transform: scale(1.8); 
		-moz-transform: scale(1.8); 
		transform-origin: bottom left;
		-ms-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
		-moz-transform-origin: bottom left;
		-webkit-transform-origin: bottom left;
	}
	#contact_email {
		background-position: 96%;
		background-repeat: no-repeat;
	}
	
	#how_heard a {
		min-height: 2.2em;
		margin-top: 1em;
	}
	
	
	</style>
</head>
<body>
<script>
$(document).ready(function () {
var check = 0;
<?php echo $overlay; ?>
	var cerebro_storage = JSON.parse(localStorage.getItem("cerebro_storage_area"));
	var fail = JSON.parse(localStorage.getItem("fail_area"));
	
	if ( cerebro_storage === null ) {
		cerebro_storage = {};
	}
	
	if ( fail === null ) {
		fail = [];
	}
	
	//console.log(cerebro_storage);
	$("#storage_area").html("<small>"+Object.keys(cerebro_storage).length+"("+fail.length+") contacts stored</small>");
	
		
	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}

	$( "#reg_form" ).click(function(e) {
	  e.preventDefault();
	  $( "#footer" ).slideUp();
	  $( "#hidden_form" ).slideDown();
	});
	$( "#cancel" ).click(function(e) {
	  e.preventDefault();
	  $( "#footer" ).show();
	  $( "#hidden_form" ).hide();
	});
	
	$( "#hidden_2" ).hide();
	$( "#hidden_3" ).hide();
	
	$( "#office" ).click(function(e) {
		e.preventDefault();
		$( "#hidden_3" ).toggle('slow');	
	});
	
	$( "#resync" ).click(function(e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			url: "log_failed.php",
			data: "data="+JSON.stringify(fail),
			 success: function (result) {
				if (result == "1") fail = [];
				$("#storage_area").html("<small>"+Object.keys(cerebro_storage).length+"("+fail.length+") contacts stored</small>");
			}
		});	
	});
	
	$( "#submit" ).click(function(e) {
	e.preventDefault();
	var invert = $("input#contact_recieve_newsletters_inv:checked").val();
	if (invert != 1){
		$("input#contact_recieve_newsletters").val("1");
	}
	
	var test = preEmailOffline(e,1);
	
	// add if failed loop
	
	if (test === true) {
		postEmailOffline(e);
	} else {
		return false;
	}
	
	
	});
	
	
	var preEmailOffline = function (e) {
		e.preventDefault();
		check = 1;
		var pre_email = $("#contact_email").val();
		
		// basic checks
		if (pre_email === '') {
			return false;
		} else {
			
			// there's something here at all
			
			if (pre_email.replace('s','').match(/^[0-9]+$/) !== null) {
				// it's a number, but is it a student number?
				// a valid student number has 7 digits (not including s)
				// and also needs to start with a year that makes logical sense
				// so, we think between 8 years behind and 1 year ahead of current year
				if (pre_email.replace('s','').length == 7) {
					var date = new Date();
					var testdigit = parseInt((''+pre_email.replace('s',''))[0]+(''+pre_email.replace('s',''))[1]);
					var nowyear = parseInt(date.getFullYear().toString().substr(2,2));
					
					if (testdigit > (nowyear+1) || testdigit < (nowyear-8)) { // this will break in 2042
						console.log('invalid UUN year');
						check = 0;
					} else {
						console.log('looks like a valid UUN year');
					}
				} else {
					console.log ('looks like invalid number');
					check = 0;
				}
				
			} else {
				// it's not a number
				// so we check for common email address formats
				
				if (pre_email.length == 8) {
					if (pre_email.substring(1).match(/^[0-9]+$/) !== null) {
						console.log('Looks like a student number with a typo');
						$("#contact_email").val('s'+pre_email.substring(1));
						var check2 = preEmailOffline(e);
						return check2;
					} else {
						console.log('Looks like a staff UUN');
					}
					
				} else {
					
					if (validateEmail(pre_email)) {
						if (pre_email.match(/@ed.ac.uk/i)) {
							// simple ed.ac.uk email
							
							if (pre_email.split('@')[0].replace('s','').match(/^[0-9]+$/) !== null) {
								console.log('Looks like a student email with a typo');
								$("#contact_email").val(pre_email.split('@')[0]);
								var check3 = preEmailOffline(e);
								return check3;
							} else {
								console.log('Looks like a staff UUN');
							}
							
							
							console.log('Looks like a staff email');
						} else if (pre_email.match(/@eusa.ed.ac.uk/i)) {
							// eusa email
							console.log('Looks like a eusa email');
						} else if (pre_email.match(/@sms.ed.ac.uk/i)) {
							// student email
							console.log('Looks like a student email');
							
						} else {
							// other email address
							console.log("pop up questions because it's another email");
							$( "#hidden_2" ).slideDown('slow');
							return true;
						}
					} else {
						check = 0;
						console.log('Invalid email');
					}
				}
				
				
			}
			
			if (check === 0) {
				// error state
				$("#contact_email").css("background-image","none");
				$( ".validation" ).remove();
				$("#contact_email_div").append(' <div class="validation" style="padding: 0.5em; background-color: #c83737; color: #fff;"><p>Invalid email or student number.</p></div>	');
				return false;
			} else {
				$( ".validation" ).remove();
				$("#contact_email").css("background-image","url(images/correct.png)");
				return true;
			}
			
		}
		
	
		
		
			
	}

	
	
	var postEmailOffline = function (e) {
		e.preventDefault();
		
		// we already know we have a valid email or UUN
		if ($("#contact_first").val() !== '') {
			cerebro_storage[$("#contact_email").val()] = {
				uun : $("#contact_email").val(),
				contact_first: $("#contact_first").val(),
				contact_last: $("#contact_last").val(),
				contact_type: $("#contact_type").val(),
				contact_title: $("#contact_title").val(),
				contact_company: $("#contact_company").val(),
				how_heard_val: $("#how_heard_val").val()
			};
		} else {
			cerebro_storage[$("#contact_email").val()] = {
				uun : $("#contact_email").val(),
				how_heard_val: $("#how_heard_val").val()
			};
		}
		cerebro_storage[$("#contact_email").val()]['note'] = $("#note").val();
		//Disable auto form note
		cerebro_storage[$("#contact_email").val()]['no_note'] = true;
		cerebro_storage[$("#contact_email").val()]['checksum'] = 1;
		cerebro_storage[$("#contact_email").val()]['attended'] = 1;
		cerebro_storage[$("#contact_email").val()]['function'] = 'event_import';
		cerebro_storage[$("#contact_email").val()]['type'] = 'contact';
		cerebro_storage[$("#contact_email").val()]['link_0'] = <? echo $_GET['event_id']; ?>;
		cerebro_storage[$("#contact_email").val()]['assigned_to'] = $("#assigned_to").val();
		if ($("#contact_recieve_newsletters_inv:checked").val() == 1) cerebro_storage[$("#contact_email").val()]['contact_recieve_newsletters'] = 0;
		else cerebro_storage[$("#contact_email").val()]['contact_recieve_newsletters'] = 1;
		
		$("#contact_email").val('');
		$("#contact_first").val('');
		$("#contact_last").val('');
		$("#contact_type").val('');
		$("#contact_title").val('');
		$("#contact_company").val('');
		$("#how_heard_val").val('');
		$("#assigned_to").val('');
		$("#note").val('');
		$("#contact_recieve_newsletters_inv").prop('checked',true);
		$("#how_heard a").css("background-color","#fff");
		$("#contact_email").css("background-image","none");
		$("#contact_email").blur();
		
		localStorage.setItem('cerebro_storage_area', JSON.stringify(cerebro_storage));
		$("#storage_area").html("<small>"+Object.keys(cerebro_storage).length+"("+fail.length+") contacts stored</small>");
		$( "#hidden_3" ).hide();
		$( "#hidden_2" ).hide();		
		iosOverlay({
					text: "Thanks!",
					duration: 7e2,
					icon: "images/check.png"
				});	
		console.debug(cerebro_storage);
	}
	
	
	
	$("#contact_email").blur(preEmailOffline);
	
	$("#how_heard a").click(function(e) {
		e.preventDefault();
		var how_heard = $(this).attr("id");
		$("#how_heard a").css("background-color","#fff");
		$(this).css("background-color","#c7ebab");
		$("#how_heard_val").val(how_heard);
	
	});
	
	window.setInterval(function(){
		$("#storage_area").html("<small>"+Object.keys(cerebro_storage).length+"("+fail.length+") contacts stored</small>");
		
		n = 0;
		t = 5;	
		for (var key in cerebro_storage) {
			n++;
				$.ajax({
				type: "POST",
				url: "incoming.php",
				data: cerebro_storage[key],
				 success: function (result) {
					result = jQuery.parseJSON( result );
					if (result.saved == true) {
						delete  cerebro_storage[result.email];
					} else {
						fail.push([result,cerebro_storage[key]]);
						delete  cerebro_storage[key];
					}
					
					localStorage.setItem('fail_area', JSON.stringify(fail));
					localStorage.setItem('cerebro_storage_area', JSON.stringify(cerebro_storage));
					$("#storage_area").html("<small>"+Object.keys(cerebro_storage).length+"("+fail.length+") contacts stored</small>");
				},
				fail: function (result) {
					fail.push([result,cerebro_storage[key]]);
					delete  cerebro_storage[key];
					localStorage.setItem('fail_area', JSON.stringify(fail));
					localStorage.setItem('cerebro_storage_area', JSON.stringify(cerebro_storage));
					$("#storage_area").html("<small>"+Object.keys(cerebro_storage).length+"("+fail.length+") contacts stored</small>");
				}
			});	
			
			if (n > t) return;
	
		};
		
	}, 30000);
	
 });


	

</script>
<div class="header">
		<div class="container">
		
			
			<div class="sixteen columns " style="padding-top: 5px; padding-bottom: 5px; background: none;">
				<img src="images/logos.png" /> 
				
			</div>

		</div><!-- container -->
	</div><!-- header -->
<div class="container">
	<div class="sixteen columns content">
		<p><br /></p>
		<div class="sixteen columms alpha omega">
			
			<div class="two-thirds column alpha">
			
				<h1>Sign in to this event</h1>
			</div>
					</div>
		<p> <br /></p>
		<div class = "row form_display">
			<form name="interest_form" id="interest_form" method="POST" action="kiosk.php?event_id=<?php echo $_GET['event_id']; ?>"> 
				<div class="form_area">
				
							<div class="row primary_item even">
							
								<div class="fifteen columns alpha" id="contact_email_div"></br>
									<label for="contact_email" class="required"><img src="images/form_email_icon.png" alt="Email" /><span class="form_label_text">Your University Email (or personal email)</span> </label>
									<input type="text" name="contact_email" id="contact_email"   class="req has_image" value="" autocomplete="off"/>
								</div>
								
							</div>
							
							<div class="row primary_item even">
							
								<div class="fifteen columns alpha" id="how_heard">
									<h3>I heard about it by...</h3>
									<input type="hidden" name="how_heard_val" id="how_heard_val" value="NOT_SELECTED" />
									
									<a href="#" id="heard_uni_website" class="two columns alpha button">University website</a>
									<a href="#" id="heard_other_website" class="two columns button">Another website</a>
									<a href="#" id="heard_email_invitation" class="two columns button">Email or invitation</a>
									<a href="#" id="heard_social_media" class="two columns button">Social media</a>
									<a href="#" id="heard_flyer_poster" class="two columns button">Flyer or poster</a>
									<a href="#" id="heard_word_mouth" class="two columns button">Word of mouth</a>
									<a href="#" id="heard_other" class="one column omega button">Other</a>
								</div>
								
							</div>	
					<p> <br /> </p>
					
					<div id="hidden_2">
					
					<div class="row primary_item">
					
						<div class="sixteen columns alpha">
                             <h4> We just need to ask a couple of extra things... </h4>
                        </div>
					</div>
					
					<div class="row primary_item even">
						<div class="six columns alpha" id="contact_first1">
							<label for="contact_first" class="required"><span class="form_label_text">First name</span> </label>
							<input type="text" name="contact_first" id="contact_first"  class="req" value=""/>
						</div>
						<div class="six columns omega" id="contact_last1">
							<label for="contact_last" class="required"><span class="form_label_text">Last name</span> </label>
							<input type="text" name="contact_last" id="contact_last"  class="req" value=""/>
						</div>
					</div>
					
					<div class="row primary_item">
							<div class="four columns alpha">
                            <select id="contact_type" name="contact_type" data-placeholder="Select type"><option value=""></option>
							<option value="5" >Undergraduate Student</option><option value="4" >Postgraduate Student</option>
							<option value="1" >Academic Staff</option><option value="2" >Technical Support Staff</option><option value="3" >Administrative / Managerial Support Staff</option><option value="6" >Alumnus</option><option value="7" >Media contact</option><option value="0">Member of the public</option></select></div>
				
							<div class="three column alpha" id="contact_title1">
                                <label for="contact_title" class=""><span class="form_label_text">Course / Job Title</span> </label>
                                <input type="text" name="contact_title" id="contact_title"  class="" value=""/>
                            </div>
                            <div class="three columns omega" id="contact_company1">
                                <label for="contact_company" class=""><span class="form_label_text">Department / Company</span> </label>
                                <input type="text" name="contact_company" id="contact_company"  class="" value=""/>
                            </div>
						</div>
					</div>
					
				
					</div>
					<p><br /><br /></p>
					
					<div class="row primary_item" style="background-color: #f4f4f4 !important; padding: 1em !important;">
						<div class="fifteen columns alpha checkbox">
							<label for="contact_recieve_newsletters_inv" ><span class="form_label_text">Sign up to newsletter?</label>
							<input name="contact_recieve_newsletters_inv" type="checkbox" id="contact_recieve_newsletters_inv" value="1" checked="" />
						</div>
						<div class="fifteen columns alpha" style="background-color: #f4f4f4 !important;">
							<h3>What happens to my data?</h3>
							<p><strong>Don't worry.</strong> As a University, we're committed to the highest possible standards of data protection. We keep your details in order to let you know about what's happening, and in order to learn which things interest people the most.</p>
							<ul class="circle">
								<li><strong>We won't bother you too much</strong>. We send about one email a month.</li>
								<li><strong>You can unsubscribe whenever you like</strong>. Just click the link at the bottom of the emails we send you, or get in touch.</li>
								<li><strong>Your personal information is safe</strong>. We won't sell your data to others and <strong>we use University-approved systems to send our emails</strong>.</li>
								
							</ul>
						</div>
					</div>
					<div id="hidden_3">
						<div class="row primary_item">
							<div class = "ten columns">
								<h3>Add note</h3>
							</div>
						</div>
					
						<div class="row primary_item">
							<div class = "ten columns add_note alpha omega">
								<textarea id = "note" name = "note"></textarea>
							</div>	
						</div>
						<div class="row primary_item">
							<div class="thirteen columns add_note action_bar alpha omega">
								<a href="#" class="button" id="resync">Resync</a>
							</div>
						</div>
						<div class="row primary_item">
							<div class="four columns alpha">
								<select id="assigned_to" name="assigned_to" data-placeholder="Select user"><option value=""></option>
								<?php echo $userstring; ?>
								</select>
							</div>
						</div>
				
						
					</div>
				
					
					

						<input type="hidden" name="contact_specific_meeting" id="contact_specific_meeting" value="<?php echo $_GET['event_id']; ?>" />
						
						<input type="hidden" name="contact_existing_id_0" id="contact_existing_id_0" value="NONE" />
						
						<input type="hidden" name="contact_recieve_newsletters" id="contact_recieve_newsletters" value="0" />
						
						<input type="hidden" name="contact_when_we_spoke" id="contact_when_we_spoke" value="<?php echo date('Y-m-d'); ?>" />

						<input type="hidden" name="contact_how_we_spoke" id="contact_how_we_spoke" value="<?php echo $event_type; ?>" />
						
						
																			
						<div class="row primary_item" style="background-color: #f4f4f4 !important;">
							<div class="two columns alpha">
									<a href="#" class="button" id="office"><img src="images/fold_down.png" alt="Add">Office use</a>
							</div>		
							<div class="thirteen columns omega action_bar">
									<a href="#" class="button" id="submit">Submit</a>

							</div>							
						</div>
						
						<div id="storage_area"></div>
					
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<?php
