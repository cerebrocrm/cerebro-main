$(document).ready(function () {
	
	$.cerebro = new Object();
	$.cerebro.password_popup_visible = 0;
	$.cerebro.timeout_time = 4100000;
	

	//validation
	var happy = {
		

			// matches mm/dd/yyyy (requires leading 0's (which may be a bit silly, what do you think?)
			date: function (val) {
				return /^(?:0[1-9]|1[0-2])\/(?:0[1-9]|[12][0-9]|3[01])\/(?:\d{4})/.test(val);
			},

			email: function (val) {
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(val);
			},

			minLength: function (val, length) {
				return val.length >= length;
			},

			maxLength: function (val, length) {
				return val.length <= length;
			},

			equal: function (val1, val2) {
				return (val1 == val2);
			}
		};
    
            $('.calendar_chooser > input ').datepicker({
            dateFormat: "yy-mm-dd",
            onSelect: function () {
                $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").hide(0);
            }
            });
            
        $(':text').focus(function () {
            $("label[for=\'"+$(this).attr('id')+"']").children(".form_label_text").hide(0);
        });

        $(':text').blur(function () {

            if ( !$(this).val() ) {
                $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").show(0);
            }

        });
        
        
        $(':text').each (function () {
        	if ( $(this).val() ) {
        		$("label[for='"+$(this).attr('id')+"']").children(".form_label_text").hide(0);
            }
        });
        

        $(':password').focus(function () {
            $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").hide(0);
        });

        $(':password').blur(function () {
            if ( !$(this).val() ) {
                $("label[for='"+$(this).attr('id')+"']").children(".form_label_text").show(0);
            }
        });
        
        $('.unfolding > div > a').click(function (e) {
            e.preventDefault();
            $(this).parent().parent().children('.row').fadeToggle('medium');
            var text_contents = $(this).parents().children('.opener').text();
            if (text_contents == 'Open') {
                $(this).parents().children('.opener').text('Close');
            } else {
                $(this).parents().children('.opener').text('Open');
            }
        });
        
        $('select').chosen({disable_search_threshold: 5});

        
        $("#password :password").complexify({}, function (valid, complexity) {
            var password_value = $('#password :password').val()
            if (!valid && password_value != '') {
                $('#password_validate p').removeClass('normal').removeClass('ok').removeClass('meh').addClass('bad');
                $('#password_validate p').text('Bad password');
            } else if (valid && complexity < 70 ) {
                $('#password_validate p').removeClass('normal').removeClass('bad').removeClass('ok').addClass('meh');
                $('#password_validate p').text('Average password');
            } else if (valid) {
                $('#password_validate p').removeClass('normal').removeClass('meh').removeClass('bad').addClass('ok');
                $('#password_validate p').text('Great password!');
            } else {
                $('#password_validate p').removeClass('normal').removeClass('bad').removeClass('meh').addClass('normal');
                $('#password_validate p').text('Type a new password');
            }
        });
        
        $("#password_repeat").keyup (function() {
            var password_value = $('#password input').val()
            var password_repeat_value = $('#password_repeat').val()
            var password_repeat_length = password_repeat_value.length;
            if (password_value == password_repeat_value && password_repeat_length > 1) {
                $('#password_validate_repeat').html('<p class="validation ok">They match!</p>');
            } else {
                $('#password_validate_repeat').html('<p class="validation bad">Doesn\'t match</p>');
            }
        });
        
        $("#password_random").click (function(e) {
            e.preventDefault();
            $.ajax({
                type: "GET",
                url: "random_pass.php",
                data: null,
                 success: function (data) {
                    $('#password input').val(data)
                    $('#password_repeat').val(data)
                    $('#password_validate p').removeClass('normal').removeClass('meh').removeClass('bad').addClass('ok');
                    $('#password_validate p').html('Set to <strong>' + data + '</strong>');
                    $('label[for=user_password_0').children(".form_label_text").hide(0);
                    $('label[for=password_repeat').children(".form_label_text").hide(0);

                }
            });
        });
		
		tinymce.init({ 
			selector: ".html_editor > textarea",
			mode : 'exact',
            menubar:false,
			plugins: 'link paste lists',
			toolbar: 'undo redo | styleselect | bold italic  | bullist numlist outdent indent | link',
			height : '400'
        });

		
		window.setInterval(function(){
			/// check if user logged in
		
            $.ajax({
                type: "GET",
                url: "login_check.php",
                data: null,
                 success: function (data) {
                    var obj = jQuery.parseJSON( data );
					
					if (obj.logged_in == 0 && $.cerebro.password_popup_visible == 0) {
						
						$.cerebro.password_popup_visible = 1;
						
						$.magnificPopup.open({
						  items: {
							  src: '#logged-out',
							  type: 'inline'
						  }
						});
						
						$.cerebro.timeout_time = 10000;

					} else {
						if ($.cerebro.password_popup_visible == 1) {
							$.magnificPopup.close();
						}
					}
                }
            });
			
        }, $.cerebro.timeout_time);
		
		$('.countdown').each(function () {
				var date_selected = $(this).text();
				$(this).countdown({
					date: date_selected,
					render: function(data) {
						$(this.el).html('<span title="Due in '+data.days + " days, " + data.hours + " hours, " + data.min + " minutes, " + data.sec+' seconds.">'+data.days + "d " + data.hours + "h " + data.min + "m " + data.sec + "s </span>");
					}
				});
		});
});
