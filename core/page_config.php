<?php
include 'microindex.php';
if (Users::GetCurrentUser() == null) {
	$_SESSION['login_temp'] = $_SERVER["REQUEST_URI"];
	header('Location: login.php');
	die();
}

if (Users::GetCurrentUser()['category'] == 99) setcookie('usertype', 99, time() + (86400 * 30 * 12), "/");

$accesslevel = Users::GetCurrentUser()['user_level'];
if (!isset($accesslevel) || $accesslevel != '1' || $_SESSION['logintype'] == 'esa') {
	die('You must be an administrator to use this screen. (' . $accesslevel . ' ' . $_SESSION['logintype'] . ')');
}

$linkedby = array();
$linkedto = array();
$update_cache = 0;
//Remove any modules to be deleted from page
if (isset($_POST['remove'])) {
	mysqli_query($modulemanager->connection, "DELETE FROM modules WHERE instance = '" . mysqli_real_escape_string($modulemanager->connection, $_POST['remove']) . "'");
	mysqli_query($modulemanager->connection, "DELETE FROM modlinks WHERE instance = " . mysqli_real_escape_string($modulemanager->connection, $_POST['remove']) . " OR calledinstance = " . mysqli_real_escape_string($modulemanager->connection, $_POST['remove']));
	$update_cache = 1;
}
//Remove any pages to be deleted
if (isset($_POST['removepage'])) {
	$links = '';
	$instances = mysqli_query($modulemanager->connection, "SELECT instance FROM modules WHERE page = '" . mysqli_real_escape_string($modulemanager->connection, $_POST['removepage']) . "'");
	while ($row = mysqli_fetch_array($instances)) {
		$links.= ' OR instance = ' . $row['instance'];
		$links.= ' OR calledinstance = ' . $row['instance'];
	}
	mysqli_query($modulemanager->connection, "DELETE FROM modules WHERE page = '" . mysqli_real_escape_string($modulemanager->connection, $_POST['removepage']) . "'");
	mysqli_query($modulemanager->connection, "DELETE FROM modlinks WHERE instance = 0 " . $links);
	$update_cache = 1;
}
//Add new modules to page
if (isset($_POST['add'])) {
	mysqli_query($modulemanager->connection, "INSERT INTO modules (page, type, description, include) VALUES
        (
            '" . mysqli_real_escape_string($modulemanager->connection, $_POST['page']) . "',
            '" . mysqli_real_escape_string($modulemanager->connection, $_POST['type']) . "',
            '" . mysqli_real_escape_string($modulemanager->connection, $_POST['description']) . "',
            '" . mysqli_real_escape_string($modulemanager->connection, $_POST['include']) . "'
        )
    ");
	$update_cache = 1;
}
//Add new page to page list
if (isset($_POST['newpage'])) {
	mysqli_query($modulemanager->connection, "INSERT INTO modules (page, type, description, include) VALUES
        (
            '" . mysqli_real_escape_string($modulemanager->connection, $_POST['title']) . "',
            'ViewControllerRev2',
            'Collates content to be displayed on page',
            'modules/viewcontroller'
        )
    ");
	mysqli_query($modulemanager->connection, "INSERT INTO page_access (page, allowed) VALUES
        (
            '" . mysqli_real_escape_string($modulemanager->connection, $_POST['title']) . "',
            '1,2,3'
        )
    ");
	$update_cache = 1;
}
// clear cache
if ($update_cache == 1) {
	apc_clean_cache(); //clear out the caches

}
//Get list of available pages
$pages = mysqli_query($modulemanager->connection, "SELECT * FROM modules ORDER BY instance ASC");
while ($row = mysqli_fetch_array($pages)) {
	$pagelist[$row['page']][$row['instance']] = array($row['type'], $row['description'], $row['include']);
	// Build menu
	$page = 'page_config';
	$menu = '';
	$tabs = mysqli_query($modulemanager->connection, "SELECT * FROM menu");
	while ($row = mysqli_fetch_assoc($tabs)) {
		$active = explode(',', $row['Alias']);
		$select = '';
		if ($page == strtolower($row['Name']) || in_array($page, $active)) $select = 'class="selected"';
		$menu.= '<a href="/?page=' . strtolower($row['Name']) . '"' . $select . '>' . $row['Name'] . '</a>';
	}
}
?>


<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>Cerebro | Page config</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
    <link rel="stylesheet" href="../stylesheets/base.css">
    <link rel="stylesheet" href="../stylesheets/skeleton.css">
    <link rel="stylesheet" href="../stylesheets/layout.css">
    <link rel="stylesheet" href="../stylesheets/cerebro.css">
    <link rel="stylesheet" href="../stylesheets/magnific-popup.css">
    <link rel="stylesheet" href="../stylesheets/south-street/jquery-ui-1.8.21.custom.css">
    <link rel="stylesheet" href="../stylesheets/goalProgress.css">
	<link rel="stylesheet" href="../stylesheets/iosOverlay.css">
    <link rel="stylesheet" href="../chosen/chosen.min.css">

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">	</script>

	<script type="text/javascript" src="js/magnific_popup.js" ></script>


	<script type="text/javascript">
	$( document ).ready(function() {

		$('.popup').magnificPopup({
		  type: 'iframe',
		  iframe: {
			 markup: '<div class="white-popup sixteen columns" style="min-width: 80%;">'+
						'<div class="mfp-close"></div>'+
						'<iframe class="mfp-iframe" frameborder="0" allowfullscreen style="width:100%; height: 100%; min-height: 40em;"></iframe>'+'</div>'
		  }

		});
	});
	</script>
    <!-- Print CSS -->
    <link rel="stylesheet" href="../stylesheets/cerebro-print.css" type="text/css" media="print" />

    <!--[if lt IE 9]>
        <script src="../http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="../images/favicon.ico">

    </head>
<body>

    <!-- Primary Page Layout
    ================================================== -->

    <!-- Delete everything in this .container and get started on your own site! -->
    <div class="header">
        <div class="container">

            <div class="sixteen columns">
                <a href="../index.php"><img src="../images/header.png" alt="Cerebro"/></a>

            </div>

             <div class="two-thirds column global_nav">
                <div class="tabs">

                    <a href="/?page=Home" class="home_tab"><img src="images/home.png" alt="Home" title="Home"/></a>

                    <a href="/?page=advanced-search" class="search_tab"><img src="images/search.png" alt="Search" title="Search"/></a>

                    <?php echo $menu ?>

					<a href="page_config.php" class="search_tab selected"><img src="images/settings.png" alt="Settings" title="Settings"/></a>
                </div>

            </div>

            <div class="one-third column">

                <div class="search">
                    <form action="/" method="get">
                    <input type="hidden" name="page" value="simple-search"/>
                    <input type="hidden" name="field_all_0" value="all"/>
                    <input type="hidden" name="op_all_0" value="equal"/>
                    <input type="hidden" name="inf_all_0" value="zend_all"/>

                    <input type="text" name="parama_all_0" id="simplesearchbox"/>
                    </form>
                </div>
			</div>
        </div><!-- container -->
    </div><!-- header -->
    <div class="container">
       <div class="row sixteen columns">

	   <div class=" sixteen columns info_section sub_nav" style="height: 2.7em;">
			<div class="tabs">
				<a href="#" id= "Issues_button" class="selected links_button">Pages</a>
				<a href="#" id= "Issues_button" class="links_button">Menus</a>
				<a href="#" id= "Teams_button" class="links_button">General</a>
		   </div>

		   <h1>Settings</h1>


	   </div>


<?php
//If none is selected, display list of  pages
if (!isset($_GET['page']) && !isset($_POST['page'])) {
	echo "<h2>Pages</h2>";
	$even = 'odd';
	foreach (array_keys($pagelist) as $value) {
		if ($value != 'headermenu') {
			echo "<div class=\"row primary_item $even\"><div class=\"six columns alpha\"><p class=\"issue\"><a href='page_config.php?page=" . $value . "'> $value</a></p></div><div class=\"three columns omega action_bar\"><form name='input' action='page_config.php' onSubmit='return confirm(\"Are you sure you want to PERMANENTLY remove this?\");' method='post'><input type = 'hidden' name = 'removepage' value = '" . $value . "'><input type='submit' value='remove'></form></div></div>";
			if ($even == 'odd') {
				$even = 'even';
			} else {
				$even = 'odd';
			}
		}
	}
	echo "<h3>Add new page</h3>
	<div class=\"form_area\"><form name='input' action='page_config.php' method='post'>
        <input type='hidden' name='newpage' value = 'yes'>
        <div class=\"five columns\"><input type='text' name='title' placeholder='Title'></div>
        <div class=\"three columns\"><input type='submit' value='Submit' style=\"height: 100%;\"></div>
        </form></div>";
} else {
	//Display modules associated with page
	if (isset($_GET['page'])) {
		$page = $_GET['page'];
	} else {
		$page = $_POST['page'];
	}
	echo "<a href='page_config.php'> Return to page list</a>
	<h2>Page: $page</h2>
	<h3>Includes</h3><p>";
	foreach ($pagelist[$page] as $key => $value) {
		if ($value[2] != null) {
			echo $value[2] . '<br />';
		}
	}
	echo '</p>

	<h3>Modules</h3>';
	$even = 'odd';
	foreach ($pagelist[$page] as $key => $value) {
		echo "<div class=\"row primary_item $even\"><form name='input' action='page_config.php' onSubmit='return confirm(\"Are you sure you want to PERMANENTLY remove this?\");' method='post'><div class=\"six columns alpha\"><p><a class=\"popup\" href='instance_config.php?instance=" . $key . "&page=" . $page . "'>$value[0]:</a> $value[1]</p></div><div class=\"three columns action_bar omega\"><input type = 'hidden' name = 'page' value = '" . $page . "'><input type='hidden' name = 'remove' value = '" . $key . "'>";
		echo "<input type='submit' value='remove'></div>";
		echo "</form></div>";
		if ($even == 'odd') {
			$even = 'even';
		} else {
			$even = 'odd';
		}
	}
	echo 'Add module:<br /><form name="input" action="page_config.php" method="post">
    <input type="hidden" name="page" value = "' . $page . '">
    <input type="hidden" name="add" value = "yes">
    Type: <input type="text" name="type">
    Description: <input type="text" name="description">
    Include: <input type="text" name="include">
    <input type="submit" value="Submit">
    </form>';
	//If selected, show further details about a specific module on the page
	if (isset($_GET['module'])) {
		$links = mysqli_query($modulemanager->connection, "SELECT * FROM modlinks");
		while ($row = mysqli_fetch_array($links)) {
			if ($row['instance'] == $_GET['module'] && $row['calledinstance'] != null) {
				$linkedto[] = $row['calledinstance'];
			}
			if ($row['calledinstance'] == $_GET['module']) {
				$linkedby[] = $row['instance'];
			}
		}
		echo '<a href = instance_config.php?instance=' . $_GET['module'] . '&page=' . $page . '>' . $pagelist[$page][$_GET['module']][0] . '</a>';
		echo "<br />Linked by:<br />";
		foreach ($linkedby as $key) {
			echo $pagelist[$page][$key][0] . "<br />" . $pagelist[$page][$key][1] . "<br /><br />";
		}
		echo "Linked to:<br />";
		foreach ($linkedto as $key) {
			echo $pagelist[$page][$key][0] . "<br />" . $pagelist[$page][$key][1] . "<br /><br />";
		}
	}
}
?>
		</div>
    </div><!-- container -->



<!-- End Document
================================================== -->
<div class="container">
    <div class="row sixteen columns">
           <p style="color: gray;font-size: 80%;"><strong>Cerebro </strong>is open source, licenced under the <a href="https://www.apache.org/licenses/">Apache License</a>.</p>

    </div>
</div>
</body>
</html>
