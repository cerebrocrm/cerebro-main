<?php
include 'microindex.php';
session_start();
if (!isset($_SESSION['user']) || $_SESSION['user'] == null) {
	$_SESSION['login_temp'] = $_SERVER["REQUEST_URI"];
	header('Location: login.php');
	die();
}
$data = $dbmanager->Query(array('id', 'user_level'), array(array('category' => 99, 'user_email' => $_SESSION['user'])), null, 0, 0);
foreach ($data['result'] as $id => $result) {
	$accesslevel = $result['user_level'];
	$userid = $id;
}
if ($_GET['action'] == 'updateHomePage') {
	$output = '';
	$i = 0;
	foreach ($_POST as $this_post => $val) {
		if ($i == 0) {
			$output.= $this_post . '=' . $val;
		} else {
			$output.= '&' . $this_post . '=' . $val;
		}
		$i++;
	}
	echo $output;
	$output = mysqli_real_escape_string($connectionmanager->connection, $output);
	$response = mysqli_query($connectionmanager->connection, "UPDATE extra_char SET val='" . $output . "' WHERE meta_id=606 AND item_id = " . $userid);
} elseif ($_GET['action'] == 'addWidget') {
	$type = mysqli_real_escape_string($connectionmanager->connection, $_GET['type']);
	$subject_id = mysqli_real_escape_string($connectionmanager->connection, $_GET['subject_id']);
	if (substr($type, -1) == 's' && ($type != 'YourContacts') && ($type != 'WebFormSignUps')) {
		$type = substr($type, 0, -1);
		if ($type != 'Contact' && $type != 'Location') $type = 'Item';
	}
	$output = $type . '-' . $subject_id . '_X=999'; // the numbers don't matter - they get rewritten on load whatever we type here
	$check = mysqli_fetch_row(mysqli_query($connectionmanager->connection, "SELECT COUNT(*) FROM extra_char WHERE meta_id = 606 AND item_id = " . $userid));
	if ($check[0] < 1) {
		$response = mysqli_query($connectionmanager->connection, "INSERT INTO extra_char  (val,meta_id,item_id) VALUES ('$output&',606,$userid)");
	} else {
		$response = mysqli_query($connectionmanager->connection, "UPDATE extra_char SET val= IFNULL(CONCAT('$output&',val), '$output') WHERE meta_id = 606 AND item_id = " . $userid);
	}
	$_SESSION['msg'] = Array('type' => 'correct', 'message' => 'Widget added to your home page.', 'id' => null, 'table' => null);
	header('Location: /?page=home');
}
