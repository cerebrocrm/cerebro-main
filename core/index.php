<?php
/*

## CEREBRO CRM by Joseph Farthing
Based on Simple Customer by simplecustomer.com
ALL source files (including this one) have been modified

   Copyright 2011 Simple Customer
   Copyright 2011-12 Joseph Farthing / Transition Edinburgh University
   Copyright 2012 Joseph Farthing
   Copyright 2012-16 Left Join Ltd
   Copyright 2012-16 The University of Edinburgh

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/
/**
 * Main file
 *
 *
 * @package cerebro
 * @subpackage core
 *
 */
// fix www -> non www
if (substr($_SERVER['SERVER_NAME'], 0, 4) === 'www.') {
	$pageURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL.= substr($_SERVER['SERVER_NAME'], 4) . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
	} else {
		$pageURL.= substr($_SERVER['SERVER_NAME'], 4) . $_SERVER["REQUEST_URI"];
	}
	header('Location: ' . $pageURL);
	die('Please don\'t add a www in front of your web address.');
}
include 'microindex.php';
// testing time start
global $time_start;
$time_start = microtime(true);
// version
$version = '2.1.0';
$legacy = array();
$legacy['contact-details'] = 1;
$legacy['issue-details'] = 4;
$legacy['project-details'] = 3;
$legacy['location-details'] = 2;
$legacy['event-details'] = 5;
$legacy['contact-add-edit'] = 1;
$legacy['issue-add-edit'] = 4;
$legacy['project-add-edit'] = 3;
$legacy['location-add-edit'] = 2;
$legacy['event-add-edit'] = 5;
$legacy['user-add-edit'] = 6;
$legacy['user-details'] = 6;
//When we switched to a new database structure the ID format changed - this detects and converts old IDs so people wouldn't lose bookmarks etc; this is no longer needed
if (isset($_GET['id'])) {
	if ($_GET['id'] < 10000 && array_key_exists(strtolower($_GET['page']), $legacy)) $item_id = ($legacy[strtolower($_GET['page']) ] * 10000) + $_GET['id'];
	else $item_id = $_GET['id'];
}
//Check/set user data
$_SESSION['login_temp'] = $_SERVER["REQUEST_URI"];
$hashcheck = 0;
if (Users::GetCurrentUser() == null) {
	header('Location: login.php');
	die();
}
$get = $_GET;
$_GET = array();

if (Users::GetCurrentUser()['category'] == 99) setcookie('usertype', 99, time() + (86400 * 30 * 12), "/");

if (Users::GetCurrentUser()['user_level'] == 5) {
	die('This account is disabled. <a href="logout.php">Log out</a>');
}
$_GET = $get;
// redirect and write messages
// so what we do here is take the information about what type of item just updated
// and then redirect to that page, adding a nice message to the info centre
/*
if (isset($_SESSION['just_updated'])) {

    print_r($_SESSION);
    $updated_type = rtrim($_SESSION['just_updated']['type'], 's');
    $updated_id = $_SESSION['just_updated']['id'];
    $_SESSION['msg'] = Array('type' => 'correct', 'message'=>'Your '.$updated_type.' has been successfully saved.');
    $_SESSION['just_updated'] = null;
    header('Location: ?page='.$updated_type.'-details&id='.$updated_id);

}*/
//Skin the app
if (!isset($_GET['page'])) $_GET['page'] = 'notset';
$templates = mysqli_query($connectionmanager->connection, "SELECT * FROM user_templates WHERE user_level = " . Users::GetCurrentUser()['user_level'] . " OR page_override LIKE '" . $_GET['page'] . "' ORDER BY page_override DESC");
if (mysqli_num_rows($templates) == 0) {
	$home = 'home';
	$template = 'template.php';
	if ($_GET['page'] == 'notset') {
		$page = 'home';
	} else $page = $_GET['page'];
	$generate_menu = 'menu.php';
} else {
	$templates = mysqli_fetch_assoc($templates);
	$template = 'custom/' . $templates['template'] . '/template.php';
	if ($_GET['page'] == 'notset') {
		$page = $templates['homepage'];
	} else $page = $_GET['page'];
	$home = $templates['homepage'];
	$generate_menu = 'custom/' . $templates['template'] . '/menu.php';
}
//Lookup the default page for the category associated with the item, if the page is not explicitly set
if ($_GET['page'] == 'notset' && isset($_GET['id'])) {
	$item = mysqli_query($connectionmanager->connection, "SELECT * FROM items WHERE id = " . $item_id);
	$item = mysqli_fetch_assoc($item);
	$cat = mysqli_fetch_assoc(mysqli_query($connectionmanager->connection, "SELECT * FROM categories WHERE cat_id = " . $item['category']));
	$_GET['page'] = $cat['cat_page'];
	$page = $cat['cat_page'];
	//echo "SELECT * FROM categories WHERE cat_id = ".$cat['category'];

}
//Check if user is authorised to view page
$pageaccess = mysqli_fetch_assoc(mysqli_query($modulemanager->connection, "SELECT * FROM page_access WHERE page LIKE '" . $page . "'"));
$pageaccess = explode(',', $pageaccess['allowed']);
if (!(in_array(Users::GetCurrentUser()['user_level'], $pageaccess))) {
	$page = 'sorry-forbidden';
}
$instances = array();
$mlinkdata = array();
$clinkdata = array();
$viewcontroller = null;
$access = array();
//Get all modules and links
// CACHE cache the modules
$modules = apc_fetch(get_instance() . '-modules5-' . $page); //get any cached variable we already have
if ($modules === false) {
	$i = 0;
	$modules = array();
	$d = mysqli_query($modulemanager->connection, "SELECT * FROM modules LEFT JOIN modlinks ON modules.instance=modlinks.instance WHERE page='" . $page . "' ORDER BY `modules`.`instance` ASC");
	while ($row = mysqli_fetch_assoc($d)) {
		$modules[$i] = array();
		foreach ($row as $f => $v) {
			$modules[$i][$f] = $v;
		}
		$i++;
	}
	apc_store(get_instance() . '-modules5-' . $page, $modules, 1200); // save for 20 minutes

}
// CACHE cache the accesslevels
$accesslevels = apc_fetch(get_instance() . '-access5-' . $page); //get any cached variable we already have
//User access was originally intented to be set per function, but is now set per page - this section is no longer needed
if ($accesslevels === false) {
	$i = 0;
	$accesslevels = array();
	$d = mysqli_query($modulemanager->connection, "SELECT * FROM user_access  WHERE page='" . $page . "'");
	while ($row = mysqli_fetch_assoc($d)) {
		$accesslevels[$i] = array();
		foreach ($row as $f => $v) {
			$accesslevels[$i][$f] = $v;
		}
		$i++;
	}
	apc_store(get_instance() . '-access5-' . $page, $accesslevels, 1200); // save for 20 minutes

}
//add access levels to array
foreach ($accesslevels as $row) {
	$access[$row['instance']][$row['function']] = $row['level'];
}
//Instantiate each unique module instance, storing the instance number in an array, and all link data in seperate arrays
foreach ($modules as $row) {
	//Set viewcontroller instance number
	if ($row['type'] == 'ViewController' || $row['type'] == 'ViewControllerRev2') {
		$viewcontroller = $row['instance'];
	}
	if (!array_key_exists($row['instance'], $instances)) {
		if ($row['include'] != null && $row['include'] != '') {
			include ($row['include'] . '.php');
		}
		$ {
			'module_' . $row['instance']
		} = new $row['type']($row['instance']);
		$instances[$row['instance']] = 1;
	}
	if ($row['calledinstance'] != null) {
		$mlinkdata[] = array($row['instance'], $row['varname'], $row['varposition'], $row['variable'], $row['calledinstance']);
	} else {
		$clinkdata[] = array($row['instance'], $row['varname'], $row['varposition'], $row['variable']);
	}
}
//Assign each link to an array based upon the instance it belongs to, in format $module_123['variable'][variable_position]
//For constants
foreach ($clinkdata as $x => $x_value) {
	$ {
		'clinks_' . $x_value[0]
	}
	[$x_value[1]][$x_value[2]] = $x_value[3];
}
foreach ($instances as $x => $x_value) {
	if (isset($ {
		'clinks_' . $x
	})) {
		$ {
			'module_' . $x
		}->cLink($ {
			'clinks_' . $x
		});
	}
	//Add user acces levels
	if (array_key_exists($x, $access)) $ {
		'module_' . $x
	}->SetAccess($access[$x]);
}
//For method calls
foreach ($mlinkdata as $x => $x_value) {
	$ {
		'mlinks_' . $x_value[0]
	}
	[$x_value[1]][$x_value[2]] = array($x_value[4], $x_value[3]);
}
foreach ($instances as $x => $x_value) {
	if (isset($ {
		'mlinks_' . $x
	})) {
		$ {
			'module_' . $x
		}->mLink($ {
			'mlinks_' . $x
		});
	}
}
// test to see whether the "Specific()" method is set in viewcontroller
// and then remove
$specific_test = null;
if (method_exists($ {
	'module_' . $viewcontroller
}, 'Specific')) {
	$specific_test = $ {
		'module_' . $viewcontroller
	}->Specific();
}
if (isset($_GET['instance'])) {
	//Output for specific module associated with page
	echo $ {
		'module_' . $_GET['instance']
	}->$_GET['method']('');
} elseif ($specific_test != null) {
	//Echo specific page content, omit template
	echo $ {
		'module_' . $viewcontroller
	}->Output();
} else {
	//General output based upon template
	$variablecontent = $ {
		'module_' . $viewcontroller
	}->Output();
	$sidebarcontent = $ {
		'module_' . $viewcontroller
	}->Sidebar();
	include $generate_menu;
	include $template;
}
