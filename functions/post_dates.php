<?php
function post_dates($post){
	
	if (isset($post['event_date']) || isset($post['event_end_date'])){
		$post['event_unixtime'] = strtotime($post['event_date'].' '.$post['event_time']);
		$post['event_end_unixtime'] = strtotime($post['event_end_date'].' '.$post['event_end_time']);
	}
	return $post;
}