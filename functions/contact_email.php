<?php

/**
* Formats physical addresses and phone numbers when they're used instead of emails.
*
* @global Object dbmanager
* @global Object the module instance that called the module
* @param array $caller The module instance number that called the function, used to instantiate the global
* @return integer Side effect on <module>->result
* @todo Add phone numbers
*/
function contact_email($caller) {
	global $ {
		'module_' . $caller
	};
	$data = $ {
		'module_' . $caller
	}->result;
	$listplace = 0;
	foreach ($data as $index => $row) {
		
		$current_email = $ {
			'module_' . $caller
		}->result[$index]['contact_email'];
		
		if (strpos($current_email,'@physical') !== false) {
			
			global $dbmanager;
			$check = $dbmanager->Query(array('contact_address1','contact_address2','contact_city','contact_postcode','contact_country'),array(array('id' => $index)),null,null,null);
			
			
			foreach ($check['result'] as $i => $r) {
				$city = $r['contact_city'];
			}
			
			$ {
			'module_' . $caller
			}->result[$index]['contact_email'] = '<p><span class="fa  fa-map-marker fa-lg" aria-hidden="true"></span> Physical address in '.$city.'</p>';
		
		} else {
			$ {
			'module_' . $caller
			}->result[$index]['contact_email'] = '<p>'.$ {
				'module_' . $caller
			}->result[$index]['contact_email'].'</p>';
		}
		$listplace++;
		
	}
}
